WZ-31:CSS Style Sheet Issues implementing Bootstrap library
WZ-30:Account & Contact Search page - Default Search Key = Account Name; Search by TM
WZ-29:Account Details - Display Sales Rep Name and Territory Name
WZ-27:My Cart - Error Message when ship via is PKP
WZ-26:My Cart - Checkout Error Message
WZ-25:Null Pointer Exception
WZ-23:Mingledorffs Logo Resize Issues
WZ-11:Error Message when ship via is PKP
WZ-10:Checkout Error Message
WZ-6:Supersede Qty Available
WZ-6:Supersede Qty Available Changes
- Item has been suspended. View replacement Items link implemented.
- Remove "Item has been suspended." verbiage for the SuperSede Products and SuperSede North Alabama category product list pages.
- Change "View replacement items." verbiage to "View latest items."
- Change "View replacement items." verbiage to "Click here to View Current Model"
WZ-33:Customer Unable Change req. ship date
WZ-46:Update View Current Model to reflect Related Products