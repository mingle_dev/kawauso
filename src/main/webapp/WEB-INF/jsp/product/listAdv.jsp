<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="/WEB-INF/kawauso-utils.tld" prefix="util"%>

<t:layout>
  <jsp:body>


<script>
$(document).ready(function () {

    $("#search-results-page, #search-results-sort").mouseover(function (e) {
        $('>ul', this).css({ 'visibility' : 'visible', 'display' : 'block' });
    });
    
    $("#search-results-page, #search-results-sort").mouseout(function (e) {
        $('>ul', this).css({ 'visibility' : 'hidden', 'display' : 'block' });
    });

    $('.row input').on('change', function(e) {
        e.preventDefault();
        
        var id = e.target.id;
        
        if(USER_ID == '') {
            window.location.href = BASE_PATH + "user/login.htm";
            return false;
        }

        var target = $.number($('#' + id + ' input[name=calcSaleAmt]').val(), 2, '', '.');

        var values = {'productCode': $('#' + id + ' #product a').text(),
                      'target' : target,
                      'targetType' : $('#' + id + ' select[name=targetType]').val()
                     };
        $.ajax({
            url: '<c:url value="/product/calc-claim.htm"/>',
            type: "post",
            data: values,
            beforeSend:function() {
                $('#' + id + ' input[name=calcSaleAmt]').val(target);
                $('#' + id + ' #salePrice').text('---');
                $('#' + id + ' #rebateType').text('---');
                $('#' + id + ' #rebateRecord').text('---');
                $('#' + id + ' #contractRecord').text('---');
                $('#' + id + ' #calcRebateAmount').text('---');
                $('#' + id + ' #margin').text('---');
                $('#' + id + ' #marginPct').text('---');
            },
            success: function(data) {
                data = JSON.parse(data);
               
                $('#' + id + ' #salePrice').currency(data.salePrice, 2);
                $('#' + id + ' #rebateType').text(data.rebateType);
                $('#' + id + ' #rebateRecord').text(data.rebateRecord);
                $('#' + id + ' #contractRecord').text(data.contractRecord);
                $('#' + id + ' #calcRebateAmount').currency(data.calcRebateAmount, 2);
                $('#' + id + ' #calcRebateAmount').currency(data.calcRebateAmount, 2);
                $('#' + id + ' #margin').currency(data.margin, 2);
                $('#' + id + ' #marginPct').percentage(data.marginPct, 2);
            },
            error:function(){
                noty({type: 'error', text: 'An error occurred'});
            }
        });       
    });
});
</script>

<div class="container" style="width: 100%; margin-bottom:16px; padding-bottom:16px;">    
    <c:if test="${not empty term}">    
    <div class="left" style="font-size:1.4em;">
        Search results for <b>${term}</b>
        <a href="<c:url value="/product/search.htm?id=${menuId}&page=${page}&limit=${limit}&sort=${sort}&layout=advanced"/>"><img src="http://www.qc.cuny.edu/PublishingImages/close_red.gif" border="0"></a>
    </div>
    </c:if>
    
    <div class="right" style="font-size:1.4em;">
         ${searchCount} Products
    </div>
</div>

<div class="container" style="width: 100%; margin-bottom:16px; padding-bottom:16px;">
    <div class="left">
        <ul class="dropdown-menu">
            <li id="search-results-page">
                <span class="fake-select-head">
                    <a href="<c:url value="/product/search.htm?term=${term}&id=${menuId}&page=1&limit=20"/>">Default</a>
                </span>
            </li>            
            
            <li id="search-results-page">
                <span class="fake-select-head">
                    <a href="#">${limit} Results Per Page</a>
                </span>
                <ul class="fake-select-items">
                    <li><a href="<c:url value="/product/search.htm?term=${term}&id=${menuId}&page=1&limit=20&layout=advanced"/>">20 Results Per Page</a></li>
                    <li><a href="<c:url value="/product/search.htm?term=${term}&id=${menuId}&page=1&limit=40&layout=advanced"/>">40 Results Per Page</a></li>
                    <li><a href="<c:url value="/product/search.htm?term=${term}&id=${menuId}&page=1&limit=60&layout=advanced"/>">60 Results Per Page</a></li>
                </ul>
            </li>

            <li id="search-results-sort">
                <span class="fake-select-head">
                    <a href="#">Sorted by ${sortKey} <c:choose><c:when test="${sortDir eq 'desc'}">(Z-A)</c:when><c:otherwise>(A-Z)</c:otherwise></c:choose></a>
                </span>
                <ul class="fake-select-items">
                    <li><a href="<c:url value="/product/search.htm?term=${term}&id=${menuId}&page=1&limit=${limit}&sort=prod:asc&layout=advanced"/>">Sorted by Product (A-Z)</a></li>
                    <li><a href="<c:url value="/product/search.htm?term=${term}&id=${menuId}&page=1&limit=${limit}&sort=prod:desc&layout=advanced"/>">Sorted by Product (Z-A)</a></li>
                    <li><a href="<c:url value="/product/search.htm?term=${term}&id=${menuId}&page=1&limit=${limit}&sort=descrip:asc&layout=advanced"/>">Sorted by Description (A-Z)</a></li>
                    <li><a href="<c:url value="/product/search.htm?term=${term}&id=${menuId}&page=1&limit=${limit}&sort=descrip:desc&layout=advanced"/>">Sorted by Description (Z-A)</a></li>
                    <li><a href="<c:url value="/product/search.htm?term=${term}&id=${menuId}&page=1&limit=${limit}&sort=popular:desc&layout=advanced"/>">Sorted by Popularity</a></li>
                </ul>
            </li>       
        </ul>
    </div>

    <div style="float:right;">
        <div class="ui-buttonset">
            <c:if test="${page > 1}">
              <a href="<c:url value="/product/search.htm?term=${term}&id=${menuId}&page=${page-1}&limit=${limit}&sort=${sortKey}:${sortDir}&layout=advanced"/>" class="ui-button"> < </a>
            </c:if>
            <c:choose>
                <c:when test="${numPages < 6}">
                  <c:forEach var="i" begin="1" end="${numPages}" step="1">
                    <c:choose>
                      <c:when test="${i == page}"><a href="#" class="ui-button ui-button-active"> ${i} </a></c:when>
                      <c:otherwise><a href="<c:url value="/product/search.htm?term=${term}&id=${menuId}&page=${i}&limit=${limit}&sort=${sortKey}:${sortDir}&layout=advanced"/>" class="ui-button"> ${i} </a></c:otherwise>
                    </c:choose>
                  </c:forEach>
                </c:when>
                <c:when test="${page < 6}">
                  <c:forEach var="i" begin="1" end="5" step="1">
                    <c:choose>
                      <c:when test="${i == page}"><a href="#" class="ui-button ui-button-active"> ${i} </a></c:when>
                      <c:otherwise><a href="<c:url value="/product/search.htm?term=${term}&id=${menuId}&page=${i}&limit=${limit}&sort=${sortKey}:${sortDir}&layout=advanced"/>" class="ui-button"> ${i} </a></c:otherwise>
                    </c:choose>
                  </c:forEach>
                </c:when>          
                <c:when test="${(page + 3) > numPages}">
                  <c:forEach var="i" begin="${numPages - 4}" end="${numPages}" step="1">
                    <c:choose>
                      <c:when test="${i == page}"><a href="#" class="ui-button ui-button-active"> ${i} </a></c:when>
                      <c:otherwise><a href="<c:url value="/product/search.htm?term=${term}&id=${menuId}&page=${i}&limit=${limit}&sort=${sortKey}:${sortDir}&layout=advanced"/>" class="ui-button"> ${i} </a></c:otherwise>
                    </c:choose>
                  </c:forEach>
                </c:when>
                <c:otherwise>
                <c:forEach var="i" begin="${page - 2}" end="${page + 2}" step="1">
                  <c:choose>
                      <c:when test="${i == page}"><a href="#" class="ui-button ui-button-active"> ${i} </a></c:when>
                      <c:otherwise><a href="<c:url value="/product/search.htm?term=${term}&id=${menuId}&page=${i}&limit=${limit}&sort=${sortKey}:${sortDir}&layout=advanced"/>" class="ui-button"> ${i} </a></c:otherwise>
                  </c:choose>
                </c:forEach>
                </c:otherwise>
            </c:choose>
            <c:if test="${page < numPages}">
              <a href="<c:url value="/product/search.htm?term=${term}&id=${menuId}&page=${page+1}&limit=${limit}&sort=${sortKey}:${sortDir}&layout=advanced"/>" class="ui-button"> > </a>
            </c:if>
        </div>          
    </div>  
</div>
<br/><br/>

<div class="fake-table">
    <div class="header">
        <div class="row">
            <div class="cell">SKU / Description</div>
            <div class="cell">Sale Price</div>
            <div class="cell">Price Record</div>
            <div class="cell">Price Type</div>
            <div class="cell">List Price</div>
            <div class="cell">Standard Cost</div>
            <div class="cell">Rebate Record</div>
            <div class="cell"><div class="center">Available</div></div>
            <div class="cell">GM %</div>
            <div class="cell">GM $</div>
            <div class="cell">Claim Amount</div>
            <div class="cell">Target Amt/GM%</div>
        </div>
    </div>
    <div class="body">
        <c:forEach items="${products}" var="item" varStatus="i">
        <div id="${i.count}" class="row">
            <div id="product" class="cell">
                <span class="narrow"><br/></span>
                <a href="<c:url value="/product/detail.htm?prod=${util:base64Encode(item.product)}"/>" style="font-size:125%">${item.product}</a><br/>
                ${item.description}
            </div>
            
            <div class="cell">
                <span class="narrow"><b>Sell Price:</b></span>
                <span id="salePrice"><fmt:formatNumber value="${item.price}" type="currency" currencySymbol="$" pattern="¤#,##0.00;¤-#,##0.00"/></span>
            </div>
            
            <div class="cell">
                <span class="narrow"><b>Rebate Record:</b></span>
                <span id="rebateRecord">${item.pdRecord.rebateRecord}</span>
            </div>
            <div class="cell">
                <span class="narrow"><b>Rebate Type:</b></span>
                <span id="rebateType">${item.pdRecord.rebateType}</span>
            </div>
            <div class="cell">
                <span class="narrow"><b>List Price:</b></span>
                <span id="listPrice"><fmt:formatNumber value="${item.pdRecord.listPrice}" type="currency" currencySymbol="$" pattern="¤#,##0.00;¤-#,##0.00"/></span>
            </div>
            <div class="cell">
                <span class="narrow"><b>Standard Cost:</b></span>
                <span id="standardCost"><fmt:formatNumber value="${item.pdRecord.standardCost}" type="currency" currencySymbol="$" pattern="¤#,##0.00;¤-#,##0.00"/></span>
            </div>
            <div class="cell">
                <span class="narrow"><b>Contract:</b></span>
                <span id="contractRecord">${item.pdRecord.contractRecord}</span>
            </div>
                
            <div class="cell">
                <div class="center">
                    <span class="narrow"><b>Qty Available:</b></span>
                    <c:choose>
                        <c:when test="${not empty session.warehouse}">
                            ${item.netAvail}<br/>
                        </c:when>
                        <c:otherwise><a href="<c:url value="/locations.htm"/>">Select Branch</a></c:otherwise>
                    </c:choose>
                </div>
            </div>             
            <div class="cell">
                <span class="narrow"><b>Margin %:</b></span>
                <span id="marginPct"><fmt:formatNumber value="${item.pdRecord.marginPct}" type="percent" currencySymbol="%" pattern="#,##0.00¤;-#,##0.00¤"/></span>
            </div>
            <div class="cell">
                <span class="narrow"><b>Margin $:</b></span>
                <span id="margin"><fmt:formatNumber value="${item.pdRecord.margin}" type="currency" currencySymbol="$" pattern="¤#,##0.00;¤-#,##0.00"/></span>
            </div>
            <div class="cell">
                <span class="narrow"><b>Calc Rebate Amt:</b></span>
                <span id="calcRebateAmount"><fmt:formatNumber value="${item.pdRecord.calcRebateAmount}" type="currency" currencySymbol="$" pattern="¤#,##0.00;¤-#,##0.00"/></span>
            </div>

            <div class="cell">
                <div style="padding-bottom:4px;"><input type="text" style="width:60px" id="${i.count}" name="calcSaleAmt" style="margin-bottom:4px;"/></div>
                <select id="${i.count}" name="targetType">
                    <option value="0">$</option>
                    <option value="1">%</option>
                </select>
                <span class="narrow"><br/><br/></span>
            </div>                
        </div>
        </c:forEach>
    </div>
</div>

  </jsp:body>
</t:layout>