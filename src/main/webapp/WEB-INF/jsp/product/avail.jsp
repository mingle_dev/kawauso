<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="/WEB-INF/kawauso-utils.tld" prefix="util"%>

<t:modal>
    <jsp:attribute name="modalTitle">Location Availability for ${util:base64Decode(param.prod)}</jsp:attribute>

    <jsp:body>
        
<div class="fake-table">
    <div class="header">
        <div class="row">
            <div class="cell">Location</div>
            <div class="cell txt-center">Availability</div>
            <div class="cell narrow-hide"></div>
        </div>
    </div>
    <div class="body">
        <c:forEach var="entry" items="${availability}">
        <div class="row">
            <div class="cell">
                    ${entry.address}<br/>
                    ${entry.city}, ${entry.state} ${entry.zipcd}<br/>
                    <a href="">${entry.phoneno}</a><br/>
                    <span class="narrow"><br/>Make <a href="<c:url value="/locations/set.htm?code=${entry.whse}&prod=${param.prod}"/>">${entry.state}${entry.whse}</a> my Location</span>
            </div>
            <div class="cell txt-center">${entry.available}</div>
            <div class="cell narrow-hide">Make <a href="<c:url value="/locations/set.htm?code=${entry.whse}&prod=${param.prod}"/>">${entry.state}${entry.whse}</a> my Location</div>
        </div>
        </c:forEach>
        <c:if test="${empty availability}">
            <div class="row text-zero">no inventory found in any warehouse location</div>
        </c:if>
    </div>
</div>
    </jsp:body>
</t:modal>

