<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="/WEB-INF/kawauso-utils.tld" prefix="util"%>

<t:accordian>
  <jsp:body>
    <div>        
    <c:forEach items="${accessories}" var="item">
        <div class="third">
            <div style="margin:20px; line-height:1.5em;">
                <div>
                    <c:choose>
                        <c:when test="${not empty item.image}">
                           <img src='http://mgl1939.s3-website-us-east-1.amazonaws.com/products/tn70/tn70_${item.image}' class="border" style='height:70px; width:auto;'> 
                        </c:when>
                        <c:otherwise>
                            <img src='http://mgl1939.s3-website-us-east-1.amazonaws.com/img/camera-icon-256.png' class="border" style='height:70px; width:auto;'> 
                        </c:otherwise>
                    </c:choose>
                </div><br/>
                <a href="<c:url value="/product/detail.htm?prod=${util:base64Encode(item.product)}"/>" style="font-size:125%;">${item.description}</a><br/>
            <b>Manufacturer:</b><br/>
            <b>SKU:</b> ${item.product}<br/>
            
            <c:if test="${not empty item.price}">
                <div style="padding:10px 0 10px 0; font-size:125%;"><b>Your Price:</b>
                <fmt:formatNumber value="${item.price}" type="currency" currencySymbol="$" pattern="¤#,##0.00;¤-#,##0.00"/></div>
            </c:if>
            <b>Qty Available:</b> ${item.avail}<br/><br/>
            
            <form name="cartItemForm" id="cartItemForm" action="/web/cart/addItem.htm" class="cartForm">
                <%--<input type="hidden" name="productCode" style="width:65px; text-align: right; padding-right: 10px;" value="${util:base64Encode(item.product)}"> --%>
                <input type="hidden" name="productCode" style="width:65px; text-align: right; padding-right: 10px;" value="${item.product}">
                <input type="text" name="quantity" style="width:65px; text-align: right; padding-right: 10px;" value="1">
                <input type="submit" class="btnCart" name="Add to Cart" value="Add to Cart">
            </form><br/>
            
            </div>
        </div>
    </c:forEach>      
    </div>
  </jsp:body>
</t:accordian>