<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="/WEB-INF/kawauso-utils.tld" prefix="util"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<t:layout>
    <jsp:body>
    <form action="<c:url value="/pricebook/admin/save.htm"/>" method="POST">
        <input type="hidden" name="id" value="${profile.id}"/>

        <c:choose>
            <c:when test="${mode == 'new'}">
                <input type="hidden" name="mode" value="new"/>
                <h2>Retail Pricing - New Profile</h2>
            </c:when>
            <c:otherwise>
                <input type="hidden" name="mode" value="edit"/>
                <h2>Retail Pricing - Edit Profile</h2>
            </c:otherwise>
        </c:choose>

        <table id="mytable" border="0" cellspacing="0" cellpadding="6" width="100%">
            <tr>
                <td width="100">Profile Name:</td>
                <td width="200"><input type="text" id="profile_name" name="profile_name" style="width:300px;" value="${profile.name}"></td>
                <td rowspan="2" align="right"><a href="https://s3.amazonaws.com/mgl1939/content/images/rpb-formula-full.png" target="fma"><img src="https://s3.amazonaws.com/mgl1939/content/images/rpb-formula.png" border="0"></a></td>
            </tr>
            <tr>
                <td>Tax Rate:</td>
                <td><input type="number" id="tax_rate" name="tax_rate" style="width:70px;" value="${profile.taxRate}"> %</td>
            </tr>
        </table><br/>

        <c:set var="currCatId" value="0" scope="page" />
        <c:forEach items="${itemGroups}" var="group" varStatus="loop">
            <c:if test="${currCatId ne group.categoryId}">
                <c:if test="${currCatId ne 0}">
                    </div>
                    </div>
                    <br/><br/>
                </c:if>
                <h3>${group.categoryName}</h3>
                <div class="fake-table">
                <div class="header">
                    <div class="row">
                        <div class="cell" style="width:150px;">Item Group</div>
                        <div class="cell">Description</div>
                        <div class="cell" style="width:100px;"><div class="center">Labor<br/>Cost $</div></div>
                        <div class="cell" style="width:100px;"><div class="center">Misc<br/>Cost $</div></div>
                        <div class="cell" style="width:100px;"><div class="center">Target<br/>GM %</div></div>
                        <div class="cell" style="width:100px;"><div class="center">Commission<br/>Rate %</div></div>
                    </div>
                </div>
                <div class="body">
                <c:set var="currCatId" value="${group.categoryId}" scope="page" />
            </c:if>
                    <div class="row">
                        <div class="cell">
                            <input type="text" id="formItem[${loop.count}].groupName" name="formItem[${loop.count}].groupName" value="${group.name}"/>
                        </div>
                        <div class="cell">
                            ${group.description}
                        </div>
                        <div class="cell">
                            <div class="center">
                                <input type="number" id="formItem[${loop.count}].laborAmt" name="formItem[${loop.count}].laborAmt" value="<fmt:formatNumber type="number" minFractionDigits="2" value="${group.laborAmt}"/>" style="width:85px"/>
                            </div>
                        </div>
                        <div class="cell">
                            <div class="center">
                                <input type="number" id="formItem[${loop.count}].miscAmt" name="formItem[${loop.count}].miscAmt" value="<fmt:formatNumber type="number" minFractionDigits="2" value="${group.miscAmt}"/>" style="width:85px"/>
                            </div>
                        </div>
                        <div class="cell">
                            <div class="center">
                                <input type="number" id="formItem[${loop.count}].gmPct" name="formItem[${loop.count}].gmPct" value="<fmt:formatNumber type="number" minFractionDigits="2" value="${group.gmPct}"/>" style="width:85px"/>
                            </div>
                        </div>
                        <div class="cell">
                            <div class="center">
                                <input type="number" id="formItem[${loop.count}].commissionRate" name="formItem[${loop.count}].commissionRate" value="<fmt:formatNumber type="number" minFractionDigits="2" value="${group.commissionRate}"/>" style="width:85px"/>
                            </div>
                        </div>
                    </div>
        </c:forEach>
        </div>
        </div>
        <br/>

        <div class="right">
            <a class="ui-link-  button" href="<c:url value="/pricebook/admin/index.htm"/>">Cancel</a>
            <input type="submit" name="submit" value="Save">
        </div>

    </form>
    </jsp:body>
</t:layout>