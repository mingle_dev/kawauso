<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="/WEB-INF/kawauso-utils.tld" prefix="util"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<t:layout>
  <jsp:body>

<style>
.checkboxList {
    padding: 6px 15px; 
    list-style-type: none;
}

.checkboxList li {
    font-size: 125%;
    padding-bottom: 8px;
}

.checkboxList li label {
    padding-left: 6px;
}
</style>

      <!-- Implementation of JQuery 1.11 functionality overwrote .datapicker functionality -->
<!--
      <div class="modal fade" id="myModal" style="display: block;width: 500px;background: red;height: 300px;font-size: 15px;/* font-weight: bold; *//* font-family: -webkit-body; */color: white;">
      <div class="modal-header" style="border-bottom: none">
      <a class="close" data-dismiss="modal">×</a>
      </div>
      <div class="modal-body">
      <p>Please limit the number of pricebook templates to 3 to 4 templates per request. The external mail server restricts the delivery of the pricebook message, if the message size exceeds message size limits.</p>
      </div>
      <div class="modal-footer" style="border-top: none">
      <a href="javascript:$('#myModal').hide()" class="btn">Close</a>
      </div>
      </div>

-->
<div class="left">
    <h1>Pricebooks - ${type} Pricing</h1>
</div>

<c:if test="${type eq 'Retail'}">
<div class="content-desktop right" style="font-size:150%; padding:10px;">
    <a class="ui-link-button" href="<c:url value="/pricebook/admin/index.htm"/>">Admin</a>
</div>
</c:if>
<div class="clear"></div>

<div class="section" style="font-size:125%">
    Select one or more templates from the list below, verify your profile information and click 'Submit'. The book will be
    sent to the email address that is stored in your profile.
</div>
<br/><br/>

<div class="container border">
    <h2 class="header active">
        Templates
    </h2>
    
    <div class="section">
        <form:form action="schedule.htm" commandName="checkboxList" method="post">
        <input type="hidden" name="type" value="${type}"/>

        <ul class="checkboxList">
            <form:checkboxes element="li" path="checkboxItems" items="${checkboxesMap}"></form:checkboxes>
        </ul>
    </div>
</div>
<br/><br/>      

<div class="container border">
    <h2 class="header active">
        Profile Information
    </h2>
    <div class="section">
        <dl style="font-size:125%">
            <dt>Full Name:</dt><dd>${session.contact.firstName} ${session.contact.lastName}</dd>
            <dt>Company:</dt><dd>${session.customerName}</dd>
            <dt>Customer #:</dt><dd>${session.customerNumber}</dd>
            <dt>Email Address:</dt><dd>${session.contact.emailAddr}</dd>
        </dl>
    </div>
</div>
<br/><br/>  

<input type="submit" name="submit" value="Submit">
    
</form:form>
<br/><br/><br/><br/>

Note: If you do not receive your pricebook, please <a href="mailto:ecommerce@mingledorffs.com">let us know</a>!
<br/><br/>

  </jsp:body>
</t:layout>