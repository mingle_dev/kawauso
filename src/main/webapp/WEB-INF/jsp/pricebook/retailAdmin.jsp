<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="/WEB-INF/kawauso-utils.tld" prefix="util"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<t:layout>
    <jsp:body>

        <script>
            $(function() {
                $(".retailAdminTask").change(function () {
                    window.location.href = $(this).val();
                });
            });
        </script>

        <table style="width:100%">
            <tr>
                <td>
                    <h2>Retail Pricing Administration</h2>
                </td>
                <td style="text-align:right;">
                    <a class="ui-link-button" href="<c:url value="/pricebook/admin/new.htm"/>">New Profile</a>
                </td>
            </tr>
        </table>

        <div class="fake-table">
            <div class="header">
                <div class="row">
                    <div class="cell">Profile Name</div>
                    <div class="cell">Last Saved</div>
                    <div class="cell"><div class="center">Active</div></div>
                    <div class="cell">Tools</div>
                </div>
            </div>
            <div class="body">
                <c:forEach items="${profiles}" var="profile">
                    <div class="row">
                        <div class="cell">
                            <a href="<c:url value="/pricebook/admin/edit.htm?id=${profile.id}"/>">${profile.name}</a>
                        </div>
                        <div class="cell">
                            ${profile.createDate}
                        </div>
                        <div class="cell">
                            <div class="center">
                                <c:choose>
                                    <c:when test="${profile.active == true}">
                                        [ <a href="<c:url value="/pricebook/admin/deactivate.htm?id=${profile.id}"/>">Yes</a> ]
                                    </c:when>
                                    <c:otherwise>
                                        [ <a href="<c:url value="/pricebook/admin/activate.htm?id=${profile.id}"/>">No</a> ]
                                    </c:otherwise>
                                </c:choose>
                            </div>
                        </div>
                        <div class="cell">
                            <select class="retailAdminTask">
                                <option></option>
                                <%--<option value="<c:url value="/pricebook/admin/download.htm?id=${profile.id}"/>">Download</option> --%>
                                <option value="<c:url value="/pricebook/admin/clone.htm?id=${profile.id}"/>">Clone</option>
                                <option value="<c:url value="/pricebook/admin/delete.htm?id=${profile.id}"/>">Delete</option>
                            </select>
                        </div>
                    </div>
                </c:forEach>
            </div>
        </div>

    </jsp:body>
</t:layout>