<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="/WEB-INF/kawauso-utils.tld" prefix="util"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<t:layout>
    <jsp:body>
<h1>Pricebook Builder</h1>        
        
<div class="section"> 
    
    <div class="half center">
        <a href="<c:url value="/pricebook/dealer.htm"/>"><img src="https://s3.amazonaws.com/mgl1939/img/res_dpb.png" border="0"></a>
        <p style="padding:0.5em 15px;">
            Create a PDF with the most popular items we carry along with YOUR pricing.
        </p>
    </div>
        
    <div class="half center">
        <a href="<c:url value="/pricebook/retail.htm"/>"><img src="https://s3.amazonaws.com/mgl1939/img/res_rpb.png" border="0"></a>
        <p style="padding:0.5em 15px;">
            Set retail pricing on popular items and create a PDF for print use. 
        </p>
    </div>        
        
</div>
<br/><br/><br/><br/>

    </jsp:body>
</t:layout>