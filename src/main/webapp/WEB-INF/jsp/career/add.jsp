<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<t:layout>
  <jsp:body>

<script src="//tinymce.cachefly.net/4.1/tinymce.min.js"></script>
<script>
        tinymce.init({
            menu : {},
            selector : 'textarea',
            statusbar : false,
            width : 700
        });
</script>        
      
    <h1>Add Position</h1>
      
    <form:form method="POST" commandName="positionForm" action="/web/career/add.htm"> 
        <dl class="wide">
            <dt>
                <label class="label-top">Position Title:</label>
            </dt>
            <dd>
                <form:input type="text" path="name" style="width:400px"/><form:errors path="name" element="div" cssClass="fieldError" /><br/><br/>
            </dd>
            <dt>
                <label class="label-top">Direct Report:</label>
            </dt>
            <dd>
                <form:input type="text" path="report" style="width:400px"/><form:errors path="report" element="div" cssClass="fieldError" /><br/><br/>
            </dd>            
            <dt>
                <label class="label-top">Classification:</label>
            </dt>
            <dd>
                <form:input type="text" path="classification" style="width:400px"/><form:errors path="classification" element="div" cssClass="fieldError" /><br/><br/>
            </dd>
            <dt>
                <label class="label-top">Location:</label>
            </dt>
            <dd>
                <form:input type="text" path="location" style="width:600px"/><form:errors path="location" element="div" cssClass="fieldError" /><br/><br/>
            </dd>
            <dt>
                <label class="label-top">Purpose:</label>
            </dt>
            <dd>
                <form:textarea path="purpose" rows="3" style="width:800px"/><form:errors path="purpose" element="div" cssClass="fieldError" /><br/><br/>
            </dd>
            <dt>
                <label class="label-top">Accountabilities:</label>
            </dt>
            <dd>
                <form:textarea path="accountabilities" rows="8" style="width:600px"/><form:errors path="accountabilities" element="div" cssClass="fieldError" /><br/><br/>
            </dd>
            <dt>
                <label class="label-top">Qualifications:</label>
            </dt>
            <dd>
                <form:textarea path="qualifications" rows="8" style="width:600px"/><form:errors path="qualifications" element="div" cssClass="fieldError" /><br/><br/>
            </dd>
            <dt>
                <label class="label-top">Other Information:</label>
            </dt>
            <dd>
                <form:textarea path="otherInfo" rows="4" style="width:600px"/><form:errors path="otherInfo" element="div" cssClass="fieldError" /><br/><br/>
            </dd>            
        </dl>
        <div style="padding-left:15px;">
            <input type="submit" name="submit" value="Submit"><br/><br/>
        </div>
    </form:form>
      
  </jsp:body>
</t:layout>
      