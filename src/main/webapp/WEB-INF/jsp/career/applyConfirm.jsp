<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<t:layout>
    <jsp:body>
   
<div style="font-size:125%; line-height:1.5">       
    Job Title: ${position.name}<br/>
    Location: ${position.location}<br/>        
</div><br/>
        
<div class="sb-right" style=" margin-bottom:10px;">
    <div class="container border">
        <h2 class="header active">
            Application Steps
        </h2>
        <div class="container" style="padding:10px; line-height:1.5">
            <img src="https://s3.amazonaws.com/mgl1939/img/check_green_small.gif"> Terms and Conditions</a><br/>
            <img src="https://s3.amazonaws.com/mgl1939/img/check_green_small.gif"> Personal Information<br/>
            <img src="https://s3.amazonaws.com/mgl1939/img/check_green_small.gif"> Pre-Employment Questions<br/>
            <img src="https://s3.amazonaws.com/mgl1939/img/check_green_small.gif"> Education<br/>
            <img src="https://s3.amazonaws.com/mgl1939/img/check_green_small.gif"> Experience<br/>
            <img src="https://s3.amazonaws.com/mgl1939/img/check_green_small.gif"> Resume<br/>
            <img src="https://s3.amazonaws.com/mgl1939/img/check_green_small.gif"> Review Your Application<br/>
        </div>
    </div>
</div>          
        
<div class="content" style="min-width:286px; margin-bottom:10px;">
    <div class="container border">
        <h2 class="header active">
            Application for Employment - Confirmation
        </h2>
       
        <div class="section" style="padding:10px; line-height: 1.5em; min-height:70px;">
            <p style="font-size:125%;">
                We appreciate your interest in Mingledorff’s, Inc. and acknowledge receipt of your resume. 
                In the event that we wish to arrange a formal interview, we will contact you. 
                Again, thank you for your interest in employment at Mingledorff’s, Inc.
            </p>
            <br/><br/>

            <div style="text-align:right;">
                <a class="ui-link-button" href="<c:url value="/index.htm"/>">Continue</a>
            </div>
            <br/><br/><br/><br/>
        </div>
    </div>
</div>   
<br/>         

    </jsp:body>
</t:layout>        
        