<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<t:layout>
    <jsp:body>
   
<div style="font-size:125%; line-height:1.5">       
    Job Title: ${position.name}<br/>
    Location: ${position.location}<br/>        
</div><br/>
        
<div class="sb-right" style=" margin-bottom:10px;">
    <div class="container border">
        <h2 class="header active">
            Application Steps
        </h2>
        <div class="container" style="padding:10px; line-height:1.5">
            <img src="https://s3.amazonaws.com/mgl1939/img/double-arrow.gif"> <b>Terms and Conditions</b><br/>
            <img src="https://s3.amazonaws.com/mgl1939/img/double-arrow.gif"> Personal Information<br/>
            <img src="https://s3.amazonaws.com/mgl1939/img/double-arrow.gif"> Pre-Employment Questions<br/>
            <img src="https://s3.amazonaws.com/mgl1939/img/double-arrow.gif"> Education<br/>
            <img src="https://s3.amazonaws.com/mgl1939/img/double-arrow.gif"> Experience<br/>
            <img src="https://s3.amazonaws.com/mgl1939/img/double-arrow.gif"> Resume<br/>
            <img src="https://s3.amazonaws.com/mgl1939/img/double-arrow.gif"> Review Your Application<br/>
        </div>
    </div>
</div>          
        
<div class="content" style="min-width:286px; margin-bottom:10px;">
    <div class="container border">
        <h2 class="header active">
            Application for Employment - Terms
        </h2>

        <p style="padding:20px; font-size:125%;">
            <b>Terms of Application:</b><br/>
            All employment with Mingledorff's is contingent upon a successful completion 
            of a background and drug screen.<br/><br/>
            This is an application for employment with Mingledorff's. This application 
            will be considered current for a period of six (6) months following the date 
            of application. If, at the end of this period, you still wish to be considered 
            for employment, it will be necessary for you to complete another application.<br/><br/>
            In order to complete this job application, you will need to acknowledge, 
            give consent to receive and respond to information in electronic form. 
            If you do not wish to consent to electronic transactions, please exit 
            the system now by clicking the "CANCEL APPLICATION" link below.
            <br/><br/><br/><br/>
        </p>
        
    <form:form method="POST" commandName="applicationForm" action="/web/career/apply.htm"> 
        <form:input type="hidden" path="step" value="1"></form:input>
        <form:input type="hidden" path="jobId"/>
        
        <div style="text-align:right;">
            <a class="ui-link-button" href="<c:url value="/career/index.htm"/>">Cancel Application</a>
            <input type="submit" name="submit" value="Begin"><br/><br/>
        </div>
    </form:form>
                
        </div>
    </div>
</div>   
<br/>         

    </jsp:body>
</t:layout>        
        