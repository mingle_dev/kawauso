<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<t:layout>
    <jsp:body>
   
<div style="font-size:125%; line-height:1.5">       
    Job Title: ${position.name}<br/>
    Location: ${position.location}<br/>        
</div><br/>
        
<div class="sb-right" style=" margin-bottom:10px;">
    <div class="container border">
        <h2 class="header active">
            Application Steps
        </h2>
        <div class="container" style="padding:10px; line-height:1.5">
            <img src="https://s3.amazonaws.com/mgl1939/img/check_green_small.gif"> Terms and Conditions</a><br/>
            <img src="https://s3.amazonaws.com/mgl1939/img/check_green_small.gif"> Personal Information<br/>
            <img src="https://s3.amazonaws.com/mgl1939/img/check_green_small.gif"> Pre-Employment Questions<br/>
            <img src="https://s3.amazonaws.com/mgl1939/img/check_green_small.gif"> Education<br/>
            <img src="https://s3.amazonaws.com/mgl1939/img/double-arrow.gif"> <b>Experience</b><br/>
            <img src="https://s3.amazonaws.com/mgl1939/img/double-arrow.gif"> Resume<br/>
            <img src="https://s3.amazonaws.com/mgl1939/img/double-arrow.gif"> Review Your Application<br/>
        </div>
    </div>
</div>          
        
<div class="content" style="min-width:286px; margin-bottom:10px;">
    <div class="container border">
        <h2 class="header active">
            Application for Employment - Experience
        </h2>
        <div class="section" style="padding:10px; line-height: 1.5em; min-height:70px;">

    <form:form method="POST" commandName="applicationForm" action="/web/career/apply.htm"> 
        <form:input type="hidden" path="step" value="5"></form:input>
        <dl class="wide">
            <dt style="width:70%">
                <label class="label-top">May we contact your present employer:</label>
            </dt>
            <dd>
                <form:select path="eduHighDiploma">
                    <form:option value="0">No</form:option>
                    <form:option value="1">Yes</form:option>
                </form:select>
            </dd>
        </dl><br/>

        <h3>Most Recent Employer</h3><hr/>    
        <dl class="wide">
            <dt>
                <label class="label-top">Name:</label>
            </dt>
            <dd>
                <form:input type="text" path="previousJobs[0].name" style="width:300px"/><form:errors path="previousJobs[0].name" element="div" cssClass="fieldError" /><br/><br/>
            </dd>
            <dt>
                <label class="label-top">City, State Zip:</label>
            </dt>
            <dd>
                <form:input type="text" path="previousJobs[0].city" style="width:250px"/><form:errors path="previousJobs[0].city" element="div" cssClass="fieldError" /><br/><br/>
            </dd> 
            <dt>
                <label class="label-top">Phone:</label>
            </dt>
            <dd>
                <form:input type="text" path="previousJobs[0].phone" style="width:150px"/><form:errors path="previousJobs[0].phone" element="div" cssClass="fieldError" /><br/><br/>
            </dd>    
            <dt>
                <label class="label-top">Position Held:</label>
            </dt>
            <dd>
                <form:input type="text" path="previousJobs[0].positionHeld" style="width:250px"/><form:errors path="previousJobs[0].positionHeld" element="div" cssClass="fieldError" /><br/><br/>
            </dd>    
            <dt>
                <label class="label-top">Dates (From/To):</label>
            </dt>
            <dd>
                <form:input type="text" path="previousJobs[0].employDates" style="width:200px"/><form:errors path="previousJobs[0].employDates" element="div" cssClass="fieldError" /><br/><br/>
            </dd>  
            <dt>
                <label class="label-top">Reason for Leaving:</label>
            </dt>
            <dd>
                <form:input type="text" path="previousJobs[0].reasonLeave" style="width:400px"/><form:errors path="previousJobs[0].reasonLeave" element="div" cssClass="fieldError" /><br/><br/>
            </dd>   
            <dt>
                <label class="label-top">Pay Rate upon Leaving:</label>
            </dt>
            <dd>
                <form:input type="text" path="previousJobs[0].payRate" style="width:150px"/><form:errors path="previousJobs[0].payRate" element="div" cssClass="fieldError" /><br/><br/>
            </dd>     
            <dt>
                <label class="label-top">Supervisor:</label>
            </dt>
            <dd>
                <form:input type="text" path="previousJobs[0].supervisor" style="width:200px"/><form:errors path="previousJobs[0].supervisor" element="div" cssClass="fieldError" /><br/><br/>
            </dd>
            <dt>
                <label class="label-top">Duties</label>
            </dt>
            <dd>
                <form:textarea path="previousJobs[0].duties" rows="5" cols="80"></form:textarea><form:errors path="previousJobs[0].duties" element="div" cssClass="fieldError" /><br/><br/>
            </dd>               
        </dl><br/>

        <h3>Next Most Recent Employer</h3><hr/>    
        <dl class="wide">
            <dt>
                <label class="label-top">Name:</label>
            </dt>
            <dd>
                <form:input type="text" path="previousJobs[1].name" style="width:300px"/><form:errors path="previousJobs[1].name" element="div" cssClass="fieldError" /><br/><br/>
            </dd>
            <dt>
                <label class="label-top">City, State Zip:</label>
            </dt>
            <dd>
                <form:input type="text" path="previousJobs[1].city" style="width:250px"/><form:errors path="previousJobs[1].city" element="div" cssClass="fieldError" /><br/><br/>
            </dd> 
            <dt>
                <label class="label-top">Phone:</label>
            </dt>
            <dd>
                <form:input type="text" path="previousJobs[1].phone" style="width:150px"/><form:errors path="previousJobs[1].phone" element="div" cssClass="fieldError" /><br/><br/>
            </dd>    
            <dt>
                <label class="label-top">Position Held:</label>
            </dt>
            <dd>
                <form:input type="text" path="previousJobs[1].positionHeld" style="width:250px"/><form:errors path="previousJobs[1].positionHeld" element="div" cssClass="fieldError" /><br/><br/>
            </dd>    
            <dt>
                <label class="label-top">Dates (From/To):</label>
            </dt>
            <dd>
                <form:input type="text" path="previousJobs[1].employDates" style="width:200px"/><form:errors path="previousJobs[1].employDates" element="div" cssClass="fieldError" /><br/><br/>
            </dd>  
            <dt>
                <label class="label-top">Reason for Leaving:</label>
            </dt>
            <dd>
                <form:input type="text" path="previousJobs[1].reasonLeave" style="width:400px"/><form:errors path="previousJobs[1].reasonLeave" element="div" cssClass="fieldError" /><br/><br/>
            </dd>   
            <dt>
                <label class="label-top">Pay Rate upon Leaving:</label>
            </dt>
            <dd>
                <form:input type="text" path="previousJobs[1].payRate" style="width:150px"/><form:errors path="previousJobs[1].payRate" element="div" cssClass="fieldError" /><br/><br/>
            </dd>     
            <dt>
                <label class="label-top">Supervisor:</label>
            </dt>
            <dd>
                <form:input type="text" path="previousJobs[1].supervisor" style="width:200px"/><form:errors path="previousJobs[1].supervisor" element="div" cssClass="fieldError" /><br/><br/>
            </dd>
            <dt>
                <label class="label-top">Duties</label>
            </dt>
            <dd>
                <form:textarea path="previousJobs[1].duties" rows="5" cols="80"></form:textarea><form:errors path="previousJobs[1].duties" element="div" cssClass="fieldError" /><br/><br/>
            </dd>               
        </dl><br/>      
        
        <h3>Next Most Recent Employer</h3><hr/>    
        <dl class="wide">
            <dt>
                <label class="label-top">Name:</label>
            </dt>
            <dd>
                <form:input type="text" path="previousJobs[2].name" style="width:300px"/><form:errors path="previousJobs[2].name" element="div" cssClass="fieldError" /><br/><br/>
            </dd>
            <dt>
                <label class="label-top">City, State Zip:</label>
            </dt>
            <dd>
                <form:input type="text" path="previousJobs[2].city" style="width:250px"/><form:errors path="previousJobs[2].city" element="div" cssClass="fieldError" /><br/><br/>
            </dd> 
            <dt>
                <label class="label-top">Phone:</label>
            </dt>
            <dd>
                <form:input type="text" path="previousJobs[2].phone" style="width:150px"/><form:errors path="previousJobs[2].phone" element="div" cssClass="fieldError" /><br/><br/>
            </dd>    
            <dt>
                <label class="label-top">Position Held:</label>
            </dt>
            <dd>
                <form:input type="text" path="previousJobs[2].positionHeld" style="width:250px"/><form:errors path="previousJobs[2].positionHeld" element="div" cssClass="fieldError" /><br/><br/>
            </dd>    
            <dt>
                <label class="label-top">Dates (From/To):</label>
            </dt>
            <dd>
                <form:input type="text" path="previousJobs[2].employDates" style="width:200px"/><form:errors path="previousJobs[2].employDates" element="div" cssClass="fieldError" /><br/><br/>
            </dd>  
            <dt>
                <label class="label-top">Reason for Leaving:</label>
            </dt>
            <dd>
                <form:input type="text" path="previousJobs[2].reasonLeave" style="width:400px"/><form:errors path="previousJobs[2].reasonLeave" element="div" cssClass="fieldError" /><br/><br/>
            </dd>   
            <dt>
                <label class="label-top">Pay Rate upon Leaving:</label>
            </dt>
            <dd>
                <form:input type="text" path="previousJobs[2].payRate" style="width:150px"/><form:errors path="previousJobs[2].payRate" element="div" cssClass="fieldError" /><br/><br/>
            </dd>     
            <dt>
                <label class="label-top">Supervisor:</label>
            </dt>
            <dd>
                <form:input type="text" path="previousJobs[2].supervisor" style="width:200px"/><form:errors path="previousJobs[2].supervisor" element="div" cssClass="fieldError" /><br/><br/>
            </dd>
            <dt>
                <label class="label-top">Duties</label>
            </dt>
            <dd>
                <form:textarea path="previousJobs[2].duties" rows="5" cols="80"></form:textarea><form:errors path="previousJobs[2].duties" element="div" cssClass="fieldError" /><br/><br/>
            </dd>               
        </dl><br/>        
        
        <div style="text-align:right;">
            <input type="submit" name="next" value="Next"><br/><br/>
        </div>
    </form:form>
                
        </div>
    </div>
</div>   
<br/>         

    </jsp:body>
</t:layout>