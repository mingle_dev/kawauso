<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<t:layout>
    <jsp:body>
     
<div style="font-size:125%; line-height:1.5">       
    Job Title: ${position.name}<br/>
    Location: ${position.location}<br/>        
</div><br/>
        
<div class="sb-right" style=" margin-bottom:10px;">
    <div class="container border">
        <h2 class="header active">
            Application Steps
        </h2>
        <div class="container" style="padding:10px; line-height:1.5">
            <img src="https://s3.amazonaws.com/mgl1939/img/check_green_small.gif"> Terms and Conditions</a><br/>
            <img src="https://s3.amazonaws.com/mgl1939/img/check_green_small.gif"> Personal Information<br/>
            <img src="https://s3.amazonaws.com/mgl1939/img/double-arrow.gif"> <b>Pre-Employment Questions</b><br/>
            <img src="https://s3.amazonaws.com/mgl1939/img/double-arrow.gif"> Education<br/>
            <img src="https://s3.amazonaws.com/mgl1939/img/double-arrow.gif"> Experience<br/>
            <img src="https://s3.amazonaws.com/mgl1939/img/double-arrow.gif"> Resume<br/>
            <img src="https://s3.amazonaws.com/mgl1939/img/double-arrow.gif"> Review Your Application<br/>
        </div>
    </div>
</div>          
        
<div class="content" style="min-width:286px; margin-bottom:10px;">
    <div class="container border">
        <h2 class="header active">
            Application for Employment - Pre-Employment Questions
        </h2>
        <div class="section" style="padding:10px; line-height: 1.5em; min-height:70px;">

    <form:form method="POST" commandName="applicationForm" action="/web/career/apply.htm">
        <form:input type="hidden" path="step" value="3"></form:input>
        <dl class="wide">
            <dt style="width:70%">
                <label class="label-top"><span style="color:#FF0000">*</span>
                    Are you at least 18 years of age and legally eligible for work in the United States:
                </label>
            </dt>
            <dd>
                <form:select path="eligible">
                    <form:option value="0">No</form:option>
                    <form:option value="1">Yes</form:option>
                </form:select><br/><br/>
            </dd>
            
            <dt style="width:70%">
                <label class="label-top"><span style="color:#FF0000">*</span>
                    Will you work overtime if necessary:
                </label>
            </dt>
            <dd>
                <form:select path="overtime">
                    <form:option value="0">No</form:option>
                    <form:option value="1">Yes</form:option>
                </form:select><br/><br/>
            </dd>
            
            <dt style="width:70%">
                <label class="label-top"><span style="color:#FF0000">*</span>
                    Are you on layoff and subject to recall:
                </label>
            </dt>
            <dd>
                <form:select path="recall">
                    <form:option value="0">No</form:option>
                    <form:option value="1">Yes</form:option>
                </form:select><br/><br/>
            </dd>            
            
            <dt style="width:70%">
                <label class="label-top"><span style="color:#FF0000">*</span>
                    Do you have a valid driver's license:
                </label>
            </dt>
            <dd>
                <form:select path="validLicense">
                    <form:option value="0">No</form:option>
                    <form:option value="1">Yes</form:option>
                </form:select><br/><br/>
            </dd>
            
            <dt style="width:70%">
                <label class="label-top"><span style="color:#FF0000">*</span>
                    Have you ever been discharged or asked to resign from a job:
                </label>
            </dt>
            <dd>
                <form:select path="discharged">
                    <form:option value="0">No</form:option>
                    <form:option value="1">Yes</form:option>
                </form:select><br/><br/>
            </dd>
            
            <dt style="width:70%">
                <label class="label-top"><span style="color:#FF0000">*</span>
                    Have you ever been convicted of or pled guilty to a felony:
                </label>
            </dt>
            <dd>
                <form:select path="felony">
                    <form:option value="0">No</form:option>
                    <form:option value="1">Yes</form:option>
                </form:select><br/><br/>
            </dd>
            
            <dt style="width:70%">
                <label class="label-top"><span style="color:#FF0000">*</span>
                    Have you previously been employed by Mingledorff's Inc. or a subsidiary:
                </label>
            </dt>
            <dd>
                <form:select path="prevEmploy">
                    <form:option value="0">No</form:option>
                    <form:option value="1">Yes</form:option>
                </form:select><br/><br/>
            </dd>
            
            <dt style="width:70%">
                <label class="label-top"><span style="color:#FF0000">*</span>
                    Do you have any relatives working for Mingledorff's Inc. or a subsidiary:
                </label>
            </dt>
            <dd>
                <form:select path="relatives">
                    <form:option value="0">No</form:option>
                    <form:option value="1">Yes</form:option>
                </form:select><br/><br/>
            </dd>

        </dl>
        <div style="text-align:right;">
            <input type="submit" name="next" value="Next"><br/><br/>
        </div>
    </form:form>
                
        </div>
    </div>
</div>   
<br/>         

    </jsp:body>
</t:layout>