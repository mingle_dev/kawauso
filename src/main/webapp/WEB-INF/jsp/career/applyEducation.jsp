<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<t:layout>
    <jsp:body>
   
<div style="font-size:125%; line-height:1.5">       
    Job Title: ${position.name}<br/>
    Location: ${position.location}<br/>        
</div><br/>
        
<div class="sb-right" style=" margin-bottom:10px;">
    <div class="container border">
        <h2 class="header active">
            Application Steps
        </h2>
        <div class="container" style="padding:10px; line-height:1.5">
            <img src="https://s3.amazonaws.com/mgl1939/img/check_green_small.gif"> Terms and Conditions</a><br/>
            <img src="https://s3.amazonaws.com/mgl1939/img/check_green_small.gif"> Personal Information<br/>
            <img src="https://s3.amazonaws.com/mgl1939/img/check_green_small.gif"> Pre-Employment Questions<br/>
            <img src="https://s3.amazonaws.com/mgl1939/img/double-arrow.gif"> <b>Education</b><br/>
            <img src="https://s3.amazonaws.com/mgl1939/img/double-arrow.gif"> Experience<br/>
            <img src="https://s3.amazonaws.com/mgl1939/img/double-arrow.gif"> Resume<br/>
            <img src="https://s3.amazonaws.com/mgl1939/img/double-arrow.gif"> Review Your Application<br/>
        </div>
    </div>
</div>          
        
<div class="content" style="min-width:286px; margin-bottom:10px;">
    <div class="container border">
        <h2 class="header active">
            Application for Employment - Education
        </h2>
        <div class="section" style="padding:10px; line-height: 1.5em; min-height:70px;">

    <form:form method="POST" commandName="applicationForm" action="/web/career/apply.htm"> 
        <form:input type="hidden" path="step" value="4"></form:input>
        <h3>High School</h3><hr/>
        <dl class="wide">
            <dt>
                <label class="label-top">School Name:</label>
            </dt>
            <dd>
                <form:input type="text" path="eduHighSchool" style="width:250px"/><form:errors path="eduHighSchool" element="div" cssClass="fieldError" /><br/><br/>
            </dd>
            <dt>
                <label class="label-top">City, County, State:</label>
            </dt>
            <dd>
                <form:input type="text" path="eduHighState" style="width:250px"/><form:errors path="eduHighState" element="div" cssClass="fieldError" /><br/><br/>
            </dd>            
            <dt>
                <label class="label-top">Did you earn a diploma:</label>
            </dt>
            <dd>
                <form:select path="eduHighDiploma">
                    <form:option value="0">No</form:option>
                    <form:option value="1">Yes</form:option>
                </form:select>
            </dd>
        </dl><br/>

        <h3>College</h3><hr/>    
        <dl class="wide">
            <dt>
                <label class="label-top">School Name:</label>
            </dt>
            <dd>
                <form:input type="text" path="eduUgradSchool" style="width:250px"/><form:errors path="eduUgradSchool" element="div" cssClass="fieldError" /><br/><br/>
            </dd>
            <dt>
                <label class="label-top">City, State:</label>
            </dt>
            <dd>
                <form:input type="text" path="eduUgradState" style="width:250px"/><form:errors path="eduUgradState" element="div" cssClass="fieldError" /><br/><br/>
            </dd>            
            <dt>
                <label class="label-top">Field of Study:</label>
            </dt>
            <dd>
                <form:input type="text" path="eduUgradField" style="width:250px"/><form:errors path="eduUgradField" element="div" cssClass="fieldError" /><br/><br/>
            </dd>  
            <dt>
                <label class="label-top">Degree / Certificate / Diploma:</label>
            </dt>
            <dd>
                <form:input type="text" path="eduUgradDegree" style="width:250px"/><form:errors path="eduUgradDegree" element="div" cssClass="fieldError" /><br/><br/>
            </dd>             
        </dl><br/>
        
        <h3>Graduate School</h3><hr/>        
        <dl class="wide">
            <dt>
                <label class="label-top">School Name:</label>
            </dt>
            <dd>
                <form:input type="text" path="eduGradSchool" style="width:250px"/><form:errors path="eduGradSchool" element="div" cssClass="fieldError" /><br/><br/>
            </dd>
            <dt>
                <label class="label-top">City, State:</label>
            </dt>
            <dd>
                <form:input type="text" path="eduGradState" style="width:250px"/><form:errors path="eduGradState" element="div" cssClass="fieldError" /><br/><br/>
            </dd>            
            <dt>
                <label class="label-top">Field of Study:</label>
            </dt>
            <dd>
                <form:input type="text" path="eduGradField" style="width:250px"/><form:errors path="eduGradField" element="div" cssClass="fieldError" /><br/><br/>
            </dd>  
            <dt>
                <label class="label-top">Degree / Certificate / Diploma:</label>
            </dt>
            <dd>
                <form:input type="text" path="eduGradDegree" style="width:250px"/><form:errors path="eduGradDegree" element="div" cssClass="fieldError" /><br/><br/>
            </dd>
        </dl><br/>
        
        <h3>Trade, Business, or Other School</h3><hr/>        
        <dl class="wide">
            <dt>
                <label class="label-top">School Name:</label>
            </dt>
            <dd>
                <form:input type="text" path="eduTradeSchool" style="width:250px"/><form:errors path="eduTradeSchool" element="div" cssClass="fieldError" /><br/><br/>
            </dd>
            <dt>
                <label class="label-top">City, State:</label>
            </dt>
            <dd>
                <form:input type="text" path="eduTradeState" style="width:250px"/><form:errors path="eduTradeState" element="div" cssClass="fieldError" /><br/><br/>
            </dd>            
            <dt>
                <label class="label-top">Field of Study:</label>
            </dt>
            <dd>
                <form:input type="text" path="eduTradeField" style="width:250px"/><form:errors path="eduTradeField" element="div" cssClass="fieldError" /><br/><br/>
            </dd>  
            <dt>
                <label class="label-top">Degree / Certificate / Diploma:</label>
            </dt>
            <dd>
                <form:input type="text" path="eduTradeDegree" style="width:250px"/><form:errors path="eduTradeDegree" element="div" cssClass="fieldError" /><br/><br/>
            </dd>
        </dl><br/>
        
        <b>Please list any professional licenses, designations, certifications, etc. that may relate to the position for which you are
        applying. Include date granted, name of organization, and any other relevant information:</b><br/>
        <form:textarea path="certifications" cols="80" rows="5"></form:textarea><br/><br/>
        
        <b>Please list any special skills (include software skills, language skills, etc.) you may have that relate to the
        position for which you are applying:</b><br/>
        <form:textarea path="skills" cols="80" rows="5"></form:textarea><br/><br/>

        <div style="text-align:right;">
            <input type="submit" name="next" value="Next"><br/><br/>
        </div>
    </form:form>
                
        </div>
    </div>
</div>   
<br/>         

    </jsp:body>
</t:layout>