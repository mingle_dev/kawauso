<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="/WEB-INF/kawauso-utils.tld" prefix="util"%>

<t:layout>
  <jsp:body>
      <br/><br/><br/><br/>
      <center>
          <div style="font-size:125%; vertical-align:middle;">
              <c:choose>
                  <c:when test="${status eq 'success'}">
                    <img src="https://s3.amazonaws.com/mgl1939/img/green_check.png" style="width:32px; vertical-align:middle;"/>
                  </c:when>
                  <c:when test="${status eq 'warn'}">
                    <img src="https://s3.amazonaws.com/mgl1939/img/yellow_alert.png" style="width:32px; vertical-align:middle;"/>
                  </c:when>
                  <c:when test="${status eq 'error'}">
                    <img src="https://s3.amazonaws.com/mgl1939/img/red_error.png" style="width:32px; vertical-align:middle;"/>
                  </c:when>
              </c:choose>
              ${message}
          </div>
      <br/><br/>

      <%-- custom defined based on attribute --%>
      <c:if test="${not empty itemResults}">
          <div class="fake-table">
              <div class="header">
                  <div class="row">
                      <div class="cell">Product</div>
                      <div class="cell">Result</div>
                  </div>
              </div>
              <div class="body">
                  <c:forEach var="item" items="${itemResults}">
                      <div class="row">
                          <div class="cell">
                              ${item[0]}
                          </div>
                          <div class="cell">
                                  ${item[1]}
                          </div>
                      </div>
                  </c:forEach>
              </div>
          </div>
          <br/><br/>
      </c:if>

      <c:if test="${not empty button_yes}">
          <a class="ui-link-button" href="<c:url value="${button_yes}"/>">Yes</a>
      </c:if>
      
      <c:if test="${not empty button_no}">
      <a class="ui-link-button"  href="<c:url value="${button_no}"/>">No</a>
      </c:if>

      <c:if test="${not empty button_cancel}">
          <a class="ui-link-button"  href="<c:url value="${button_cancel}"/>">Cancel</a>
      </c:if>

      <c:if test="${not empty button_continue}">
      <a class="ui-link-button"  href="<c:url value="${button_continue}"/>">Continue</a>
      </c:if>

      </center>
      <br/><br/><br/><br/>
  </jsp:body>
</t:layout>