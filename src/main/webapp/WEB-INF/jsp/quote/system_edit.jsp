<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="/WEB-INF/kawauso-utils.tld" prefix="util"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<%--
http://stackoverflow.com/questions/4018615/jquery-ui-autocomplete-multiple-sources
--%>

<t:layout>
<jsp:body>

  <style>
    .ui-state-focus {
      background: #e6e6e6 url("http://code.jquery.com/ui/1.11.2/themes/smoothness/images/ui-bg_glass_75_e6e6e6_1x400.png") 50% 50% repeat-x;
    }
    .ui-autocomplete-loading {
      background: white url('https://s3.amazonaws.com/mgl1939/content/images/ui-anim_basic_16x16.gif') right center no-repeat;
    }
    .ui-helper-hidden-accessible {
      display: none !important;
    }
    .ui-menu {
      list-style: none;
      padding: 2px;
      margin: 0;
      display: block;
      float: left;
    }

    .ui-autocomplete {
      position: absolute;
      cursor: default;
      max-height: 300px;
      overflow-y: auto;
      overflow-x: hidden;
    }

    .ui-menu .ui-menu-item {
      cursor: pointer;
      margin: 0px;
      padding: 3px 0px 3px 0px;
      zoom: 1;
      float: left;
      clear: left;
      width: 100%;
      list-style-image: url("data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7");
    }

    .ui-widget-content {
      border: 1px solid #aaaaaa;
      background: #ffffff;
      color: #222222;
    }

    .ui-state-hover {
      background: #cccccc;
    }
    .custom-combobox-toggle {
      position: absolute;
      top: 0;
      bottom: 0;
      margin-left: -1px;
      padding: 0;
    }
    .ui-corner-all, .ui-corner-bottom, .ui-corner-right, .ui-corner-br {
      border-bottom-right-radius: 4px;
    }
    .ui-corner-all, .ui-corner-top, .ui-corner-right, .ui-corner-tr {
      border-top-right-radius: 4px;
    }
    .ui-button-icon-only {
      width: 2.2em;
    }
    .custom-combobox-input {
      margin: 0;
      padding: 5px 10px;
    }
    .ui-corner-all, .ui-corner-bottom, .ui-corner-left, .ui-corner-bl {
      border-bottom-left-radius: 4px;
    }
    .ui-corner-all, .ui-corner-top, .ui-corner-left, .ui-corner-tl {
      border-top-left-radius: 4px;
    }
    .ui-state-default, .ui-widget-content .ui-state-default, .ui-widget-header .ui-state-default {
      border: 1px solid #d3d3d3;
      background: #e6e6e6 url("http://code.jquery.com/ui/1.11.2/themes/smoothness/images/ui-bg_glass_75_e6e6e6_1x400.png") 50% 50% repeat-x;
      font-weight: normal;
      color: #555555;
    }
    .ui-state-default .ui-icon {
      background-image: url("http://code.jquery.com/ui/1.11.2/themes/smoothness/images/ui-icons_888888_256x240.png");
    }
    .ui-button-icon-only .ui-icon {
      left: 50%;
      margin-left: -8px;
    }
    .ui-button-icon-only .ui-icon, .ui-button-text-icon-primary .ui-icon, .ui-button-text-icon-secondary .ui-icon, .ui-button-text-icons .ui-icon, .ui-button-icons-only .ui-icon {
      position: absolute;
      top: 50%;
      margin-top: -8px;
    }
    .ui-icon-triangle-1-s {
      background-position: -64px -16px;
    }
    .ui-icon, .ui-widget-content .ui-icon {
      background-image: url("http://code.jquery.com/ui/1.11.2/themes/smoothness/images/ui-icons_222222_256x240.png");
    }
    .ui-icon {
      width: 16px;
      height: 16px;
    }
    .ui-icon {
      display: block;
      text-indent: -99999px;
      overflow: hidden;
      background-repeat: no-repeat;
    }

    .ui-button {
      display: inline-block;
      position: relative;
      padding: 0;
      line-height: normal;
      margin-right: .1em;
      margin-bottom: 4px;
      cursor: pointer;
      vertical-align: middle;
      text-align: center;
      overflow: visible;
    }
  </style>

  <script>
    (function( $ ) {
      $.widget( "custom.combobox", {
        options: {
        },
        _create: function () {
          var id = this.element.attr('id');
          var element = this.element;

          this.wrapper = this.element.wrap('<div class="ui-widget"><span class="custom-combobox"></span></div>');
          this._createAutocomplete(element);
          this._createShowAllButton();
        },

        _createAutocomplete: function(element) {

//          var availableTags = [
//            "ActionScript",
//            "AppleScript",
//            "Asp",
//            "BASIC",
//            "Clojure",
//            "COBOL"];

          //this.input = $("#accessories2\\.sku").autocomplete({
          this.input = element.autocomplete({
            source: "/web/product/quicklist.htm",
            minLength: 3,
            focus: function( event, ui ) {
              $(this).val(ui.item.product);
              return false;
            },
            select: function( event, ui ) {
              $(this).val(ui.item.product);
              return false;
            }
          }).click(function () {
            $(this).val('');
            $(this).autocomplete( "option", "minLength", 3 );
            $(this).autocomplete( "option", "source", "/web/product/quicklist.htm" );
          });

          this.input.autocomplete( "instance" )._renderItem = function( ul, item ) {
            //console.log(item);
            return $( "<li>" )
                    .append( item.product + '<br>' + item.description )
                    .appendTo( ul );
          };
        },

        _createShowAllButton: function() {
          var input = this.input;

          $("<a>")
              .attr( "tabIndex", -1 )
              .appendTo( this.wrapper.parent() )
                .button({
                  icons: {
                    primary: "ui-icon-triangle-1-s"
                  },
                  text: false
                })
                .removeClass( "ui-corner-all" )
                .addClass( "custom-combobox-toggle ui-corner-right" )
              .click(function() {
                var source = [];
                $('#accessory-combobox option').each(function(){
                  source.push({"product": this.value, "description": this.text });
                });

                input.val('').focus();
                input.autocomplete( "option", "minLength", 0 );
                input.autocomplete( "option", "source", source );
                input.autocomplete( "search", "" );
              });
        }
      });
    })( jQuery );

    $(document).ready(function () {
      //add custom combo-box to accessories
      $('input[id$=".sku"]').each(function() {
      //$("form").delegate('input[id$=".sku"]', "keyup", function(e) {
        if ( $(this).attr('id').indexOf('accessories') !== -1 ) {
          $(this).combobox({source: 'accessory-combobox'});
        }
      });

      //add line to products if none empty
      $('#qb-addline').click(function (e) {
        var numBlank = 0;
        $('input[id$=".sku"]').each(function() {
          if($(this).attr('id').indexOf("products") > -1 && $(this).val().length == 0)
            numBlank++;
        });
        if(numBlank > 0)
          return;

        var copy = $(".qb-prodline:last").clone(true);
        var arr = copy.attr('id').split('-');
        var curr = parseInt(arr[1]);
        var next = curr+1;

        //change id of div
        copy.attr('id', 'row-' + next);

        //change id of all form fields
        copy.html(function(i, oldHTML) {
          return oldHTML.replace(new RegExp('products'+curr, 'g'), 'products'+next);
        });

        //change name of all form fileds
        copy.html(function(i, oldHTML) {
          return oldHTML.replace(new RegExp('products\\['+curr+'\\]', 'g'), 'products['+next+']');
        });

        copy.insertAfter(".qb-prodline:last");
      });

      //add line to accessories if none empty
      //@todo; merge this with #qb-addline
      $('#qb-addline-acc').click(function (e) {
        var numBlank = 0;
        $('input[id$=".sku"]').each(function() {
          if($(this).attr('id').indexOf("accessories") > -1 && $(this).val().length == 0)
            numBlank++;
        });
        if(numBlank > 0)
          return;

        var copy = $(".qb-accline:last").clone(true);
        var arr = copy.attr('id').split('-');
        var curr = parseInt(arr[1]);
        var next = curr+1;

        //change id of div
        copy.attr('id', 'row-' + next);

        //change id of all form fields
        copy.html(function(i, oldHTML) {
          return oldHTML.replace(new RegExp('accessories'+curr, 'g'), 'accessories'+next);
        });

        //change name of all form fileds
        copy.html(function(i, oldHTML) {
          return oldHTML.replace(new RegExp('accessories\\['+curr+'\\]', 'g'), 'accessories['+next+']');
        });

        copy.insertAfter(".qb-accline:last");

        //@todo; this does activate the dropdown BUT it doubles the styling
        //from the clone above
        //copy.combobox({source: 'accessory-combobox'});
      });

      //calculate subtotals
      $('#subtot').click(function(e) {
        var subtot = 0;
        $('input[id$=".extPrice"]').each(function() {
          var p = parseFloat( $(this).val() );
          if(p > 0) subtot += p;
        });
        $('#subtot').val( subtot.toFixed(2) );
      });

      //register current and future component fields for autocomplete
      $("form").delegate('input[id$=".sku"]', "keyup", function(e) {
        var parentId = $(this).parent().attr('id');

        if( parentId === undefined || parentId.indexOf("component") < 0) {
          return;
        }

        console.log('yep');

        $(this).autocomplete({
          source: "/web/product/quicklist.htm",
          minLength: 3,
          delay: 100,
          focus: function( event, ui ) {
            $(this).val(ui.item.product);
            return false;
          },
          select: function( event, ui ) {
            $(this).val(ui.item.product);
            console.log(ui.item.product);
            return false;
          }
        })
        .autocomplete( "instance" )._renderItem = function( ul, item ) {
          return $( "<li>" )
                  .append( item.product + '<br>' + item.description )
                  .appendTo( ul );
        };
      });

      //fetch extended product information
      //register current and future input fields for product details
      $("form").delegate('input[id$=".sku"]', "blur", function(e) {
        e.preventDefault();

        var id = $(this).attr('id');
        var parentId = $(this).closest('div[id]').attr('id');
        var rowId = id.substr(0, id.indexOf('.'));

        if($(this).val().length == 0) {
          $("#"+rowId+"\\.description").attr("placeholder", "");
          $("#"+rowId+"\\.description").val("");
          $("#"+rowId+"\\.qty").val("");
          $("#"+rowId+"\\.cost").val("");
          $("#"+rowId+"\\.basePrice").val("");
          $("#"+rowId+"\\.sellPrice").val("");
          $("#"+rowId+"\\.extPrice").val("");
          $("#"+rowId+"\\.marginPct").val("");
          return;
        }

        var qty = 0;

        $.ajax({
          url: '/web/product/get-prod-json.htm',
          data: {
            'custno' : ${quote.customerNumber},
            'shipTo': '${quote.shipTo}',
            'prod' : $(this).val()},
          type: 'GET',
          beforeSend: function() {
            qty = $("#"+rowId+"\\.qty").val();

            if( parentId.indexOf("component") > -1) {
              $('#qb-addline').trigger('click');
              $('#fetch').trigger('click');
            }
            if( parentId.indexOf("accessory") > -1)
              $('#qb-addline-acc').trigger('click');

            $("#"+rowId+"\\.description").attr("placeholder", "Fetching Details...");
            $("#"+rowId+"\\.description").val("");
            $("#"+rowId+"\\.qty").val("");
            $("#"+rowId+"\\.cost").val("");
            $("#"+rowId+"\\.basePrice").val("");
            $("#"+rowId+"\\.sellPrice").val("");
            $("#"+rowId+"\\.extPrice").val("");
            $("#"+rowId+"\\.marginPct").val("");
          },
          success: function(res) {
            var obj = jQuery.parseJSON(res);

            $("#"+rowId+"\\.description").attr("placeholder", "");
            $("#"+rowId+"\\.description").val(obj.description);
            if(qty.length == 0)
              $("#"+rowId+"\\.qty").val("1");
            else
              $("#"+rowId+"\\.qty").val(qty);

            $("#"+rowId+"\\.cost").val(obj.pdRecord.standardCost.toFixed(2));
            $("#"+rowId+"\\.basePrice").val(obj.price.toFixed(2));
            $("#"+rowId+"\\.sellPrice").val(obj.price.toFixed(2));

            if(obj.pdRecord.marginPct.toFixed(2) < 1)
              $("#"+rowId+"\\.marginPct").val( (obj.pdRecord.marginPct * 100).toFixed(2) );
            else
              $("#"+rowId+"\\.marginPct").val(obj.pdRecord.marginPct.toFixed(2));

            $("#"+rowId+"\\.extPrice").val(obj.price.toFixed(2));

            $("#"+rowId+"\\.nonStock").prop("checked", false);

            $('#subtot').trigger('click');
          },
          error: function() {
            $("#"+rowId+"\\.description").attr("placeholder", "Item not found. Assumed Non-Stock.");

            $("#"+rowId+"\\.nonStock").prop("checked", true);

            $("#"+rowId+"\\.qty").val('1');
            $("#"+rowId+"\\.cost").val(0.00);
            $("#"+rowId+"\\.sellPrice").val(0.00);
            $("#"+rowId+"\\.extPrice").val(0.00);
            $("#"+rowId+"\\.marginPct").val(0.00);

            $("#"+rowId+"\\.cost").attr("readonly", false);
            $("#"+rowId+"\\.marginPct").attr("readonly", true);
          }
        });
      });

      //update extended price
      //register current and future intput fields
      $("form").delegate('input[id$=".qty"]', "blur", function(e) {
        e.preventDefault();
        var id = $(this).attr('id');
        var rowId = id.substr(0, id.indexOf('.'));
        var ext = $("#"+rowId+"\\.qty").val() * $("#"+rowId+"\\.sellPrice").val();
        $("#"+rowId+"\\.extPrice").val(ext.toFixed(2));

        $('#subtot').trigger('click');
      });

      $("form").delegate('input[id$=".cost"]', "blur", function(e) {
        var id = $(this).attr('id');
        var rowId = id.substr(0, id.indexOf('.'));
        $("#"+rowId+"\\.marginPct").val( ( ( $("#"+rowId+"\\.sellPrice").val() - $("#"+rowId+"\\.cost").val() ) / $("#"+rowId+"\\.sellPrice").val() ) * 100);
      });

      //calculate margin percent from sale price
      //register current and future input fields
      $("form").delegate('input[id$=".sellPrice"]', "blur", function(e) {
        e.preventDefault();
        var id = $(this).attr('id');
        var rowId = id.substr(0, id.indexOf('.'));

        //non-stock don't bother calling service
        if($("#"+rowId+"\\.nonStock").prop( "checked") == true) {
          var ext = $("#"+rowId+"\\.qty").val() * $("#"+rowId+"\\.sellPrice").val();
          $("#"+rowId+"\\.extPrice").val(ext.toFixed(2));
          $("#"+rowId+"\\.marginPct").val( ( ( $("#"+rowId+"\\.sellPrice").val() - $("#"+rowId+"\\.cost").val() ) / $("#"+rowId+"\\.sellPrice").val() ) * 100);

          $('#subtot').trigger('click');
          return;
        }

        $.ajax({
          url: '/web/product/calc-claim.htm',
          data: {
            'customerNumber' : ${quote.customerNumber},
            'shipTo' : '${quote.shipTo}',
            'productCode' : $("#"+rowId+"\\.sku").val(),
            'target' : $("#"+rowId+"\\.sellPrice").val(),
            'targetType' : 0
          },
          type: 'POST',
          beforeSend: function() {
            $("#"+rowId+"\\.marginPct").val('---');
          },
          success: function(res) {
            var obj = jQuery.parseJSON(res);
            if(obj.marginPct.toFixed(2) < 1)
              $("#"+rowId+"\\.marginPct").val( (obj.marginPct * 100).toFixed(2) );
            else
              $("#"+rowId+"\\.marginPct").val( obj.marginPct.toFixed(2) );
          },
          error: function() {
            $("#"+rowId+"\\.marginPct").val(0.00);
          }
        });

        var ext = $("#"+rowId+"\\.qty").val() * $("#"+rowId+"\\.sellPrice").val();
        $("#"+rowId+"\\.extPrice").val(ext.toFixed(2));
        $('#subtot').trigger('click');
      });

      //recalculate sale price based on given margin percent
      //register current and future input fields
      $("form").delegate('input[id$=".marginPct"]', "blur", function(e) {
        e.preventDefault();
        var id = $(this).attr('id');
        var rowId = id.substr(0, id.indexOf('.'));

        //recalcuate gm %
        $.ajax({
          url: '/web/product/calc-claim.htm',
          data: {
            'customerNumber' : ${quote.customerNumber},
            'shipTo' : '${quote.shipTo}',
            'productCode' : $("#"+rowId+"\\.sku").val(),
            'target' : $("#"+rowId+"\\.marginPct").val(),
            'targetType' : 1
          },
          type: 'POST',
          beforeSend: function() {
            $("#"+rowId+"\\.sellPrice").val('---');
          },
          success: function(res) {
            var obj = jQuery.parseJSON(res);
            $("#"+rowId+"\\.sellPrice").val(obj.salePrice.toFixed(2));

            var ext = $("#"+rowId+"\\.qty").val() * $("#"+rowId+"\\.sellPrice").val();
            $("#"+rowId+"\\.extPrice").val(ext.toFixed(2));
            $('#subtot').trigger('click');
          },
          error: function() {
            $("#"+rowId+"\\.sellPrice").val(0.00);
            $("#"+rowId+"\\.extPrice").val(0.00);
            $('#subtot').trigger('click');
          }
        });
      });

      //get accessories and load into options
      $('#fetch').on('click', function(e) {
        var products = "";
        $('input[id$=".sku"]').each(function() {
          if($(this).attr('id').indexOf("products") > -1 && $(this).val().length > 0)
            products += 'products=' + $(this).val() + "&";
        });

        $.ajax({
          url: '/web/product/get-acc-json.htm?' + products,
          type: 'GET',
          beforeSend: function() {
            $("#accessory-combobox").empty();
            $("#fetch").val("Loading...");
          },
          success: function(res) {
            res = jQuery.parseJSON(res);
            $.each(res, function( index, object ) {
              var opt = $("<option></option>").attr("value", object.product).text(object.description);
              $("#accessory-combobox").append(opt);
            });
            $("#fetch").val("Fetch Accessories");
          },
          error: function() {
            $("#fetch").val("Fetch Accessories");
          }
        });
      });

      $('#subtot').trigger('click');

    });
  </script>

  <form:form method="POST" commandName="system" action="system-save.htm">
    <input type="hidden" name="qid" value="${param.qid}"/>
    <form:input type="hidden" path="id" value="${param.sid}"/>

    <div class="section">
      <div class="half">
        <b>Mark For:</b><br/>
        <form:input type="text" path="systemName" placeholder="Enter Name" style="width:300px;" /><form:errors path="systemName" element="div" cssClass="fieldError" /><br/><br/>
        <b>Description:</b><br/>
        <form:input type="text" path="description" placeholder="Enter Description" style="width:300px;" /><br/><br/>
      </div>
      <div class="half">
        <b>Notes:</b><br/>
        <form:textarea path="notes" rows="10" cols="56" />
        <br/><br/>
      </div>
    </div>
    <br/><br/>

    <div id="qb-addline" class="left" style="vertical-align:bottom;"><b>Components:</b></div>
    <div class="fake-table">
      <div class="header">
        <div class="row">
          <div class="cell">Product</div>
          <div class="cell">Description</div>
          <div class="cell">Quantity</div>
          <div class="cell">Cost</div>
          <div class="cell">Auto Price</div>
          <div class="cell">Sell Price</div>
          <div class="cell">Ext. Price</div>
          <div class="cell">Margin %</div>
        </div>
      </div>
      <div class="body">
        <%--<c:forEach items="${system.products}" var="product" varStatus="loop">--%>
        <c:forEach begin="0" end="${fn:length(system.products)}" varStatus="loop">
        <div id="row-${loop.index}" class="qb-prodline row">
          <div id="component-${loop.index}" class="cell">
            <form:checkbox id="products${loop.index}.nonStock" path="products[${loop.index}].nonStock" style="display:none;"/>
            <form:input type="text" path="products[${loop.index}].sku" style="width:200px;" /><br/>
          </div>
          <div class="cell">
            <form:input type="text" path="products[${loop.index}].description" style="width:250px;" />
          </div>
          <div class="cell">
            <form:input type="number" path="products[${loop.index}].qty" min="0" style="width:50px;" />
          </div>
          <div class="cell">
            <form:input type="number"  path="products[${loop.index}].cost" step="any" style="width:80px;" readonly="true" />
          </div>
          <div class="cell">
            <form:input type="number" path="products[${loop.index}].basePrice" step="any" style="width:80px;" readonly="true" />
          </div>
          <div class="cell">
            <form:input type="number" path="products[${loop.index}].sellPrice" step="any" style="width:80px;" />
          </div>
          <div class="cell">
            <form:input type="number" path="products[${loop.index}].extPrice" step="any" style="width:80px;" readonly="true"/>
          </div>
          <div class="cell">
            <form:input type="number" path="products[${loop.index}].marginPct" step="any" style="width:80px;" />
          </div>
        </div>
        </c:forEach>
      </div>
    </div>
    <br/><br/>


    <div class="hidden">
      <select id="accessory-combobox">
        <c:forEach var="item" items="${accessories}">
          <option value="${item.product}">${item.description}</option>
        </c:forEach>
      </select>
    </div>

    <div id="qb-addline-acc" class="left" style="vertical-align:bottom;"><b>Accessories:</b></div>
    <div class="right">
      <%--<div id="acc-spinner" style="display:none;">Loading...</div>--%>
      <input id="fetch" type="button" name="fetch" value="Fetch Accessories" style="display:none;"/>
    </div>
    <div class="fake-table">
      <div class="header">
        <div class="row">
          <div class="cell">Product</div>
          <div class="cell">Description</div>
          <div class="cell">Quantity</div>
          <div class="cell">Cost</div>
          <div class="cell">Auto Price</div>
          <div class="cell">Sell Price</div>
          <div class="cell">Ext. Price</div>
          <div class="cell">Margin %</div>
        </div>
      </div>
      <div class="body">
        <c:forEach begin="0" end="${fn:length(system.accessories)}" varStatus="loop">
          <div id="row-${loop.index}" class="qb-accline row">
            <div id="accessory-${loop.index}" class="cell">
                <form:checkbox id="accessories${loop.index}.nonStock" path="accessories[${loop.index}].nonStock" style="display:none;"/>
                <form:input type="text" path="accessories[${loop.index}].sku" autocomplete="off" style="width:200px;" />
            </div>
            <div class="cell">
              <form:input type="text" path="accessories[${loop.index}].description" style="width:250px;" />
            </div>
            <div class="cell">
              <form:input type="number" path="accessories[${loop.index}].qty" min="0" style="width:50px;" />
            </div>
            <div class="cell">
              <form:input type="number" path="accessories[${loop.index}].cost" step="any" style="width:80px;" readonly="true" />
            </div>
            <div class="cell">
              <form:input type="number" path="accessories[${loop.index}].basePrice" step="any" style="width:80px;" readonly="true" />
            </div>
            <div class="cell">
              <form:input type="number" path="accessories[${loop.index}].sellPrice" step="any" style="width:80px;" />
            </div>
            <div class="cell">
              <form:input type="number" path="accessories[${loop.index}].extPrice" step="any" style="width:80px;" readonly="true"/>
            </div>
            <div class="cell">
              <form:input type="number" path="accessories[${loop.index}].marginPct" step="any" style="width:80px;" />
            </div>
          </div>
        </c:forEach>
      </div>
    </div>
    <br/><br/>

    <div class="right">
      Total: <input type="text" id="subtot" name="subtot" style="width:100px;"><br/><br/><br/><br/>
      <input type="submit" name="submit" value="Cancel"/>
      <input type="submit" name="submit" value="Delete"/>
      <input type="submit" name="submit" value="Save"/>
    </div>

  </form:form>

</jsp:body>
</t:layout>