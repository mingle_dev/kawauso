<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="/WEB-INF/kawauso-utils.tld" prefix="util"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<t:layout>
    <jsp:body>

    <script>
    $(document).ready(function () {
        $('#publish').click(function (e) {
            if($('#pricingLevel').val().length == 0) {
                noty({type: 'error', text: 'You must specify a pricing detail option.'});
                return;
            }
            window.location.href="publish.htm?qid=${quote.id}&level=" + $('#pricingLevel').val();
        });
    });
    </script>

    <style>
        .truncate {
            max-width: 200px;
            white-space: nowrap;
            overflow: hidden;
            text-overflow: ellipsis;
        }
    </style>

    <div class="section" style="width:100%;">
    <div class="half">
        <dl>
            <dt>Quote #:</dt><dd><a href="<c:url value="/quote/s1.htm?qid=${quote.id}"/>">${quote.quoteId}</a></dd>
            <dt>Quote Name:</dt><dd>${quote.quoteName}</dd>
            <dt>Account #:</dt><dd>${quote.customerNumber} - ${quote.shipTo}</dd>
            <dt>Create Date:</dt><dd>${quote.createDate}</dd>
            <dt>Expire Date:</dt><dd>${quote.expireDate}</dd>
            <dt>Status:</dt><dd>${quote.status}</dd>
        </dl>
    </div>

    <div class="half">
        <dl>
            <dt>Attention:</dt><dd>${quote.contactName}</dd>
            <dt>Email Address:</dt><dd>${quote.emailAddress}</dd>
            <dt>Ship To:</dt>
            <dd>
                ${quote.addrStreet}<br/>
                ${quote.addrCity} ${quote.addrState} ${quote.addrZip}
            </dd>
            <dt>Pricing Detail:</dt>
            <dd>
            <c:choose>
                <c:when test="${quote.status == 'DRAFT'}">
                    <select id="pricingLevel">
                        <option></option>
                        <option value="SKU">Sku Level</option>
                        <option value="SYSTEM">Mark For Level</option>
                        <option value="QUOTE">Quote Level</option>
                    </select>
                </c:when>
                <c:otherwise>
                    ${quote.priceDetail}
                </c:otherwise>
            </c:choose>

            </dd>
        </dl>
    </div>
    </div>
    <br/><br/>

    <c:if test="${quote.status == 'DRAFT'}">
    <div class="left">
        <a class="ui-link-button" href="<c:url value="/quote/s3_template.htm?qid=${quote.id}"/>">Quote from Template</a>
        <%--<a class="ui-link-button" href="<c:url value="/quote/s3_history.htm?qid=${quote.id}"/>">Quote from History</a>--%>
        <a class="ui-link-button" href="<c:url value="/quote/s3_blank.htm?qid=${quote.id}"/>">Quote from Scratch</a>
    </div>
    <br/><br/>
    </c:if>

    <c:if test="${fn:length(quote.shares) > 0}">
        <h3>Attached Accounts:</h3>
        <div class="fake-table">
            <div class="header">
                <div class="row">
                    <div class="cell">Account #</div>
                    <div class="cell">Account Name</div>
                    <div class="cell">Contact Name</div>
                    <div class="cell">Contact Email Address</div>
                </div>
            </div>
            <div class="body">
            <c:forEach items="${quote.shares}" var="share">
                <div class="row">
                    <div class="cell">${share.customerNumber}</div>
                    <div class="cell">${share.customerNumber}</div>
                    <div class="cell">${share.contactName}</div>
                    <div class="cell">${share.emailAddress}</div>
                </div>
            </c:forEach>
            </div>
        </div>
    </c:if>

    <br/><br/>
    <h3>Systems:</h3>
    <c:set var="total" value="${0}"/>
    <c:forEach items="${quote.systems}" var="system">
    <c:set var="systemTotal" value="${0}"/>

    <div class="left">
        <p style="font-size:125%; margin-bottom:0px;">
            ${system.systemName}<br/>
            ${system.description}
        </p>
    </div>
    <c:if test="${quote.status == 'DRAFT'}">
    <div class="right">
        <br/>
        <a class="ui-link-button" style="margin-bottom:2px;" href="<c:url value="/quote/s3_edit.htm?qid=${quote.id}&sid=${system.id}"/>">Edit</a>
    </div>
    </c:if>

    <div class="fake-table">
        <div class="header">
            <div class="row">
                <div class="cell">Product</div>
                <div class="cell txt-center">Quantity</div>
                <div class="cell txt-right">Sell Price</div>
                <div class="cell txt-right">Extend Price</div>
                <div class="cell txt-right">Margin %</div>
            </div>
        </div>
        <div class="body">
        <c:forEach items="${system.products}" var="product">
        <c:if test="${not empty product.sku}">
            <c:set var="systemTotal" value="${systemTotal + (product.qty * product.sellPrice)}"/>

            <div class="row">
            <div class="cell truncate">
                ${product.sku}<br/>
                ${product.description}
            </div>
            <div class="cell">${product.qty}</div>
            <div class="cell">${product.sellPrice}</div>
            <div class="cell">${product.extPrice}</div>
            <div class="cell">${product.marginPct}</div>
        </div>
        </c:if>
        </c:forEach>

        <c:forEach items="${system.accessories}" var="product">
            <c:set var="systemTotal" value="${systemTotal + (product.qty * product.sellPrice)}"/>
        <div class="row">
            <div class="cell truncate">
                ${product.sku}<br/>
                ${product.description}
            </div>
            <div class="cell">${product.qty}</div>
            <div class="cell">${product.sellPrice}</div>
            <div class="cell">${product.extPrice}</div>
            <div class="cell">${product.marginPct}</div>
        </div>
        </c:forEach>
        </div>
    </div>
    <div class="left"><c:if test="${not empty system.notes}"><b>Notes:</b> ${system.notes}</c:if></div>
    <div class="right">Total: <fmt:formatNumber value='${systemTotal}' type="currency" groupingUsed='true' /></div>
    <br/><br/>

        <c:set var="total" value="${total + systemTotal}"/>
    </c:forEach>

    <div class="right" style="font-weight:bold;">
        Grand Total: <fmt:formatNumber value='${total}' type="currency" groupingUsed='true' />
    </div><br/><br/>

    <c:if test="${fn:length(quote.systems) > 0 && quote.status eq 'DRAFT'}">
        <div class="right">
            <a class="ui-link-button" href="<c:url value="/quote/s3_edit_all.htm?qid=${quote.id}"/>">Edit All</a>
            <a class="ui-link-button" href="<c:url value="/quote/account-add.htm?qid=${param.qid}"/>">Add Account</a>
            <div id="publish" class="ui-link-button">Publish</div>
        </div>
    </c:if>

    <c:if test="${quote.status eq 'PUBLISHED'}">
        <div class="right">
            <a class="ui-link-button" href="<c:url value="/quote/account-add.htm?qid=${param.qid}"/>">Add Account</a>
            <a class="ui-link-button" href="<c:url value="/quote/clone.htm?qid=${param.qid}"/>">Clone</a>
        </div>
    </c:if>
    </jsp:body>
        
   
</t:layout>