<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="/WEB-INF/kawauso-utils.tld" prefix="util"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<t:layout>
  <jsp:body>
    <%--

        ${nav.ancestors}

    <c:if test="${not empty nav}">
    nav:<br/>
    <a href="<c:url value="/quote/s3_template.htm?qid=${param.qid}"/>">Home</a>
    <c:forEach items="${nav.ancestors}" var="item">
      > <a href="<c:url value="/quote/s3_template.htm?qid=${param.qid}&menuid=${util:base64Encode(item)}"/>">${item}</a>
    </c:forEach>
    > ${nav.id}
    <br/><hr/><br/>
    </c:if>


    menu-nav:<br/>
    <c:forEach items="${menu[0].ancestors}" var="item">
      > <a href="<c:url value="/quote/s3_template.htm?menuid=${util:base64Encode(item)}"/>">${item}</a>
    </c:forEach>
    <br/><hr/><br/>
    --%>

<div class="section">
  <div>
    <div id="product-categories">
      <c:forEach items="${menu}" var="item">
        <div class="fourth">
          <a href="<c:url value="/quote/s3_template.htm?qid=${param.qid}&menuid=${util:base64Encode(item.id)}"/>" title="${item.id}" class="product">
            <h3>
                <span class="img">
                    <img src="http://www.adpeso.de/wp-content/uploads/2013/08/camera-icon-256.png" width="75" height="75" alt="${item.id}">
                </span>
                ${item.id}
            </h3>
          </a>
        </div>
      </c:forEach>
    </div>
  </div>
</div>

<c:if test="${fn:length(templates) > 0}">

  <form method="GET" action="<c:url value="/quote/template-convert-mult.htm"/>">
  <input type="hidden" name="qid" value="${param.qid}"/>
  <c:forEach items="${templates}" var="item">
    <c:set var="sum" value="${100}"/>
    <div class="left"><p style="font-size:125%; margin-bottom:0px;">${item.systemName}</p></div>
    <div class="right">
      <a class="ui-link-button" style="margin-bottom:2px;" href="<c:url value="/quote/template-convert.htm?qid=${param.qid}&tid=${item.id}"/>">Add</a>
      <input name="tid" value="${item.id}" type="checkbox"/>
    </div>
    <div class="fake-table">
    <div class="header">
      <div class="row">
        <div class="cell">Product</div>
        <div class="cell">Description</div>
        <div class="cell">Warehouse<br/>Availability</div>
        <div class="cell">Company<br/>Availability</div>
        <div class="cell">Price</div>
      </div>
    </div>
    <div class="body">
      <c:forEach items="${item.prodComponents}" var="component">
        <c:set var="sum" value="${sum + component.basePrice}"/>
        <div class="row">
        <div class="cell">
          ${component.sku}
        </div>
        <div class="cell">${component.description}</div>
        <div class="cell">${component.qty}</div>
        <div class="cell">${component.netAvail}</div>
        <div class="cell"><fmt:formatNumber value='${component.basePrice}' type="currency" /></div>
      </div>
      </c:forEach>
      <c:forEach items="${item.prodAccessories}" var="accessory">
        <c:set var="sum" value="${sum + accessory.basePrice}"/>
        <div class="row">
          <div class="cell">
            ${accessory.sku}
          </div>
          <div class="cell">${accessory.description}</div>
          <div class="cell">${accessory.qty}</div>
          <div class="cell">${accessory.netAvail}</div>
          <div class="cell"><fmt:formatNumber value='${accessory.basePrice}' type="currency" /></div>
        </div>
      </c:forEach>
      <%--
      <div class="left"><b>${item.systemName}</b></div>
      <div class="right">[ <a href="<c:url value="/quote/template-convert.htm?qid=${param.qid}&tid=${item.id}"/>">Convert</a> ]</div>
      <br/>

      <c:forEach items="${item.components}" var="component">
        c: ${component}<br/>
      </c:forEach>
      <c:forEach items="${item.accessories}" var="accessory">
        a: ${accessory}<br/>
      </c:forEach>
    --%>

    </div>
    </div>
    <div class="right">Total: <fmt:formatNumber value='${sum}' type="currency" groupingUsed='true' /></div>
    <br/><br/>
  </c:forEach>


    <div class="right">
      <input type="submit" nam="submit" value="Add Checked"/>
    </div>
  </form>
</c:if>


  </jsp:body>
</t:layout>