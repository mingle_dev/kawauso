<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="/WEB-INF/kawauso-utils.tld" prefix="util"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<t:layout>
    <jsp:body>

        <%--
         <script type="text/javascript" src="http://jquery.iceburg.net/jqModal/jqModal.js"></script>

        <link rel="stylesheet" type="text/css" href="http://jquery.iceburg.net/jqModal/jqModal.css"/>
        --%>

        <%--
        <style>
            .jqmWindow {
                display: none;
                top: 10%;
                position: absolute;
                z-index: 10000;
                left: 8px;
                right: 8px;
                margin-left: auto;
                margin-right: auto;
                
                height: auto;
                background-color: #fff;
                border: 1px solid black;
                padding: 12px;
            } 
            
            .jqmOverlay {
                background-color:#000;
            }
        </style>
            
        
        <script>
            $().ready(function() {
                
               $('#clickme').on('click', function(e) {
                  
                    $.ajax({
                       url: '/web/product/avail.htm?prod=MjRBTkl2MThBMDAz',
                       beforeSend: function() {
                           $('#dialog').jqm({overlay: 60});
                           //$('.jqmWindow').html('asdf');
                           $('#dialog').jqmShow();
                       },
                       success: function(res) {
                           $('.jqmWindow').html(res);
                       }
                    });
                });
                
               /*
               $('#dialog').jqm({
                   onshow: function(hash) {
                       hash.o.prependTo('body');
                       hash.w.css('opacity', 0.88).fadeIn();
                   }
               });
               */
              
               /*
               $('#dialog').jqm({
                   overlay: 50,
                   ajax: '/web/product/avail.htm',
                   ajaxText: 'Loading',
                   modal: true
               });
               */
              
            });
        </script>
            
        <a href="#" class="jqModal" data-url="www.mingledorffs.com">View</a>
        
        <a href="#" id="clickme">click me</a>
        --%>
        
        <h1>QuoteBuilder</h1>
        
        [ <a href="<c:url value="/quote/s1.htm"/>">Create Quote</a> |
        <a href="<c:url value="/quote/admin-menu.htm"/>">Menu Admin</a> |
        <a href="<c:url value="/quote/s3_template.htm"/>">Menu Browser</a>
        ]
        <br/><br/><hr/><br/><br/>

        <div class="fake-table">
        <div class="header">
            <div class="row">
                <div class="cell">Quote</div>
                <div class="cell txt-center">Customer #</div>
                <div class="cell txt-center">Create Date</div>
                <div class="cell txt-center">Expire Date</div>
                <div class="cell txt-right">Status</div>
                <%-- <div class="cell txt-right">Revision</div> --%>
            </div>
        </div>
        <div class="body">
            <c:forEach items="${quotes}" var="quote">
            <div class="row">
                <div class="cell">
                    <a href="<c:url value="/quote/review.htm?qid=${quote.id}"/>">${quote.quoteName}</a>
                </div>
                <div class="cell">
                    ${quote.customerNumber}
                </div>
                <div class="cell">
                        ${quote.createDate}
                </div>
                <div class="cell">
                        ${quote.expireDate}
                </div>
                <div class="cell">
                    ${quote.status}
                </div>
            </div>
            </c:forEach>
        </div>
        </div>
        <br/><br/>

    </jsp:body>
        
   
</t:layout>