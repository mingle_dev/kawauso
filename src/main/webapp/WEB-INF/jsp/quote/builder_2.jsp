<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<t:layout>

  <jsp:body>

      Step 2:<br/>
      
        a. <a href="<c:url value="/quote/s3_template.htm?qid=${param.qid}"/>">Select from Template</a><br/>
        b. <a href="<c:url value="/quote/s3_history.htm?qid=${param.qid}"/>">Select from History</a><br/>
        c. <a href="<c:url value="/quote/s3_blank.htm?qid=${param.qid}"/>">Build from Scratch</a><br/>

    <br/><br/>${fn:length(quote.systems)}
    <br/>
    <c:if test="${fn:length(quote.systems) > 0}">
      <a href="<c:url value="/quote/review.htm?qid=${param.qid}"/>">Review Quote</a>
    </c:if>

  </jsp:body>
</t:layout>