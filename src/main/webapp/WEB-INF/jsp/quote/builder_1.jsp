<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<t:layout>

  <jsp:body>

    <script>
        (function( $ ) {
            $.widget( "custom.combobox", {
                options: {
                    source: 'asdf'
                },
                _create: function() {
                    var id = this.element.attr('id');
                    this.wrapper = $( "<span>" )
                            .addClass( "custom-combobox" )
                            .insertAfter( this.element );

                    //console.log(this.options.source);

                    //change select id to something else
                    //jQuery(this).prev("li")="newid" <-- untested

                    this.element.hide();
                    this._createAutocomplete(id);
                    this._createShowAllButton();
                },

                _createAutocomplete: function(id) {
                    var selected = this.element.children( ":selected" ),
                            value = selected.val() ? selected.text() : "";

                    this.input = $( "<input>" )
                            .appendTo( this.wrapper )
                            .val( value )
                            //.attr( "id", id+"-input" )
                            //.attr( "name", id+"-input" )
                            .attr( "id", "contactName" )
                            .attr( "name", "contactName" )
                            .attr( "type", "text" )
                            .width( "250px" )
                            .addClass( "custom-combobox-input ui-widget ui-widget-content ui-state-default ui-corner-left" )
                            .autocomplete({
                                delay: 0,
                                minLength: 0,
                                source: $.proxy( this, "_source" )
                            })
                            .tooltip({
                                tooltipClass: "ui-state-highlight"
                            });

                    this._on( this.input, {
                        autocompleteselect: function( event, ui ) {
                            ui.item.option.selected = true;
                            this._trigger( "select", event, {
                                item: ui.item.option
                            });

                            if(ui.item.option.value.length > 0)
                                $('#emailAddress').val(ui.item.option.value);

                            //console.log('click select ' + ui.item.option.value);
                        },

                        //autocompletechange: "_removeIfInvalid"
                        autocompletechange: "_flagIfInvalid"
                    });
                },

                _createShowAllButton: function() {
                    var input = this.input,
                            wasOpen = false;

                    $( "<a>" )
                            .attr( "tabIndex", -1 )
                            //.attr( "title", "Show All Items" )
                            //.tooltip()
                            .appendTo( this.wrapper )
                            .button({
                                icons: {
                                    primary: "ui-icon-triangle-1-s"
                                },
                                text: false
                            })
                            .removeClass( "ui-corner-all" )
                            .addClass( "custom-combobox-toggle ui-corner-right" )
                            .mousedown(function() {
                                wasOpen = input.autocomplete( "widget" ).is( ":visible" );
                            })
                            .click(function() {
                                input.focus();

                                //console.log('click down arrow');

                                // Close if already visible
                                if ( wasOpen ) {
                                    return;
                                }

                                // Pass empty string as value to search for, displaying all results
                                input.autocomplete( "search", "" );
                            });
                },

                _source: function( request, response ) {
                    console.log(this.element);

                    var matcher = new RegExp( $.ui.autocomplete.escapeRegex(request.term), "i" );
                    response( this.element.children( "option" ).map(function() {
                        var text = $( this ).text();
                        if ( this.value && ( !request.term || matcher.test(text) ) )
                            return {
                                label: text,
                                value: text,
                                option: this
                            };
                    }) );
                },

                _removeIfInvalid: function( event, ui ) {

                    // Selected an item, nothing to do
                    if ( ui.item ) {
                        return;
                    }

                    // Search for a match (case-insensitive)
                    var value = this.input.val(),
                            valueLowerCase = value.toLowerCase(),
                            valid = false;
                    this.element.children( "option" ).each(function() {
                        if ( $( this ).text().toLowerCase() === valueLowerCase ) {
                            this.selected = valid = true;
                            return false;
                        }
                    });

                    // Found a match, nothing to do
                    if ( valid ) {
                        return;
                    }

                    // Remove invalid value
                    this.input
                            .val( "" )
                            .attr( "title", value + " didn't match any item" )
                            .tooltip( "open" );
                    this.element.val( "" );
                    this._delay(function() {
                        this.input.tooltip( "close" ).attr( "title", "" );
                    }, 2500 );
                    this.input.autocomplete( "instance" ).term = "";
                },

                _flagIfInvalid: function( event, ui ) {
                    // Selected an item, nothing to do
                    if ( ui.item ) {
                        return;
                    }

                    // Search for a match (case-insensitive)
                    var value = this.input.val(),
                            valueLowerCase = value.toLowerCase(),
                            valid = false;
                    this.element.children( "option" ).each(function() {
                        if ( $( this ).text().toLowerCase() === valueLowerCase ) {
                            this.selected = valid = true;
                            return false;
                        }
                    });

                    // Found a match, nothing to do
                    if ( valid ) {

                    }
                },

                _destroy: function() {
                    this.wrapper.remove();
                    this.element.show();
                }
            });
        })( jQuery );

    $(function() {
        $( "#contactName-combobox" ).combobox({source: 'abc'});
        $( "#toggle" ).click(function() {
            console.log('toggle');
            $( "#contactName-combobox" ).toggle();
        });
    });

      $(document).ready(function () {

          $(document).keydown(function(e) {
              var elid = $(document.activeElement).is('input[type="text"]:focus, textarea:focus');
              if (e.keyCode === 8 && !elid) { return false; } //block backspace
              if (e.keyCode == 13 && elid) { return false; } //block enter
          });

/*
          $('#contactName').autocomplete({
              minLength: 3,
              delay: 100,
              source: function(request, response) {
                  $.getJSON("/web/account/get-contacts-json.htm", {
                      term: request.term,
                      custno: $('#customerNumber').val(),
                      limit: 15
                  }, function(data) {
                      //data is an array of objects and must be transformed for autocomplete to use
                      var array = data.error ? [] : $.map(data, function(d) {
                          return {
                              value: d.firstName + " " + d.lastName,
                              email: d.emailAddr
                          };
                      });
                      response(array);
                  });
              },
              select: function( event, ui ) {
                  $('#contactName').val(ui.item.id);
                  $('#emailAddress').val(ui.item.email);
              }
          });
*/
          $('#customerName').autocomplete({
              source: "/web/account/quicklist.htm",
              minLength: 3,
              delay: 100,
              focus: function( event, ui ) {
                  $('#customerNumber').val(ui.item.custno);
                  $('#customerName').val(ui.item.name);
                  return false;
              },
              select: function( event, ui ) {
                  $('#customerNumber').val(ui.item.custno);
                  $('#customerNumber').trigger('blur');

                  return false;
              }
          })
          .autocomplete( "instance" )._renderItem = function( ul, item ) {
              return $( "<li>" )
                      .append( item.name + '<br>' + item.city + ', ' + item.state )
                      .appendTo( ul );
          };

          $('#customerNumber').on('keyup', function(e) {
             if($('#customerNumber').val().length == 0) {
                 $('#customerName').val('');
                 $('#shipTo').hide();
                 $('#shipTo')
                         .find('option')
                         .remove()
                         .end()
                         .append('<option></option>')
                         .val('');
             }
          });

          $('#customerNumber').on('change', function(e) {
              if($('#customerNumber').val().length == 0)
                  return;

              $.ajax({
                  url: '/web/account/quicklist.htm',
                  data: {'term' : $(this).val() },
                  type: 'GET',
                  beforeSend: function() {
                      $('#customerName').val('');
                  },
                  success: function(res) {
                      var obj = jQuery.parseJSON(res);
                      if(obj != null && obj.length == 1)
                          $('#customerName').val(obj[0].value);
                  },
                  error: function() {
                      noty( { type: 'error', text: 'Error' } );
                  }
              });
          });

          $('#customerNumber').on('blur', function(e) {
              if($('#customerNumber').val().length == 0)
                  return;

              $.ajax({
                  url: '/web/account/getall-shipto.htm',
                  data: {'custno' : $(this).val() },
                  type: 'POST',
                  beforeSend: function() {
                      $('#shipTo').hide();
                      $('#shipto-spinner').show();

                  },
                  success: function(res) {
                      var obj = jQuery.parseJSON(res);

                      if(obj != null) {
                          $.each(obj, function( index, value ) {
                              $('#shipTo').append($('<option/>', {
                                  value: value.id,
                                  text : value.name
                              }));
                          });
                          $('#shipto-spinner').hide();
                          $('#shipTo').show();
                      }else{
                          $('#shipto-spinner').hide();
                      }
                  },
                  error: function() {
                      $('#shipto-spinner').hide();
                      $('#shipTo').show();
                      noty( { type: 'error', text: 'Error' } );
                  }
              });

              //fetch contacts tied to account #
              $.ajax({
                  url: '/web/account/get-contacts-json.htm',
                  data: {'custno' : $(this).val() },
                  type: 'GET',
                  beforeSend: function() {
                      $('#contactName-combobox')
                              .find('option')
                              .remove()
                              .end()
                              .append('<option></option>')
                              .val('');
                  },
                  success: function(res) {
                      var obj = jQuery.parseJSON(res);
                      $.each(obj, function( index, value ) {
                          $('#contactName-combobox').append($('<option/>', {
                              value: value.emailAddr,
                              text : value.firstName + ' ' + value.lastName
                          }));
                      });
                  },
                  error: function() {
                      noty( { type: 'error', text: 'Error' } );
                  }
              });
          });
      });
    </script>

    <style>

        .ui-state-focus {
            background: #e6e6e6 url("http://code.jquery.com/ui/1.11.2/themes/smoothness/images/ui-bg_glass_75_e6e6e6_1x400.png") 50% 50% repeat-x;
        }

        .ui-autocomplete-loading {
            background: white url('https://s3.amazonaws.com/mgl1939/content/images/ui-anim_basic_16x16.gif') right center no-repeat;
        }
        .ui-helper-hidden-accessible {
            display: none !important;
        }

        .ui-menu {
            list-style: none;
            padding: 2px;
            margin: 0;
            display: block;
            float: left;
        }

        .ui-autocomplete {
            position: absolute;
            cursor: default;
            max-height: 300px;
            overflow-y: auto;
            overflow-x: hidden;
        }

        /* IE6 doesn't support max-height
        html .ui-autocomplete {
            height: 300px;
        }
        */

        .ui-menu .ui-menu-item {
            cursor: pointer;
            margin: 0px;
            padding: 3px 0px 3px 0px;
            zoom: 1;
            float: left;
            clear: left;
            width: 100%;
            list-style-image: url("data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7");
        }

        .ui-widget-content {
            border: 1px solid #aaaaaa;
            background: #ffffff;
            color: #222222;
        }

        .ui-state-hover {
            background: #cccccc;
        }

        /*---*/

        .custom-combobox-toggle {
            position: absolute;
            top: 0;
            bottom: 0;
            margin-left: -1px;
            padding: 0;
        }
        .ui-corner-all, .ui-corner-bottom, .ui-corner-right, .ui-corner-br {
            border-bottom-right-radius: 4px;
        }
        .ui-corner-all, .ui-corner-top, .ui-corner-right, .ui-corner-tr {
            border-top-right-radius: 4px;
        }
        .ui-button-icon-only {
            width: 2.2em;
        }
        /*
        .ui-widget .ui-widget {
            font-size: 1em;
        }
        .ui-widget input, .ui-widget select, .ui-widget textarea, .ui-widget button {
            font-family: Verdana,Arial,sans-serif;
            font-size: 1em;
        }
        */
        .custom-combobox-input {
            margin: 0;
            padding: 5px 10px;
        }
        .ui-corner-all, .ui-corner-bottom, .ui-corner-left, .ui-corner-bl {
            border-bottom-left-radius: 4px;
        }
        .ui-corner-all, .ui-corner-top, .ui-corner-left, .ui-corner-tl {
            border-top-left-radius: 4px;
        }
        .ui-state-default, .ui-widget-content .ui-state-default, .ui-widget-header .ui-state-default {
            border: 1px solid #d3d3d3;
            background: #e6e6e6 url("http://code.jquery.com/ui/1.11.2/themes/smoothness/images/ui-bg_glass_75_e6e6e6_1x400.png") 50% 50% repeat-x;
            font-weight: normal;
            color: #555555;
        }
        /*
        .ui-widget-content {
            border: 1px solid #aaaaaa;
            background: #ffffff url("http://code.jquery.com/ui/1.11.2/themes/smoothness/images/ui-bg_flat_75_ffffff_40x100.png") 50% 50% repeat-x;
            color: #222222;
        }
        */

        .ui-state-default .ui-icon {
            background-image: url("http://code.jquery.com/ui/1.11.2/themes/smoothness/images/ui-icons_888888_256x240.png");
        }
        .ui-button-icon-only .ui-icon {
            left: 50%;
            margin-left: -8px;
        }
        .ui-button-icon-only .ui-icon, .ui-button-text-icon-primary .ui-icon, .ui-button-text-icon-secondary .ui-icon, .ui-button-text-icons .ui-icon, .ui-button-icons-only .ui-icon {
            position: absolute;
            top: 50%;
            margin-top: -8px;
        }
        .ui-icon-triangle-1-s {
            background-position: -64px -16px;
        }
        .ui-icon, .ui-widget-content .ui-icon {
            background-image: url("http://code.jquery.com/ui/1.11.2/themes/smoothness/images/ui-icons_222222_256x240.png");
        }
        .ui-icon {
            width: 16px;
            height: 16px;
        }
        .ui-icon {
            display: block;
            text-indent: -99999px;
            overflow: hidden;
            background-repeat: no-repeat;
        }

        .ui-button {
            display: inline-block;
            position: relative;
            padding: 0;
            line-height: normal;
            margin-right: .1em;
            margin-bottom: 4px;
            cursor: pointer;
            vertical-align: middle;
            text-align: center;
            overflow: visible;
        }

    </style>

    <h1>New Quote</h1>



    <form:form method="POST" commandName="quote"  action="s1.htm">
      <form:input type="hidden" path="id" />
      <form:input type="hidden" path="nextStep" value="2" />

        <%--
        /* Begin Testing idea - no logic yet*/<br/>
        <div class="ui-widget">
            <select id="contactName-combobox">
            </select>
        </div>
        /* End Testing idea */<br/><br/>
        --%>

        <div class="section pad10">
            <div class="half">
                <dl>
                    <dt>
                        <label class="label-top">Quote #:</label>
                    </dt>
                    <dd>
                        <form:input type="text" path="quoteId" placeholder="Auto Generate" style="width:125px;" /><br/><br/>
                    </dd>
                    <dt>
                        <label class="label-top">Quote Name:</label>
                    </dt>
                    <dd>
                        <form:input type="text" path="quoteName" style="width:300px;" /><form:errors path="quoteName" element="div" cssClass="fieldError" /><br/><br/>
                    </dd>
                    <dt>
                        <label class="label-top">Account #:</label>
                    </dt>
                    <dd>
                        <form:input type="text" path="customerNumber" placeholder="Account #" style="width:125px;" /><form:errors path="customerNumber" element="div" cssClass="fieldError" />
                        <span id="shipto-spinner" style="display:none;">Searching...</span>
                        <form:select path="shipTo" style="display:none;">
                            <option></option>
                        </form:select>
                        <br/><br/>
                    </dd>
                    <dt>
                        <label class="label-top">Account Name:</label>
                    </dt>
                    <dd>
                        <form:input type="text" path="customerName" style="width:300px;" /><form:errors path="customerName" element="div" cssClass="fieldError" />
                        <br/><br/>
                    </dd>
                    <dt>
                        <label class="label-top">Expire Date:</label>
                    </dt>
                    <dd>
                        <form:input type="date" path="expireDate" placeholder="Expire Date" style="width:150px;" /><br/><br/>
                    </dd>
                    <dt>
                        <label class="label-top">Notes (Internal):</label>
                    </dt>
                    <dd>
                        <form:textarea path="noteInternal" rows="10" cols="80"/><br/><br/>
                    </dd>
                    <dt>
                        <label class="label-top">Notes (External):</label>
                    </dt>
                    <dd>
                        <form:textarea path="noteExternal" rows="10" cols="80"/><br/><br/>
                    </dd>
                </dl>
            </div>
            <div class="half">
                <dl>
                    <dt>
                        <label class="label-top" style="width:100px;">Contact Name:</label>
                    </dt>
                    <dd>
                        <div class="ui-widget">
                            <select id="contactName-combobox">
                            </select>
                        </div>
                        <%--<form:input type="text" path="contactName" placeholder="Contact Name" style="width:200px;" />--%>
                        <form:errors path="contactName" element="div" cssClass="fieldError" />
                        <br/>
                    </dd>
                    <dt>
                        <label class="label-top" style="width:125px;">Email Address:</label>
                    </dt>
                    <dd>
                        <form:input type="email" path="emailAddress" placeholder="Email Address" style="width:300px;" /><form:errors path="emailAddress" element="div" cssClass="fieldError" /><br/><br/>
                    </dd>
                    <dt>
                        <label class="label-top" style="width:125px;">Ship To Address:</label>
                    </dt>
                    <dd>
                        <form:input type="text" id="quote_title" path="addrStreet" placeholder="Street Address" style="width:300px;" /><br/><br/>
                        <form:input type="text" id="quote_title" path="addrCity" placeholder="City" style="width:150px;" />,
                        <form:input type="text" id="quote_title" path="addrState" placeholder="State" style="width:60px;" />
                        <form:input type="text" id="quote_title" path="addrZip" placeholder="Zip" style="width:80px;" /><br/><br/>
                    </dd>
                    <dt>
                        <label class="label-top">Visibility:</label>
                    </dt>
                    <dd>
                        <form:select path="visibility">
                            <option>Public</option>
                            <option>Private</option>
                        </form:select><br/><br/>
                    </dd>
                </dl>
            </div>
        </div>
        <br/><br/>

      <div class="right">
            <input type="submit" name="submit" value="Next" />
      </div>

    </form:form>

  </jsp:body>
</t:layout>