<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="/WEB-INF/kawauso-utils.tld" prefix="util"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<t:layout>

  <jsp:body>
    <style>
      .ui-state-focus {
        background: #e6e6e6 url("http://code.jquery.com/ui/1.11.2/themes/smoothness/images/ui-bg_glass_75_e6e6e6_1x400.png") 50% 50% repeat-x;
      }
      .ui-autocomplete-loading {
        background: white url('https://s3.amazonaws.com/mgl1939/content/images/ui-anim_basic_16x16.gif') right center no-repeat;
      }
      .ui-helper-hidden-accessible {
        display: none !important;
      }
      .ui-menu {
        list-style: none;
        padding: 2px;
        margin: 0;
        display: block;
        float: left;
      }

      .ui-autocomplete {
        position: absolute;
        cursor: default;
        max-height: 300px;
        overflow-y: auto;
        overflow-x: hidden;
      }

      .ui-menu .ui-menu-item {
        cursor: pointer;
        margin: 0px;
        padding: 3px 0px 3px 0px;
        zoom: 1;
        float: left;
        clear: left;
        width: 100%;
        list-style-image: url("data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7");
      }

      .ui-widget-content {
        border: 1px solid #aaaaaa;
        background: #ffffff;
        color: #222222;
      }

      .ui-state-hover {
        background: #cccccc;
      }
      .custom-combobox-toggle {
        position: absolute;
        top: 0;
        bottom: 0;
        margin-left: -1px;
        padding: 0;
      }
      .ui-corner-all, .ui-corner-bottom, .ui-corner-right, .ui-corner-br {
        border-bottom-right-radius: 4px;
      }
      .ui-corner-all, .ui-corner-top, .ui-corner-right, .ui-corner-tr {
        border-top-right-radius: 4px;
      }
      .ui-button-icon-only {
        width: 2.2em;
      }
      .custom-combobox-input {
        margin: 0;
        padding: 5px 10px;
      }
      .ui-corner-all, .ui-corner-bottom, .ui-corner-left, .ui-corner-bl {
        border-bottom-left-radius: 4px;
      }
      .ui-corner-all, .ui-corner-top, .ui-corner-left, .ui-corner-tl {
        border-top-left-radius: 4px;
      }
      .ui-state-default, .ui-widget-content .ui-state-default, .ui-widget-header .ui-state-default {
        border: 1px solid #d3d3d3;
        background: #e6e6e6 url("http://code.jquery.com/ui/1.11.2/themes/smoothness/images/ui-bg_glass_75_e6e6e6_1x400.png") 50% 50% repeat-x;
        font-weight: normal;
        color: #555555;
      }
      .ui-state-default .ui-icon {
        background-image: url("http://code.jquery.com/ui/1.11.2/themes/smoothness/images/ui-icons_888888_256x240.png");
      }
      .ui-button-icon-only .ui-icon {
        left: 50%;
        margin-left: -8px;
      }
      .ui-button-icon-only .ui-icon, .ui-button-text-icon-primary .ui-icon, .ui-button-text-icon-secondary .ui-icon, .ui-button-text-icons .ui-icon, .ui-button-icons-only .ui-icon {
        position: absolute;
        top: 50%;
        margin-top: -8px;
      }
      .ui-icon-triangle-1-s {
        background-position: -64px -16px;
      }
      .ui-icon, .ui-widget-content .ui-icon {
        background-image: url("http://code.jquery.com/ui/1.11.2/themes/smoothness/images/ui-icons_222222_256x240.png");
      }
      .ui-icon {
        width: 16px;
        height: 16px;
      }
      .ui-icon {
        display: block;
        text-indent: -99999px;
        overflow: hidden;
        background-repeat: no-repeat;
      }

      .ui-button {
        display: inline-block;
        position: relative;
        padding: 0;
        line-height: normal;
        margin-right: .1em;
        margin-bottom: 4px;
        cursor: pointer;
        vertical-align: middle;
        text-align: center;
        overflow: visible;
      }
    </style>

    <script>
      (function( $ ) {
        $.widget( "custom.combobox", {
          options: {
          },
          _create: function () {
            var id = this.element.attr('id');
            var element = this.element;

            this.wrapper = this.element.wrap('<div class="ui-widget"><span class="custom-combobox"></span></div>');
            this._createAutocomplete(element);
            this._createShowAllButton();
          },

          _createAutocomplete: function(element) {
            this.input = element.autocomplete({
              source: "/web/product/quicklist.htm",
              minLength: 3,
              focus: function( event, ui ) {
                $(this).val(ui.item.product);
                return false;
              },
              select: function( event, ui ) {
                $(this).val(ui.item.product);
                return false;
              }
            }).click(function () {
              $(this).val('');
              $(this).autocomplete( "option", "minLength", 3 );
              $(this).autocomplete( "option", "source", "/web/product/quicklist.htm" );
            });

            this.input.autocomplete( "instance" )._renderItem = function( ul, item ) {
              return $( "<li>" )
                      .append( item.product + '<br>' + item.description )
                      .appendTo( ul );
            };
          },

          _createShowAllButton: function() {
            var input = this.input;

            $("<a>")
                    .attr( "tabIndex", -1 )
                    .appendTo( this.wrapper.parent() )
                    .button({
                      icons: {
                        primary: "ui-icon-triangle-1-s"
                      },
                      text: false
                    })
                    .removeClass( "ui-corner-all" )
                    .addClass( "custom-combobox-toggle ui-corner-right" )
                    .click(function() {
                      var source = [];
                      $('#accessory-combobox option').each(function(){
                        source.push({"product": this.value, "description": this.text });
                      });

                      input.val('').focus();
                      input.autocomplete( "option", "minLength", 0 );
                      input.autocomplete( "option", "source", source );
                      input.autocomplete( "search", "" );
                    });
          }
        });
      })( jQuery );

    $(document).ready(function () {
      //add custom combo-box to accessories
      //@todo; will most likely need to move to inside system iterator to uniquly identify
      //combobox per system accessories
      //OR can we detect each system and pull combobox id from it?
      $('input[id$=".sku"]').each(function() {

        console.log( $(this) );

        if ( $(this).attr('id').indexOf('accessories') !== -1 ) {
          $(this).combobox({source: 'accessory-combobox'});
        }
      });

      $("form").delegate('input[id$=".sku"]', "blur", function(e) {
        console.log('input[sku]->blur');

        //$('#qb-addline').trigger('click');
        addLine( $(this) );
      });

      //add line to products or accessories if none empty
      function addLine(element)
      {
        var numBlank = 0;
        var rowCnt = 0;

        var id = element.attr('id');
        var system = id.substring(0, id.indexOf('.'));

        if(id.indexOf('accessories') > -1) {
          $('input[id^="' + system + '.accessories"][id$=".sku"]').each(function () {
            rowCnt++;
            if ($(this).val().length == 0)
              numBlank++;
          });

          if (numBlank > 0)
            return;

          //grab prodline so that we don't get the combobox stuff
          var data = $("#" + system + " .qb-prodline:last").clone(true);

          data.attr('id', system+'-accessories-row' + rowCnt);

          //change id of all form fields
          data.html(function(i, oldHTML) {
            oldHTML = oldHTML.replace(new RegExp('products[0-9]+', 'g'), 'accessories'+rowCnt);
            return oldHTML.replace(new RegExp('products\[[0-9]+\]', 'g'), 'accessories['+rowCnt+']');
          });

          data.insertAfter("#" + system + " .qb-accline:last");

          //var ele = document.getElementById("systems0.accessories1.sku");
          //ele.combobox({source: 'accessory-combobox'});
          //$('#systems0.accessories1.sku').combobox({source: 'accessory-combobox'});

        }else {
          $('input[id^="' + system + '.products"][id$=".sku"]').each(function () {
            rowCnt++;
            if ($(this).val().length == 0)
              numBlank++;
          });

          if (numBlank > 0)
            return;

          var data = $("#" + system + " .qb-prodline:last").clone(true);

          //change id of div
          data.attr('id', system+'-products-row' + rowCnt);

          //change id of all form fields
          data.html(function(i, oldHTML) {
            return oldHTML.replace(new RegExp('products[0-9]+', 'g'), 'products'+rowCnt);
          });

          //change name of all form fileds
          data.html(function(i, oldHTML) {
            return oldHTML.replace(new RegExp('products\\['+(rowCnt-1)+'\\]', 'g'), 'products['+rowCnt+']');
          });

          data.insertAfter("#" + system + " .qb-prodline:last");
        }
      }

      //register current and future input fields for autocomplete
      $("form").delegate('input[id*="products"][id$=".sku"]', "keyup", function(e) {
        $(this).autocomplete({
          source: "/web/product/quicklist.htm",
          minLength: 3,
          delay: 100,
          focus: function( event, ui ) {
            $(this).val(ui.item.product);
            return false;
          },
          select: function( event, ui ) {
            $(this).val(ui.item.product);
            console.log(ui.item.product);
            return false;
          }
        })
        .autocomplete( "instance" )._renderItem = function( ul, item ) {
          return $( "<li>" )
                  .append( item.product + '<br>' + item.description )
                  .appendTo( ul );
        };
      });

      <%--$("form").delegate('input[id$=".sku"]', "blur", function(e) {--%>
        <%--e.preventDefault();--%>

        <%--var id = $(this).attr('id');--%>
        <%--var rowId = id.replace(".sku", "").replace('\.', '\\.');--%>

        <%--if($(this).val().length == 0) {--%>
          <%--$("form").find('input[id^="'+rowId+'"]').val('');--%>
          <%--return;--%>
        <%--}--%>

        <%--var qty = 0;--%>

        <%--$.ajax({--%>
          <%--url: '/web/product/get-prod-json.htm',--%>
          <%--data: {--%>
            <%--'custno' : ${quote.customerNumber},--%>
            <%--'shipTo': '${quote.shipTo}',--%>
            <%--'prod' : $(this).val()--%>
          <%--},--%>
          <%--type: 'GET',--%>
          <%--beforeSend: function() {--%>
            <%--qty = $("#"+rowId+"\\.qty").val();--%>

            <%--qbAddLine(id);--%>

            <%--$("#"+rowId+"\\.description").attr("placeholder", "Fetching Details...");--%>
            <%--$("#"+rowId+"\\.description").val("");--%>
            <%--$("#"+rowId+"\\.qty").val("");--%>
            <%--$("#"+rowId+"\\.cost").val("");--%>
            <%--$("#"+rowId+"\\.basePrice").val("");--%>
            <%--$("#"+rowId+"\\.sellPrice").val("");--%>
            <%--$("#"+rowId+"\\.extPrice").val("");--%>
            <%--$("#"+rowId+"\\.marginPct").val("");--%>
          <%--},--%>
          <%--success: function(res) {--%>
            <%--var obj = jQuery.parseJSON(res);--%>
            <%--$("#"+rowId+"\\.description").attr("placeholder", "");--%>
            <%--$("#"+rowId+"\\.description").val(obj.description);--%>
            <%--if(qty.length == 0)--%>
              <%--$("#"+rowId+"\\.qty").val("1");--%>
            <%--else--%>
              <%--$("#"+rowId+"\\.qty").val(qty);--%>

            <%--$("#"+rowId+"\\.cost").val(obj.pdRecord.standardCost.toFixed(2));--%>
            <%--$("#"+rowId+"\\.basePrice").val(obj.price.toFixed(2));--%>
            <%--$("#"+rowId+"\\.sellPrice").val(obj.price.toFixed(2));--%>

            <%--if(obj.pdRecord.marginPct.toFixed(2) < 1)--%>
              <%--$("#"+rowId+"\\.marginPct").val( (obj.pdRecord.marginPct * 100).toFixed(2) );--%>
            <%--else--%>
              <%--$("#"+rowId+"\\.marginPct").val(obj.pdRecord.marginPct.toFixed(2));--%>

            <%--$("#"+rowId+"\\.extPrice").val(obj.price.toFixed(2));--%>
          <%--},--%>
          <%--error: function() {--%>
            <%--$("#"+rowId+"\\.description").attr("placeholder", "Item not found. Assumed Non-Stock.");--%>
            <%--$("#"+rowId+"\\.cost").attr("readonly", false);--%>
            <%--$("#"+rowId+"\\.marginPct").attr("readonly", true);--%>
          <%--}--%>
        <%--});--%>
      <%--});--%>

      <%--//register current and future intput fields--%>
      <%--$("form").delegate('input[id$=".qty"]', "blur", function(e) {--%>
        <%--e.preventDefault();--%>

        <%--var id = $(this).attr('id');--%>
        <%--var rowId = id.replace(".qty", "").replace('\.', '\\.');--%>
        <%--var ext = $("#"+rowId+"\\.qty").val() * $("#"+rowId+"\\.sellPrice").val();--%>

        <%--$("#"+rowId+"\\.extPrice").val(ext.toFixed(2));--%>

        <%--//$('#subtot').trigger('click');--%>
      <%--});--%>

      <%--//register current and future input fields--%>
      <%--$("form").delegate('input[id$=".sellPrice"]', "blur", function(e) {--%>
        <%--e.preventDefault();--%>

        <%--var id = $(this).attr('id');--%>
        <%--var rowId = id.replace(".sellPrice", "").replace('\.', '\\.');--%>

        <%--//recalcuate gm %--%>
        <%--$.ajax({--%>
          <%--url: '/web/product/calc-claim.htm',--%>
          <%--data: {--%>
            <%--'customerNumber' : ${quote.customerNumber},--%>
            <%--'shipTo' : '${quote.shipTo}',--%>
            <%--'productCode' : $("#"+rowId+"\\.sku").val(),--%>
            <%--'target' : $("#"+rowId+"\\.sellPrice").val(),--%>
            <%--'targetType' : 0--%>
          <%--},--%>
          <%--type: 'POST',--%>
          <%--beforeSend: function() {--%>
            <%--$("#"+rowId+"\\.marginPct").val('---');--%>
          <%--},--%>
          <%--success: function(res) {--%>
            <%--var obj = jQuery.parseJSON(res);--%>
            <%--if(obj.marginPct.toFixed(2) < 1)--%>
              <%--$("#"+rowId+"\\.marginPct").val( (obj.marginPct * 100).toFixed(2) );--%>
            <%--else--%>
              <%--$("#"+rowId+"\\.marginPct").val( obj.marginPct.toFixed(2) );--%>
          <%--},--%>
          <%--error: function() {--%>
            <%--$("#"+rowId+"\\.marginPct").val(0.00);--%>
          <%--}--%>
        <%--});--%>

        <%--var ext = $("#"+rowId+"\\.qty").val() * $("#"+rowId+"\\.sellPrice").val();--%>
        <%--$("#"+rowId+"\\.extPrice").val(ext.toFixed(2));--%>
        <%--$('#subtot').trigger('click');--%>
      <%--});--%>

      <%--//register current and future input fields--%>
      <%--$("form").delegate('input[id$=".marginPct"]', "blur", function(e) {--%>
        <%--e.preventDefault();--%>

        <%--var id = $(this).attr('id');--%>
        <%--var rowId = id.replace(".marginPct", "").replace('\.', '\\.');--%>

        <%--//recalcuate sale price--%>
        <%--$.ajax({--%>
          <%--url: '/web/product/calc-claim.htm',--%>
          <%--data: {--%>
            <%--'customerNumber' : ${quote.customerNumber},--%>
            <%--'shipTo' : '${quote.shipTo}',--%>
            <%--'productCode' : $("#"+rowId+"\\.sku").val(),--%>
            <%--'target' : $("#"+rowId+"\\.marginPct").val(),--%>
            <%--'targetType' : 1--%>
          <%--},--%>
          <%--type: 'POST',--%>
          <%--beforeSend: function() {--%>
            <%--$("#"+rowId+"\\.sellPrice").val('---');--%>
          <%--},--%>
          <%--success: function(res) {--%>
            <%--var obj = jQuery.parseJSON(res);--%>
            <%--$("#"+rowId+"\\.sellPrice").val(obj.salePrice.toFixed(2));--%>

            <%--var ext = $("#"+rowId+"\\.qty").val() * $("#"+rowId+"\\.sellPrice").val();--%>
            <%--$("#"+rowId+"\\.extPrice").val(ext.toFixed(2));--%>
            <%--$('#subtot').trigger('click');--%>
          <%--},--%>
          <%--error: function() {--%>
            <%--$("#"+rowId+"\\.sellPrice").val(0.00);--%>
            <%--$("#"+rowId+"\\.extPrice").val(0.00);--%>
            <%--$('#subtot').trigger('click');--%>
          <%--}--%>
        <%--});--%>
      <%--});--%>

    });
    </script>

    <form:form method="POST" commandName="quote" action="system-save-all.htm">
      <form:input type="hidden" path="id"/>

      <div class="section" style="width:100%;">
        <div class="half">
          <dl>
            <dt>Quote #:</dt><dd><a href="<c:url value="/quote/s1.htm?qid=${quote.id}"/>">${quote.quoteId}</a></dd>
            <dt>Quote Name:</dt><dd>${quote.quoteName}</dd>
            <dt>Account #:</dt><dd>${quote.customerNumber} - ${quote.shipTo}</dd>
            <dt>Create Date:</dt><dd>${quote.createDate}</dd>
            <dt>Expire Date:</dt><dd>${quote.expireDate}</dd>
            <dt>Status:</dt><dd>${quote.status}</dd>
          </dl>
        </div>

        <div class="half">
          <dl>
            <dt>Attention:</dt><dd>${quote.contactName}</dd>
            <dt>Email Address:</dt><dd>${quote.emailAddress}</dd>
            <dt>Ship To:</dt>
            <dd>
                ${quote.addrStreet}<br/>
                ${quote.addrCity}, ${quote.addrState} ${quote.addrZip}
            </dd>
            <dt>Pricing Detail:</dt>
            <dd>
              <select name="pricingDetail">
                <option></option>
                <option value="SKU">Sku Level</option>
                <option value="SYSTEM">Mark For Level</option>
                <option value="QUOTE">Quote Level</option>
              </select>
            </dd>
          </dl>
        </div>
      </div><br/><br/>

      <c:set var="total" value="${0}"/>

      <%-- system loop --%>
      <c:forEach items="${quote.systems}" var="system" varStatus="sysLoop">
        <div id="systems${sysLoop.index}">
          <div>
              Mark For:<br/>
              <form:input type="text" path="systems[${sysLoop.index}].systemName" placeholder="System Name" style="width:300px;" /><br/>
              Description:<br/>
              <form:input type="text" path="systems[${sysLoop.index}].description" placeholder="System Description" style="width:300px;" />
          </div>
          <br/>

          <%--
          Components
          --%>
          <%--<p style="font-size:125%;">Components:</p>--%>
          <div id="qb-addline" class="left" style="vertical-align:bottom;"><b>Components:</b></div>
          <div class="fake-table">
            <div class="header">
              <div class="row">
                <div class="cell">Product</div>
                <div class="cell">Description</div>
                <div class="cell">Quantity</div>
                <div class="cell">Cost</div>
                <div class="cell">Auto Price</div>
                <div class="cell">Sell Price</div>
                <div class="cell">Ext. Price</div>
                <div class="cell">Margin %</div>
              </div>
            </div>
            <div class="body">
              <c:forEach begin="0" end="${fn:length(system.products)}" varStatus="prodLoop">
                  <c:set var="systemTotal" value="${systemTotal + (product.qty * product.sellPrice)}"/>

                  <div id="systems${sysLoop.index}-products-row${prodLoop.index}" class="qb-prodline row">
                    <div class="cell">
                      <form:input type="text" path="systems[${sysLoop.index}].products[${prodLoop.index}].sku" style="width:200px;" />
                    </div>
                    <div class="cell">
                      <form:input type="text" path="systems[${sysLoop.index}].products[${prodLoop.index}].description" style="width:250px;" />
                    </div>
                    <div class="cell">
                      <form:input type="number" path="systems[${sysLoop.index}].products[${prodLoop.index}].qty" min="0" style="width:50px;" />
                    </div>
                    <div class="cell">
                      <form:input type="number"  path="systems[${sysLoop.index}].products[${prodLoop.index}].cost" step="any" style="width:80px;" readonly="true" />
                    </div>
                    <div class="cell">
                      <form:input type="number" path="systems[${sysLoop.index}].products[${prodLoop.index}].basePrice" step="any" style="width:80px;" readonly="true" />
                    </div>
                    <div class="cell">
                      <form:input type="number" path="systems[${sysLoop.index}].products[${prodLoop.index}].sellPrice" step="any" style="width:80px;" />
                    </div>
                    <div class="cell">
                      <form:input type="number" path="systems[${sysLoop.index}].products[${prodLoop.index}].extPrice" step="any" style="width:80px;" readonly="true"/>
                    </div>
                    <div class="cell">
                      <form:input type="number" path="systems[${sysLoop.index}].products[${prodLoop.index}].marginPct" step="any" style="width:80px;" />
                    </div>
                  </div>
              </c:forEach>
            </div>
          </div>
          <br/>

          <%--
          Accessories
          --%>
          <p style="font-size:125%;">Accessories:</p>
          <div class="fake-table">
            <div class="body">
              <c:forEach begin="0" end="${fn:length(system.accessories)}" varStatus="prodLoop">
                <div id="systems${sysLoop.index}-accessories-row${prodLoop.index}" class="qb-accline row">
                <div class="cell">
                  <form:input type="text" path="systems[${sysLoop.index}].accessories[${prodLoop.index}].sku" style="width:200px;" />
                </div>
                <div class="cell">
                  <form:input type="text" path="systems[${sysLoop.index}].accessories[${prodLoop.index}].description" style="width:250px;" />
                </div>
                <div class="cell">
                  <form:input type="number" path="systems[${sysLoop.index}].accessories[${prodLoop.index}].qty" min="0" style="width:50px;" />
                </div>
                <div class="cell">
                  <form:input type="number"  path="systems[${sysLoop.index}].accessories[${prodLoop.index}].cost" step="any" style="width:80px;" readonly="true" />
                </div>
                <div class="cell">
                  <form:input type="number" path="systems[${sysLoop.index}].accessories[${prodLoop.index}].basePrice" step="any" style="width:80px;" readonly="true" />
                </div>
                <div class="cell">
                  <form:input type="number" path="systems[${sysLoop.index}].accessories[${prodLoop.index}].sellPrice" step="any" style="width:80px;" />
                </div>
                <div class="cell">
                  <form:input type="number" path="systems[${sysLoop.index}].accessories[${prodLoop.index}].extPrice" step="any" style="width:80px;" readonly="true"/>
                </div>
                <div class="cell">
                  <form:input type="number" path="systems[${sysLoop.index}].accessories[${prodLoop.index}].marginPct" step="any" style="width:80px;" />
                </div>
              </div>
              </c:forEach>
            </div>
          </div>
          <div class="right">Total: <fmt:formatNumber value='${systemTotal}' type="currency" groupingUsed='true' />  </div>
          <br/><br/>
          <hr/>
          <br/><br/>
        </div>
      </c:forEach>

      <div class="right">
        <input type="submit" name="submit" value="Save"/>
      </div>

    </form:form>
  </jsp:body>
</t:layout>