<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="/WEB-INF/kawauso-utils.tld" prefix="util"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<t:layout>
  <jsp:body>

    ${message}<br/><br/><br/>

    <form method="POST" action="admin-menu.htm" enctype="multipart/form-data">
      File to upload: <input type="file" name="file"><br />
      <input type="submit" value="Upload"> Press here to upload the file!
    </form>

  </jsp:body>
</t:layout>