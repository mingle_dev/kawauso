<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<t:layout>
    <jsp:body>
   
    <c:choose>
    <c:when test="${level == 0}">
        <h2>Step 1: Name your Quote</h2>
        <form>
            <input type="text" id="quote_title" name="quote_title" placeholder="Quote Name" style="width:200px;"><br/><br/>
            
            [ <a href="<c:url value="/quote/create.htm?entry=${item}&level=1"/>">Next</a> ]
        </form>
    </c:when>
    <c:otherwise>
        <h2>Step 2: Select System Category</h2>
    </c:otherwise>
    </c:choose>
        
    <div class="section">
        <div>
            <div id="product-categories">
                <c:forEach items="${levelItems}" var="item">
                <div class="fourth">
                    <a href="<c:url value="/quote/create.htm?entry=${item}&level=${level+1}"/>" title="${item}" class="product">        
                    <h3>
                        <span class="img">
                             <img src="http://www.adpeso.de/wp-content/uploads/2013/08/camera-icon-256.png" width="75" height="75" alt="${item}">
                        </span>
                        ${item}
                    </h3>
                    </a>
                </div>
                </c:forEach>
            </div>
        </div>
    </div>

    </jsp:body>
</t:layout>