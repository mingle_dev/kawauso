<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="/WEB-INF/kawauso-utils.tld" prefix="util"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<t:layout>
  <jsp:body>

    <style>
      .ui-autocomplete-loading {
        background: white url('https://s3.amazonaws.com/mgl1939/content/images/ui-anim_basic_16x16.gif') right center no-repeat;
      }
      .ui-helper-hidden-accessible {
        display: none !important;
      }

      .ui-menu {
        list-style: none;
        padding: 2px;
        margin: 0;
        display: block;
        float: left;
      }

      .ui-autocomplete {
        position: absolute;
        cursor: default;
      }

      .ui-menu .ui-menu-item {
        margin: 0;
        padding: 0;
        zoom: 1;
        float: left;
        clear: left;
        width: 100%;
      }
      .ui-widget {
        font-family: Trebuchet MS, Helvetica, Arial, sans-serif;
        font-size: 1.1em;
      }
      .ui-widget-content {
        border: 1px solid #aaaaaa;
        background: #ffffff;
        color: #222222;
      }

      .ui-state-hover {
        background: #cccccc;
      }
    </style>

    <script>
      $(document).ready(function () {

        $('#qb-addline').click(function (e) {
          var numBlank = 0;
          $('input[id$=".sku"]').each(function() {
            if($(this).attr('id').indexOf("products") > -1 && $(this).val().length == 0)
              numBlank++;
          });
          if(numBlank > 0)
            return;

          var copy = $(".qb-prodline:last").clone(true);
          var arr = copy.attr('id').split('-');
          var curr = parseInt(arr[1]);
          var next = curr+1;

          //change id of div
          copy.attr('id', 'row-' + next);

          //change id of all form fields
          copy.html(function(i, oldHTML) {
            return oldHTML.replace(new RegExp('products'+curr, 'g'), 'products'+next);
          });

          //change name of all form fileds
          copy.html(function(i, oldHTML) {
            return oldHTML.replace(new RegExp('products\\['+curr+'\\]', 'g'), 'products['+next+']');
          });

          copy.insertAfter(".qb-prodline:last");
        });

        $('#qb-addline-acc').click(function (e) {
          var numBlank = 0;
          $('input[id$=".sku"]').each(function() {
            if($(this).attr('id').indexOf("accessories") > -1 && $(this).val().length == 0)
              numBlank++;
          });
          if(numBlank > 0)
            return;

          var copy = $(".qb-accline:last").clone(true);
          var arr = copy.attr('id').split('-');
          var curr = parseInt(arr[1]);
          var next = curr+1;

          //change id of div
          copy.attr('id', 'row-' + next);

          //change id of all form fields
          copy.html(function(i, oldHTML) {
            return oldHTML.replace(new RegExp('accessories'+curr, 'g'), 'accessories'+next);
          });

          //change name of all form fileds
          copy.html(function(i, oldHTML) {
            return oldHTML.replace(new RegExp('accessories\\['+curr+'\\]', 'g'), 'accessories['+next+']');
          });

          copy.insertAfter(".qb-accline:last");
        });

        $("form").delegate('input[id$=".extPrice"]', "change", function(e) {
          console.log('changed');
        });

        $('#subtot').click(function(e) {
          var subtot = 0;
          $('input[id$=".extPrice"]').each(function() {
            var p = parseFloat( $(this).val() );
            if(p > 0) subtot += p;
          });
          $('#subtot').val( subtot.toFixed(2) );
        });

        //register current and future input fields for autocomplete
        $("form").delegate('input[id$=".sku"]', "keyup", function(e) {
          $(this).autocomplete({
            source: "/web/product/quicklist.htm",
            minLength: 3,
            delay: 100
          });
        });

        //register current and future input fields for product details
        $("form").delegate('input[id$=".sku"]', "blur", function(e) {
          e.preventDefault();

          var id = $(this).attr('id');
          var parentId = $(this).parent().attr('id');
          var rowId = id.substr(0, id.indexOf('.'));

          if($(this).val().length == 0) {
            $("#"+rowId+"\\.description").attr("placeholder", "");
            $("#"+rowId+"\\.description").val("");
            $("#"+rowId+"\\.qty").val("");
            $("#"+rowId+"\\.cost").val("");
            $("#"+rowId+"\\.basePrice").val("");
            $("#"+rowId+"\\.sellPrice").val("");
            $("#"+rowId+"\\.extPrice").val("");
            $("#"+rowId+"\\.marginPct").val("");
            return;
          }

          var qty = 0;

          $.ajax({
            url: '/web/product/get-prod-json.htm',
            data: {'custno' : '21675',
              'shipTo': '',
              'whse' : '21',
              'prod' : $(this).val()},
            type: 'GET',
            beforeSend: function() {
              qty = $("#"+rowId+"\\.qty").val();

              if( parentId.indexOf("component") > -1)
                $('#qb-addline').trigger('click');
              if( parentId.indexOf("accessory") > -1)
                $('#qb-addline-acc').trigger('click');

              $("#"+rowId+"\\.description").attr("placeholder", "Fetching Details...");
              $("#"+rowId+"\\.description").val("");
              $("#"+rowId+"\\.qty").val("");
              $("#"+rowId+"\\.cost").val("");
              $("#"+rowId+"\\.basePrice").val("");
              $("#"+rowId+"\\.sellPrice").val("");
              $("#"+rowId+"\\.extPrice").val("");
              $("#"+rowId+"\\.marginPct").val("");
            },
            success: function(res) {
              var obj = jQuery.parseJSON(res);

              $("#"+rowId+"\\.description").attr("placeholder", "");
              $("#"+rowId+"\\.description").val(obj.description);
              if(qty.length == 0)
                $("#"+rowId+"\\.qty").val("1");
              else
                $("#"+rowId+"\\.qty").val(qty);

              $("#"+rowId+"\\.cost").val(obj.pdRecord.standardCost.toFixed(2));

              $("#"+rowId+"\\.basePrice").val(obj.price.toFixed(2));
              $("#"+rowId+"\\.sellPrice").val(obj.price.toFixed(2));

              if(obj.pdRecord.marginPct.toFixed(2) < 1)
                $("#"+rowId+"\\.marginPct").val(obj.pdRecord.marginPct.toFixed(2) * 100);
              else
                $("#"+rowId+"\\.marginPct").val(obj.pdRecord.marginPct.toFixed(2));
              $("#"+rowId+"\\.extPrice").val(obj.price.toFixed(2));

              //$('#qb-addline').trigger('click');
            },
            error: function() {
              $("#"+rowId+"\\.description").attr("placeholder", "Item not found. Assumed Non-Stock.");
              $("#"+rowId+"\\.cost").attr("readonly", false);
              $("#"+rowId+"\\.marginPct").attr("readonly", true);
            }
          });
        });

        //register current and future intput fields
        //$("form").delegate('input[id$=".qty"], input[id$=".sellPrice"]', "change click", function(e) {
        $("form").delegate('input[id$=".qty"]', "blur", function(e) {
          e.preventDefault();
          var id = $(this).attr('id');
          var rowId = id.substr(0, id.indexOf('.'));
          var ext = $("#"+rowId+"\\.qty").val() * $("#"+rowId+"\\.sellPrice").val();
          $("#"+rowId+"\\.extPrice").val(ext.toFixed(2));

          $('#subtot').trigger('click');
        });

        //register current and future input fields
        $("form").delegate('input[id$=".sellPrice"]', "blur", function(e) {
          e.preventDefault();
          var id = $(this).attr('id');
          var rowId = id.substr(0, id.indexOf('.'));

          console.log( $("#"+rowId+"\\.sellPrice").val() );

          //recalcuate gm %
          $.ajax({
            url: '/web/product/calc-claim.htm',
            data: {'productCode' : $("#"+rowId+"\\.sku").val(),
              'target' : $("#"+rowId+"\\.sellPrice").val(),
              'targetType' : 0 },
            type: 'POST',
            beforeSend: function() {
              $("#"+rowId+"\\.marginPct").val('---');
            },
            success: function(res) {
              var obj = jQuery.parseJSON(res);
              console.log(obj);
              $("#"+rowId+"\\.marginPct").val(obj.marginPct.toFixed(2));
            },
            error: function() {
              $("#"+rowId+"\\.marginPct").val(0.00);
            }
          });

          var ext = $("#"+rowId+"\\.qty").val() * $("#"+rowId+"\\.sellPrice").val();
          $("#"+rowId+"\\.extPrice").val(ext.toFixed(2));
          $('#subtot').trigger('click');
        });

        //register current and future input fields
        $("form").delegate('input[id$=".marginPct"]', "blur", function(e) {
          e.preventDefault();
          var id = $(this).attr('id');
          var rowId = id.substr(0, id.indexOf('.'));

          //recalcuate gm %
          $.ajax({
            url: '/web/product/calc-claim.htm',
            data: {'productCode' : $("#"+rowId+"\\.sku").val(),
              'target' : $("#"+rowId+"\\.marginPct").val(),
              'targetType' : 1 },
            type: 'POST',
            beforeSend: function() {
              $("#"+rowId+"\\.sellPrice").val('---');
            },
            success: function(res) {
              var obj = jQuery.parseJSON(res);
              $("#"+rowId+"\\.sellPrice").val(obj.salePrice.toFixed(2));

              var ext = $("#"+rowId+"\\.qty").val() * $("#"+rowId+"\\.sellPrice").val();
              $("#"+rowId+"\\.extPrice").val(ext.toFixed(2));
              $('#subtot').trigger('click');
            },
            error: function() {
              $("#"+rowId+"\\.sellPrice").val(0.00);
              $("#"+rowId+"\\.extPrice").val(0.00);
              $('#subtot').trigger('click');
            }
          });
        });

        //register current and future input fields
        $("form").delegate('input[id$=".marginPct"]', "change click", function(e) {
          e.preventDefault();
          var id = $(this).attr('id');
          var rowId = id.substr(0, id.indexOf('.'));

          //recalcuate sale price

          //var ext = $("#"+rowId+"\\.qty").val() * $("#"+rowId+"\\.sellPrice").val();
          //$("#"+rowId+"\\.extPrice").val(ext);
          //$('#subtot').trigger('click');
        });

        $('#fetch').on('click', function(e) {
          var products = "";
          $('input[id$=".sku"]').each(function() {
            if($(this).attr('id').indexOf("products") > -1 && $(this).val().length > 0)
              products += 'products=' + $(this).val() + "&";
          });

          /*
           $("#browser").focus();
           var e = jQuery.Event("keypress");
           e.keyCode = 40;
           $("#browser").trigger(e);
           */

          $.ajax({
            url: '/web/product/get-acc-json.htm?' + products,
            //data: {'products' : data_to_send },
            type: 'GET',
            beforeSend: function() {
              $("#browsers").empty();
              //$("#acc-spinner").show();
              $("#fetch").val("Loading...");
            },
            success: function(res) {
              res = jQuery.parseJSON(res);
              $.each(res, function( index, value ) {
                var opt = $("<option></option>").attr("value", value);
                $("#browsers").append(opt);
              });
              //$("#acc-spinner").hide();
              $("#fetch").val("Fetch Accessories");
            },
            error: function() {
              //$("#acc-spinner").hide();
              $("#fetch").val("Fetch Accessories");
            }
          });

        });

      });
    </script>

    <form:form method="POST" commandName="system" action="system-save.htm">
      <input type="hidden" name="qid" value="${param.qid}"/>
      <form:input type="hidden" path="id" value="${param.sid}"/>

      <div class="section">
        <div class="left">
          <b>Mark For:</b><br/>
          <form:input type="text" path="systemName" placeholder="Enter Name" style="width:300px;" /><br/><br/>
          <b>Description:</b><br/>
          <form:input type="text" path="description" placeholder="Enter Description" style="width:300px;" /><br/><br/>
        </div>
        <div class="right">
            <%--
            <b>###</b> Systems match current config.<br/>
            <b>###</b> Accessories found.
            --%>
        </div>
      </div>
      <br/>

      <%--
      <div class="right">
        <a id="qb-addline" class="ui-button" href="#">Add Line</a>
      </div>
      --%>

      <div id="qb-addline" class="left" style="vertical-align:bottom;"><b>Components:</b></div>
      <div class="fake-table">
        <div class="header">
          <div class="row">
            <div class="cell">Product</div>
            <div class="cell">Description</div>
            <div class="cell">Quantity</div>
            <div class="cell">Cost</div>
            <div class="cell">Auto Price</div>
            <div class="cell">Sell Price</div>
            <div class="cell">Ext. Price</div>
            <div class="cell">Margin %</div>
          </div>
        </div>
        <div class="body">
            <%--<c:forEach items="${system.products}" var="product" varStatus="loop">--%>
          <c:forEach begin="0" end="${fn:length(system.products)}" varStatus="loop">
            <div id="row-${loop.index}" class="qb-prodline row">
              <div id="component-${loop.index}" class="cell">
                <form:input type="text" path="products[${loop.index}].sku" style="width:200px;" /><br/>
              </div>
              <div class="cell">
                <form:input type="text" path="products[${loop.index}].description" style="width:250px;" />
              </div>
              <div class="cell">
                <form:input type="number" path="products[${loop.index}].qty" min="0" style="width:50px;" />
              </div>
              <div class="cell">
                <form:input type="number"  path="products[${loop.index}].cost" step="any" style="width:80px;" readonly="true" />
              </div>
              <div class="cell">
                <form:input type="number" path="products[${loop.index}].basePrice" step="any" style="width:80px;" readonly="true" />
              </div>
              <div class="cell">
                <form:input type="number" path="products[${loop.index}].sellPrice" step="any" style="width:80px;" />
              </div>
              <div class="cell">
                <form:input type="number" path="products[${loop.index}].extPrice" step="any" style="width:80px;" readonly="true"/>
              </div>
              <div class="cell">
                <form:input type="number" path="products[${loop.index}].marginPct" step="any" style="width:80px;" />
              </div>
            </div>
          </c:forEach>
        </div>
      </div>
      <br/><br/>

      <div id="qb-addline-acc" class="left" style="vertical-align:bottom;"><b>Accessories:</b></div>
      <div class="right">
        <div id="acc-spinner" style="display:none;">Loading...</div>
        <input id="fetch" type="button" name="fetch" value="Fetch Accessories"/>
        <datalist id="browsers"></datalist>
      </div>
      <div class="fake-table">
        <div class="header">
          <div class="row">
            <div class="cell">Product</div>
            <div class="cell">Description</div>
            <div class="cell">Quantity</div>
            <div class="cell">Cost</div>
            <div class="cell">Auto Price</div>
            <div class="cell">Sell Price</div>
            <div class="cell">Ext. Price</div>
            <div class="cell">Margin %</div>
          </div>
        </div>
        <div class="body">
          <c:forEach begin="0" end="${fn:length(system.accessories)}" varStatus="loop">
            <div id="row-${loop.index}" class="qb-accline row">
              <div id="accessory-${loop.index}" class="cell">
                <form:input type="text" path="accessories[${loop.index}].sku" list="browsers" autocomplete="off" style="width:200px;" />
              </div>
              <div class="cell">
                <form:input type="text" path="accessories[${loop.index}].description" style="width:250px;" />
              </div>
              <div class="cell">
                <form:input type="number" path="accessories[${loop.index}].qty" min="0" style="width:50px;" />
              </div>
              <div class="cell">
                <form:input type="number" path="accessories[${loop.index}].cost" step="any" style="width:80px;" readonly="true" />
              </div>
              <div class="cell">
                <form:input type="number" path="accessories[${loop.index}].basePrice" step="any" style="width:80px;" readonly="true" />
              </div>
              <div class="cell">
                <form:input type="number" path="accessories[${loop.index}].sellPrice" step="any" style="width:80px;" />
              </div>
              <div class="cell">
                <form:input type="number" path="accessories[${loop.index}].extPrice" step="any" style="width:80px;" readonly="true"/>
              </div>
              <div class="cell">
                <form:input type="number" path="accessories[${loop.index}].marginPct" step="any" style="width:80px;" />
              </div>
            </div>
          </c:forEach>
        </div>
      </div>
      <br/><br/>

      <div class="right">
        Total: <input type="text" id="subtot" name="subtot" style="width:100px;"><br/><br/><br/><br/>
        <input type="submit" name="submit" value="Cancel"/>
        <input type="submit" name="submit" value="Delete"/>
        <input type="submit" name="submit" value="Save"/>
      </div>

    </form:form>

  </jsp:body>
</t:layout>