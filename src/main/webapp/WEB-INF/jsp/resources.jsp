<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="/WEB-INF/kawauso-utils.tld" prefix="util"%>
<t:layout>
    <jsp:body>

        <h1>Resources</h1>

        <div class="section">
            <div class="fourth center">
                <c:choose>
                    <c:when test="${session.type == 'EMPLOYEE' || session.type == 'ADMINISTRATOR'}">
                        <a href="<c:url value="/account/admin-search.htm"/>"><img src="https://s3.amazonaws.com/mgl1939/img/res_acctbal.png" width="180" height="180" style="max-width:100%; height:auto;" border="0"></a>
                        <p style="padding:0.5em 15px;">
                            View customer account details along with invoice history and other account details.
                        </p>
                    </c:when>
                    <c:otherwise>
                        <a href="<c:url value="/account/balances.htm"/>"><img src="https://s3.amazonaws.com/mgl1939/img/res_acctbal.png" width="180" height="180" style="max-width:100%; height:auto;" border="0"></a>
                        <p style="padding:0.5em 15px;">
                            View period balances along with invoice history and other account details.
                            Make a payment or schedule a future payment and even sign-up
                            for electronic delivery of some documents
                        </p>
                    </c:otherwise>
                </c:choose>
            </div>
            <div class="fourth center">
                <a href="<c:url value="/account/orders.htm"/>"><img src="https://s3.amazonaws.com/mgl1939/img/res_orderhist.png" width="180" height="180" style="max-width:100%; height:auto;" border="0"></a>
                <p style="padding:0.5em 15px;">
                    View a list of items purchased online, including images.
                    View a list of past orders and place reorders easily with Order and Item History.
                </p>
            </div>
            <div class="fourth center">
                <a href="<c:url value="/pricebook/index.htm"/>"><img src="https://s3.amazonaws.com/mgl1939/img/res_pricebook.png" width="180" height="180" style="max-width:100%; height:auto;" border="0"></a>
                <p style="padding:0.5em 15px;">
                    Create a PDF with the most popular items we carry along with YOUR pricing. Take
                    things a step further and develop retail pricing in PDF format.
                </p>
            </div>
            <div class="fourth center">
                <a href="<c:url value="/spiffs/index.htm"/>"><img src="https://s3.amazonaws.com/mgl1939/img/res_spiff.png" width="180" height="180" style="max-width:100%; height:auto;" border="0"></a>
                <p style="padding:0.5em 15px;">
                    Are you a retail salesperson? If so, earn spiffs for selling designated systems.
                </p>
            </div>
            <div class="fourth center">
                <a href="<c:url value="/techserv/training.htm"/>"><img src="https://s3.amazonaws.com/mgl1939/img/res_train.png" width="180" height="180" style="max-width:100%; height:auto;" border="0"></a>
                <p style="padding:0.5em 15px;">
                    Our experienced instructors use both classroom and hands-on training techniques to help you stay competitive and grow business.
                    Through the use of operating equipment, we are able to provide a practical approach to training – not just theoretical.
                </p>
            </div>
            <div class="fourth center">
                <a href="<c:url value="/techserv/bulletins.htm?username=${util:base64Encode(session.user.username)}"/>"><img src="https://s3.amazonaws.com/mgl1939/img/res_svcbltns.png" width="180" height="180" style="max-width:100%; height:auto;" border="0"></a>
                <p style="padding:0.5em 15px;">
                    Improve your company’s performance with help from Mingledorff's and hundreds
                    of high-quality product manufacturers. Archived Service Bulletins and
                    memos are available after logging into the portal.
                </p>
            </div>
            <div class="fourth center">
                <a href="<c:url value="/techserv/library.htm"/>"><img src="https://s3.amazonaws.com/mgl1939/img/res_techlib.png" width="180" height="180" style="max-width:100%; height:auto;" border="0"></a>
                <p style="padding:0.5em 15px;">
                    The Mingledorff's Document Library is an online repository of documents that can be located by searching
                    for a word or phrase contained within the text of the document and the assigned categories.
                </p>
            </div>

            <c:if test="${session.type eq 'EMPLOYEE' || session.type eq 'ADMINISTRATOR'}">
                <div class="fourth center">
                    <a href="<c:url value="/reports/index.htm"/>"><img src="https://s3.amazonaws.com/mgl1939/img/res_reports.png" width="180" height="180" style="max-width:100%; height:auto;" border="0"></a>
                    <p style="padding:0.5em 15px;">
                        View the mobile version of the most popular Jasper Reports.
                    </p>
                </div>
            </c:if>

            <div class="fourth center">
                <a href="<c:url value="https://www.hvacpartners.com/"/>" target="_new"><img src="https://s3.amazonaws.com/mgl1939/img/res_epic.png" width="180" height="180" style="max-width:100%; height:auto;" border="0"></a>
                <p style="padding:0.5em 15px;">
                    Access EPIC through HVACPartners.
                </p>
            </div>
        </div>
        <br/><br/><br/><br/>

    </jsp:body>
</t:layout>