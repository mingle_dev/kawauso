<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="/WEB-INF/kawauso-utils.tld" prefix="util"%>

<t:layout>
  <jsp:body>

<div class="section" style="width:100%; margin-bottom: 4px; padding-bottom: 4px;">
    <div class="half">
        <h2>Account Information</h2>
    </div>

    <div class="half right" style="padding-top:8px;">
        <c:if test="${not empty billtrustURL}">
            <a class="ui-link-button" href='${billtrustURL}' target='_new'>Online Bill Pay</a>
        </c:if>    
    </div>  
</div>

<div id="details-main" class="container border">    
    <h2 class="header active">Balance Information</h2>
    <div class="section pad10" style="font-size:125%; line-height: 1.5em;">
        <div class="half">
            <dl class="filter">
                <dt>
                    ${account.period1Text}
                </dt>
                <dd>
                    <a href="<c:url value="/account/balances.htm?period=1"/>">
                        <fmt:formatNumber value="${account.period1Bal}" type="currency" currencySymbol="$" pattern="¤#,##0.00;¤-#,##0.00"/> 
                    </a>
                </dd>
                <dt>
                    ${account.period2Text}
                </dt>
                <dd>
                    <a href="<c:url value="/account/balances.htm?period=2"/>">
                        <fmt:formatNumber value="${account.period2Bal}" type="currency" currencySymbol="$" pattern="¤#,##0.00;¤-#,##0.00"/> 
                    </a>
                </dd> 
                <dt>
                    ${account.period3Text}
                </dt>
                <dd>
                    <a href="<c:url value="/account/balances.htm?period=3"/>">
                        <fmt:formatNumber value="${account.period3Bal}" type="currency" currencySymbol="$" pattern="¤#,##0.00;¤-#,##0.00"/> 
                    </a>
                </dd>     
                <dt>
                    ${account.period4Text}
                </dt>
                <dd>
                    <a href="<c:url value="/account/balances.htm?period=4"/>">
                        <fmt:formatNumber value="${account.period4Bal}" type="currency" currencySymbol="$" pattern="¤#,##0.00;¤-#,##0.00"/> 
                    </a>
                </dd> 
                <dt>
                    ${account.period5Text}
                </dt>
                <dd>
                    <a href="<c:url value="/account/balances.htm?period=5"/>">
                        <fmt:formatNumber value="${account.period5Bal}" type="currency" currencySymbol="$" pattern="¤#,##0.00;¤-#,##0.00"/> 
                    </a>
                </dd>                 
            </dl>
        </div>
        <div class="half">
            <dl class="filter">
                <dt>
                    Future Invoice Balance:
                </dt>
                <dd>
                    <a href="<c:url value="/account/balances.htm?period=0"/>">
                        <fmt:formatNumber value="${account.futBal}" type="currency" currencySymbol="$" pattern="¤#,##0.00;¤-#,##0.00"/> 
                    </a>
                </dd> 
                <dt>
                    Total Balance:
                </dt>
                <dd>
                    <fmt:formatNumber value="${account.totalBal}" type="currency" currencySymbol="$" pattern="¤#,##0.00;¤-#,##0.00"/>
                </dd>
            </dl>
        </div>
    </div>
</div>
<br/>

<div id="details-main" class="container border">    
    <h2 class="header active">Miscellaneous Information</h2>
    <div class="section pad10" style="font-size:125%; line-height: 1.5em;">
        <div class="half">
            <dl class="filter">
                <dt>Credit Limit:</dt>
                <dd><fmt:formatNumber value="${account.credLimit}" type="currency" currencySymbol="$" pattern="¤#,##0.00;¤-#,##0.00"/></dd>
            
                <dt>Available Credit:</dt>
                <dd><fmt:formatNumber value="${account.credLimit - (account.totalBal + account.ordBal)}" type="currency" currencySymbol="$" pattern="¤#,##0.00;¤-#,##0.00"/></dd>
                <%--
                <dt>Down Payments:</dt>
                <dd><fmt:formatNumber value="${account.downPayamt}" type="currency" currencySymbol="$" pattern="¤#,##0.00;¤-#,##0.00"/></dd>

                <dt>Returns YTD:</dt>
                <dd><fmt:formatNumber value="${account.returnsytd}" type="currency" currencySymbol="$" pattern="¤#,##0.00;¤-#,##0.00"/></dd>
                <dt>Purchases YTD:</dt>
                <dd><fmt:formatNumber value="${account.salesYtd}" type="currency" currencySymbol="$" pattern="¤#,##0.00;¤-#,##0.00"/></dd>
                --%>
            </dl>
        </div>

        <div class="half">
            <dl class="filter">
                <dt>Terms:</dt>
                <dd>${account.termsDescrip}<br/>

                <dt>On Order:</dt>
                <dd><fmt:formatNumber value="${account.ordBal}" type="currency" currencySymbol="$" pattern="¤#,##0.00;¤-#,##0.00"/></dd>
                <%--
                <dt>Last Statement Balance:</dt>
                <dd><fmt:formatNumber value="${account.lastStmtBal}" type="currency" currencySymbol="$" pattern="¤#,##0.00;¤-#,##0.00"/></dd>

                <dt>Prior Statement Balance:</dt>
                <dd><fmt:formatNumber value="${account.prStmtBal}" type="currency" currencySymbol="$" pattern="¤#,##0.00;¤-#,##0.00"/></dd>
                --%>
                <dt>Last Payment:</dt>
                <dd><fmt:formatNumber value="${account.lastPayamt}" type="currency" currencySymbol="$" pattern="¤#,##0.00;¤-#,##0.00"/></dd>
            </dl>
        </div>        
    </div>
</div>
<br/><br/><br/><br/>

  </jsp:body>
</t:layout>