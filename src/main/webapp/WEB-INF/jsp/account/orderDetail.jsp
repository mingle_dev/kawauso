<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="/WEB-INF/kawauso-utils.tld" prefix="util"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>


<t:layout>
    <jsp:body>

<c:if test="${not empty param.message}">
    <img src="https://s3.amazonaws.com/mgl1939/img/yellow_alert.png" style="vertical-align:middle;" width="32"/>
    <span style="font:12px Verdana; font-weight:bold; padding-left:4px;">${param.message}</span>
</c:if>

<table style="width:100%;">
    <tr>
        <td>
            <h2>Order Details: ${order.orderNo}-<c:if test="${order.orderSuf < 10}">0</c:if>${order.orderSuf}</h2>
        </td>
        <td class="desktop-only" style="text-align:right;">
            <c:if test="${order.stageCode >= 4}">
            <c:choose>
                <c:when test="${order.orderSuf < 10}">
                    <a class="ui-link-button" href='<c:url value="/account/orders/get-doc.htm?doctype=D1175&orderno=${order.orderNo}&ordersuf=0${order.orderSuf}"/>'>Download Invoice</a>
                    <a class="ui-link-button" href='<c:url value="/account/orders/get-doc.htm?doctype=D1130&orderno=${order.orderNo}&ordersuf=0${order.orderSuf}"/>'>Download Proof of Delivery</a>
                </c:when>
                <c:otherwise>
                    <a class="ui-link-button" href='<c:url value="/account/orders/get-doc.htm?doctype=D1175&orderno=${order.orderNo}&ordersuf=0${order.orderSuf}"/>'>Download Invoice</a>
                    <a class="ui-link-button" href='<c:url value="/account/orders/get-doc.htm?doctype=D1130&orderno=${order.orderNo}&ordersuf=${order.orderSuf}"/>'>Download Proof of Delivery</a>
                </c:otherwise>
            </c:choose>
            </c:if>
        </td>
    </tr>
</table>

<div id="details-main" class="container border">    
    <h2 class="header active">Order Information</h2>
    <div class="section" style="line-height: 1.5em;">
        <div class="half">
            <dl>
                <dt>Status:</dt><dd>${order.stageText}&nbsp;</dd>
                <dt>Order Date:</dt><dd>${order.enterDt}&nbsp;</dd>
                <dt>Ship Date:</dt><dd>${order.shipDt}&nbsp;</dd>
                <dt>Ship Via:</dt><dd>${order.shipVia}&nbsp;${shipViaText}&nbsp;</dd>
                <dt>Terms:</dt><dd>${order.termsDesc}&nbsp;</dd>
                <dt>PO #:</dt><dd>${order.custPo}&nbsp;</dd>
                <dt>Warehouse:</dt><dd>${order.whse}&nbsp;</dd>
                <dt>Reference:</dt><dd>${order.reference}&nbsp;</dd>
                <dt>Instructions:</dt><dd>${order.instructions}&nbsp;</dd>
            </dl>
            <br/>
        </div>
        <div class="half">
            <dl>
            <b>Bill To Address:</b><br/>
            ${order.shipToName}<br/>
            ${order.shipToAddr1}<br/>
            ${order.shipToCity}, ${order.shipToState} ${order.shipToZip}<br/>
            <br/>
            <b>Ship To Address:</b><br/>
            ${order.shipToName}<br/>
            ${order.shipToAddr1}<br/>
            ${order.shipToCity}, ${order.shipToState} ${order.shipToZip}<br/>
            </dl>
        </div>
    </div>
</div>
<br/>

<div class="fake-table">
    <div class="header">
        <div class="row">
            <div class="cell">Product</div>
            <div class="cell text-center">Qty Ordered</div>
            <div class="cell text-center">Qty Shipped</div>
            <div class="cell text-right">Price</div>
            <div class="cell text-right">Ext. Price</div>
        </div>
    </div>
    <div class="body">
        <c:forEach items="${order.orderLines}" var="line" varStatus="itemStatus">
        <div class="row">
            <div class="cell">
                <a href="<c:url value="/product/detail.htm?prod=${util:base64Encode(line.shipProd)}"/>">${line.description}</a><br/>
               <b>SKU:</b> ${line.shipProd}</a><br/>
               <c:if test="${fn:length(line.serialNumbers) > 0}">
               <span style="font-weight: bold;">Serial #'s:</span>
               <c:forEach items="${line.serialNumbers}" var="serial" varStatus="loop">
                   ${serial} <c:if test="${!loop.last}">,</c:if>
               </c:forEach>
               </c:if>
            </div>
            <!-- <div class="cell text-center"><span class="narrow"></span> </div> -->
            <div class="cell text-center"><span class="narrow">Quantity Ordered</span> ${line.qtyOrder}</div>
            <div class="cell text-center"><span class="narrow">Quantity Shipped:</span> ${line.qtyShip}</div>
            <div class="cell text-right"><span class="narrow">Price:</span>
                <fmt:formatNumber value="${line.price}" type="currency" currencySymbol="$" pattern="¤#,##0.00;¤-#,##0.00"/>
            </div>
            <div class="cell text-right"><span class="narrow">Ext. Price:</span>
                <fmt:formatNumber value="${line.netAmount}" type="currency" currencySymbol="$" pattern="¤#,##0.00;¤-#,##0.00"/> 
            </div>            
        </div>
        </c:forEach>
    </div>
</div>            

        
  </jsp:body>
</t:layout>