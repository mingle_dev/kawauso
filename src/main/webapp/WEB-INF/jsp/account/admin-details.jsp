<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="/WEB-INF/kawauso-utils.tld" prefix="util"%>

<t:layout>

<jsp:attribute name="title">
  Account Details
  <a class="ui-link-button" href="<c:url value="/user/custno-assign.htm?custno=${account.custno}"/>">Set Profile</a>
</jsp:attribute>

<jsp:body>

<div id="details-main" class="container border">
  <h2 class="header active">${account.name}</h2>
  <div class="section pad10" style="font-size:125%; line-height: 1.5em;">
      <div class="half">
          <dl class="filter">
              <dt>Customer #:</dt>
              <dd>${account.custno}</dd>
              <dt>Address:</dt>
              <dd>
                  <a href="http://maps.apple.com/maps?q=${account.address1} ${account.city}, ${account.state} ${account.zip}">${account.address1}<br/>${account.city}, ${account.state} ${account.zip}</a>
              </dd>
          </dl>
      </div>

      <div class="half">
          <dl class="filter">
              <dt>Phone #:</dt>
              <dd>
                <c:choose>
                    <c:when test="${fn:length(account.phoneNumber) gt 10}">
                        <a href="tel:${account.phoneNumber}">
                            <c:out value="(${fn:substring(account.phoneNumber, 0, 3)}) ${fn:substring(account.phoneNumber, 3, 6)}-${fn:substring(account.phoneNumber, 6, 10)} ext. ${fn:substring(account.phoneNumber, 10, fn:length(account.phoneNumber))}"/>
                        </a>
                    </c:when>
                    <c:when test="${not empty account.phoneNumber}">
                        <a href="tel:${account.phoneNumber}">
                            <c:out value="(${fn:substring(account.phoneNumber, 0, 3)}) ${fn:substring(account.phoneNumber, 3, 6)}-${fn:substring(account.phoneNumber, 6, fn:length(account.phoneNumber))}"/>
                        </a>
                    </c:when>
                    <c:otherwise>(000) 000-0000</c:otherwise>
                </c:choose>
              </dd>
              <dt>Territory:</dt>
              <dd> ${currentLocation.whseName}, ${currentLocation.state}</dd>
              <dt>Sales Rep:</dt>
              <dd>${salesRep['name']}</dd>
          </dl>
      </div>
  </div>
</div>
<br/>

<div id="details-main" class="container border">    
    <h2 class="header active">Balance Information</h2>
    <div class="section pad10" style="font-size:125%; line-height: 1.5em;">
        <div class="half">
            <dl class="filter">
                <dt>
                    ${account.period1Text}
                </dt>
                <dd>
                    <a href="<c:url value="/account/balances.htm?period=1"/>">
                        <fmt:formatNumber value="${account.period1Bal}" type="currency" currencySymbol="$" pattern="¤#,##0.00;¤-#,##0.00"/> 
                    </a>
                </dd>
                <dt>
                    ${account.period2Text}
                </dt>
                <dd>
                    <a href="<c:url value="/account/balances.htm?period=2"/>">
                        <fmt:formatNumber value="${account.period2Bal}" type="currency" currencySymbol="$" pattern="¤#,##0.00;¤-#,##0.00"/> 
                    </a>
                </dd> 
                <dt>
                    ${account.period3Text}
                </dt>
                <dd>
                    <a href="<c:url value="/account/balances.htm?period=3"/>">
                        <fmt:formatNumber value="${account.period3Bal}" type="currency" currencySymbol="$" pattern="¤#,##0.00;¤-#,##0.00"/> 
                    </a>
                </dd>     
                <dt>
                    ${account.period4Text}
                </dt>
                <dd>
                    <a href="<c:url value="/account/balances.htm?period=4"/>">
                        <fmt:formatNumber value="${account.period4Bal}" type="currency" currencySymbol="$" pattern="¤#,##0.00;¤-#,##0.00"/> 
                    </a>
                </dd> 
                <dt>
                    ${account.period5Text}
                </dt>
                <dd>
                    <a href="<c:url value="/account/balances.htm?period=5"/>">
                        <fmt:formatNumber value="${account.period5Bal}" type="currency" currencySymbol="$" pattern="¤#,##0.00;¤-#,##0.00"/> 
                    </a>
                </dd>                 
            </dl>
        </div>
        <div class="half">
            <dl class="filter">
                <dt>
                    Future Invoice Balance:
                </dt>
                <dd>
                    <a href="<c:url value="/account/balances.htm?period=0"/>">
                        <fmt:formatNumber value="${account.futBal}" type="currency" currencySymbol="$" pattern="¤#,##0.00;¤-#,##0.00"/> 
                    </a>
                </dd> 
                <dt>
                    Total Balance:
                </dt>
                <dd>
                    <fmt:formatNumber value="${account.totalBal}" type="currency" currencySymbol="$" pattern="¤#,##0.00;¤-#,##0.00"/>
                </dd>
            </dl>
        </div>
    </div>
</div>
<br/>

<div id="details-main" class="container border">    
    <h2 class="header active">Miscellaneous Information</h2>
    <div class="section pad10" style="font-size:125%; line-height: 1.5em;">
        <div class="half">
            <dl class="filter">
                <dt>Credit Limit:</dt>
                <dd><fmt:formatNumber value="${account.credLimit}" type="currency" currencySymbol="$" pattern="¤#,##0.00;¤-#,##0.00"/></dd>
            
                <dt>Available Credit:</dt>
                <dd><fmt:formatNumber value="${account.credLimit - (account.totalBal + account.ordBal)}" type="currency" currencySymbol="$" pattern="¤#,##0.00;¤-#,##0.00"/></dd>
                <%--
                <dt>Down Payments:</dt>
                <dd><fmt:formatNumber value="${account.downPayamt}" type="currency" currencySymbol="$" pattern="¤#,##0.00;¤-#,##0.00"/></dd>

                <dt>Returns YTD:</dt>
                <dd><fmt:formatNumber value="${account.returnsytd}" type="currency" currencySymbol="$" pattern="¤#,##0.00;¤-#,##0.00"/></dd>
                <dt>Purchases YTD:</dt>
                <dd><fmt:formatNumber value="${account.salesYtd}" type="currency" currencySymbol="$" pattern="¤#,##0.00;¤-#,##0.00"/></dd>
                --%>
            </dl>
        </div>

        <div class="half">
            <dl class="filter">
                <dt>Terms:</dt>
                <dd>${account.termsDescrip}<br/>

                <dt>On Order:</dt>
                <dd><fmt:formatNumber value="${account.ordBal}" type="currency" currencySymbol="$" pattern="¤#,##0.00;¤-#,##0.00"/></dd>
                <%--
                <dt>Last Statement Balance:</dt>
                <dd><fmt:formatNumber value="${account.lastStmtBal}" type="currency" currencySymbol="$" pattern="¤#,##0.00;¤-#,##0.00"/></dd>

                <dt>Prior Statement Balance:</dt>
                <dd><fmt:formatNumber value="${account.prStmtBal}" type="currency" currencySymbol="$" pattern="¤#,##0.00;¤-#,##0.00"/></dd>
                --%>
                <dt>Last Payment:</dt>
                <dd><fmt:formatNumber value="${account.lastPayamt}" type="currency" currencySymbol="$" pattern="¤#,##0.00;¤-#,##0.00"/></dd>
            </dl>
        </div>        
    </div>
</div>
<br/>

<c:if test="${fn:length(contacts) > 0}">
<div id="details-main" class="container border">
    <h2 class="header active">Contacts</h2>
    <div class="section pad10" style="font-size:125%; line-height: 1.5em; padding:10px;">
        <div class="fake-table">
            <div class="header">
                <div class="row">
                    <div class="cell">Account #</div>
                    <div class="cell">Contact Name</div>
                    <div class="cell">Phone #</div>
                    <div class="cell">Email Address</div>
                </div>
            </div>
            <div class="body">
                <c:forEach items="${contacts}" var="contact" varStatus="itemStatus">
                    <div class="row">
                        <div class="cell">
                            <a href="<c:url value="/account/admin-details.htm?custno=${contact.customerNumber}"/>">${contact.customerNumber}</a>
                        </div>
                        <div class="cell"><span class="narrow">Contact Name:</span> ${contact.lastName}, ${contact.firstName}</div>
                        <div class="cell"><span class="narrow">Phone #:</span>${contact.phoneWork}</div>
                        <div class="cell"><span class="narrow">Email:</span>${contact.emailAddr}</div>
                    </div>
                </c:forEach>
            </div>
        </div>
    </div>
</div>
</c:if>

  </jsp:body>
</t:layout>