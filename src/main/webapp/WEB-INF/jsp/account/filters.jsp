<%--
  Created by IntelliJ IDEA.
  User: twall
  Date: 9/20/15
  Time: 3:56 PM
  To change this template use File | Settings | File Templates.
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib uri="http://java.sun.com/jstl/core" prefix="c"%>
<%@taglib uri="/WEB-INF/kawauso-utils.tld" prefix="util"%>

<t:filtermodal>
  <jsp:attribute name="modalTitle">Filter By Date</jsp:attribute>
  <jsp:body>
    <div>
      <form action="${pageContext.request.contextPath}/account/filters.htm" method="post" onsubmit="return validateFilterParams(this);">
        <input type="hidden" name="page" value="${param.page}" />
        <input type="hidden" name="stage" value="${param.stage}" />
        <input type="hidden" name="term" value="${param.term}" />
        <input type="hidden" name="type" value="${param.type}" />
        <input type="hidden" name="limit" value="${param.limit}" />
        <input type="hidden" name="criteria" value="${param.criteria}" />
        <a href="${pageContext.request.contextPath}/account/orders.htm?term=${param.term}&type=${param.type}&stage=${param.stage}&page=${param.page}&limit=${param.limit}" title="Close" class="close">X</a>
        <div class="fake-table">
          <h3>Filter By Date</h3>
          <div class="right" style="color:red;font-weight:bold;" id="message">
          </div>
          <div style="align: center;font-size: 16px;">Enter a valid date range with each date entry in <b>mm/dd/yyyy</b> format.</div>
          <hr />
          <div class="body">
            <div class="left">
              <div class="row"><div style="width-fix:25%">From:</div><div style="width-fix:65%"><input type="text" name="from_dt" id="from_dt" placeholder="mm/dd/yyyy" /><div id="fmsg_id" style="display:none;color:red;font-weight:bold">*</div></div></div>
              <div class="row"><div style="width-fix:25%">To:</div><div style="width-fix:65%"><input type="text" name="to_dt" id="to_dt" placeholder="mm/dd/yyyy" /><div id="tmsg_id" style="display:none;color:red;font-weight:bold">*</div></div></div>
            </div>
            <div class="row">&nbsp;</div>
            <div class="row">
              <input type="button" name="cancel" value="cancel" onClick="location.href='${pageContext.request.contextPath}/account/orders.htm?term=${param.term}&type=${param.type}&stage=${param.stage}&page=${param.page}&limit=${param.limit}'" />
              <input type="submit" value="submit" />
            </div>
          </div>
        </div>
      </form>
    </div>
    <script>
      var space = " ";
      var emptyspace = "";

      function getQueryParams(qs) {
        qs = qs.split('+').join(' ');

        var params = {},
                tokens,
                re = /[?&]?([^=]+)=([^&]*)/g;

        while (tokens = re.exec(qs)) {
          params[decodeURIComponent(tokens[1])] = decodeURIComponent(tokens[2]);
        }

        return params;
      }

      function validateFilterParams(){


        var query = $('form').serialize();
        var params = getQueryParams(query);
        var reason = space;

        var from_dt = params["from_dt"];

        if (from_dt == undefined || from_dt == emptyspace) {
          reason = 'Please enter from date value.<br/>';
          document.getElementById("from_dt").focus();
          document.getElementById("fmsg_id").style.display = "inline";
          this.from_dt.style.backgroundColor = 'Yellow';
        }
        else {
          var tmp = validateDateFormat(from_dt);

          if (tmp != space) {
            reason = reason + tmp;
          }
        }

        var to_dt = params["to_dt"];

        if (to_dt == undefined || to_dt == emptyspace) {
          reason = reason + 'Please enter to date value.<br/>';
          document.getElementById("to_dt").focus();
          document.getElementById("tmsg_id").style.display = "inline";
          this.to_dt.style.backgroundColor = 'Yellow';
        }
        else{
          var tmp = validateDateFormat(to_dt);

          if (tmp != space) {
            reason = reason + tmp;
          }
        }

        if (reason != (space)) {
          document.getElementById('message').innerHTML=reason;
          return false;
        }

        var tmp = validateDateRange(from_dt, to_dt);
        if (tmp != space){
          reason = reason + tmp;
        }

        if (reason != (space)) {

          document.getElementById('message').innerHTML=reason;
          document.getElementById("from_dt").focus();
          document.getElementById("fmsg_id").style.display = "inline";
          this.from_dt.style.backgroundColor = 'Yellow';

          document.getElementById("to_dt").focus();
          document.getElementById("tmsg_id").style.display = "inline";
          this.to_dt.style.backgroundColor = 'Yellow';
          return false;

        }

        return true;

      }

      function validateDateFormat(dt)
      {
        var error = space;
        // regular expression to match required date format
        // var re = '[0-9]{2}/[0-9]{2}/[0-9]{4}';
        var re = '^(0[1-9]|1[012])([- /.])(0[1-9]|[12][0-9]|3[01])([- /.])(19|20)\\d\\d$';

        if(dt != '' && !dt.match(re)) {
          error = "Invalid date format: " + dt + "<br/>";
          return error;
        }
        return error;

      }

      function validateDateRange(from_date, to_date)
      {
        var from = new Date(from_date);
        var to   = new Date(to_date);
        var error = space;

        if (from > to)
        {
          error = "Invalid date range: " + from_date + " occurs before " + to_date + " <br/>";
          return error;
        }
        return error;
      }
    </script>
  </jsp:body>
</t:filtermodal>

