<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="/WEB-INF/kawauso-utils.tld" prefix="util"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<t:layout>
    <jsp:attribute name="inlineScript">
        <script type="text/javascript">
            $(document).ready(function () {
                $("#search-results-page, #search-results-sort").mouseover(function (e) {
                    $('>ul', this).css({ 'visibility' : 'visible', 'display' : 'block' });
                });
                $("#search-results-page, #search-results-sort").mouseout(function (e) {
                    $('>ul', this).css({ 'visibility' : 'hidden', 'display' : 'block' });
                });
            });
        </script>
    </jsp:attribute>

    <jsp:attribute name="mainSearch">
        <form id="fastKeywordSearch" class="product-search relative" action="<c:url value="/account/orders.htm"/>" method="GET">
            <div>
                <input type="text" id="mainSearch" name="term" placeholder="Order #, PO #, Product or Serial #" class="term" value="" min="2" max="255" required="" autocomplete="off"><span role="status" aria-live="polite" class="ui-helper-hidden-accessible"></span>
            </div>
            <input type="submit" value="Search" class="btn-search">
        </form>
    </jsp:attribute>

    <jsp:attribute name="title">
        Order Inquiry
    </jsp:attribute>

    <jsp:body>

        <c:if test="${not empty term}">
            <div class="left" style="font-size:1.1em; width: 100%; margin-bottom:10px; padding-bottom:10px;">
                Search results for <b>${term}</b>
                <a href="<c:url value="/account/orders.htm?id=${menuId}&page=${page}&limit=${limit}&sort=${sort}${attrStr}"/>"><img src="http://www.qc.cuny.edu/PublishingImages/close_red.gif" border="0"></a>
            </div>
        </c:if>


        <div class="container" style="width: 100%; margin-bottom:16px; padding-bottom:16px;">
            <div class="left">
                <ul class="dropdown-menu">
                    <li id="search-results-page">
                <span class="fake-select-head">
                    <a href="#">${limit} Results Per Page</a>
                </span>
                        <ul class="fake-select-items">
                            <li><a href="<c:url value="/account/orders.htm?term=${term}&type=${type}&stage=${stage}&page=1&limit=20&criteria=${criteria}&from_dt=${from_dt}&to_dt=${to_dt}"/>">20 Results Per Page</a></li>
                            <li><a href="<c:url value="/account/orders.htm?term=${term}&type=${type}&stage=${stage}&page=1&limit=40&criteria=${criteria}&from_dt=${from_dt}&to_dt=${to_dt}"/>">40 Results Per Page</a></li>
                            <li><a href="<c:url value="/account/orders.htm?term=${term}&type=${type}&stage=${stage}&page=1&limit=60&criteria=${criteria}&from_dt=${from_dt}&to_dt=${to_dt}"/>">60 Results Per Page</a></li>
                        </ul>
                    </li>

                    <li id="search-results-sort">
                <span class="fake-select-head">
                    <a href="#">Sorted by ${sort}</a>
                </span>
                        <ul class="fake-select-items">
                            <li><a href="<c:url value="/account/orders.htm?term=${term}&type=${type}&stage=${stage}&page=1&limit=${limit}&sort=Invoice&criteria=${criteria}&from_dt=${from_dt}&to_dt=${to_dt}"/>">Sorted by Invoice</a></li>
                            <li><a href="<c:url value="/account/orders.htm?term=${term}&type=${type}&stage=${stage}&page=1&limit=${limit}&sort=PO&criteria=${criteria}&from_dt=${from_dt}&to_dt=${to_dt}"/>">Sorted by PO </a></li>
                            <li><a href="<c:url value="/account/orders.htm?term=${term}&type=${type}&stage=${stage}&page=1&limit=${limit}&sort=Type&criteria=${criteria}&from_dt=${from_dt}&to_dt=${to_dt}"/>">Sorted by Type</a></li>
                            <li><a href="<c:url value="/account/orders.htm?term=${term}&type=${type}&stage=${stage}&page=1&limit=${limit}&sort=Status&criteria=${criteria}&from_dt=${from_dt}&to_dt=${to_dt}"/>">Sorted by Status</a></li>
                            <li><a href="<c:url value="/account/orders.htm?term=${term}&type=${type}&stage=${stage}&page=1&limit=${limit}&sort=Entered&criteria=${criteria}&from_dt=${from_dt}&to_dt=${to_dt}"/>">Sorted by Entered</a></li>
                            <li><a href="<c:url value="/account/orders.htm?term=${term}&type=${type}&stage=${stage}&page=1&limit=${limit}&sort=Invoiced&criteria=${criteria}&from_dt=${from_dt}&to_dt=${to_dt}"/>">Sorted by Invoiced</a></li>
                            <li><a href="<c:url value="/account/orders.htm?term=${term}&type=${type}&stage=${stage}&page=1&limit=${limit}&sort=Amount&criteria=${criteria}&from_dt=${from_dt}&to_dt=${to_dt}"/>">Sorted by Amount</a></li>
                        </ul>
                    </li>
                </ul>
            </div>

            <div class="left">
                <ul class="dropdown-menu">
                    <li id="search-results-page">
                <span class="fake-select-head">
                    <c:choose>
                        <c:when test="${type eq 'SO'}"><a href="#">Stock Orders</a></c:when>
                        <c:when test="${type eq 'DO'}"><a href="#">Direct Orders</a></c:when>
                        <c:when test="${type eq 'QU'}"><a href="#">Quotes</a></c:when>
                        <c:when test="${type eq 'BO'}"><a href="#">Back Orders</a></c:when>
                        <c:when test="${type eq 'CR'}"><a href="#">Corrections</a></c:when>
                        <c:when test="${type eq 'RA'}"><a href="#">Returns</a></c:when>
                        <c:otherwise><a href="#">All Orders</a></c:otherwise>
                    </c:choose>
                </span>
                        <ul class="fake-select-items">
                            <li><a href="<c:url value="/account/orders.htm?term=${term}&stage=${stage}&page=1&limit=${limit}&criteria=${criteria}&from_dt=${from_dt}&to_dt=${to_dt}"/>">All Orders</a></li>
                            <li><a href="<c:url value="/account/orders.htm?term=${term}&type=SO&stage=${stage}&page=1&limit=${limit}&criteria=${criteria}&from_dt=${from_dt}&to_dt=${to_dt}"/>">Stock Orders</a></li>
                            <li><a href="<c:url value="/account/orders.htm?term=${term}&type=DO&stage=${stage}&page=1&limit=${limit}&criteria=${criteria}&from_dt=${from_dt}&to_dt=${to_dt}"/>">Direct Orders</a></li>
                            <li><a href="<c:url value="/account/orders.htm?term=${term}&type=QU&stage=${stage}&page=1&limit=${limit}&criteria=${criteria}&from_dt=${from_dt}&to_dt=${to_dt}"/>">Quotes</a></li>
                            <li><a href="<c:url value="/account/orders.htm?term=${term}&type=BO&stage=${stage}&page=1&limit=${limit}&criteria=${criteria}&from_dt=${from_dt}&to_dt=${to_dt}"/>">Back Orders</a></li>
                            <li><a href="<c:url value="/account/orders.htm?term=${term}&type=CR&stage=${stage}&page=1&limit=${limit}&criteria=${criteria}&from_dt=${from_dt}&to_dt=${to_dt}"/>">Corrections</a></li>
                            <li><a href="<c:url value="/account/orders.htm?term=${term}&type=RA&stage=${stage}&page=1&limit=${limit}&criteria=${criteria}&from_dt=${from_dt}&to_dt=${to_dt}"/>">Returns</a></li>
                        </ul>
                    </li>

                    <li id="search-results-page">
                <span class="fake-select-head">
                    <c:choose>
                        <c:when test="${stage eq 1}"><a href="#">Ordered</a></c:when>
                        <c:when test="${stage eq 2}"><a href="#">Picked</a></c:when>
                        <c:when test="${stage eq 3}"><a href="#">Shipped</a></c:when>
                        <c:when test="${stage eq 4}"><a href="#">Invoiced</a></c:when>
                        <c:when test="${stage eq 5}"><a href="#">Paid</a></c:when>
                        <c:when test="${stage eq 9}"><a href="#">Canceled</a></c:when>
                        <c:otherwise><a href="#">Any Stage</a></c:otherwise>
                    </c:choose>
                </span>
                        <ul class="fake-select-items">
                            <li><a href="<c:url value="/account/orders.htm?term=${term}&type=${type}&page=1&limit=${limit}&criteria=${criteria}&from_dt=${from_dt}&to_dt=${to_dt}"/>">Any Stage</a></li>
                            <li><a href="<c:url value="/account/orders.htm?term=${term}&type=${type}&stage=1&page=1&limit=${limit}&criteria=${criteria}&from_dt=${from_dt}&to_dt=${to_dt}"/>">Ordered</a></li>
                            <li><a href="<c:url value="/account/orders.htm?term=${term}&type=${type}&stage=2&page=1&limit=${limit}&criteria=${criteria}&from_dt=${from_dt}&to_dt=${to_dt}"/>">Picked</a></li>
                            <li><a href="<c:url value="/account/orders.htm?term=${term}&type=${type}&stage=3&page=1&limit=${limit}&criteria=${criteria}&from_dt=${from_dt}&to_dt=${to_dt}"/>">Shipped</a></li>
                            <li><a href="<c:url value="/account/orders.htm?term=${term}&type=${type}&stage=4&page=1&limit=${limit}&criteria=${criteria}&from_dt=${from_dt}&to_dt=${to_dt}"/>">Invoiced</a></li>
                            <li><a href="<c:url value="/account/orders.htm?term=${term}&type=${type}&stage=5&page=1&limit=${limit}&criteria=${criteria}&from_dt=${from_dt}&to_dt=${to_dt}"/>">Paid</a></li>
                            <li><a href="<c:url value="/account/orders.htm?term=${term}&type=${type}&stage=9&page=1&limit=${limit}&criteria=${criteria}&from_dt=${from_dt}&to_dt=${to_dt}"/>">Canceled</a></li>
                        </ul>
                    </li>
                    <li id="search-results-page">
                <span class="fake-select-head">
                    <c:choose>
                        <c:when test="${criteria eq 'entered_date'}"><a href="#">Filter by Entered Date</a></c:when>
                        <c:when test="${criteria eq 'invoice_date'}"><a href="#">Filter by Invoice Date</a></c:when>
                        <c:otherwise><a href="#">Select Filter Criteria</a></c:otherwise>
                    </c:choose>
                </span>
                        <ul class="fake-select-items">
                            <li><a href="<c:url value="#"/>">Select Filter Criteria</a></li>
                            <li><a href="<c:url value="/account/filters.htm?criteria=entered_date&term=${term}&type=${type}&stage=${stage}&page=1&limit=${limit}"  />" >Filter by Entered Date</a></li>
                                <%--<li><a href="<c:url value="/account/orders.htm?term=${term}&type=${type}&stage=${stage}&page=1&limit=${limit}&criteria=entered_date" />">Filter by Entered Date</a></li>--%>
                            <li><a href="<c:url value="/account/filters.htm?term=${term}&type=${type}&stage=${stage}&page=1&limit=${limit}&criteria=invoice_date"/>">Filter by Invoice Date</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
            <div class="left">
                <ul class="dropdown-menu">
                    <c:if test="${not empty criteria}">
                        <li id="search-results-page" style="padding:0 0 0 0;">
                <span class="fake-select-head">
                    <a href="<c:url value="/account/orders.htm?term=${term}&type=${type}&stage=${stage}&page=${i}&limit=${limit}&sort=${sort}"/>" alt="Clear Filter" style="height:24px;">
                        <svg style="width:24px;height:24px" viewBox="0 0 24 24">
                            <path fill="#000000" d="M12,20C7.59,20 4,16.41 4,12C4,7.59 7.59,4 12,4C16.41,4 20,7.59 20,12C20,16.41 16.41,20 12,20M12,2C6.47,2 2,6.47 2,12C2,17.53 6.47,22 12,22C17.53,22 22,17.53 22,12C22,6.47 17.53,2 12,2M14.59,8L12,10.59L9.41,8L8,9.41L10.59,12L8,14.59L9.41,16L12,13.41L14.59,16L16,14.59L13.41,12L16,9.41L14.59,8Z" />
                        </svg>
                    </a>
                </span>
                            <ul class="fake-select-items">
                                <li><a href="<c:url value="/account/orders.htm?term=${term}&type=${type}&stage=${stage}&page=${i}&limit=${limit}&sort=${sort}"/>">Clear Filter</a></li>
                            </ul>
                        </li>
                    </c:if>
                </ul>
            </div>


            <div class="right-left">
                <div class="ui-buttonset">
                    <c:if test="${page > 1}">
                        <a href="<c:url value="/account/orders.htm?term=${term}&type=${type}&stage=${stage}&page=${page-1}&limit=${limit}&sort=${sort}&criteria=${criteria}&from_dt=${from_dt}&to_dt=${to_dt}"/>" class="ui-button"> < </a>
                    </c:if>
                    <c:choose>
                        <c:when test="${numPages > 0 && numPages < 6}">
                            <c:forEach var="i" begin="1" end="${numPages}" step="1">
                                <c:choose>
                                    <c:when test="${i == page}"><a href="#" class="ui-button ui-button-active"> ${i} </a></c:when>
                                    <c:otherwise><a href="<c:url value="/account/orders.htm?term=${term}&type=${type}&stage=${stage}&page=${i}&limit=${limit}&sort=${sort}&criteria=${criteria}&from_dt=${from_dt}&to_dt=${to_dt}"/>" class="ui-button"> ${i} </a></c:otherwise>
                                </c:choose>
                            </c:forEach>
                        </c:when>

                        <c:when test="${page < 6}">
                            <c:if test="${numPages > 0}">
                                <c:forEach var="i" begin="1" end="5" step="1">
                                    <c:choose>
                                        <c:when test="${i == page}"><a href="#" class="ui-button ui-button-active"> ${i} </a></c:when>
                                        <c:otherwise><a href="<c:url value="/account/orders.htm?term=${term}&type=${type}&stage=${stage}&page=${i}&limit=${limit}&sort=${sort}&criteria=${criteria}&from_dt=${from_dt}&to_dt=${to_dt}"/>" class="ui-button"> ${i} </a></c:otherwise>
                                    </c:choose>
                                </c:forEach>
                            </c:if>
                        </c:when>

                        <c:when test="${(page + 3) > numPages}">
                            <c:forEach var="i" begin="${numPages - 4}" end="${numPages}" step="1">
                                <c:choose>
                                    <c:when test="${i == page}"><a href="#" class="ui-button ui-button-active"> ${i} </a></c:when>
                                    <c:otherwise><a href="<c:url value="/account/orders.htm?term=${term}&type=${type}&stage=${stage}&page=${i}&limit=${limit}&sort=${sort}&criteria=${criteria}&from_dt=${from_dt}&to_dt=${to_dt}"/>" class="ui-button"> ${i} </a></c:otherwise>
                                </c:choose>
                            </c:forEach>
                        </c:when>

                        <c:otherwise>
                            <c:forEach var="i" begin="${page - 2}" end="${page + 2}" step="1">
                                <c:choose>
                                    <c:when test="${i == page}"><a href="#" class="ui-button ui-button-active"> ${i} </a></c:when>
                                    <c:otherwise><a href="<c:url value="/account/orders.htm?term=${term}&type=${type}&stage=${stage}&page=${i}&limit=${limit}&sort=${sort}&criteria=${criteria}&from_dt=${from_dt}&to_dt=${to_dt}"/>" class="ui-button"> ${i} </a></c:otherwise>
                                </c:choose>
                            </c:forEach>
                        </c:otherwise>
                    </c:choose>

                    <c:if test="${page < numPages}">
                        <a href="<c:url value="/account/orders.htm?term=${term}&type=${type}&stage=${stage}&page=${page+1}&limit=${limit}&sort=${sort}&criteria=${criteria}&from_dt=${from_dt}&to_dt=${to_dt}"/>" class="ui-button"> > </a>
                    </c:if>
                </div>
            </div>
        </div>

        <br/><br/>

        <div class="fake-table">
            <div class="header">
                <div class="row">
                    <div class="cell">Invoice #</div>
                    <div class="cell">PO #</div>
                    <div class="cell">Order Type</div>
                    <div class="cell">Order Status</div>
                    <div class="cell text-center">Entered Date</div>
                    <div class="cell text-center">Invoice Date</div>
                    <div class="cell text-right">Order Amount</div>
                </div>
            </div>
            <div class="body">
                <c:forEach items="${orders}" var="order" varStatus="itemStatus">
                    <div class="row">
                        <div class="cell"><span class="narrow"><b>Order #:</b></span>
                            <a href="<c:url value="/account/orders/detail.htm?orderno=${order.orderNo}&ordersuf=${order.orderSuf}"/>">${order.orderNo}-<c:if test="${order.orderSuf < 10}">0</c:if>${order.orderSuf}</a>
                        </div>
                        <div class="cell"><span class="narrow">PO #:</span> ${order.custPo}</div>
                        <div class="cell"><span class="narrow">Order Type</span> ${order.transTypeText}</div>
                        <div class="cell"><span class="narrow">Order Status</span> ${order.stageText}</div>
                        <div class="cell text-center"><span class="narrow">Entered Date:</span> ${order.enterDt}</div>
                        <div class="cell text-center"><span class="narrow">Invoice Date:</span> ${order.invoiceDt}</div>
                        <div class="cell text-right"><span class="narrow">Order Amount:</span>
                                ${order.taxAmount}
                            <fmt:formatNumber value="${order.invAmount}" type="currency" currencySymbol="$" pattern="¤#,##0.00;¤-#,##0.00"/>
                        </div>
                    </div>
                </c:forEach>
            </div>
        </div>
    </jsp:body>
</t:layout>