<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="/WEB-INF/kawauso-utils.tld" prefix="util"%>

<t:layout>

  <jsp:attribute name="mainSearch">
    <form id="fastKeywordSearch" class="product-search relative" action="<c:url value="/account/orders.htm"/>" method="GET">
      <div>
        <input type="text" id="mainSearch" name="term" placeholder="Order #, PO #, Product or Serial #" class="term" value="" min="2" max="255" required="" autocomplete="off"><span role="status" aria-live="polite" class="ui-helper-hidden-accessible"></span>
      </div>
      <input type="submit" value="Search" class="btn-search">
    </form>
  </jsp:attribute>

  <jsp:attribute name="title">
    Account & Contact Search
  </jsp:attribute>

  <jsp:body>

    <form method="get" action="admin-search.htm">
    <div>
      <dl>
        <dt><label class="label-input">Search Key:</label></dt>
        <dd>
          <select name="key">
            <option value="custno" ${selcustno}>Account #</option>
            <option value="name" ${selname}>Account Name</option>
            <option value="contact" ${selcontact}>Contact Name</option>
            <option value="city" ${selcity}>City</option>
            <option value="state" ${selstate}>State</option>
            <option value="zip" ${selzip}>Zip Code</option>
            <option value="tm" ${seltm}>Territory Manager</option>
          </select>
        </dd>
        <dt><label class="label-input">Search Value:</label></dt>
        <dd>
          <input type="text" name="val" style="width:200px" value="${val}" />
          <input type="submit" id="acctSearch" class="ui-link-button" name="submit" value="Search"/>
        </dd>
      </dl>
    </div>
    </form>

    <c:choose>
      <c:when test="${fn:length(contacts) > 0}">

        <div class="fake-table">
          <div class="header">
            <div class="row">
              <div class="cell">Account #</div>
              <div class="cell">Contact Name</div>
              <div class="cell">Phone #</div>
              <div class="cell">Email Address</div>
              <div class="cell"></div>
            </div>
          </div>
          <div class="body">
            <c:forEach items="${contacts}" var="contact" varStatus="itemStatus">
              <div class="row">
                <div class="cell">
                  <a href="<c:url value="/account/admin-details.htm?key=${param.key}&val=${param.val}&custno=${contact.customerNumber}"/>">${contact.customerNumber}</a>
                </div>
                <div class="cell"><span class="narrow">Contact Name:</span> ${contact.lastName}, ${contact.firstName}</div>
                <div class="cell"><span class="narrow">Phone #:</span>${contact.phoneWork}</div>
                <div class="cell"><span class="narrow">Email:</span>${contact.emailAddr}</div>
                <div class="cell">
                  <input type="file" accept="image/*" capture="camera">
                </div>
              </div>
            </c:forEach>
          </div>
        </div>

      </c:when>
      <c:otherwise>
        <div class="fake-table">
          <div class="header">
            <div class="row">
              <div class="cell">Customer Name</div>
              <div class="cell">Customer #</div>
              <div class="cell">Territory Manager</div>
              <div class="cell">City</div>
              <div class="cell">State</div>
              <div class="cell txt-center">Zip Code</div>
              <div class="cell txt-center">Sales</div>
            </div>
          </div>
          <div class="body">
            <c:forEach items="${accounts}" var="account" varStatus="itemStatus">
              <div class="row">
                <div class="cell">
                  <a href="<c:url value="/account/admin-details.htm?key=${param.key}&val=${param.val}&custno=${account.custno}"/>">${account.name}</a>
                </div>
                <div class="cell"><span class="narrow">Customer #:</span> ${account.custno}</div>
                <div class="cell"><span class="narrow">Territory Manager</span> ${account.slsRepOut}</div>
                <div class="cell"><span class="narrow">City</span> ${account.city}</div>
                <div class="cell"><span class="narrow">State</span> ${account.state}</div>
                <div class="cell txt-center"><span class="narrow">Zip Code:</span> ${account.zip}</div>
                <div class="cell txt-center">
                  <span class="narrow">Sales YTD:</span>
                  <span style="<c:choose><c:when test="${account.salesYtd > account.lastSalesYtd}">color:green;</c:when><c:otherwise>color:red;</c:otherwise></c:choose>">
                  <fmt:formatNumber value="${account.salesYtd}" type="currency" currencySymbol="$" pattern="¤#,##0.00;¤-#,##0.00"/>
                  /
                  <fmt:formatNumber value="${account.lastSalesYtd}" type="currency" currencySymbol="$" pattern="¤#,##0.00;¤-#,##0.00"/>
                  </span>
                </div>
              </div>
            </c:forEach>
          </div>
        </div>
      </c:otherwise>
    </c:choose>


  </jsp:body>
</t:layout>