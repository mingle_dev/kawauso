<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="/WEB-INF/kawauso-utils.tld" prefix="util"%>

<t:layout>
  <jsp:attribute name="title">
    Sales Detail
  </jsp:attribute>

    <jsp:body>
        <table style="margin-left:auto; margin-right:auto; border-collapse: collapse;padding : 0; background : #fff; font: normal 18px Helvetica;-webkit-border-radius : 8px; width : 96%;">
            <c:forEach items="${results.results}" var="row">
                <tr>
                    <td style="padding: 8px 8px 8px 8px; border-bottom : 1px solid #B4B4B4;">
                        <a href="<c:url value="/account/orders/detail.htm?orderno=${row[0]}&ordersuf=${row[1]}"/>">${row[0]}-<c:if test="${row[1] < 10}">0</c:if>${row[1]}</a><br/>
                        Customer #: ${row[2]}<br/>
                        Ship To: ${row[3]}<br/>
                        Taken By: ${row[4]}
                    </td>
                    <td style="padding: 8px 8px 8px 8px; border-bottom : 1px solid #B4B4B4; text-align:right;">
                        <span style="font:bold 18px Helvetica;">
                            <fmt:formatNumber type="currency" value="${row[5]}" />
                        </span><br/>
                        <fmt:formatNumber type="percent" maxFractionDigits="2" value="${row[8] / 100}" /> Margin
                    </td>
                </tr>
            </c:forEach>
        </table>
        <br/><br/><br/><br/>
    </jsp:body>
</t:layout>