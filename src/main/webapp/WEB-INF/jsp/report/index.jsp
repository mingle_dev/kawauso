<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="/WEB-INF/kawauso-utils.tld" prefix="util"%>

<t:layout>
  <jsp:attribute name="title">
    ${navTree[fn:length(navTree)-1].name}
  </jsp:attribute>

  <jsp:body>

      <div class="fake-table">
          <div class="body">
              <c:forEach var="entry" items="${menu}">
                  <div class="row">
                      <div class="cell" style="font-size:125%">
                          <a href="<c:url value="${entry.value}"/>">${entry.key}</a>
                      </div>
                  </div>
              </c:forEach>
          </div>
      </div>

    <br/><br/><br/><br/>
  </jsp:body>
</t:layout>