<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="/WEB-INF/kawauso-utils.tld" prefix="util"%>

<t:layout>

    <jsp:attribute name="title">
        Sales Totals
    </jsp:attribute>

  <jsp:body>

<c:set var="net" value="0"/>
<c:set var="mgn" value="0"/>
<c:set var="mgnpct" value="0"/>

<table style="margin-left:auto; margin-right:auto; border-collapse: collapse;padding : 0; background : #fff; font: normal 18px Helvetica;-webkit-border-radius : 8px; width : 96%;">
  <c:forEach items="${results.results}" var="row">
      <c:set var="net" value="${net + row[2]}"/>
      <c:set var="mgn" value="${mgn + row[3]}"/>
  <tr>
      <td style="padding: 8px 8px 8px 8px; border-bottom : 1px solid #B4B4B4;">
      <span style="font:bold 18px Helvetica;">

        <c:choose>
            <c:when test="${row[0] == 'SO'}">Stock Orders</c:when>
            <c:when test="${row[0] == 'DO'}">Direct Orders</c:when>
            <c:when test="${row[0] == 'RM'}">Return Orders</c:when>
            <c:when test="${row[0] == 'CS'}">Counter Sales</c:when>
            <c:when test="${row[0] == 'CR'}">Credit/Rebills</c:when>
            <c:otherwise>${row[0]}</c:otherwise>
        </c:choose>

      </span><br/>
          Count: ${row[1]}
      </td>
      <td style="padding: 8px 8px 8px 8px; border-bottom : 1px solid #B4B4B4; text-align:right;">
         <span style="font:bold 18px Helvetica;">
             <fmt:formatNumber type="currency" value="${row[2]}" />
         </span><br/>
          <c:choose>
              <c:when test="${row[2] == 0 || row[2] eq '0.00'}">
                  0% Margin
              </c:when>
              <c:otherwise>
                  <fmt:formatNumber type="percent" maxFractionDigits="2" value="${row[3] / row[2]}" /> Margin
              </c:otherwise>
          </c:choose>

      </td>
  </tr>
  </c:forEach>
</table>
<br/><br/><br/>

<table style="margin-left:auto; margin-right:auto; border-collapse: collapse;padding : 0; background : #fff; font: normal 18px Helvetica;-webkit-border-radius : 8px; width : 96%;">
  <tr>
      <td style="padding: 8px 8px 8px 8px; border-bottom : 1px solid #B4B4B4;">
          Total Net Sales:
      </td>
      <td style="padding: 8px 8px 8px 8px; border-bottom : 1px solid #B4B4B4; text-align:right;">
          <fmt:formatNumber value='${net}' type="currency" groupingUsed='true' />
      </td>
  </tr>
    <tr>
        <td style="padding: 8px 8px 8px 8px; border-bottom : 1px solid #B4B4B4;">
            Margin:
        </td>
        <td style="padding: 8px 8px 8px 8px; border-bottom : 1px solid #B4B4B4; text-align:right;">
            <fmt:formatNumber value='${mgn}' type="currency" groupingUsed='true' />
        </td>
    </tr>
    <tr>
        <td style="padding: 8px 8px 8px 8px; border-bottom : 1px solid #B4B4B4;">
            Margin Percent:
        </td>
        <td style="padding: 8px 8px 8px 8px; border-bottom : 1px solid #B4B4B4; text-align:right;">
            <c:choose>
                <c:when test="${net ==  0 || net eq '0.00'}">
                    0%
                </c:when>
                <c:otherwise>
                    <fmt:formatNumber value='${mgn / net}' type="percent" maxFractionDigits="2" groupingUsed='true' />
                </c:otherwise>
            </c:choose>
        </td>
    </tr>
</table>

    <br/><br/><br/><br/>
  </jsp:body>
</t:layout>