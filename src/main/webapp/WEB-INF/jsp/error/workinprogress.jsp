<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="/WEB-INF/kawauso-utils.tld" prefix="util"%>

<t:layout>
    <jsp:body>
        
        <h2>Oops! Work in Progress</h2>
        
        <p style="font-size:125%">
        Seems you have found a section that we haven't quite finished yet. 
        No worries, your visit to this page sent an email to the development team 
        reminding them how important this feature is to you.
        </p>
        
    </jsp:body>
</t:layout>