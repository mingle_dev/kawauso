<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="/WEB-INF/kawauso-utils.tld" prefix="util"%>

<t:layout>
  <jsp:body>
  <h1>Uh oh! (404 Error)</h1>
  <p style="font-size:125%;">We are really sorry but the page you requested cannot be found</p>
  <p>It seems that the page you were trying to reach doesn't exist anymore, or maybe it has just been moved.
      We think that the best thing to do is to start again from the home page.
      Feel free to contact us if the problem persists or if you definitely can't find what
      you are looking for.
  </p>
    <center>       
      <img src="https://s3.amazonaws.com/mgl1939/img/404.jpg" style="max-width:100%; max-height:100%;"/>
    </center>
  </jsp:body>
</t:layout>