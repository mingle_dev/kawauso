<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="/WEB-INF/kawauso-utils.tld" prefix="util"%>

<t:layout>
    <jsp:body>

        <div>
            <table width="100%" role="presentation">
                <tr><td width="50%"><h1>Service Bulletins</h1></td><c:if test="${dsbwriteaccess}"><td id="fileupload" width="50%" align="right" valign="center"  style="padding-bottom: 1em;"><a id="btnmanagebulletin"  class="button-link" style="color:white;" href="/web/techserv/manage.htm">Click Here To Manage Service Bulletins</a></td></c:if></tr>
            </table>
        </div>


        <script type="text/javascript" src="http://jquery.iceburg.net/jqModal/jqModal.js"></script>

        <style>

            .ui-widget-header {

                font-weight: bold;
            }

            .ui-widget-header .ui-icon {
                background-image: url('http://code.jquery.com/ui/1.10.4/themes/smoothness/images/ui-icons_222222_256x240.png');
            }

            .ui-icon-minusthick {
                background-position: 0px 0px;
            }
            .ui-icon-plusthick {
                background-position: -64px 0px;
            }

            .ui-icon {
                width: 16px;
                height: 16px;
            }
            .ui-icon {
                display: block;
                text-indent: -99999px;
                overflow: hidden;
                background-repeat: no-repeat;
            }



        </style>

        <script>
            $(function() {
                $( ".portlet" )
                        .addClass( "ui-widget ui-widget-content" )
                        .find( ".portlet-header" )
                        .addClass( "ui-widget-header portlet-header-border" )
                        .prepend( "<span class='ui-icon ui-icon-minusthick portlet-toggle'></span>");

                $( ".portlet-toggle" ).click(function() {
                    var icon = $(this);
                    icon.toggleClass( "ui-icon-minusthick ui-icon-plusthick" );
                    icon.closest( ".portlet" ).find( ".portlet-header" ).toggleClass( "portlet-header-border portlet-header-noborder" );
                    icon.closest( ".portlet" ).find( ".portlet-content" ).toggle();
                });
            });
        </script>

        <style>
            .fake-select-head {
                display: inline-block;
                background-color: #eaeaea;
                border: 1px solid #c8ccce;
                cursor: pointer;
                line-height: 24px;
                padding: 8px;

            }

            .fake-select-head a {
                font-size: 14px;
                color: #555;
                font-weight: bold;
            }

            .fake-select {
                white-space: nowrap;
                display: inline-block;
                /* IE7 inline-block fix */
                *display: inline;
                *zoom: 1;
            }

            fake-select li {
                margin: 0;
                padding: 0;
                display: inline-block;
                /* IE7 inline-block and padding fix */
                *display: inline;
                *zoom: 1;
                *vertical-align: bottom;
            }

            .fake-select,
            .fake-select ul {
                margin: 0;
                padding: 4px;
                list-style: none;
            }
            .fake-select ul {
                display: none;
                position: absolute;
                z-index: 1000000;
            }

            .fake-select-items {
                font-size: 14px;
                background: #fff;
                border: 1px solid #000;
                line-height: 1.5;
            }

            .fake-select-items li {
                padding: 0px 15px 0px 4px;
            }

            .fake-select li a {
                display: block;
            }
            .fake-select ul li {
                position: relative;
                display: block;
            }


            .square.icon {
                background: url('//mgl1939.s3-website-us-east-1.amazonaws.com/img/red_square.gif') 0px 50% no-repeat;
                padding-left: 12px;
            }

            .narrow {
                display: none;
            }


        </style>


        <script>
            $(document).ready(function () {

                $("#search-results-page, #search-results-sort").mouseover(function (e) {
                    $('>ul', this).css({ 'visibility' : 'visible', 'display' : 'block' });
                });

                $("#search-results-page, #search-results-sort").mouseout(function (e) {
                    $('>ul', this).css({ 'visibility' : 'hidden', 'display' : 'block' });
                });

                $('input[type=checkbox]').change(function (e) {
                    e.preventDefault();

                    var checkValues = $('input[type=checkbox]:checked').map(function() {
                        return $(this).val();
                    }).get();

                    var attr = "";
                    for(var i=0; i<checkValues.length; i++)
                        attr += "&attr=" + checkValues[i];

                    window.location = "<c:url value=""/>?term=${term}&id=${menuId}&page=1&limit=${limit}&" + attr;
                });
            });
        </script>

        <%--
        <div class="container" style="width: 100%; margin-bottom:16px; padding-bottom:16px;">
            <c:if test="${not empty term}">
            <div class="left" style="font-size:1.4em;">
                Search results for <b>${term}</b>
                <a href="<c:url value="/product/search.htm?id=${menuId}&page=${page}&limit=${limit}&sort=${sort}${attrStr}"/>"><img src="http://www.qc.cuny.edu/PublishingImages/close_red.gif" border="0"></a>
            </div>
            </c:if>

            <div class="right" style="font-size:1.4em;">
               ${searchCount} Products
            </div>
        </div>


        <div class="container" style="width: 100%; margin-bottom:16px; padding-bottom:16px;">
            <div class="left">

                <ul class="dropdown-menu">
                    <c:if test="${session.type eq 'ADMINISTRATOR' || session.type eq 'EMPLOYEE'}">
                    <li id="search-results-page">
                        <span class="fake-select-head">
                            <a href="<c:url value="/product/search.htm?term=${term}&id=${menuId}&page=1&limit=20&layout=advanced"/>">Worksheet</a>
                        </span>
                    </li>
                    </c:if>

                    <li id="search-results-page">
                        <span class="fake-select-head">
                            <a href="#">${limit} Results Per Page</a>
                        </span>
                        <ul class="fake-select-items">
                            <li><a href="<c:url value="/product/search.htm?term=${term}&id=${menuId}&page=1&limit=20${attrStr}"/>">20 Results Per Page</a></li>
                            <li><a href="<c:url value="/product/search.htm?term=${term}&id=${menuId}&page=1&limit=40${attrStr}"/>">40 Results Per Page</a></li>
                            <li><a href="<c:url value="/product/search.htm?term=${term}&id=${menuId}&page=1&limit=60${attrStr}"/>">60 Results Per Page</a></li>
                        </ul>
                    </li>

                    <li id="search-results-sort">
                        <span class="fake-select-head">
                            <a href="#">Sorted by ${sortKey} <c:choose><c:when test="${sortDir eq 'desc'}">(Z-A)</c:when><c:otherwise>(A-Z)</c:otherwise></c:choose></a>
                        </span>
                        <ul class="fake-select-items">
                            <li><a href="<c:url value="/product/search.htm?term=${term}&id=${menuId}&page=1&limit=${limit}&sort=prod:asc${attrStr}"/>">Sorted by Product (A-Z)</a></li>
                            <li><a href="<c:url value="/product/search.htm?term=${term}&id=${menuId}&page=1&limit=${limit}&sort=prod:desc${attrStr}"/>">Sorted by Product (Z-A)</a></li>
                            <li><a href="<c:url value="/product/search.htm?term=${term}&id=${menuId}&page=1&limit=${limit}&sort=descrip:asc${attrStr}"/>">Sorted by Description (A-Z)</a></li>
                            <li><a href="<c:url value="/product/search.htm?term=${term}&id=${menuId}&page=1&limit=${limit}&sort=descrip:desc${attrStr}"/>">Sorted by Description (Z-A)</a></li>
                            <li><a href="<c:url value="/product/search.htm?term=${term}&id=${menuId}&page=1&limit=${limit}&sort=popular:desc${attrStr}"/>">Sorted by Popularity</a></li>
                        </ul>
                    </li>
                </ul>
            </div>

            <div style="float:right;">
                <div class="ui-buttonset">
                    <c:if test="${page > 1}">
                      <a href="<c:url value="/product/search.htm?term=${term}&id=${menuId}&page=${page-1}&limit=${limit}&sort=${sortKey}:${sortDir}${attrStr}"/>" class="ui-button"> < </a>
                    </c:if>
                    <c:choose>
                        <c:when test="${numPages < 6}">
                          <c:forEach var="i" begin="1" end="${numPages}" step="1">
                            <c:choose>
                              <c:when test="${i == page}"><a href="#" class="ui-button ui-button-active"> ${i} </a></c:when>
                              <c:otherwise><a href="<c:url value="/product/search.htm?term=${term}&id=${menuId}&page=${i}&limit=${limit}&sort=${sortKey}:${sortDir}${attrStr}"/>" class="ui-button"> ${i} </a></c:otherwise>
                            </c:choose>
                          </c:forEach>
                        </c:when>
                        <c:when test="${page < 6}">
                          <c:forEach var="i" begin="1" end="5" step="1">
                            <c:choose>
                              <c:when test="${i == page}"><a href="#" class="ui-button ui-button-active"> ${i} </a></c:when>
                              <c:otherwise><a href="<c:url value="/product/search.htm?term=${term}&id=${menuId}&page=${i}&limit=${limit}&sort=${sortKey}:${sortDir}${attrStr}"/>" class="ui-button"> ${i} </a></c:otherwise>
                            </c:choose>
                          </c:forEach>
                        </c:when>
                        <c:when test="${(page + 3) > numPages}">
                          <c:forEach var="i" begin="${numPages - 4}" end="${numPages}" step="1">
                            <c:choose>
                              <c:when test="${i == page}"><a href="#" class="ui-button ui-button-active"> ${i} </a></c:when>
                              <c:otherwise><a href="<c:url value="/product/search.htm?term=${term}&id=${menuId}&page=${i}&limit=${limit}&sort=${sortKey}:${sortDir}${attrStr}"/>" class="ui-button"> ${i} </a></c:otherwise>
                            </c:choose>
                          </c:forEach>
                        </c:when>
                        <c:otherwise>
                        <c:forEach var="i" begin="${page - 2}" end="${page + 2}" step="1">
                          <c:choose>
                              <c:when test="${i == page}"><a href="#" class="ui-button ui-button-active"> ${i} </a></c:when>
                              <c:otherwise><a href="<c:url value="/product/search.htm?term=${term}&id=${menuId}&page=${i}&limit=${limit}&sort=${sortKey}:${sortDir}${attrStr}"/>" class="ui-button"> ${i} </a></c:otherwise>
                          </c:choose>
                        </c:forEach>
                        </c:otherwise>
                    </c:choose>
                    <c:if test="${page < numPages}">
                      <a href="<c:url value="/product/search.htm?term=${term}&id=${menuId}&page=${page+1}&limit=${limit}&sort=${sortKey}:${sortDir}${attrStr}"/>" class="ui-button"> > </a>
                    </c:if>
                </div>
            </div>
        </div>
        <br/><br/>
        --%>
        <center>
            [
            <c:forEach items="${smbYears}" var="year" varStatus="loop">
                <a href="<c:url value="/techserv/bulletins.htm?year=${year}"/>">${year}</a> <c:if test="${!loop.last}"> | </c:if>
            </c:forEach>
            ]
            <br/><br/>

            <form method="POST" action="<c:url value="/techserv/bulletins.htm"/>">
                Keyword or Model# Search (at least 3 characters):<br>
                <table>
                    <tr>
                        <td style="vertical-align:top;"><input type="text" name="query" size="40" id="tf_sml" class="tf_sml"></td>
                        <td style="vertical-align:top;"><input type="submit" name="submit" value="Search"></td>
                    </tr>
                </table>
            </form>
            (keyword examples:  furnace, heat pump, evolution, 30gt, 58m, 661cjx, etc.)
        </center>

        <br/><br/>

        <div class="fake-table">
            <div class="header">
                <div class="row">
                    <div class="cell nowrap" style="width:125px;">DSB</div>
                    <div class="cell">Description</div>
                    <div class="cell nowrap" style="width:100px;">Year</div>
                </div>
            </div>
            <div class="body">
                <c:forEach items="${documents}" var="document">
                    <div class="row">
                        <div class="cell">
                            <a href="<c:url value="/techserv/bulletin-download.htm?id=${document.id}"/>" style="font-size:125%;">${document.smbId}</a>
                        </div>
                        <div class="cell">
                                ${document.subject}
                        </div>
                        <div class="cell desktop-only">
                                ${document.year}
                        </div>
                    </div>
                </c:forEach>
            </div>
        </div>

    </jsp:body>
</t:layout>