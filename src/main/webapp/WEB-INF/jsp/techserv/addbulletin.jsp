<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@taglib uri="/WEB-INF/kawauso-utils.tld" prefix="util"%>
<t:layout>
    <jsp:body>
        <script>

            /*$( document ).ready(function() {

                var endIndex = document.referrer.indexOf('?');

                if (endIndex == -1)
                {
                    endIndex = document.referrer.length;
                }
                else {
                    endIndex = document.referrer.indexOf('?');
                }

                var redirectURL = document.referrer.substring(0, endIndex);
                $("#comebackUrl").val(redirectURL);
                console.log($("#comebackUrl").val());
            });*/

            function cancelButtonClicked(){

                location.href=$("#comebackUrl").val();

            };


            function validate() {
                submitFlag = true;

                if(document.getElementById("year").value < 1900 && document.getElementById("year").value < 9999 ){
                    submitFlag=false;
                    alert("year must be 4 digits and a value after 1900");
                }
                return submitFlag;
            };

        </script>
        <div>
            <h1>Add Service Bulletin</h1>
        </div>
        <c:if test="${not empty message}">
            <div class="message-text">
                <c:if test="${status == 0}">
                    <img class="message-logo" src="https://s3.amazonaws.com/mgl1939/img/red_error.png" />
                </c:if>
                <c:if test="${status == 1}">
                    <img class="message-logo" src="https://s3.amazonaws.com/mgl1939/img/green_check.png" />
                </c:if>
                    ${message}
            </div>
            <br/><br/>
        </c:if>
        <!--<div><span class="input-label" style="font-style: normal">Enter the service bulletin document information below</span></div>-->
        <div><br/></div>
        <form method="POST"
              enctype="multipart/form-data"
              action="/web/techserv/addbulletin.htm"
              onsubmit="return validate()">
            <input id="comebackUrl" name="comebackUrl" value="/web/techserv/manage.htm" type="hidden">
            <div class="input-label">Bulletin Id:</div>
            <div><input class="input-text" name="smbid" id="smbid" class="input-textwidth" placeholder="ex:TIC16-0001,DSB15-0001" style="font-size:larger; width:250px;"></div>
            <br/><br/>

            <div class="input-label">Description:</div>
            <div><input class="input-text" name="subject" id="subject" class="input-textwidth" placeholder="limit to 256 characters" style="font-size:larger; width:250px;"></div>
            <br/><br/>

            <div class="input-label">Keywords:</div>
            <div><input class="input-text" name="keywords" id="keywords" class="input-textwidth" placeholder="limit to 256 characters" style="font-size:larger; width:250px;"></div>
            <br/><br/>

            <div class="input-label">Year:</div>
            <div><input class="input-text" name="year" id="year" class="input-textwidth" placeholder="enter 4-digit year(i.e. 2016)" style="font-size:larger; width:250px;"></div>
            <br/><br/>

            <div class="input-label">Select file to upload:</div>
            <div>
                <input id="uploadedfile" name="uploadedfile" type="file"/>
                <!--<input id="password" name="password" type="password" class="input-textwidth">-->
            </div>
            <br/><br/>

            <div>
                <input type="submit" id="submit" name="pwdButton" value="Add Bulletin">
                <input type="button" id="cancel" name="cancelBtn" value="Cancel" onClick="javascript:cancelButtonClicked()" />
                <input type="reset" id="clear" name="clearBtn" value="Clear" style="height:38px; width: 80px; background:#D11600; color: white; text-align: center;font-size:12px;"/>
            </div>
        </form>
        <br/>
    </jsp:body>
</t:layout>