<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="/WEB-INF/kawauso-utils.tld" prefix="util"%>

<t:layout>
  <jsp:body>

<h1>Training Course</h1>      

<div class="container border">
    <h2 class="header active">${course.name}</h2>
    <div class="section" style="font-size:125%; padding:15px;">
        ${course.description}
    </div>
</div>
<br/><br/>

<div class="fake-table">
    <div class="header">
        <div class="row">
            <!-- <div class="cell nowrap">Event #</div> -->
            <div class="cell">Event Date</div>
            <div class="cell">Event Location</div>
            <div class="cell">Event Fee</div>
            <div class="cell">Event Length</div>
       </div>
    </div>
    <div class="body">
        <c:forEach items="${course.trainingEvents}" var="event">
        <div class="row">
            <!-- <div class="cell">
                <div class="narrow"><b>Event #:</b></div>
                <a href="<c:url value="/techserv/training/event.htm?id=${event.id}"/>">${event.eventNumber}</a>
            </div> -->
            <div class="cell">
                <div class="narrow"><b>Date:</b></div>
                ${event.startDt}
            </div>         
            <div class="cell">
                <div class="narrow"><b>Location:</b></div>
                ${event.location}
            </div>
            <div class="cell">
                <div class="narrow"><b>Fee:</b></div>
                ${event.fee}
            </div>
            <div class="cell">
                <div class="narrow"><b>Length:</b></div>
                ${event.length}
            </div>            
        </div>
        </c:forEach>
    </div>
</div>
<br/><br/>To register, print and complete the <a href="https://s3.amazonaws.com/mgl1939/docs/training_register_form.pdf">registration form</a>. Email the completed form to <a href="mailto:servicetraining@mingledorffs.com?subject=Training Registration">servicetraining@mingledorffs.com</a>
       or fax form to 770-239-2137. <i>Registration must be received no later than <b>one week</b> prior to class start date.</i> All classes, instructors and fees are subject to revision and/or cancellation.
<br/><br/><br/><br/>

  </jsp:body>
</t:layout>