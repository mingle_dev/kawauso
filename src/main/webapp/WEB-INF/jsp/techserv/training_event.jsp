<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="/WEB-INF/kawauso-utils.tld" prefix="util"%>

<t:layout>
  <jsp:body>

<h1>Training Event</h1>      
   
<div class="container border">
    <h2 class="header active">${event.course.name}</h2>
    <div class="section" style="font-size:125%;">
        <dl>
            <dt>Description:</dt><dd>${event.course.description}</dd>
            <dt>Location:</dt><dd>${event.location}</dd>
            <dt>Date:</dt><dd>${event.startDt}</dd>
            <dt>Length:</dt><dd>${event.length}</dd>
            <dt>Fee:</dt><dd>${event.fee}</dd>
        </dl>
    </div>
</div>
<br/><br/>
    
<div class="container border">
    <div class="section" style="background-color:#EFEFEF; padding:4px;">
        All classes, instructors, locations, and tuitions are subject to change. 
        If the student cannot attend, you may send a substitute. If you fail to 
        cancel your registration within the 24-hour period prior to the class you 
        may be billed up to $50.00 per student.
    </div>
</div>
<br/><br/>



<form id="regForm">
    <input id="student_name"  type="hidden"   value="${session.contact.firstName} ${session.contact.lastName}"/>
    <input id="customer_no"   type="hidden"   value="1" <%--value="${session.customerNumber}"/>>--%> />
    <input id="course_name"   type="hidden"   value="${event.course.name}"/>
    <input id="location"      type="hidden"   value="${event.location}"/>
    <input id="start_date"    type="hidden"   value="${event.startDt}"/>
    <input id="course_length" type="hidden"   value="${event.length}" />
    <input id="fee"           type="hidden"   value="${event.fee}"  />

    <input id="regCheckBox" type="checkbox" style="padding-bottom: 1em;"> I agree that myself or someone on my behalf has authorized my attendance to this class (Registration button will not work until you agree).</input>

  <table>
        <tr style=""border: none; padding-top:1em;">
            <td><button  type="button" id="regButton" name="regButton" form="regForm" disabled="disabled" <%--onclick="register();"--%>>Register</button></td>
            <td><div id="email_sent" hidden style="font-size:medium;">Your registration was submitted and emailed to our staff. Thank you!</div></td>
        </tr>
    </table>

</form>

      <div id="registration"></div>
      <script type="text/javascript">
                  $(document).ready( function() {

                              $("#regCheckBox").click( function(){

                                  if($('#regCheckBox').is(':checked')) {
                                      $('#regButton').prop('disabled',false);

                                  } else {
                                      $('#regButton').prop('disabled',true);

                                  }
                              })
                          }

                  );
      </script>
      <script>
         // function register()

          $(document).ready(function(){

              $("#regButton").click(function(){
                  $("#email_sent").show(0).delay(5000).hide(0);

                  //call ajax here
                  $.ajax({
                              url: "/web/training/register/email.htm",
                              type: "post",
                              data: {
                                         studentname: $("#student_name").val(),
                                         customer: $("#customer_no").val(),
                                         coursename: $("#course_name").val(),
                                         location: $("#location").val(),
                                         startdate: $("#start_date").val(),
                                        // courselength: $("course_length").val(),
                                         fee: $("#fee").val()
                                    },
                              success: function(data)
                                     {
                                          $("#email_sent").html("Your registration email has been sent. Thank you!");
                                          $("#regCheckBox").prop('checked',false);
                                          $("#regButton").prop('disabled',true);

                                     },
                              error: function(data)   { $("#email_sent").html("There was an error in sending your email. Sorry. Please call!");}
                          }
                  );



                  return false;
              });
          });
      </script>
  </jsp:body>
</t:layout>