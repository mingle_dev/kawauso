<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="/WEB-INF/kawauso-utils.tld" prefix="util"%>

<t:layout>
    <jsp:body>

        <div>
            <table width="100%">
                <tr><td width="50%"><h1>Manage Service Bulletins</h1></td><td width="50%" align="right"><a id="btnaddbulletin' hiddden="" class="button-link" style="color:white;" href="/web/techserv/addbulletin.htm?username=${util:base64Encode(session.user.username)}">Add Service Bulletin</a></td></tr>
            </table>
        </div>

        <c:if test="${not empty message}">
            <div class="message-text">
                <c:if test="${status == 0}">
                    <img class="message-logo" src="https://s3.amazonaws.com/mgl1939/img/red_error.png" />
                </c:if>
                <c:if test="${status == 1}">
                    <img class="message-logo" src="https://s3.amazonaws.com/mgl1939/img/green_check.png" />
                </c:if>
                    ${message}
            </div>
            <br/><br/>
        </c:if>

        <script type="text/javascript" src="http://jquery.iceburg.net/jqModal/jqModal.js"></script>

        <style>

            .ui-widget-header {

                font-weight: bold;
            }

            .ui-widget-header .ui-icon {
                background-image: url('http://code.jquery.com/ui/1.10.4/themes/smoothness/images/ui-icons_222222_256x240.png');
            }

            .ui-icon-minusthick {
                background-position: 0px 0px;
            }
            .ui-icon-plusthick {
                background-position: -64px 0px;
            }

            .ui-icon {
                width: 16px;
                height: 16px;
            }
            .ui-icon {
                display: block;
                text-indent: -99999px;
                overflow: hidden;
                background-repeat: no-repeat;
            }



        </style>

        <script>
            $(function() {
                $( ".portlet" )
                        .addClass( "ui-widget ui-widget-content" )
                        .find( ".portlet-header" )
                        .addClass( "ui-widget-header portlet-header-border" )
                        .prepend( "<span class='ui-icon ui-icon-minusthick portlet-toggle'></span>");

                $( ".portlet-toggle" ).click(function() {
                    var icon = $(this);
                    icon.toggleClass( "ui-icon-minusthick ui-icon-plusthick" );
                    icon.closest( ".portlet" ).find( ".portlet-header" ).toggleClass( "portlet-header-border portlet-header-noborder" );
                    icon.closest( ".portlet" ).find( ".portlet-content" ).toggle();
                });
            });

            function onclickDelete(url, smbid) {
                var x;
                if (confirm("Confirm deletion of service bulletin " + smbid) == true) {

                    window.location = url;

                } else {

                    x = "You pressed Cancel!";
                }
                document.getElementById("demo").innerHTML = x;
            }

        </script>

        <style>
            .fake-select-head {
                display: inline-block;
                background-color: #eaeaea;
                border: 1px solid #c8ccce;
                cursor: pointer;
                line-height: 24px;
                padding: 8px;

            }

            .fake-select-head a {
                font-size: 14px;
                color: #555;
                font-weight: bold;
            }

            .fake-select {
                white-space: nowrap;
                display: inline-block;
                /* IE7 inline-block fix */
                *display: inline;
                *zoom: 1;
            }

            fake-select li {
                margin: 0;
                padding: 0;
                display: inline-block;
                /* IE7 inline-block and padding fix */
                *display: inline;
                *zoom: 1;
                *vertical-align: bottom;
            }

            .fake-select,
            .fake-select ul {
                margin: 0;
                padding: 4px;
                list-style: none;
            }
            .fake-select ul {
                display: none;
                position: absolute;
                z-index: 1000000;
            }

            .fake-select-items {
                font-size: 14px;
                background: #fff;
                border: 1px solid #000;
                line-height: 1.5;
            }

            .fake-select-items li {
                padding: 0px 15px 0px 4px;
            }

            .fake-select li a {
                display: block;
            }
            .fake-select ul li {
                position: relative;
                display: block;
            }


            .square.icon {
                background: url('//mgl1939.s3-website-us-east-1.amazonaws.com/img/red_square.gif') 0px 50% no-repeat;
                padding-left: 12px;
            }

            .narrow {
                display: none;
            }


        </style>


        <script>
            $(document).ready(function () {

                $("#search-results-page, #search-results-sort").mouseover(function (e) {
                    $('>ul', this).css({ 'visibility' : 'visible', 'display' : 'block' });
                });

                $("#search-results-page, #search-results-sort").mouseout(function (e) {
                    $('>ul', this).css({ 'visibility' : 'hidden', 'display' : 'block' });
                });

                $('input[type=checkbox]').change(function (e) {
                    e.preventDefault();

                    var checkValues = $('input[type=checkbox]:checked').map(function() {
                        return $(this).val();
                    }).get();

                    var attr = "";
                    for(var i=0; i<checkValues.length; i++)
                        attr += "&attr=" + checkValues[i];

                    window.location = "<c:url value=""/>?term=${term}&id=${menuId}&page=1&limit=${limit}&" + attr;
                });
            });
        </script>


        <center>
            [
            <c:forEach items="${smbYears}" var="year" varStatus="loop">
                <a href="<c:url value="/techserv/manage.htm?year=${year}&username=${util:base64Encode(session.user.username)}"/>">${year}</a> <c:if test="${!loop.last}"> | </c:if>
            </c:forEach>
            ]
            <br/><br/>

            <form method="POST" action="<c:url value="/techserv/manage.htm"/>">
                Keyword or Model# Search (at least 3 characters):<br>
                <table>
                    <tr>
                        <td style="vertical-align:top;"><input type="text" name="query" size="40" id="tf_sml" class="tf_sml"></td>
                        <td style="vertical-align:top;"><input type="submit" name="submit" value="Search"></td>
                    </tr>
                </table>
            </form>
            (keyword examples:  furnace, heat pump, evolution, 30gt, 58m, 661cjx, etc.)
        </center>

        <br/><br/>

        <div class="fake-table">
            <div class="header">
                <div class="row">

                    <div class="cell nowrap" style="width:125px;">DSB</div>
                    <div class="cell">Description</div>
                    <div class="cell nowrap" style="width:100px;">Year</div>
                    <div class="cell nowrap" style="width:10px; text-align:right;"></div>
                </div>
            </div>
            <div class="body">
                <c:forEach items="${documents}" var="document">
                    <div class="row">
                        <div class="cell">
                            <a href="<c:url value="/techserv/bulletin-download.htm?id=${document.id}"/>" style="font-size:125%;">${document.smbId}</a>
                        </div>
                        <div class="cell">
                                ${document.subject}
                        </div>
                        <div class="cell desktop-only">
                                ${document.year}
                        </div>
                        <div class="cell">
                            <a onclick="javascript:onclickDelete('${pageContext.request.contextPath}/techserv/deletebulletin.htm?year=${document.year}&id=${document.id}&smbid=${document.smbId}','${document.smbId}');">Delete</a>
                        </div>
                    </div>
                </c:forEach>
            </div>
        </div>

    </jsp:body>
</t:layout>
