<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="/WEB-INF/kawauso-utils.tld" prefix="util"%>

<t:layout>
  <jsp:body>

<h1>Training</h1>      
<h3>Strengthen your skills, broaden your expertise</h3>

<p style="font-size:125%;">
Mingledorff's Inc. offers a comprehensive portfolio of technical training and 
education services designed for individuals, companies, and public organizations 
to acquire, maintain, and optimize their HVAC skills.	
</p><br/>

<h2>What we currently offer.</h2>

<div class="fake-table">
    <div class="header">
        <div class="row">
            <div class="cell nowrap">Course Name</div>
            <div class="cell">Course Description</div>
       </div>
    </div>
    <div class="body">
        <c:forEach items="${courses}" var="course">
        <div class="row">
            <div class="cell">
                <a href="<c:url value="/techserv/training/course.htm?id=${course.id}"/>">${course.name}</a>
            </div>            
            <div class="cell">
                ${course.description}
            </div>         
        </div>
        </c:forEach>
    </div>
</div>
<br/><br/><br/><br/>
  </jsp:body>
</t:layout>