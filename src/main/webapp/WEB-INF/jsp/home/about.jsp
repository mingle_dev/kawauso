<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<t:layout>
  <jsp:body>

<p>
Walter Lee Mingledorff, Jr. founded the Company in 1939. A graduate of Georgia Tech with a degree in mechanical engineering, Mr. Mingledorff was 25 years old at the time. Mingledorff's was originally a franchised installation contractor purchasing heating and air conditioning equipment directly from the Carrier Air Conditioning Company. The Company was located in Savannah, Georgia, with a territory that encompassed the eastern lower half of the state of Georgia and part of the Florida Panhandle, including Tallahassee.
</p>
<p>
Our very first job was to install heating and air conditioning equipment in the home of Mr. Mingledorff's father-in-law in Savannah. The original compressor that was installed in 1939 is now on display at the corporate headquarters in Norcross, Georgia. This equipment was still operating when it was replaced in 1988--49 years after it was first started.
</p>
<p>
During World War II, the Carrier Corporation, headquartered in Syracuse, New York, built tank components for the U.S. Army. Mingledorff's ceased operations as a business and Lee Mingledorff went to work for the Savannah Machine and Foundry, building mine sweepers for the Navy. Other employees were also put to work for the defense industry, or they went off to war. At the end of the war, the Company was restarted as a direct contractor/distributor with the Carrier Air Conditioning Company. With demand for consumer goods at an all time high after the war, Mingledorff's diversified into the retail appliance business and even invented and installed shrimp boat refrigeration equipment. In fact, the first shrimp boat ever specifically built to be refrigerated from the keel up contained our equipment.
</p>
<p>
During the late 1950's, we underwent some major consolidations. Our franchise with Carrier was changed to a wholesale distributorship only. Our contracting business, operating by that time in five cities, was sold to the branch managers running those businesses. The appliance business was sold to the manager of the appliance division. The shrimp boat refrigeration business, which had never made a profit, was closed.
</p>
<p>
In 1958, while all of this was going on, Lee Mingledorff was mayor of Savannah and had no desire to move to Atlanta, which was just beginning to become the transportation and commerce hub of the new South. Ed Eckles, then vice-president (with a background in accounting), became President and the organization moved to Atlanta. Ed Eckles had been the Credit Manager at the Savannah Machine and Foundry during the war and had been employed ever since the restart in 1945. Also in 1958, we hired the two wholesale salesmen working for Carrier Atlanta--the other direct contractor/distributor for Carrier. One of those two salesmen was Bob Kesterton, who became President of the Company in 1969, upon the retirement of Ed Eckles.
</p>
<p>
From 1969 until 1995, Bob Kesterton and his team grew the company from 5 million in total sales to 97 million total sales in 1995. During this time period, the company was able to navigate through the mid-70s residential housing downturn and energy crisis. In addition, Mingledorff's multi-location expansion began with the opening of our first satellite location in Marietta, Georgia in 1983 and in 1985 we moved our headquarters to Norcross, Georgia-- our present location.
</p>
<p>
In 1990, Mingledorff's expanded our product offering acquiring the Bryant air conditioning distribution rights for the territory. Mingledorff's continued to expand in the early 90's under Bob's leadership and ended 1995 with 9 distribution locations including 3 district headquarters located in Savannah, Macon, and Norcross.
</p>
<p>
In November 1995, Bud Mingledorff took over as President and Mr. Kesterton became Chairman of the Board. Under Mr. Mingledorff's leadership the company continued its expansion and diversification in the HVAC business. During the late 1990's Mingledorff's Inc. opened several new satellite locations including Conyers, Athens, Gainesville, Valdosta, Albany, Columbus and Warner Robins. In addition, several locations went through major expansion, such as Augusta and Lawrenceville.
</p>
<p>
January 2008 brought more change when Bud Mingledorff retired and became Chairman of the Board. David Kesterton assumed the duties of President of the company with Bob Kesterton serving as Chairman Emeritus. February 2008 announced the expansion of AirOn Supply, Inc into Nashville, Tennessee with the Tempstar line. The Company looks to continue expansion and diversification, focusing on adding value for our stakeholders, our customers and creating opportunities for all our employees who seek personal and professional growth.
</p>
<p>
Since 1958, Mingledorff's concentrated on our core business as a wholesale distributor to air conditioning contractors and industrial accounts. We have expanded the business with the addition of numerous product lines and regional locations of offices, warehouses, and parts stores. No more retail appliances and shrimp boats!
</p>

  </jsp:body>
</t:layout>