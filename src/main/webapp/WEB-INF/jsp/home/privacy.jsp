<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<t:layout>
  <jsp:body>
    <br><br>
    <p style="text-align: center;"><b>Privacy Statement</b></p>

    <p style="text-align: center;"><em>(Updated 2018)</em></p>

    <p style="text-align: justify;">Mingledorff&rsquo;s, Inc. and its subsidiaries (the &ldquo;<u>Company</u>&rdquo; or &ldquo;We&rdquo;) respect the privacy of every individual and his or her company who visits our website at PORTAL.MINGLEDORFFS.COM or communicates with us electronically.&nbsp; Your privacy is very important to us.&nbsp; We have prepared this Privacy Statement to inform You about how we collect, use and disclose the information You entrust to us.&nbsp;</p>

    <p style="text-align: justify;"></p>

    <h5 style="text-align: justify;">Information We Collect and How We Use It</h5>

    <p style="text-align: justify;">Our web servers collect general data pertaining to each site visitor including but not limited to, the IP address, tracking codes and patterns, the domain names, the web pages visited, website activity, referral sites, the key clicks and the length of time spent on the website.&nbsp; The Website may use cookies and tracking technology depending on the feature offered, including the use of Google Analytics which is utilized to track unique cookies across multiple devises.&nbsp; To learn about how Google Analytics collects and processes data You can go to <a href="http://www.google.com/policies/privacy/partners/">www.google.com/policies/privacy/partners/</a>.&nbsp; Personal information cannot be collected via cookies and other tracking technology, however if you previously provided personal information, cookies may be tied to such personal information.&nbsp; All of this information is used to improve the customer&rsquo;s experience while online and to further improve our Company&rsquo;s marketing efforts.&nbsp;</p>

    <p style="text-align: justify;">We also collect company information and data obtained through the Website or electronic communications with our representatives to provide services and information, process product orders and respond to customer inquiries, questions and comments.&nbsp; We may use this information to contact companies with special offers and promotions, to notify companies of new products, and for other lawful and legitimate purposes. &nbsp;If you do not wish to receive promotional or product information, you may opt-out of future communications by following the instructions on the email communication or send an email to unsubscribe@mingledorffs.com.&nbsp;</p>

    <p style="text-align: justify;">By sharing this information through use of the Website, You are consenting to this privacy policy.&nbsp;</p>

    <h5 style="text-align: justify;">Our Security</h5>

    <p style="text-align: justify;">To help prevent unauthorized access, maintain data accuracy, and to ensure the correct use of information, we have implemented appropriate physical, electronic and managerial procedures to assist us in safeguarding and securing the information we collect online.&nbsp; However, as with any Internet website, it is not possible to guarantee that the Website will not be attacked or that information you provided may not be compromised. &nbsp;</p>

    <h5 style="text-align: justify;">Sharing Information</h5>

    <p style="text-align: justify;">Company respects your privacy.&nbsp; We will not sell any of the information we collect to third parties.&nbsp; We may provide aggregate statistics to reputable parties, our affiliates and business partners about the visitors to our Website, but these statistics will not include any personal identifying information about You. &nbsp;</p>

    <p style="text-align: justify;">We may share information provided by our visitors with service providers and contractors we have retained to perform the services on our behalf.&nbsp; We reserve the right to transfer any information we have about you in the event we sell or transfer all or a portion of our business or assets.&nbsp; &nbsp;</p>

    <p style="text-align: justify;">We may disclose information about you (i) if we are required to do so by law, subpoena or other legal process, (ii) to law enforcement authorities or other governmental officials, or (iii) when we believe disclosure is necessary or appropriate to prevent physical harm, fraud, or financial loss in connection with an investigation of suspected or actual illegal activity or activity that otherwise violates our <a href="/terms-of-use"><u>Terms of Use</u></a>.</p>

    <h5 style="text-align: justify;">Other Information Collectors</h5>

    <p style="text-align: justify;">Except as otherwise expressly discussed in this Privacy Statement, this document only addresses the use and disclose of information we collect from You.&nbsp; Our Website may include links to the websites of our business partners, vendors and advertisers.&nbsp; To the extent You disclose Your information to other parties, You are subject to the privacy customs and policies of that third party.&nbsp; We are unsure whether these third parties have Privacy Statements similar to ours, and therefore, the Company cannot be responsible for the content or the privacy practices employed by any third party.&nbsp;</p>

    <p style="text-align: justify;">Your use of this Website constitutes Your consent to the collection and use of this information by the Company.&nbsp; If we change our Privacy Statement, we will update this page of the Website so You will always be aware of how we use Your information.</p>

    <h5 style="text-align: justify;">Reviewing and Changing Your Personally Identifiable Information</h5>

    <p style="text-align: justify;">You have the right to access and correct Your personal information.&nbsp; You can request access to&nbsp; Your personal information by sending an e-mail to webadmin@mingledorffs.com or by notifying us in writing at Mingledorff&rsquo;s, Inc., Website Administrative Services, 6675 Jones Mills Road, Peachtree Corners, GA 30092. &nbsp;For more information about Google Analytics, including information about how to opt-out of these technologies go to Google Analytics Data <a href="https://support.google.com/analytics/topic/2919631?hl=en&amp;ref_topic=1008008">Privacy &amp; Security.</a></p>

    <h5 style="text-align: justify;"><strong>Questions and Contact Information</strong></h5>

    <p style="text-align: justify;">If You have any questions, concerns or comments about this Privacy Statement, please send an email to webadmin@mingledorffs.com.&nbsp; You can send written comments to: Mingledorff&rsquo;s, Inc., Legal Department, 6675 Jones Mills Road, Peachtree Corners, GA 30092.</p>


  </jsp:body>
</t:layout>