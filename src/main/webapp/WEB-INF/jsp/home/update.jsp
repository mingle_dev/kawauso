<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<t:layout>
  <jsp:body>

<BR>

<p style="font-weight: bold; font-size:1.2em; padding:4px;">
Mingledorff’s is announcing the end of life for WeatherZone (anticipated 5/31/2019).
</p>


<ul>
<li style="font-size:1.2em; padding:4px; ">
Our updated eCommerce website, Singlesource, has been live since our Dealer Meeting (2/21/2019).
</li>
<li style="font-size:1.2em; padding:4px; ">
You can visit Singlesource at <a href="https://singlesource.mingledorffs.com" target="blank">https://singlesource.mingledorffs.com</a>
</li>
<li style="font-size:1.2em; padding:4px; ">
We are currently working on ensuring that the following resources are also available with the new website:
	<ul>
	<li style="font-size:1.0em; padding:4px; ">Account Information</li>
	<li style="font-size:1.0em; padding:4px; ">Pricebooks</li>
	<li style="font-size:1.0em; padding:2px; ">Service  Bulletins</li>
	</ul>
</li>
<li style="font-size:1.2em; padding:4px; ">
Please speak with your TM to transition to Mingledorff’s new eCommerce website as soon as possible.
</li>
<li style="font-size:1.2em; padding:4px; ">
We can also transfer all existing saved lists for you to Singlesource – please make sure your TM initiates that request.
</li>
<li style="font-size:1.2em; padding:4px; ">
If you have any questions, please ask your TM.
</li>
</ul>


  </jsp:body>
</t:layout>