<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<t:layout>
  <jsp:body>
    <br>
    <br>
    <p style="text-align: center;"><b>Terms of Use</b></p>

    <p style="text-align: center;"><em>(Last Updated 2018)</em></p>

    <p style="text-align: justify;"></p>

    <p style="text-align: justify;">IMPORTANT! THESE TERMS OF USE (&ldquo;<u>TERMS</u>&rdquo;) GOVERN YOUR USE OF THE PORTAL.MINGLEDORFFS.COM WEBSITE AND ANY MOBILE APPLICATION RELATED THERETO (&ldquo;<u>WEBSITE</u>&rdquo;) PROVIDED BY MINGLEDORFF&rsquo;S, INC. AND ITS SUBSIDIARIES AND AFFILIATES (THE &ldquo;<u>COMPANY</u>&rdquo;).&nbsp; BY ACCESSING, USING, REGISTERING FOR OR PURCHASING PRODUCTS FROM OR THROUGH THE WEBSITE, YOU ACCEPT, WITHOUT LIMITATION OR QUALIFICATIONS, THE FOLLOWING TERMS.</p>

    <p style="text-align: justify;"></p>

    <ol>
      <li style="text-align: justify;"><strong>Permitted Use.</strong> &nbsp;Company grants you a limited, personal, non-exclusive, non-transferable license to use the Website for your own personal, internal business use.&nbsp; You will not and will not authorize any other person to (i) Co-brand the Website or portion thereof, or (ii) frame the Website or portion thereof (whereby the Website or portion thereof will appear on the same screen with a portion of another website).&nbsp; &ldquo;Co- branding&rdquo; means to display a name, logo, trademark, or other means of attribution or identification of any party in such a manner reasonably likely to give a user the impression that such other party has the right to display, publish, or distribute the Website or content accessible within the Website.&nbsp; You agree to cooperate with the Company in causing any unauthorized Co-branding, framing or linking to immediately cease.</li>
    </ol>

    <p style="margin-left: 0.5in; text-align: justify;"></p>

    <ol>
      <li style="text-align: justify;" value="2"><strong>Restrictions.</strong>&nbsp; You may use the Website for lawful purposes only.&nbsp; You may not post or transmit through the Website any material which violates or infringes in any way upon the rights of others, which is unlawful, threatening, abusive, defamatory, libelous, invasive of privacy or publicity rights, vulgar, obscene, profane or otherwise objectionable, which encourages conduct that would constitute a criminal offense, gives rise to civil liability or otherwise violates the law.&nbsp; The Company will fully cooperate with any law enforcement authorities or court order requesting or directing the Company to disclose the identity of anyone posting such information or materials.</li>
    </ol>

    <p style="margin-left: 0.5in; text-align: justify;"></p>

    <ol>
      <li style="text-align: justify;" value="3"><strong>Member Accounts.</strong>&nbsp; You are responsible for maintaining the confidentiality of the User ID and password and are fully responsible for all activities that occur in connection with your password or account, whether or not you authorize such activities. You agree to immediately notify Company of any unauthorized use of either your password or account or any other breach of security. &nbsp;You warrant that You possess the legal authority, right and freedom to use and make purchases through the Website on behalf of the person or entity You represent.&nbsp; To learn more about how Company protects the privacy of the personal information in your account, please visit our <a href="/privacy-statement">Privacy Statement.</a> You agree to provide true, accurate, current, and complete information when registering and update such information.&nbsp; Company reserves the right to suspend or terminate your account without notice for any reason and to refuse any and all current and future use of the Website.&nbsp;</li>
    </ol>

    <p style="margin-left: 0.5in; text-align: justify;"></p>

    <ol>
      <li style="text-align: justify;" value="4"><strong>Product Orders.&nbsp; </strong>All purchases made through this Website are subject to the <a href="<c:url value="/sales-terms.htm"/>"><u>Terms and Conditions of Sale</u></a>.&nbsp; While our goal is a 100% error-free site, Company does not guaranty that any content is accurate or complete, including price information and product specifications.&nbsp; If Company discovers price errors, they will be corrected on our systems as soon as possible, and the corrected price will apply to Your order.&nbsp;</li>
    </ol>

    <p style="text-align: justify;"></p>

    <ol>
      <li style="text-align: justify;" value="5"><strong>Trademarks.&nbsp; </strong>The trademarks, logos, and service marks (collectively the &ldquo;<u>Trademarks</u>&rdquo;) displayed on the Website are registered and unregistered Trademarks of Company and its licensors, content providers, and business partners.&nbsp; Nothing contained on the Website should be construed as granting by implication, estoppels, or otherwise, any license or right to use any Trademark displayed on the Website without the written permission of the Company or such party that may own the Trademark.&nbsp; Your use of the Trademarks displayed on the Website, or any other content on the Website, except as provided in these Terms, is strictly prohibited.&nbsp; The Company will aggressively enforce its intellectual property rights to the fullest extent of the law, including seeking criminal prosecution.</li>
    </ol>

    <p style="text-align: justify;"></p>

    <ol>
      <li style="text-align: justify;" value="6"><strong>Proprietary Information.&nbsp; </strong>The content accessible within the Website, including without limitation, all articles, press releases, Trademarks, graphics, charts, screen captures, clipart, text, links, product specifications, product information and specifications, pricing and other information (collectively, the &ldquo;<u>Content</u>&rdquo;) is the proprietary information of the Company and its content providers, and the Company and its content providers retain all right, title, and interest in the Content. &nbsp;You agree that You won&rsquo;t attempt to or in fact do the following: download, modify, reproduce, adapt, translate, reverse engineer, create any derivative works based upon, publicly display, sell, rent, license or in any way commercially exploit any portion of the Website or its Content, except as expressly permitted under these Terms.&nbsp; You will not remove, alter, or cause to be removed or altered, any copyright, trademark, trade name, service mark, or any other proprietary notice or legend appearing on any of the Content.</li>
    </ol>

    <p style="margin-left: 0.5in; text-align: justify;"></p>

    <ol>
      <li style="text-align: justify;" value="7"><strong>Indemnity.&nbsp; </strong>You will indemnify and hold the Company, its licensors, content providers, service providers and contractors, and their employees, agents, officers and directors (collectively the &ldquo;<u>Indemnified Parties</u>&rdquo;) harmless from Your breach of these Terms and Your use of Content other than as expressly authorized in these Terms.&nbsp; You agree that the Indemnified Parties will have no liability in connection with any such breach or unauthorized use, and agree to indemnify any and all resulting loss, damages, judgments, awards costs, expenses, and attorneys&rsquo; fees of the Indemnified Parties in connection therewith.&nbsp; You will also indemnify and hold the Indemnified Parties harmless from and against any claims brought by third parties arising out of Your use of the information accessed from the Website.</li>
    </ol>

    <p style="margin-left: 0.5in; text-align: justify;"></p>

    <ol>
      <li style="text-align: justify;" value="8"><strong>Disclaimers.</strong>&nbsp; Your use of and browsing in the Website are at your sole risk.&nbsp; THE WEBSITE AND ALL CONTENT ARE PROVIDED TO YOU &ldquo;AS IS&rdquo; WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, IMPLIED WARRANTIES OF MERCHANTABILITY, INFRINGEMENT, TITLE OR FITNESS FOR A PARTICULAR PURPOSE.&nbsp; THE COMPANY DOES NOT WARRANT THAT THE WEBSITE, PRODUCT INFORMATION OR CONTENT WILL BE ERROR FREE OR UNINTERRUPTED; NOR DOES THE COMPANY MAKE ANY WARRANTY AS TO THE RESULTS THAT MAY BE OBTAINED FROM USE OF THE WEBSITE, PRODUCT INFORMATION OR CONTENT OR AS TO THE ACCURACY, COMPLETENESS, TRUTHFULNESS, RELIABILITY OF ANY INFORMATION PROVIDED THROUGH THE WEBSITE.&nbsp; Some jurisdictions may not allow the exclusions of certain implied warranties, so some of the above exclusions may not apply to You.</li>
    </ol>

    <p style="margin-left: 0.5in; text-align: justify;"></p>

    <p style="margin-left: 0.5in; text-align: justify;">The Company does not guarantee or warrant the files available for downloading from the Internet will be free of infection or viruses, worms, Trojan horses or other code that manifest contaminating or destructive properties.&nbsp; You are responsible for implementing sufficient procedures and checkpoints to satisfy Your particular requirements for accuracy of data input and output, and for maintaining a means external to the Website for the reconstruction of any lost data.&nbsp; The Company does not assume any responsibility or risk for Your use of the Internet.&nbsp;</p>

    <p style="margin-left: 0.5in; text-align: justify;"></p>

    <ol>
      <li style="text-align: justify;" value="9"><strong>Limitation of Liability.</strong>&nbsp; THE COMPANY, ITS LICENSORS, SERVICE PROVIDERS, CONTENT PROVIDERS, AND THEIR EMPLOYEES, AGENTS, OFFICERS AND DIRECTORS (COLLECTIVELY THE &ldquo;<u>COMPANY AND ITS AFFILIATES</u>&rdquo;) WILL NOT BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, CONSEQUENTIAL, OR SPECIAL DAMAGES, INCLUDING LOSS OF REVENUE OR INCOME, PAIN AND SUFFERING, EMOTIONAL DISTRESS OR SIMILAR DAMAGES ARISING OUT OF OR IN ANY WAY CONNECTED WITH YOUR USE OF OR ACCESS TO THE WEBSITE, CONTENTS OR INABILITY TO USE THE WEBSITE, OR OTHERWISE ARISING IN CONNECTION WITH THIS WEBSITE, EVEN IF THE COMPANY HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.&nbsp; IN NO EVENT WILL THE COLLECTIVE LIABILITY OF THE COMPANY AND ITS AFFILIATES (REGARDLESS OF THE FORM OF ACTION, WHETHER IN CONTRACT, TORT OR OTHERWISE) EXCEED THE AMOUNT YOU HAVE PAID TO THE COMPANY FOR THE APPLICABLE CONTENT OR SERVICE OUT OF WHICH LIABILITY AROSE, OR IF YOU HAVEN&rsquo;T MADE ANY PAYMENTS TO COMPANY, THEN THE LIABILITY OF COMPANY AND ITS AFFILIATES SHALL NOT EXCEED $100.&nbsp;</li>
    </ol>

    <p style="margin-left: 0.5in; text-align: justify;"></p>

    <ol>
      <li style="text-align: justify;" value="10"><strong>License.</strong> By communication with the Company (by email or otherwise) or participating in any discussion forum, You grant the Company an irrevocable, non-exclusive, transferable and world wide royalty-free right and license to use, reproduce, modify, adapt, translate, publicly perform, and display, distribute, sublicense, create derivative works from, and sell any information, message, suggestion, idea or concept You communicate to the Company or post on the Website (in whole or in part) for any purpose the Company chooses, commercial, public or otherwise.&nbsp; Please review our <a>Privacy Statement</a>&nbsp;to see how we collect and use personal information.&nbsp;&nbsp;</li>
    </ol>

    <p style="margin-left: 0.5in; text-align: justify;"></p>

    <ol>
      <li style="text-align: justify;" value="11"><strong>Links.</strong>&nbsp; This Website may contain hyperlinks to websites operated by third parties other than the Company.&nbsp; Such hyperlinks are provided for Your references only.&nbsp; The Company does not control such third party websites and is not responsible for their content or the products and services offered on the third party websites.&nbsp; The Company&rsquo;s inclusion of the hyperlinks to such websites does not imply any endorsement of the material on such website or any association with their operations.&nbsp; Operators of the other websites may not maintain links to this Website without the Company&rsquo;s prior written consent.&nbsp; The Company reserves the right to prohibit other websites from linking to this Website.&nbsp;</li>
    </ol>

    <p style="margin-left: 0.5in; text-align: justify;"></p>

    <ol>
      <li style="text-align: justify;" value="12"><strong>Changes.</strong>&nbsp; The Company has the right at any time to change or discontinue any aspect or feature of the Website.&nbsp; These Terms are subject to change by the Company at any time in its discretion.&nbsp; Your use of the Website after such changes are implemented constitutes Your acceptance of the changes.&nbsp; Please consult these Terms regularly.</li>
    </ol>

    <p style="margin-left: 0.5in; text-align: justify;"></p>

    <ol>
      <li style="text-align: justify;" value="13"><strong>Miscellaneous.</strong>&nbsp; The Section headings of these Terms are for ease of reference only and shall not be admissible in any action to alter, modify or interpret the contents of any Section hereof.&nbsp; If any Section or provision of these Terms are held illegal, unenforceable or in conflict with any law by a court of competent jurisdiction or arbitral tribunal, such Section or provision shall be deemed severed from these Terms and the validity of the remainder of these Terms shall not be affected thereby.&nbsp; These Terms shall be governed by and construed in accordance with the laws of Georgia applicable to contracts made and to be enforced wholly within such state.&nbsp; The waiver by either party of any provision of these Terms on any occasion and upon any particular circumstances shall not operate as a waiver of such provision of these Terms on any other occasion or upon any other circumstances.&nbsp; These Terms may be waived and amended only in writing signed by both You and the Company.</li>
    </ol>

    <p paraeid="{aab2252f-acc6-4a22-86c8-238ceaec8ae3}{97}" paraid="169127596" style="text-align: justify;"></p>

      
  </jsp:body>
</t:layout>