<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<t:layout>
    <jsp:body>
     
<script src="http://maps.google.com/maps?file=api&amp;v=2&amp;key=ABQIAAAA1KqmDyrtzKr6_BY-F83ICxTBBXLAhocxbEceXcMb2aXGQJ8IrRQ2jUJaDtCeiKVjCLn924_j-KtoYA" type="text/javascript"></script>

<script>
$().ready(function() {    
    if (GBrowserIsCompatible()) {
        var map = new GMap2(document.getElementById("map"));
        map.addControl(new GLargeMapControl());
        map.addControl(new GMapTypeControl());

        var point = new GLatLng( parseFloat("${location.latitude}"), parseFloat("${location.longitude}") );
        map.addOverlay( new GMarker(point) ); 

        map.setCenter(new GLatLng(parseFloat("${location.latitude}"), parseFloat("${location.longitude}")), 13);    
    }
    
    $(".location-set").click(function(e) {
        e.preventDefault();

        $.ajax({
            url: "<c:url value="/locations/set.htm"/>",
            type: "post",
            data: { 'code' : $(this).data('value') },
            beforeSend: function() {
                //change button to indicate background process
            },
            success: function(data) {
                console.log(data);
                data = JSON.parse(data);
                noty( { type: 'information', text: 'Location now set to ' + data.state + data.whseId + ' - ' + data.whseName } );
            },
            error:function() {
                noty( { type: 'error', text: 'An error occurred changing locations' } );
            }
        });

    });
});
</script>


<h1>${location.state}${location.whseId} - ${location.whseName}, ${location.state} ${location.zipCd}</h1>
<div class="container">
    <a class="ui-link-button" href='tel:${location.phoneNo}'>Call</a>
    <c:if test="${not empty session.warehouse && session.warehouse != location.whseId}">
    <%--<a class="ui-link-button location-set" href='#' data-value="${location.whseId}">Set as my Location</a>--%>
    <a class="ui-link-button" href='<c:url value="/locations/set.htm?code=${location.whseId}"/>'>Set as my Location</a>
    </c:if>
</div>

<h2>Address</h2>
<div class="container">
    ${location.addr}<br/>
    ${location.whseName}, ${location.state} ${location.zipCd}<br/>
    Phone #: ${location.phoneNo}<br/>
    Email: ${location.emailAddr}
    <br/><br/><br/>
</div>

<div id="map" style="width: 100%; height: 500px; border:1px solid #333;"></div>        

    </jsp:body>
</t:layout>