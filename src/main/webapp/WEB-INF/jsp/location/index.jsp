<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<t:layout>
    <jsp:body>
      
<script>
var latitude = 0;
var longitude = 0;

var error = false;

$().ready(function() {

    $("#locSearchKey").change( function() {
        switch( $(this).val() ) {
            case "loc":
                $("#locSearchVal").val('${session.warehouse}');
                $("#locSearchVal").attr("placeholder", "City, State");
                break;
            case "geo":
                if(navigator.geolocation) {
                    navigator.geolocation.getCurrentPosition(handle_geolocation_query, handle_errors);
                    $("#locSearchVal").val('');
                    $("#locSearchVal").attr("placeholder","Searching...");
                }else{
                    $("#locSearchVal").val('Unsupported');
                }
                break;
            default:
                $("#locSearchVal").val('');
                $("#locSearchVal").attr("placeholder","Zip Code");
        }
    });

    jQuery("#geoLocInit").click(fetchLocations);
});               

function handle_errors(error)
{
    switch(error.code)
    {
        case error.PERMISSION_DENIED: $("#locSearchVal").val('Not Shared');
        break;

        case error.POSITION_UNAVAILABLE: $("#locSearchVal").val('Not Detected');
        break;

        case error.TIMEOUT: $("#locSearchVal").val('Timed Out');
        break;

        default: $("#locSearchVal").val('Error');
        break;
    }
}

function handle_geolocation_query(position)
{
    $("#locSearchVal").val(position.coords.latitude + ',' + position.coords.longitude);
}  

function fetchLocations() {
    values = {};

    $.ajax({
        url: "<c:url value="/locations/list.htm"/>",
        type: "post",
        data: values,
        beforeSend: function() {
            //change button to indicate background process
            $(".fake-table .body").html('');
        },
        success: function(data) {
            var objArr = jQuery.parseJSON(data);
            $.each( objArr, function(val) {

                $(".fake-table .body").append(
                        '<div class="row"> ' +
                        '<div class="cell">'+ 
                        '  <b>Store Code:</b> ' + objArr[val]['state'] + objArr[val]['whseId'] + '<br/>' +
                        '  <b>Phone #:</b> ' + objArr[val]['phoneNo'] + '<br/>' +
                        '  <b>Fax #</b> ' + objArr[val]['phoneNo'] + '<br/>' +
                        '  <b>Email:</b> ' + objArr[val]['emailAddr'] + '<br/>' +
                        '</div>' +
                        '<div class="cell">' +
                        objArr[val]['addr'] + '<br/>' +
                        objArr[val]['whseName'] + ', ' + objArr[val]['state'] + ' ' + objArr[val]['zipCd'] + '<br/>' +
                        '<a href="">Map and Directions</a><br/><br/>' +
                        '<a href="">Make '+ objArr[val]['state'] + objArr[val]['whseId'] + ' your Branch</a>' +
                        '</div>' +
                        '</div>'
                );
            });
        },
        error:function(){
            console.log("error");
        }
    });
}
</script>
   
<c:if test="${not empty currentLocation}">        
<div class="sb-right" style=" margin-bottom:10px;">
    <div class="container border">
        <h2 class="header active">
            My Branch
        </h2>
        <div class="container" style="padding:10px;">
            <a href="<c:url value="/locations/detail.htm?code=${currentLocation.whseId}"/>"><strong>${currentLocation.state}${currentLocation.whseId}</strong></a><br/>
            ${currentLocation.addr}<br/>
            ${currentLocation.whseName}, ${currentLocation.state} ${currentLocation.zipCd}<br/>
            Phone #: <a href="tel:${currentLocation.phoneNo}">${currentLocation.phoneNo}</a><br/>
            <a href="mailto:${currentLocation.emailAddr}">${currentLocation.emailAddr}</a>
        </div>
    </div>
</div>        
</c:if>       
        
<div class="content" style="min-width:286px; margin-bottom:10px;">
    <div class="container border">
        <h2 class="header active">
            Location Search
        </h2>
        <div class="section" style="padding:10px; font-size:125%; line-height: 1.5em; min-height:70px;">
            <form id="location">
                <div style="float:left; padding-right:6px; padding-bottom: 12px;">
                    <b>Search By:</b>
                    <select id="locSearchKey" name="locSearchKey">
                        <option value="zip">Zip Code</option>
                        <option value="loc">Closest to</option>
                        <option value="geo">Current Location</option>
                    </select>
                </div>
                <div style="float:left;">
                    <input type="text" id="locSearchVal" name="locSearchVal" value="" placeholder="Zip Code">
                    <button id="geoLocInit">Search</button>       
                </div>
            </form>
        </div>
    </div>
</div>
 
<div style="clear:both;"></div>        

<c:if test="${fn:length(locations) gt 0}">
<div class="fake-table">
    <div class="header">
        <div class="row">
            <div class="cell">Location</div>
            <div class="cell">Address</div>
        </div>
    </div>
    <div class="body">
        <c:forEach items="${locations}" var="location" varStatus="itemStatus">
            <div class="row">
                <div class="cell">
                    <b>Store Code:</b> <a href="<c:url value="/locations/detail.htm?code=${location.whseId}"/>">${location.state}${location.whseId}</a><br/>
                    <b>Phone #:</b> ${location.phoneNo}<br/>
                    <b>Fax #</b> ${location.phoneNo}<br/>
                    <b>Email:</b> ${location.emailAddr}<br/>
                </div>
                <div class="cell">
                    ${location.addr}<br/>
                    ${location.whseName}, ${location.state} ${location.zipCd}<br/>
                    <a href="<c:url value="/locations/detail.htm?code=${location.whseId}"/>">Map and Directions</a><br/><br/>
                    <a href="<c:url value="/locations/set.htm?code=${location.whseId}"/>">Make ${location.state}${location.whseId} your Branch</a>
                </div>
            </div>
        </c:forEach>
    </div>
</div>
</c:if>    
    
<div style="padding-top:20px;">
    <a href="<c:url value="/locations.htm?locSearchKey=all&locSearchVal=true"/>">View all Mingledorff's locations</a>
</div>

    </jsp:body>
</t:layout>