
<c:set var="root" value="${pageContext.request.contextPath}" />

<!DOCTYPE html>

<html>
<head>
    <title>Mingledorff's Portal</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, maximum-scale=1, minimum-scale=1">
    <meta name="decorator" content="home" />
    <meta name="Keywords" content="Mingledorff's HVAC Distribution Carrier Bryant Payne Heil" />
    <meta name="Description" content="Mingledorff's has been in the HVAC business for over 75 years. Serving Georgia, Florida, Alabama, South Carolina and Mississippi." />
    <link rel="shortcut icon" href="http://www.mingledorffs.com/wp-content/themes/mingledorff/favicon.ico" />

    <!-- 08/7/2017<script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/jquery/1.10.2/jquery.min.js" async></script> -->
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="/resources/demos/style.css">
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

    <!-- 08/7/2017<link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css" rel="stylesheet" type="text/css"/>-->
    <!--<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js" async></script>-->
    <!-- 08/7/2017<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/jquery-ui.min.js" async></script> -->

    <link rel="stylesheet" type="text/css" href="/web/style.css" media="all" />
    <link rel="stylesheet" type="text/css" href="/web/print-test.css" media="print" />
    <!-- 08/7/2017 <script type="text/javascript" src="http://code.jquery.com/ui/1.11.2/jquery-ui.js" async></script> -->
    <script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/modernizr/2.7.1/modernizr.min.js" async></script>
    <script type="text/javascript" src="http://mgl1939.s3-website-us-east-1.amazonaws.com/js/noty/packaged/jquery.noty.packaged.min.js" async></script>
    <script type="text/javascript" src="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js" async></script>

<!--
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
    <script src="https://gitcdn.github.io/bootstra
    p-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
-->
    <!--
    <link rel="stylesheet" type="text/css" href="/web/style.css" media="all" />
    <link rel="stylesheet" type="text/css" href="/web/print-test.css" media="print" />

    <link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css" rel="stylesheet" type="text/css"/>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/jquery-ui.min.js"></script>
    <script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    -->


    <!-- <script type="text/javascript" src="http://code.jquery.com/ui/1.11.2/jquery-ui.js"></script> -->

    <!-- Bootstrap - Latest compiled and minified JavaScript -->
<!--
    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <!-- <script type="text/javascript" src="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script> -->
<!--
    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/modernizr/2.7.1/modernizr.min.js"></script>
    <script type="text/javascript" src="//mgl1939.s3-website-us-east-1.amazonaws.com/js/noty/packaged/jquery.noty.packaged.min.js"></script>
    <script src="/web/jquery.ui.datepicker.min.js"></script> -->
<!--
    <link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css" rel="stylesheet" type="text/css"/>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/jquery-ui.min.js"></script>
    <link rel="stylesheet" type="text/css" href="/web/style.css" media="all" />
    <link rel="stylesheet" type="text/css" href="/web/print-test.css" media="print" />

    <script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <script type="text/javascript" src="http://code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/modernizr/2.7.1/modernizr.min.js"></script>
    <script type="text/javascript" src="//mgl1939.s3-website-us-east-1.amazonaws.com/js/noty/packaged/jquery.noty.packaged.min.js"></script>
-->
    <script type="text/javascript">
        var BASE_PATH = '<c:url value="/"/>';
        var USER_ID = '<c:url value="${session.userId}"/>';
    </script>
    <script type="text/javascript" src="<c:url value="/jqModal.min.js"/>" async></script>
    <script type="text/javascript" src="<c:url value="/scripts.js"/>" async></script>
    <script type="text/javascript" src="<c:url value="/hogan.js"/>" async></script>
    <script type="text/javascript" src="<c:url value="/jquery.typeahead.js"/>" async></script>
    
<style>
    .ui-datepicker {
        z-index:3000;
    }
    #cover {
        position: absolute;
        height: 100%;
        width: 100%;
        z-index: 1; /* make sure logout_box has a z-index of 2 */
        background-color: none;
        display: none;
    }

    .button-link {
        padding: 10px 15px;
        background: #4479BA;
        color: #FFF;
        -webkit-border-radius: 4px;
        -moz-border-radius: 4px;
        border-radius: 4px;
        border: solid 1px #20538D;
        text-shadow: 0 -1px 0 rgba(0, 0, 0, 0.4);
        -webkit-box-shadow: inset 0 1px 0 rgba(255, 255, 255, 0.4), 0 1px 1px rgba(0, 0, 0, 0.2);
        -moz-box-shadow: inset 0 1px 0 rgba(255, 255, 255, 0.4), 0 1px 1px rgba(0, 0, 0, 0.2);
        box-shadow: inset 0 1px 0 rgba(255, 255, 255, 0.4), 0 1px 1px rgba(0, 0, 0, 0.2);
        -webkit-transition-duration: 0.2s;
        -moz-transition-duration: 0.2s;
        transition-duration: 0.2s;
        -webkit-user-select:none;
        -moz-user-select:none;
        -ms-user-select:none;
        user-select:none;
    }
    .button-link:hover {
        background: #356094;
        border: solid 1px #2A4E77;
        text-decoration: none;
    }
    .button-link:active {
        -webkit-box-shadow: inset 0 1px 4px rgba(0, 0, 0, 0.6);
        -moz-box-shadow: inset 0 1px 4px rgba(0, 0, 0, 0.6);
        box-shadow: inset 0 1px 4px rgba(0, 0, 0, 0.6);
        background: #2E5481;
        border: solid 1px #203E5F;
    }

.update {
        font-size: 1.2em;
        padding-left: 0px;
        background-color: none;
        color: #f44248;
        text-align: right;
    	}
.shutdownLink {
        color: #f44248;
        text-decoration: underline;
    	}   	
</style>
    <style>
        .toggle.android { border-radius: 0px;}
        .toggle.android .toggle-handle { border-radius: 0px; }
    </style>
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-86923084-1', 'auto');
        ga('send', 'pageview');

    </script>
</head>

<body>
<script>
    $(document).ready(function() {
        var resizeTimer,
                $window = $(window);

        function imageresize()
        {
            if ($window.width() < 1200 && $window.width() > 900) {
                $('#img-logo-id').height("88px");
            }
            else if ($window.width() <= 900 && $window.width() > 650) {
                $('#img-logo-id').height("80px");
            }
            else if ($window.width() <= 650 && $window.width() > 440) {
                $('#img-logo-id').height("64px");
            }
            else if ($window.width() <= 440) {
                $('#img-logo-id').height("60px");
            }
            else {
                $('#img-logo-id').height("100px");
            }
        }
        imageresize();//Triggers when document first loads

        $window.resize(function() {
            //clearTimeout(resizeTimer);
            requestAnimationFrame(imageresize);
            //resizeTimer = setTimeout(imageresize, 100);
        });

    });

</script>

<div id="cover"></div>

<div id="dialog" class="jqmWindow"></div>    
    
<div id="wrap">

	<div id="header">
	    <div class="width-fix">
	        <div id="logo"  style="white-space: nowrap; width:80%;">
				<img id="img-logo-id" src="http://mgl1939.s3-website-us-east-1.amazonaws.com/img/Ming-Inc-logo-2015.png" alt="Mingledorff's Inc." title="" height="100px"/>
	        </div>
	    </div>
	    <div class="mobile-search">
	            <div class="header-sprite hdr-sprite-search left"></div>
	    </div>
	
	</div>

	<div id="controls">
	    <div class="width-fix">
	
	        <ul class="menu nav-left">
	            <li class="mobile-only">
	                <div>
	                <a href="<c:url value="/resources.htm"/>">
	                    <div class="header-sprite hdr-sprite-tools left"></div>
	                </a>
	                </div>
	            </li>            
	        </ul>
	        <ul class="menu nav-right">
	        </ul>
	    </div>
	</div>


    <div id="content">
    <br/>
	<table>
		<tr>
		<td width="15%"></td>
		<td>
			<p style="font-weight: bold; padding:4px;">
				Mingledorff's WeatherZone end of life.
			</p>
			
			<ul>
			<li style="padding:4px; ">
			Our updated eCommerce website, Singlesource, has been live since our Dealer Meeting (2/21/2019).
			</li>
			<li style="padding:4px; ">
			You can visit Singlesource at <a href="https://singlesource.mingledorffs.com" target="blank">https://singlesource.mingledorffs.com</a>
			</li>
			
			<li style="padding:4px; ">
			Please speak with your TM to transition to Mingledorff's new eCommerce website as soon as possible.
			</li>
			<li style="padding:4px; ">
			We can also transfer all existing saved lists for you to Singlesource - please make sure your TM initiates that request.
			</li>
			<li style="padding:4px; ">
			If you have any questions, please ask your TM.
			</li>
			</ul>
		</td>
		<td width="15%"></td>
		</tr>
	</table>
		

    
	    <c:choose>
	    <c:when test="${not empty ignoreWidthFix && ignoreWidthFix == true}">
	    <div>
	    </c:when>
	    <c:otherwise>
	    <div class="width-fix" style="padding: 0px 10px 0px 10px;">
	    </c:otherwise>
	    </c:choose>
    </div>
</div>

<div id="test-foot" class="footer">
    <div class="width-fix">
        <table border="0" width="100%">
            <tr>
                <td id="test-foot4">
                    <div id="foot-social-wrap" class="social-container">
                        <div class="subtitle">KEEP CONNECTED</div>
                        <a href="http://www.facebook.com/mingledorffs" target="_blank" class="social left" id="facebook"></a>
                        <a href="http://www.linkedin.com/company/104105?trk=tyah&trkInfo=tas%3Amingledorff%2Cidx%3A2-1-6" target="_blank" class="social left" id="linkedin"></a>
                        <a href="http://twitter.com/mingledorffs" target="_blank" class="social left" id="twitter"></a>
                        <a href="http://www.youtube.com/mingledorffs" target="_blank" class="social left" id="youtube"></a>
                    </div>					
                </td>
            </tr>
        </table>
        <div style="padding-left:10px; color: #fff;" class="foot-menu">
            Copyright &copy; 2013 Mingledorff's Inc. - All rights reserved. 
        </div> 
    </div>
</div>
                
</body>
</html>