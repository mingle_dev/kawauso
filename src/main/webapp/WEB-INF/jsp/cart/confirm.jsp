<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="/WEB-INF/kawauso-utils.tld" prefix="util"%>

<t:layout>
    <jsp:body>

<div class="section" style="width:100%;">
    <div class="left">
        <h2>Order Confirmation ${results.orderDetails.orderNo}-<c:if test="${results.orderDetails.orderSuf < 10}">0</c:if>${results.orderDetails.orderSuf}</h2>
    </div>

    <div class="follow-sprite follow-sprite-active left" style="margin-top:9px">
        <a href="#" style="height:22px; display:block;" title="Click to follow"></a>
    </div>
</div>

<c:if test="${not empty message}">
<div class="section" style="width:100%; padding-bottom:10px;">
    <img src="https://s3.amazonaws.com/mgl1939/img/red_error.png" border="0" style="width:32px; vertical-align:middle"> ${message}
</div>
</c:if>

<div id="details-main" class="container border">    
    <h2 class="header active">Order Information</h2>
    <div class="section" style="line-height: 1.5em;">
        <div class="half">
            <dl>
                <dt>Status:</dt><dd>${results.orderDetails.stageText}&nbsp;</dd>
                <dt>Order Date:</dt><dd><fmt:formatDate pattern="MM/dd/yyyy" value="${results.orderDetails.enterDt}"/>&nbsp;</dd>
                <dt>Request Date:</dt><dd><fmt:formatDate pattern="MM/dd/yyyy" value="${results.orderDetails.reqShipDt}"/>&nbsp;</dd>
                <dt>Ship Date:</dt><dd><fmt:formatDate pattern="MM/dd/yyyy" value="${results.orderDetails.reqShipDt}"/>&nbsp;</dd>
                <dt>Ship Via:</dt><dd>${results.orderDetails.shipVia}&nbsp;</dd>
                <dt>Terms:</dt><dd>${results.orderDetails.termsDesc}&nbsp;</dd>
                <dt>PO #:</dt><dd>${results.orderDetails.custPo}&nbsp;</dd>
                <dt>Warehouse:</dt><dd>${results.orderDetails.whse}&nbsp;</dd>
                <dt>Reference:</dt><dd>${results.orderDetails.reference}&nbsp;</dd>
                <dt>Instructions:</dt><dd>${results.orderDetails.instructions}&nbsp;</dd>
            </dl>
            <br/>
        </div>
        <div class="half">
            <dl>
            <b>Bill To Address:</b><br/>
            ${results.orderDetails.shipToName}<br/>
            ${results.orderDetails.shipToAddr1}<br/>
            ${results.orderDetails.shipToAddr2}<br/>
            ${results.orderDetails.shipToCity}, ${results.orderDetails.shipToState} ${results.orderDetails.shipToZip}<br/>
            <br/>
            <b>Ship To Address:</b><br/>
            ${results.orderDetails.shipToName}<br/>
            ${results.orderDetails.shipToAddr1}<br/>
            ${results.orderDetails.shipToAddr2}<br/>
            ${results.orderDetails.shipToCity}, ${results.orderDetails.shipToState} ${results.orderDetails.shipToZip}<br/>
            </dl>
        </div>
    </div>
</div>
<br/>

<div class="fake-table">
    <div class="header">
        <div class="row">
            <div class="cell">Product</div>
            <div class="cell txt-center">Qty Ordered</div>
            <div class="cell txt-center">Qty Shipped</div>
            <div class="cell txt-right">Price</div>
            <div class="cell txt-right">Ext. Price</div>
        </div>
    </div>
    <div class="body">
        <c:forEach items="${results.orderDetails.orderLines}" var="line" varStatus="itemStatus">
        <div class="row">
            <div class="cell">
                <a href="<c:url value="/product/detail.htm?prod=${util:base64Encode(line.shipProd)}"/>">${line.description}</a><br/>
               <b>SKU:</b> ${line.shipProd}
            </div>
            <div class="cell txt-center"><span class="narrow">Quantity Ordered</span> ${line.qtyOrder}</div>
            <div class="cell txt-center"><span class="narrow">Quantity Shipped:</span> ${line.qtyShip}</div>
            <div class="cell txt-right"><span class="narrow">Price:</span>
                <fmt:formatNumber value="${line.price}" type="currency" currencySymbol="$" pattern="¤#,##0.00;¤-#,##0.00"/>
            </div>
            <div class="cell txt-right"><span class="narrow">Ext. Price:</span>
                <fmt:formatNumber value="${line.netAmount}" type="currency" currencySymbol="$" pattern="¤#,##0.00;¤-#,##0.00"/>
            </div>
        </div>
        </c:forEach>
    </div>
</div>
<br/>

<div class="right" style="padding-right:5px;">
    Sub Total: <fmt:formatNumber value="${results.orderDetails.subTotal}" type="currency" currencySymbol="$" pattern="¤#,##0.00;¤-#,##0.00"/><br/>
    Taxes: <fmt:formatNumber value="${results.orderDetails.taxAmount}" type="currency" currencySymbol="$" pattern="¤#,##0.00;¤-#,##0.00"/><br/><br/>
    <b>Order Total: <fmt:formatNumber value="${results.orderDetails.invAmount}" type="currency" currencySymbol="$" pattern="¤#,##0.00;¤-#,##0.00"/></b>
</div>
<br/><br/>

    </jsp:body>
</t:layout>