<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<t:layout>
  <jsp:body>

<style>
    .enroll dd{
        padding-bottom:20px;
    }
</style>
          
      
      <h1>Mingledorff's Retail Spiff Enrollment</h1>
      
Please review the information below. All fields are required.      

<form:form method="POST" commandName="spiffEnrollForm" action="/web/spiffs/enroll.htm"> 
    <dl class="enroll">
        <dt><label class="label-input">Name:</label></dt>
        <dd><form:input path="fullName" value="${user.fullName}" style="width:200px" /></dd>
        
        <dt><label class="label-input">Dealership:</label></dt>
        <dd><form:input path="dealership" style="width:200px;" /></dd>
        
        <dt><label class="label-input">*Street Address:</label></dt>
        <dd><form:input path="addrStreet" style="width:200px;" /></dd>
        
        <dt><label class="label-input">*City, St Zip:</label></dt>
        <dd>
            <form:input path="addrCity" style="width:125px;" />,
            <form:input path="addrState" style="width:50px;" />
            <form:input path="addrZip" style="width:75px;" /><br/>
            *The address where you would like your checks mailed
        </dd>
        
        <dt><label class="label-input">Email Address:</label></dt>
        <dd><form:input path="emailAddr" style="width:225px;" /></dd>
        
        <dt><label class="label-input">Conditions of Use:</label></dt>
        <dd>
            <form:textarea path="conditions" rows="5" cols="35"></form:textarea></br>
            By Clicking on 'Accept' below you are agreeing to the conditions of use above.
        </dd>
        
        <dt><label class="label-input"></label></dt>
        <dd>
            <input type="submit" name="submit" value="Accept"/>
        </dd>
        
    </dl>
</form:form>
        
  </jsp:body>
</t:layout>