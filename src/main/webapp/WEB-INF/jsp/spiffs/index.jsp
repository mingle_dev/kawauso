<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="/WEB-INF/kawauso-utils.tld" prefix="util"%>

<t:layout>
  <jsp:body>
      <!-- Implementation of JQuery 1.11 functionality overwrote .datapicker functionality -->
      <div class="modal fade" id="myModal" style="display: block;width: 500px;background: red;height: 300px;font-size: 15px;/* font-weight: bold; *//* font-family: -webkit-body; */color: white;">
          <div class="modal-header" style="border-bottom: none">
              <a class="close" data-dismiss="modal">×</a>
          </div>
          <div class="modal-body">
              <p>As of Monday, May 30, 2017, WeatherZone retail sales spiff claims functionality will no longer be available.  Please contact Carol Caldwell if you have any questions.</p>
          </div>
          <div class="modal-footer" style="border-top: none">
              <a data-dismiss="modal" class="btn">Close</a>
          </div>
      </div>
      <div class="left" style="padding:8px;"><h1>2016-2017 Retail Spiff Program</h1></div>
<div class="right" style="padding:8px;">

    <c:if test="${spiffType == 2 or spiffType == 3}">
        <img src="https://s3.amazonaws.com/mgl1939/content/images/new_carrier_opt.jpeg"/>
    </c:if>
    <c:if test="${spiffType == 1 or spiffType == 3}">
        <img src="https://s3.amazonaws.com/mgl1939/content/images/new_bryant_opt.jpeg"/>
    </c:if>
</div>
<div class="clear"></div>
<br/>

<div style="font-size:115%;">
    Your total claim amount this year is: <fmt:formatNumber value="${totClaimAmt}" type="currency"/> based on ${numClaims} claims.
</div>
<br/>
      <c:if test="${session.access == 'SPIFFS'}">
<div class="right">
    [
    <c:if test="${spiffType == 1 or spiffType == 3}">
        <a href="<c:url value="/spiffs/add.htm?type=B"/>">New Bryant Claim</a>
    </c:if>
    <c:if test="${spiffType == 3}">
        |
    </c:if>
    <c:if test="${spiffType == 2 or spiffType == 3}">
        <a href="<c:url value="/spiffs/add.htm?type=C"/>">New Carrier Claim</a>
    </c:if>
    ]
</div>
    </c:if>
<br/>

<div class="fake-table">
    <div class="header">
        <div class="row">
            <div class="cell">Claim #</div>
            <div class="cell">Homeowner Name</div>
            <div class="cell center">Entered Date</div>
            <div class="cell center">Approved Date</div>
            <div class="cell center">Paid Date</div>
            <div class="cell right">Spiff Amount</div>
       </div>
    </div>
    <div class="body">
        <c:forEach items="${claims}" var="claim">
        <div class="row">
            <div class="cell">
                <div class="narrow"><b>Claim #:</b></div>
                <a href="<c:url value="/spiffs/detail.htm?id=${claim.id}"/>">${claim.claimNumber}</a>
            </div>
            <div class="cell">
                <div class="narrow"><b>Customer Name:</b></div>
                ${claim.customerName}
            </div>
            <div class="cell center">
                <div class="narrow"><b>Entered Date:</b></div>
                ${claim.enteredDate}
            </div>
            <div class="cell center">
                <div class="narrow"><b>Approved Date:</b></div>
                ${claim.approvedDate}
            </div>   
            <div class="cell center">
                <div class="narrow"><b>Paid Date:</b></div>
                ${claim.paidDate}
            </div>            
            <div class="cell right">
                <div class="narrow"><b>Amount:</b></div>
                <fmt:formatNumber value="${claim.amount}" type="currency"/>
            </div>            
        </div>
        </c:forEach>
    </div>
</div>
<br/><br/><br/><br/>
      <script type='text/javascript'>
          $(document).ready(function(){
              $('#myModal').modal('show');
          });
      </script>
  </jsp:body>
</t:layout>