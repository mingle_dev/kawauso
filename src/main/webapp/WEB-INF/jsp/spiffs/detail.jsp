<%--
  Created by IntelliJ IDEA.
  User: cbyrd
  Date: 5/13/15
  Time: 10:45 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="/WEB-INF/kawauso-utils.tld" prefix="util"%>

<t:layout>
    <jsp:body>
        <br/>
        <h2>Spiff Claim Number Details: ${claim.claimNumber}</h2>
        <div style="width:100%; padding-top: 1em; padding-bottom: 5em;">
            <div id ="claim_info">
                <div align="center" style="bottom-padding: 1em;"><h2 class="header active" style="width: auto; text-align: left; font-size: 1.5em;">Claim Detail Information</h2></div>
                <table width="100%">
                    <tr><td width="25%" style="padding-left: 100px; font-weight: bold;">Customer:</td><td width="25%">${claim.customerName}</td>
                        <fmt:formatDate value="${claim.installDate}" var="installDate"
                                        type="date" pattern="MMM dd, yyyy" />
                        <td style="font-weight: bold; padding-left: 100px; ">Installation Date:</td><td width="25%">${installDate}</td>
                    </tr>
                    <tr><td width="25%" style="padding-left: 100px; font-weight: bold;">Address:</td><td width="25%">${claim.addrStreet}</td>
                        <fmt:formatDate value="${claim.enteredDate}" var="enteredDate"
                                        type="date" pattern="MMM dd, yyyy" />
                        <td style="font-weight: bold; padding-left: 100px; ">Date Entered:</td><td width="25%">${enteredDate}</td>
                    </tr>
                    <tr>
                        <td width="25%" style="padding-left: 100px; font-weight: bold;">City:</td><td width="25%">${claim.addrCity}</td>

                        <fmt:formatDate value="${claim.approvedDate}" var="approvedDate"
                                        type="date" pattern="MMM dd, yyyy" />

                        <td style="font-weight: bold; padding-left: 100px; ">Approval Date:</td><td width="25%">${approvedDate}</td>
                    </tr>
                    <tr>
                        <td width="25%" style="padding-left: 100px; font-weight: bold;">State:</td><td width="25%">${claim.addrState}</td>

                        <fmt:formatDate value="${claim.paidDate}" var="paidDate"
                                        type="date" pattern="MMM dd, yyyy" />

                        <td style="font-weight: bold; padding-left: 100px; ">Date Paid:</td><td width="25%">${paidDate}</td>
                    </tr>
                    <tr><td width="25%" style="padding-left: 100px; font-weight: bold;" >Zip:</td><td width="25%">${claim.addrZip}</td><td></td><td></td></tr>
                    <tr><td width="25%" style="padding-left: 100px; font-weight: bold;">Amount Paid:</td>
                        <fmt:formatNumber type="currency" value="${claim.amount}" var="claimAmount"/>
                        <td width="25%">${claimAmount}</td><td width="25%" ></td>
                        <td width="25%"></td>
                    </tr>
                    <tr class="blank-row">
                        <td colspan="4"></td>
                    </tr>
                </table>
                <br/>
            </div>
            <br/><br/>
            <div id = "claim-info-details">
                    ${details}
            </div>
        </div>
        <br/><br/><br/>
    </jsp:body>
</t:layout>