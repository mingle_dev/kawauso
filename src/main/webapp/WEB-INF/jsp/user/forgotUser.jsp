<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<t:layout>
  <jsp:body>
        <h1>Forgot Username</h1>
        ${message}
        Enter the email address for your mingledorffs.com account. You will receive an email with your username.
        <br/><br/>
        <form action="<c:out value="/user/forgotPass.htm"/>" method="post">
            <b style="font-size:125%">Email Address:</b><br/><input type="text" name="p_username" style="width:250px"><br/><br/>
            <input type="submit" name="continue" value="Continue"><br/><br/>
        </form>
  </jsp:body>
</t:layout>