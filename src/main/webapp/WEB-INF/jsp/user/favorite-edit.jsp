<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<t:layout>
  <jsp:body>
<style>
.border {
    border: 1px solid #c8ccce;
}
.pad10 {
    padding: 10px;
}

.nowrap {
    white-space:nowrap;
}

input[readonly] {
    background-color: #eaeaea;
}
</style>

<%-- ${session} --%>

<form:form method="POST" commandName="profileForm" action="/web/user/favorite-maint.htm">
    
<div id="details-main" class="container border">    
    <h2 class="header active">Edit Favorite</h2>
    <div class="section pad10">

<input type="hidden" name="id" value="${favorite.id}" />
<input type="hidden" name="url" value="${favorite.url}" />

<table border="0" width="100%">
    <tr>
        <td>
            <dl class="wide">
                <dt><label class="label-input">Name:</label></dt>
                <dd><input type="text" name="name" value="${favorite.name}" style="width:225px" /><br/><br/></dd>

                <dt><label class="label-input">Link Icon:</label></dt>
                <dd>

                    <select name="link_icon">
                        <option value="fav_star75.png">Star</option>
                        <option value="fav_link75.png" <c:if test="${favorite.linkIcon == 'fav_link75.png'}">SELECTED</c:if>>Link</option>
                        <option value="fav_heart75.png"<c:if test="${favorite.linkIcon == 'fav_heart75.png'}">SELECTED</c:if>>Heart</option>
                    </select>
                    <br/><br/>
                </dd>
        </dl>
    </td>
              </tr>
          </table>
    </div>        
</div>      
<br/>

<div class="section">
    <input type="submit" name="func" value="Update"/>
    <input type="submit" name="func" value="Delete"/>
</div>
<br/>

</form:form>        
        
  </jsp:body>
</t:layout>