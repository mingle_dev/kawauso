<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<t:layout>
  <jsp:body>
        <h1>Reset Password</h1>
        ${message}
        Enter the username and email address for your mingledorffs.com account. You will receive an email with instructions for creating a new password.
        <br/><br/>
        <form action="<c:out value="/user/forgotPass.htm"/>" method="post">
            <b style="font-size:125%">Email Address:</b><br/><input type="text" name="p_username" style="width:250px"><br/><br/>
            <b style="font-size:125%">Username:</b><br/><input type="password" name="p_password" style="width:250px"><br/><br/>
            
            <input type="checkbox" id="login_remember" name="login_remember" class="css-checkbox" value="true" CHECKED="CHECKED"/>
            <label for="login_remember" name="login_remember_lbl" class="css-label">Keep me signed in</label><br/><br/>
            
            <input type="submit" name="continue" value="Continue"><br/><br/>
        </form>
  </jsp:body>
</t:layout>

