<%--
  Created by IntelliJ IDEA.
  User: twall
  Date: 5/21/15
  Time: 10:18 AM
  To change this template use File | Settings | File Templates.
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<t:layout>
  <jsp:body>
    <h1>Change Password</h1>

    <c:if test="${not empty message}">
        <div class="message-text">
          <c:if test="${status == -1}">
            <img class="message-logo" src="https://s3.amazonaws.com/mgl1939/img/red_error.png" />
          </c:if>
          <c:if test="${status == 1}">
            <img class="message-logo" src="https://s3.amazonaws.com/mgl1939/img/green_check.png" />
          </c:if>
          ${message}
        </div>
      <br/><br/>
    </c:if>


    <form id="pwdForm" action="<c:url value="/user/password.htm"/>" onsubmit="return validateFormOnSubmit(this)" method="post">
      <input id="comebackUrl" name="comebackUrl" type="hidden">
      <input id="username" name="username" type="hidden" value="${user.username}">
      <input id="old_password" name="old_password" type="hidden" value="${user.password}">

      <div class="input-label">Username:</div>
      <div class="input-text">${user.username}</div>
      <br/><br/>

      <div class="input-label">Old Password:</div>
      <div>
        <input id="password" name="password" type="password" class="input-textwidth">
      </div>
      <br/><br/>

      <div class="input-label">New Password:</div>
      <div>
        <input id="new_password" name="new_password" type="password" class="input-textwidth">
      </div>
      <br/><br/>

      <div class="input-label">Confirm Password:</div>
      <div>
        <input id="confirm_password" name="confirm_password" type="password" class="input-textwidth">
      </div>
      <br/><br/>

      <div>
        <input type="submit" id="submit" name="pwdButton" value="Change Password">
        <input type="button" id="cancel" name="cancelBtn" value="Cancel" onClick="cancelButtonClicked()" />
      </div>

    </form>
    <script>

      $( document ).ready(function() {

        var endIndex = document.referrer.indexOf('?');

        if (endIndex == -1)
        {
          endIndex = document.referrer.length;
        }
        else {
          endIndex = document.referrer.indexOf('?');
        }

        var redirectURL = document.referrer.substring(0, endIndex);
        $("#comebackUrl").val(redirectURL);
        console.log($("#comebackUrl").val());
      });

      function cancelButtonClicked(){

        location.href=$("#comebackUrl").val();

      }

      function validateFormOnSubmit(theForm) {
          var reason = "";

          reason += validatePassword(theForm.new_password);
          reason += validatePassword(theForm.confirm_password);
          <%--reason += validateOldPassword(theForm.old_password, theForm.password); --%>
          reason += validateNewPassword(theForm.password, theForm.new_password);
          reason += validateConfirmPassword(theForm.new_password, theForm.confirm_password);

          if (reason != "") {
            alert("Some fields need correction:\n" + reason);
            return false;
          }

          return true;
        }

        function validatePassword(fld) {
          var error = "";
          var illegalChars = /[\W_]/; // allow only letters and numbers

          if (fld.value == "") {
            fld.style.background = 'Yellow';
            error = "You didn't enter a " + fld.name + ".\n";
          } else if ((fld.value.length < 7) || (fld.value.length > 15)) {
            error = "The password is the wrong length. \n";
            fld.style.background = 'Yellow';
          }
          <%--else if (illegalChars.test(fld.value)) {
            error = "The password contains illegal characters.\n";
            fld.style.background = 'Yellow';
          } else if (!((fld.value.search(/(a-z)+/)) && (fld.value.search(/(0-9)+/)))) {
            error = "The password must contain at least one numeral.\n";
            fld.style.background = 'Yellow';
          }
          --%>
          else {
            fld.style.background = 'White';
          }
          return error;
        }

        function validateEmpty(fld) {
          var error = "";

          if (fld.value.length == 0) {
            fld.style.background = 'Yellow';
            error = "The required field has not been filled in.\n"
          } else {
            fld.style.background = 'White';
          }
          return error;
        }

        function validateOldPassword(fld1, fld2) {
          var error = "";

          if (fld1.value != fld2.value) {
            fld1.style.background = 'Yellow';
            fld2.style.background = 'Yellow';
            error = "Please enter the valid user password.\n";
          }
          return error;
        }

        function validateNewPassword(fld1, fld2) {
          var error = "";

          if (fld1.value == fld2.value) {
            fld1.style.background = 'Yellow';
            fld2.style.background = 'Yellow';
            error = "The new user password matches old user password.\n";
          }
          return error;
        }

        function validateConfirmPassword(fld1, fld2) {
          var error = "";

          if (fld1.value != fld2.value) {
            fld1.style.background = 'Yellow';
            fld2.style.background = 'Yellow';
            error = "The new user password does not match the confirm password.\n";
          }
          return error;
        }
    </script>
  </jsp:body>
</t:layout>