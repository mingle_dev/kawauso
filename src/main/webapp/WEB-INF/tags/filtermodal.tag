<%-- 
    Document   : layout
    Created on : Jan 16, 2014, 4:59:55 PM
    Author     : chris.weaver
--%>
<%@tag description="WZ Portal Modal" pageEncoding="UTF-8"%>
<%@attribute name="modalTitle" fragment="true"%>

<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<html>
<head>
    <link rel="stylesheet" href="http://code.jquery.com/ui/1.8.18/themes/base/jquery-ui.css" type="text/css" media="all" />
    <link rel="stylesheet" href="http://static.jquery.com/ui/css/demo-docs-theme/ui.theme.css" type="text/css" media="all" />
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
    <script src="http://code.jquery.com/ui/1.8.18/jquery-ui.min.js" type="text/javascript"></script>
    <script src="http://jquery-ui.googlecode.com/svn/tags/latest/external/jquery.bgiframe-2.1.2.js" type="text/javascript"></script>
    <script src="http://jquery-ui.googlecode.com/svn/tags/latest/ui/minified/i18n/jquery-ui-i18n.min.js" type="text/javascript"></script>

    <script>
        $('.jqmClose').on('click', function(e) {
            $('#dialog').jqmHide();
        });
        $( document ).ready(function() {
            window.location.href = '#openModal';
            console.log( "ready!" );
        });
    </script>

<style>
    .modalDialog {
        position: fixed;
        font-family: Arial, Helvetica, sans-serif;
        top: 0;
        right: 0;
        bottom: 0;
        left: 0;
        background: rgba(0,0,0,0.8);
        z-index: 99999;
        opacity:0;
        -webkit-transition: opacity 400ms ease-in;
        -moz-transition: opacity 400ms ease-in;
        transition: opacity 400ms ease-in;
        pointer-events: none;
    }
    .modalDialog:target {
        opacity:1;
        pointer-events: auto;
    }

    .modalDialog > div {
        width: 225px;
        position: relative;
        margin: 10% auto;
        padding: 5px 20px 13px 20px;
        border-radius: 10px;
        background: #fff;
        background: -moz-linear-gradient(#fff, #999);
        background: -webkit-linear-gradient(#fff, #999);
        background: -o-linear-gradient(#fff, #999);
    }

    .close {
        background: #606061;
        color: #FFFFFF;
        line-height: 25px;
        position: absolute;
        right: -12px;
        text-align: center;
        top: -10px;
        width: 24px;
        text-decoration: none;
        font-weight: bold;
        -webkit-border-radius: 12px;
        -moz-border-radius: 12px;
        border-radius: 12px;
        -moz-box-shadow: 1px 1px 3px #000;
        -webkit-box-shadow: 1px 1px 3px #000;
        box-shadow: 1px 1px 3px #000;
    }

    .close:hover { background: #00d9ff; }

    button, input[type='button'], input[type='submit'], .button {
        display: inline-block;
        margin: 0 5px 5px 0;
        border: 0;
        border-radius: 5px;
        background-color: #D11600;
        color: #fff !important;
        padding: 0 1em;
        font-family: Verdana;
        cursor: pointer;
        white-space: nowrap;
        font-weight: normal;
        line-height: 38px;
        min-height: 38px;
        -webkit-appearance: none;
        -moz-appearance: none;
    }

</style>
</head>

<body>
    <div id="openModal" class="modalDialog">
        <jsp:doBody/>
    </div>
</body>
</html>