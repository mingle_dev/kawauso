<%-- 
    Document   : login
    Created on : Jan 22, 2014, 10:14:29 PM
    Author     : chris
--%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<form action="<c:url value='/user/login.htm'/>" method="post">
    <table border='0' style="cellspacing:0px;">
        <tr>
            <td>Username:</td>
            <td>Password:</td>
            <td></td>
        </tr>
        <tr>
            <td><input type="text" name="p_username" style="height: 24px; width:200px"/></td>
            <td><input type="password" name="p_password" style="height: 24px; width:150px"/></td>
            <td><input type="submit" name="Submit" value="Submit"/></td>
        </tr>
    </table>
</form>
<br/>
<span>Not already a user? <a href="/reg" style="padding:0px; display:inline;">Register for Access</a></span>