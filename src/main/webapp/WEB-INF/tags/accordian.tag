<%-- 
    Document   : layout
    Created on : Jan 16, 2014, 4:59:55 PM
    Author     : chris.weaver
--%>
<%@tag description="WZ Portal Accoridan" pageEncoding="UTF-8"%>
<%@attribute name="modalTitle" fragment="true"%>

<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<div class="section">
    <jsp:doBody/>
</div>
