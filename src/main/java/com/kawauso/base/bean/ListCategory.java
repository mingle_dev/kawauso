/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kawauso.base.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import org.hibernate.annotations.Formula;
import org.hibernate.annotations.GenericGenerator;

/**
 *
 * @author chris
 */
@Entity
@Table(name = "savelist_categories")
@GenericGenerator(name = "uuid-strategy", strategy = "uuid.hex")
public class ListCategory implements Serializable
{
    @Id
    @GeneratedValue(generator = "uuid-strategy")
    @Column(name = "categoryid")    
    private String id;
    
    @Column(name = "user_id")
    private String userId;
    
    @Column(name = "name")
    private String name;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "listCategory", cascade = CascadeType.ALL)
    //private Set<ListItem> listItems = new HashSet<ListItem>();
    private List<ListItem> listItems = new ArrayList<ListItem>();
    
    //@Column(name = "shared")
    //@Formula(value="(SELECT COUNT(s.savelist_id) FROM savelist_shares s WHERE s.owner_id=this_.user_id and s.user_id is not null)")
    //private int shared;
    
    //http://www.sergiy.ca/how-to-write-many-to-many-search-queries-in-mysql-and-hibernate/
    //http://stackoverflow.com/questions/5052924/hibernate-criteria-and-count-column
    //@Formula(value="(SELECT count(*) FROM savelist_items i WHERE i.categoryid = this_.categoryid)")
    //private int numItems;
    
    @Transient
    private boolean shared;
    
    @Transient
    private Integer numItems;
    
    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return the userId
     */
    public String getUserId() {
        return userId;
    }

    /**
     * @param userId the userId to set
     */
    public void setUserId(String userId) {
        this.userId = userId;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the listItems
     */
    public List<ListItem> getListItems() {
        return listItems;
    }

    /**
     * @param listItems the listItems to set
     */
    public void setListItems(List<ListItem> listItems) {
        this.listItems = listItems;
    }

    public void addListItem(ListItem listItem)
    {
        if(this.listItems == null)
            this.listItems = new ArrayList<ListItem>();
        
        this.listItems.add(listItem);
    }
    
    /**
     * @return the shared
     */
    public boolean isShared() {
        return shared;
    }

    /**
     * @param shared the shared to set
     */
    public void setShared(boolean shared) {
        this.shared = shared;
    }

    /**
     * @return the numItems
     */
    public Integer getNumItems() {
        if(numItems != null)
            return numItems;
        else
            return listItems.size();
    }

    /**
     * @param numItems the numItems to set
     */
    public void setNumItems(Integer numItems) {
        this.numItems = numItems;
    }
}