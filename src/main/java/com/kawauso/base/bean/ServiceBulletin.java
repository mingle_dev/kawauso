/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kawauso.base.bean;

import java.io.Serializable;
import java.sql.Blob;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;


/**
 *
 * @author chris
 */
@Entity
@Table(name = "service_bulletins")
@GenericGenerator(name = "uuid-strategy", strategy = "uuid.hex")
public class ServiceBulletin implements Serializable
{
    @Id
    @GeneratedValue(generator = "uuid-strategy")
    @Column(name = "uuid")
    private String id;
    
    @Column(name = "year")
    private int year;
    
    @Column(name = "smbid")
    private String smbId;
    
    @Column(name = "subject")
    private String subject;

    @Column(name = "keywords")
    private String keywords;
    
    @Column(name = "filesize")
    private int fileSize;
    
    //@Basic(fetch = FetchType.LAZY) <-- doesn't work
    @Column(name = "filedata")
    @Lob
    private Blob fileData;

    @Column(name = "status")
    private int status;
    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return the year
     */
    public int getYear() {
        return year;
    }

    /**
     * @param year the year to set
     */
    public void setYear(int year) {
        this.year = year;
    }

    /**
     * @return the smbId
     */
    public String getSmbId() {
        return smbId;
    }

    /**
     * @param smbId the smbId to set
     */
    public void setSmbId(String smbId) {
        this.smbId = smbId;
    }

    /**
     * @return the subject
     */
    public String getSubject() {
        return subject;
    }

    /**
     * @param subject the subject to set
     */
    public void setSubject(String subject) {
        this.subject = subject;
    }

    /**
     * @return the keywords
     */
    public String getKeywords() {
        return keywords;
    }

    /**
     * @param keywords the keywords to set
     */
    public void setKeywords(String keywords) {
        this.keywords = keywords;
    }

    /**
     * @return the fileSize
     */
    public int getFileSize() {
        return fileSize;
    }

    /**
     * @param fileSize the fileSize to set
     */
    public void setFileSize(int fileSize) {
        this.fileSize = fileSize;
    }

    /**
     * @return the fileData
     */
    public Blob getFileData() {
        return fileData;
    }

    /**
     * @param fileData the fileData to set
     */
    public void setFileData(Blob fileData) {
        this.fileData = fileData;
    }

    /**
     * @return the status
     */
    public int getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(int status) {
        this.status = status;
    }

}