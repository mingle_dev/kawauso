package com.kawauso.base.bean.quote;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by chris on 11/26/14.
 */
@Document
public class QBTemplate
{
    @Id
    private String id;
    private String menuId;
    private String systemName;
    private List<String> components;
    private List<String> accessories;

    @Transient
    private List<QBProduct> prodComponents;
    private List<QBProduct> prodAccessories;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMenuId() {
        return menuId;
    }

    public void setMenuId(String menuId) {
        this.menuId = menuId;
    }

    public List<String> getComponents() {
        return components;
    }

    public void setComponents(List<String> components) {
        this.components = components;
    }

    public void addComponent(String component) {
        if(components == null)
            components = new ArrayList<>();
        components.add(component);
    }

    public List<String> getAccessories() {
        return accessories;
    }

    public void setAccessories(List<String> accessories) {
        this.accessories = accessories;
    }

    public void addAccessory(String accessory)
    {
        if(accessories == null)
            accessories = new ArrayList<>();
        accessories.add(accessory);
    }

    public String getSystemName() {
        return systemName;
    }

    public void setSystemName(String systemName) {
        this.systemName = systemName;
    }

    public List<QBProduct> getProdComponents() {
        return prodComponents;
    }

    public void setProdComponents(List<QBProduct> prodComponents) {
        this.prodComponents = prodComponents;
    }

    public List<QBProduct> getProdAccessories() {
        return prodAccessories;
    }

    public void setProdAccessories(List<QBProduct> prodAccessories) {
        this.prodAccessories = prodAccessories;
    }
}
