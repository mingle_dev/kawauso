/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.kawauso.base.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;

/**
 *
 * @author chrisw
 */
@Entity
@Table(name = "trainingCourses")
@GenericGenerator(name = "uuid-strategy", strategy = "uuid.hex")
public class TrainingCourse implements Serializable
{
    @Id
    @GeneratedValue(generator = "uuid-strategy")
    @Column(name = "id")    
    private String id;
    
    @Column(name = "name")
    private String name;
    
    @Column(name = "description")
    private String description;
    
    @Column(name = "category")
    private String category;

    @Column(name = "status")
    private String status;    
    
    @Column(name = "deleted")
    private int deleted;
    
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "course")
    private List<TrainingEvent> trainingEvents;

    public TrainingCourse(){}

    public TrainingCourse(String id, String name, String description, String category)
    {
        this.id = id;
        this.name = name;
        this.description = description;
        this.category = category;
    }

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the category
     */
    public String getCategory() {
        return category;
    }

    /**
     * @param category the category to set
     */
    public void setCategory(String category) {
        this.category = category;
    }


    public void addTrainingEvent(TrainingEvent event)
    {
        if(trainingEvents == null)
            trainingEvents = new ArrayList<TrainingEvent>();

        trainingEvents.add(event);
    }

    /**
     * @return the TrainingEvents
     */
    public List getTrainingEvents() {
        return trainingEvents;
    }

    /**
     * 
     * @param events
     */
    public void setTrainingEvents(List<TrainingEvent> events) {
        this.trainingEvents = events;
    }

    /**
     * @return the status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * @return the deleted
     */
    public int getDeleted() {
        return deleted;
    }

    /**
     * @param deleted the deleted to set
     */
    public void setDeleted(int deleted) {
        this.deleted = deleted;
    }
}
