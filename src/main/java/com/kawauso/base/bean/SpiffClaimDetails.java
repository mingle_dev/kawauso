package com.kawauso.base.bean;


import com.kawauso.base.bean.SpiffClaim;
import com.kawauso.base.bean.SpiffClaimItem;

import java.util.List;


/**
 * Created by cbyrd on 5/13/15.
 */
public class SpiffClaimDetails {

    private SpiffClaim claim;
    private List<SpiffClaimItem> claimItems;

    public List<SpiffClaimItem> getClaimItems() {
        return claimItems;
    }

    public void setClaimItems(List<SpiffClaimItem> claimItems) {
        this.claimItems = claimItems;
    }

    public SpiffClaim getClaim() {
        return claim;
    }

    public void setClaim(SpiffClaim claim) {
        this.claim = claim;
    }
}
