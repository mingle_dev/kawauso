/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kawauso.base.bean;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import org.hibernate.annotations.GenericGenerator;

/**
 *
 * @author chris
 */
@Entity
@Table(name = "cart_lines")
@GenericGenerator(name = "uuid-strategy", strategy = "uuid.hex")
public class CartItem implements Serializable
{
    @Id
    @GeneratedValue(generator = "uuid-strategy")
    @Column(name = "id")
    private String id;

    @Column(name = "user_id")
    private String userId;
    
    @Column(name = "productCode")
    private String productCode;

    //@OneToOne(mappedBy="employee", cascade=CascadeType.ALL)
    @Transient
    private Product productDetail;
    
    @Column(name = "orderQty")
    private int qty;
        
    //-- (S)tock, (K)it, (N)onStock; should change to enum
    @Column(name = "priceType")
    private String priceType;
    
    @Column(name = "unitPrice")
    private BigDecimal unitPrice;
    
    @Column(name = "note")
    private String note;
    
//    
//    //@Column(name = "created")
//    //@Temporal(javax.persistence.TemporalType.DATE)
//    private Date createDate;

    /* this is for orientdb
    public CartItem() {}
     
    public CartItem(CartItem other) {
        this.id = other.getId();
        this.productCode = other.getProductCode();
        this.qty = other.getQty();
        this.userId = other.getUserId();
        this.version = other.getVersion();
    }
    */
    
    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return the name
     */
    public String getProductCode() {
        return productCode;
    }

    /**
     * @param name the name to set
     */
    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    /**
     * @return the qty
     */
    public int getQty() {
        return qty;
    }

    /**
     * @param qty the qty to set
     */
    public void setQty(int qty) {
        this.qty = qty;
    }

    /**
     * @return the userId
     */
    public String getUserId() {
        return userId;
    }

    /**
     * @param userId the userId to set
     */
    public void setUserId(String userId) {
        this.userId = userId;
    }

    /**
     * @return the priceType
     */
    public String getPriceType() {
        return priceType;
    }

    /**
     * @param priceType the priceType to set
     */
    public void setPriceType(String priceType) {
        this.priceType = priceType;
    }

    /**
     * @return the unitPrice
     */
    public BigDecimal getUnitPrice() {
        return unitPrice;
    }

    /**
     * @param unitPrice the unitPrice to set
     */
    public void setUnitPrice(BigDecimal unitPrice) {
        this.unitPrice = unitPrice;
    }

    /**
     * @return the productDetail
     */
    public Product getProductDetail() {
        return productDetail;
    }

    /**
     * @param productDetail the productDetail to set
     */
    public void setProductDetail(Product productDetail) {
        this.productDetail = productDetail;
    }

    /**
     * @return the note
     */
    public String getNote() {
        return note;
    }

    /**
     * @param note the note to set
     */
    public void setNote(String note) {
        this.note = note;
    }
}