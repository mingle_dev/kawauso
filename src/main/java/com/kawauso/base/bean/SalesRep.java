/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kawauso.base.bean;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author chris
 */
@Entity
@Table(name = "nxt_smsn")
public class SalesRep
{
    @Id
    @GeneratedValue
    @Column(name = "prrowid", insertable = false, updatable = false)     
    private String id;
    
    @Column(name = "cono", insertable = false, updatable = false)
    private int cono;
        
    @Column(name = "slsrep", insertable = false, updatable = false)
    private String salesRepId;
    
    @Column(name = "name", insertable = false, updatable = false)
    private String name;
    
    @Column(name = "email", insertable = false, updatable = false)
    private String emailAddr;

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return the cono
     */
    public int getCono() {
        return cono;
    }

    /**
     * @param cono the cono to set
     */
    public void setCono(int cono) {
        this.cono = cono;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the emailAddr
     */
    public String getEmailAddr() {
        return emailAddr;
    }

    /**
     * @param emailAddr the emailAddr to set
     */
    public void setEmailAddr(String emailAddr) {
        this.emailAddr = emailAddr;
    }

    /**
     * @return the salesRepId
     */
    public String getSalesRepId() {
        return salesRepId;
    }

    /**
     * @param salesRepId the salesRepId to set
     */
    public void setSalesRepId(String salesRepId) {
        this.salesRepId = salesRepId;
    }
}