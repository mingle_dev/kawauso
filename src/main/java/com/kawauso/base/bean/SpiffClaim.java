/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kawauso.base.bean;

import java.io.Serializable;
import java.util.*;
import javax.persistence.*;

import org.hibernate.annotations.GenericGenerator;

/**
 *
 * @author chris.weaver
 */
@Entity
@Table(name = "spiff_claims")
@GenericGenerator(name = "uuid-strategy", strategy = "uuid.hex")
public class SpiffClaim implements Serializable
{
    @Id
    @GeneratedValue(generator = "uuid-strategy")
    @Column(name = "id")      
    private String id;
    
    @Column(name = "contact_id")
    private String contactId;
    
    @Column(name = "year")
    private int year;
    
    @Column(name = "claim_no")
    private int claimNumber;
    
    @Column(name = "customer_name")
    private String customerName;
    
    @Column(name = "customer_address")
    private String addrStreet;
    
    @Column(name = "customer_address_city")
    private String addrCity;
    
    @Column(name = "customer_address_state")
    private String addrState;
            
    @Column(name = "customer_address_zip")
    private String addrZip;
    
    @Column(name = "installed")
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date installDate;
    
    @Column(name = "entered")
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date enteredDate;
    
    @Column(name = "approved")
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date approvedDate;
                
    @Column(name = "paid")
    @Temporal(javax.persistence.TemporalType.DATE)                
    private Date paidDate;
    
    @Column(name = "claim_total")
    private Double amount;
    
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "claim", cascade = CascadeType.ALL)
    private List<SpiffClaimItem> items;
    
    @Column(name = "deleted")
    private boolean deleted;

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return the id
     */
    public String getContactId() {
        return contactId;
    }

    /**
     * @param contactId the contactId to set
     */
    public void setContactId(String contactId) {
        this.contactId = contactId;
    }

    /**
     * @return the claimNumber
     */
    public int getClaimNumber() {
        return claimNumber;
    }

    /**
     * @param claimNumber the claimNumber to set
     */
    public void setClaimNumber(int claimNumber) {
        this.claimNumber = claimNumber;
    }

    /**
     * @return the customerName
     */
    public String getCustomerName() {
        return customerName;
    }

    /**
     * @param customerName the customerName to set
     */
    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    /**
     * @return the addrStreet
     */
    public String getAddrStreet() {
        return addrStreet;
    }

    /**
     * @param addrStreet the addrStreet to set
     */
    public void setAddrStreet(String addrStreet) {
        this.addrStreet = addrStreet;
    }

    /**
     * @return the addrCity
     */
    public String getAddrCity() {
        return addrCity;
    }

    /**
     * @param addrCity the addrCity to set
     */
    public void setAddrCity(String addrCity) {
        this.addrCity = addrCity;
    }

    /**
     * @return the addrState
     */
    public String getAddrState() {
        return addrState;
    }

    /**
     * @param addrState the addrState to set
     */
    public void setAddrState(String addrState) {
        this.addrState = addrState;
    }

    /**
     * @return the addrZip
     */
    public String getAddrZip() {
        return addrZip;
    }

    /**
     * @param addrZip the addrZip to set
     */
    public void setAddrZip(String addrZip) {
        this.addrZip = addrZip;
    }

    /**
     * @return the items
     */
    public List<SpiffClaimItem> getItems() {
        return items;
    }

    /**
     * @param items the items to set
     */
    public void setItems(List<SpiffClaimItem> items) {
        this.items = items;
    }

    public void addItem(SpiffClaimItem item) {
        if(items == null)
            items = new ArrayList<>();
        items.add(item);
    }

    /**
     * @return the amount
     */
    public Double getAmount() {
        return amount;
    }

    /**
     * @param amount the amount to set
     */
    public void setAmount(Double amount) {
        this.amount = amount;
    }

    /**
     * @return the deleted
     */
    public boolean isDeleted() {
        return deleted;
    }

    /**
     * @param deleted the deleted to set
     */
    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    /**
     * @return the installDate
     */
    public Date getInstallDate() {
        return installDate;
    }

    /**
     * @param installDate the installDate to set
     */
    public void setInstallDate(Date installDate) {
        this.installDate = installDate;
    }

    /**
     * @return the enteredDate
     */
    public Date getEnteredDate() {
        return enteredDate;
    }

    /**
     * @param enteredDate the enteredDate to set
     */
    public void setEnteredDate(Date enteredDate) {
        this.enteredDate = enteredDate;
    }
    
    /**
     * @return the approvedDate
     */
    public Date getApprovedDate() {
        return approvedDate;
    }

    /**
     * @param approvedDate the approvedDate to set
     */
    public void setApprovedDate(Date approvedDate) {
        this.approvedDate = approvedDate;
    }

    /**
     * @return the paidDate
     */
    public Date getPaidDate() {
        return paidDate;
    }

    /**
     * @param paidDate the paidDate to set
     */
    public void setPaidDate(Date paidDate) {
        this.paidDate = paidDate;
    }

    /**
     * @return the year
     */
    public int getYear() {
        return year;
    }

    /**
     * @param year the year to set
     */
    public void setYear(int year) {
        this.year = year;
    }
}
