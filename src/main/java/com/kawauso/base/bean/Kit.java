/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kawauso.base.bean;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;

/**
 *
 * @author chris
 */
@Entity
@Table(name = "kit")
@GenericGenerator(name = "uuid-strategy", strategy = "uuid.hex")
public class Kit
{
    @Id
    @GeneratedValue(generator = "uuid-strategy")
    @Column(name = "id")    
    private String id;
    
    @Column(name = "name")
    private String name;
    
    @Column(name = "description")
    private String description;
    
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "kit")
    private List<KitItem> items;    

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the items
     */
    public List<KitItem> getItems() {
        return items;
    }

    /**
     * @param items the items to set
     */
    public void setItems(List<KitItem> items) {
        this.items = items;
    }
}