/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.kawauso.base.bean;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import org.hibernate.annotations.GenericGenerator;

/**
 *
 * @author chrisw
 */
@Entity
@Table(name = "trainingEvents")
@GenericGenerator(name = "uuid-strategy", strategy = "uuid.hex")
public class TrainingEvent implements Serializable
{
    @Id
    @GeneratedValue(generator = "uuid-strategy")
    @Column(name = "id") 
    private String id;
    
    //private String trainingCourse_id;
    
    @Column(name = "name")
    private String name;
    
    @Column(name = "delivery_method")
    private String deliveryMethod;
    
    @Column(name = "location")
    private String location;
    
    @Column(name = "length")
    private String length;
    
    @Column(name = "start_dt")
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date startDt;
    
    @Column(name = "fee")
    private BigDecimal fee;
    
    @Column(name = "event_num")
    private int eventNumber;

    @Column(name = "private")
    private int privateEvent;
    
    @Column(name = "deleted")
    private int deleted;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "trainingCourse_id", nullable = false)
    private TrainingCourse course;
    
    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }

//    public String getTrainingCourseId() {
//        return trainingCourse_id;
//    }
//
//    public void setTrainingCourseId(String trainingCourse_id) {
//        this.trainingCourse_id = trainingCourse_id;
//    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the deliveryMethod
     */
    public String getDeliveryMethod() {
        return deliveryMethod;
    }

    /**
     * @param deliveryMethod the deliveryMethod to set
     */
    public void setDeliveryMethod(String deliveryMethod) {
        this.deliveryMethod = deliveryMethod;
    }

    /**
     * @return the location
     */
    public String getLocation() {
        return location;
    }

    /**
     * @param location the location to set
     */
    public void setLocation(String location) {
        this.location = location;
    }

    /**
     * @return the length
     */
    public String getLength() {
        return length;
    }

    /**
     * @param length the length to set
     */
    public void setLength(String length) {
        this.length = length;
    }

    /**
     * @return the eventDate
     */
    public Date getStartDt() {
        return startDt;
    }

    /**
     * @param eventDate the eventDate to set
     */
    public void setStartDt(Date startDt) {
        this.startDt = startDt;
    }

    /**
     * @return the fee
     */
    public BigDecimal getFee() {
        return fee;
    }

    /**
     * @param fee the fee to set
     */
    public void setFee(BigDecimal fee) {
        this.fee = fee;
    }

    /**
     * @return the eventNo
     */
    public int getEventNumber() {
        return eventNumber;
    }

    /**
     * @param eventNo the eventNo to set
     */
    public void setEventNumber(int eventNumber) {
        this.eventNumber = eventNumber;
    }

    /**
     * @return the course
     */
    public TrainingCourse getCourse() {
        return course;
    }

    /**
     * @param course the course to set
     */
    public void setCourse(TrainingCourse course) {
        this.course = course;
    }

    /**
     * @return the privateEvent
     */
    public int getPrivateEvent() {
        return privateEvent;
    }

    /**
     * @param privateEvent the privateEvent to set
     */
    public void setPrivateEvent(int privateEvent) {
        this.privateEvent = privateEvent;
    }

    /**
     * @return the deleted
     */
    public int getDeleted() {
        return deleted;
    }

    /**
     * @param deleted the deleted to set
     */
    public void setDeleted(int deleted) {
        this.deleted = deleted;
    }
}
