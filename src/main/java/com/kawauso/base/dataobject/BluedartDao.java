package com.kawauso.base.dataobject;

import java.math.BigDecimal;

public interface BluedartDao
{
    String getAccountId(int cono, BigDecimal custno);
}
