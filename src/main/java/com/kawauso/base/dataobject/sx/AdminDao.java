package com.kawauso.base.dataobject.sx;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by chris on 1/26/15.
 */
public class AdminDao extends SxApiHandler
{
    public Map getPoolStats() {
        Map map = new HashMap();

        map.put("pool.numIdle", getPool().getNumIdle());
        map.put("pool.numActive", getPool().getNumActive());
        map.put("pool.maxActive", getPool().getMaxActive());
        map.put("pool.maxWait", getPool().getMaxWait());

        return map;
    }
}
