/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kawauso.base.dataobject;

import org.apache.solr.client.solrj.SolrServer;
import org.apache.solr.client.solrj.impl.HttpSolrServer;

/**
 *
 * @author chris
 */
public class SolrDao
{
   private SolrServer server = null; 
   private static SolrDao instance = null;

   private SolrDao(String url) {
       server = new HttpSolrServer(url);
   }
   
   public static SolrDao getInstance(String url) {
      if(instance == null) {
         instance = new SolrDao(url);
      }
      return instance;
   }
   
   public SolrServer getServer() {
       return server; 
   }
}