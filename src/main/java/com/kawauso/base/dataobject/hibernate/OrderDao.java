package com.kawauso.base.dataobject.hibernate;

import com.kawauso.base.bean.Order;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

public interface OrderDao
{
    List<Order> getAll(int cono, BigDecimal custno);

    List<Order> search(int cono, BigDecimal custno, int order, String whse, String po, String prod, String serial, Date fromDt, Date toDt, String type, int stage, int page, int limit);

    List<Order> search(int cono, BigDecimal custno, String term, String type, int stage, String sort, int page, int limit);

    List<Order> search(int cono, BigDecimal custno, String term, String type, int stage, String sort, int page, int limit, String filter_typ, Date fromDt, Date toDt);

    List<Object[]> getSerialNumbers(Order order);

    int getSearchResultCount();
}