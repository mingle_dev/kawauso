package com.kawauso.base.dataobject.hibernate;

import com.kawauso.base.bean.Attribute;
import com.kawauso.base.bean.Kit;
import com.kawauso.base.bean.MenuCategory;
import com.kawauso.base.bean.Product;
import com.kawauso.base.bean.ProductPDRecord;
import com.kawauso.base.bean.UserProduct;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

public interface ProductDao
{
    List<Product> quickLook(int cono, String terms, int limit);

    List<Product> getProductBySku(int cono, String term, Integer limit);

    public List<String> getAllSku(int cono, Integer limit);

    List<Product> getAccessories(String[] products);

    List<Product> search(String term, List<Attribute> attributes, String whse, Integer menuId, String sort, String dir, int start, int offset);

    List<Product> getAlternateProducts(String product);

    Map<String, String> getAttributes(String product);
    
    String getManufacturer(String product);
    
    List<Product> getAll();

    Product getProduct(String productCode);
    
    int getSearchResultCount();

    void setSearchResultCount(int cnt);
    
    String updateUserProduct(UserProduct product);
    
    ProductPDRecord getExtendedPricing(int cono, BigDecimal custno, String whse, String productCode, BigDecimal price);
    
    List<Kit> getKits();

    List<Kit> getKits(String brand);
}