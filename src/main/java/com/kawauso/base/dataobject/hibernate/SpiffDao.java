/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kawauso.base.dataobject.hibernate;

import com.kawauso.base.bean.SpiffClaim;
import com.kawauso.base.bean.SpiffGroup;

import java.math.BigDecimal;
import java.util.List;

/**
 *
 * @author chris
 */
public interface SpiffDao
{
    SpiffClaim findById(String id);

    List<SpiffClaim> findAll(int year, String contactId);

    List<SpiffGroup> findGroups(String classification);

    boolean save(SpiffClaim claim);

    String[] validateProduct(int cono, BigDecimal custno, String prod, String serial);
}
