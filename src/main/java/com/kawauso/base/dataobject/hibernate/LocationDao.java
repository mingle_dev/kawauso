package com.kawauso.base.dataobject.hibernate;

import com.kawauso.base.bean.Location;
import com.kawauso.base.bean.LocationV18;

import java.math.BigDecimal;
import java.util.List;

public interface LocationDao
{
    List<Location> findAll();

    Location getLocation(String whseId);

    List<LocationV18> getLocations(BigDecimal custno, int cono);

    Double[] getCoordinates(int zipCd);

    Double[] getCoordinates(String city, String state);
}
