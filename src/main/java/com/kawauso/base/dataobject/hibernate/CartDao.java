/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kawauso.base.dataobject.hibernate;

import com.kawauso.base.bean.Cart;
import com.kawauso.base.bean.CartItem;
import com.kawauso.base.bean.ShipVia;
import java.math.BigDecimal;
import java.util.List;

/**
 *
 * @author chris
 */
public interface CartDao
{
    Cart getCart(String userId);

    String saveCart(Cart cart);

    List<CartItem> getCartItems(String userId);

    boolean clear(String userId);

    List<ShipVia> getShipViaList();

    CartItem saveItem(String userId, String productCode, String note, int qty, boolean overrideQty, BigDecimal price);
}
