package com.kawauso.base.dataobject.hibernate;

/**
 * Created by chris on 1/30/15.
 */
public interface SequenceDao
{
    int getNext(String keyname);
}
