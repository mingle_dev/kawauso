package com.kawauso.base.dataobject.hibernate;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;

import org.apache.log4j.Logger;
import org.hibernate.SessionFactory;
import org.hibernate.jdbc.ReturningWork;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * Created by chris on 1/30/15.
 */
@Repository
public class SequenceDaoImpl implements SequenceDao
{
    private static final Logger log = Logger.getLogger( SequenceDaoImpl.class );
    @Autowired
    SessionFactory sessionFactory;

    @Override
    public int getNext(final String keyname)
    {
        try {
            /*Connection conn = ((SessionImpl)sessionFactory.openSession()).connection();
            CallableStatement cs = conn.prepareCall("{ call usp_sequence(?,?) }");
            cs.setString(1, keyname);
            cs.registerOutParameter(2, java.sql.Types.INTEGER);
            cs.execute();

            return cs.getInt(2);*/

        	
        	sessionFactory.openSession().doReturningWork(
        		    new ReturningWork<Integer>() {
        		        public Integer execute(Connection connection) throws SQLException 
        		        { 
        		        	CallableStatement cs = connection.prepareCall("{ call usp_sequence(?,?) }");
        		            cs.setString(1, keyname);
        		            cs.registerOutParameter(2, java.sql.Types.INTEGER);
        		            cs.execute();

        		            return cs.getInt(2);
        		        }
        		    }
        		);
        	
        	
        } catch (Exception ex) {
            log.error(ex);
        }

        return 0;
    }
}
