package com.kawauso.base.controller;


import com.kawauso.base.utils.dto.TrainingEmail;
import com.kawauso.base.utils.impl.TrainingEmailServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;



@Controller
public class EmailController {

    final static Logger logger = LoggerFactory.getLogger(EmailController.class);

    @RequestMapping(value = "/training/register/email",
                    method = RequestMethod.POST,
                    produces = MediaType.TEXT_PLAIN_VALUE)
    @ResponseBody
    public  String getEmailResult(@RequestParam("studentname") String studentName,
                                  @RequestParam("customer") String customerNo,
                                  @RequestParam("coursename") String courseName,
                                  @RequestParam("location") String location,
                                  @RequestParam("startdate") String startDate,
                                 // @RequestParam("courselength") String courselength,
                                  @RequestParam("fee") String fee){

        String emailStatus = null;
        TrainingEmail t = new TrainingEmail();
        TrainingEmailServiceImpl ti = new TrainingEmailServiceImpl();


        try{

            t = new TrainingEmail();

            t.setHostName("apollo.mingledorffs.com");
            t.setRecipientEmailAddr("servicetraining@mingledorffs.com");
            t.setRecipientName("Technical Service Training");
            t.setSenderName("Do Not Reply");
            t.setSenderEmailAddr("donotreply@mingledorffs.com");

            //Compose the message
            StringBuilder emailString = new StringBuilder();
            emailString.append(" Registration information for " + studentName + " \n\n");
            emailString.append(" Name: "    + studentName + "\n");
            emailString.append(" Course:  " + courseName + "\n");
            emailString.append(" Location: " + location + "\n");
            emailString.append(" Start Date: " + startDate + "\n");
            emailString.append(" Fee: " + fee + "\n");

            t.setMessage(emailString.toString());

            ti = new TrainingEmailServiceImpl();
            ti.send(t);
            logger.info("Sent registration email to training ");
            emailStatus = "REGISTRATION EMAIL SENT";
        }
        catch(Exception ex){

            logger.error("Unable to send training registration email");
            emailStatus = "REGISTRATION EMAIL NOT SENT";
            ex.printStackTrace();
        }

        return emailStatus;


    }

}
