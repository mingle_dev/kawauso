package com.kawauso.base.controller;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;


/**
 * Created by cbyrd on 5/5/15.
 */
@WebServlet("/SimpleEmailServlet")
public class SimpleEmailServlet extends HttpServlet {

    private static final long serialVeriomUID = 1L;

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException{


             doPost(request,response);

    }

    protected void doPost(HttpServletRequest request,HttpServletResponse response) throws IOException {


        String courseName = request.getParameter("coourse_name");
        String location = request.getParameter("location");
        String startDate = request.getParameter("startdate");
        String courseLength = request.getParameter("coourselength");
        String fee = request.getParameter("fee");


        response.setContentType("text/plain");
        PrintWriter out = response.getWriter();

        out.println("OK");
        out.close();


    }


}
