/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kawauso.base.controller;

import com.google.gson.Gson;
import com.kawauso.base.SecurityAnnotation;
import com.kawauso.base.bean.Breadcrumb;
import com.kawauso.base.bean.Session;
import com.kawauso.base.bean.reporting.Report;
import com.kawauso.base.service.ReportService;

import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author chris
 */
@Controller
public class ReportController
{
    @Autowired
    ReportService reportService;
    
    @RequestMapping("/reports/index")
    @SecurityAnnotation(requireLogin = true, allowedRole = { Session.Role.EMPLOYEE, Session.Role.ADMINISTRATOR })
    public ModelAndView index(@RequestParam(value = "nm", required = false) String nm,
                              @RequestParam(value = "type", required = false) String type)
    {
        Map<String, Object> model = new HashMap<String, Object>();

        //breadcrumbs
        List<Breadcrumb> crumbs = new ArrayList<Breadcrumb>();
        crumbs.add(new Breadcrumb("Reporting","/reports/index.htm",""));


        if(nm == null || nm.isEmpty()) {
            //list reports available
            Map<String, String> menu = new LinkedHashMap<String, String>();
            menu.put("Today's Total Sales", "/reports/index.htm?nm=slstot&type=dly");
            menu.put("Today's Sales by Division", "/reports/index.htm?nm=slsdiv&type=dly");
            menu.put("Today's Sales by Branch", "/reports/index.htm?nm=slsloc&type=dly");
            menu.put("Today's Sales by TM", "/reports/index.htm?nm=slstm&type=dly");
            menu.put("MTD Total Sales", "/reports/index.htm?nm=slstot&type=mtd");
            menu.put("MTD Sales by Division", "/reports/index.htm?nm=slsdiv&type=mtd");
            menu.put("MTD Sales by Branch", "/reports/index.htm?nm=slsloc&type=mtd");
            menu.put("MTD Sales by TM", "/reports/index.htm?nm=slstm&type=mtd");
            model.put("menu", menu);

        }else if(nm.equals("slstot")){
            crumbs.add(new Breadcrumb("Sales Totals","",""));

            Map<String, String> menu = new LinkedHashMap<String, String>();
            menu.put("Mingledorff's", "/reports/run.htm?nm=slstot&type="+type+"&cono=1");
            menu.put("Holden Associates", "/reports/run.htm?nm=slstot&type="+type+"&cono=2");
            model.put("menu", menu);

        }else if(nm.equals("slsdiv")){
            crumbs.add(new Breadcrumb("Sales by Division","",""));

            Map<String, String> menu = new LinkedHashMap<String, String>();
            menu.put("North Georgia", "/reports/run.htm?nm=slsdiv&type="+type+"&div=ng");
            menu.put("Southwest Georgia", "/reports/run.htm?nm=slsdiv&type="+type+"&div=sw");
            menu.put("Gulf Coast", "/reports/run.htm?nm=slsdiv&type="+type+"&div=gc");
            menu.put("Southeast Georgia", "/reports/run.htm?nm=slsdiv&type="+type+"&div=se");
            menu.put("North Alabama", "/reports/run.htm?nm=slsdiv&type="+type+"&div=na");
            model.put("menu", menu);

        }else if(nm.equals("slsloc")){
            crumbs.add(new Breadcrumb("Sales by Location","",""));

            Map<String, String> menu = new LinkedHashMap<String, String>();
            menu.put("Mingledorff's", "/reports/run.htm?nm=slsloc&type="+type+"&cono=1");
            menu.put("Holden Associates", "/reports/run.htm?nm=slsloc&type="+type+"&cono=2");
            model.put("menu", menu);

        }else if(nm.equals("slstm")) {
            crumbs.add(new Breadcrumb("Sales by TM","",""));

            Map<String, String> menu = new LinkedHashMap<String, String>();
            menu.put("Mingledorff's", "/reports/run.htm?nm=slstm&type=" + type + "&cono=1");
            menu.put("Holden Associates", "/reports/run.htm?nm=slstm&type=" + type + "&cono=2");
            model.put("menu", menu);

        }else{
            model.put("menu_error", "Unknown menu selection");
        }

        model.put("navTree", crumbs);

        return new ModelAndView("/report/index", model);
    }

    @RequestMapping("/reports/orders")
    @SecurityAnnotation(requireLogin = true, allowedRole = { Session.Role.EMPLOYEE, Session.Role.ADMINISTRATOR })
    public ModelAndView orders(@RequestParam(value = "cono", required = false) String cono,
                               @RequestParam(value = "key", required = false) String key,
                               @RequestParam(value = "val", required = false) String val)
    {
        Map<String, Object> model = new HashMap<String, Object>();

        Report report = reportService.run("orders", key, cono, val);
        model.put("results", report);

        return new ModelAndView("/report/tmpl_orders", model);
    }

    @RequestMapping("/reports/run")
    @SecurityAnnotation(requireLogin = true, allowedRole = { Session.Role.EMPLOYEE, Session.Role.ADMINISTRATOR })
    public ModelAndView run(@RequestParam(value = "nm", required = true) String nm,
                            @RequestParam(value = "type", required = false) String type,
                            @RequestParam(value = "cono", required = false) String cono,
                            @RequestParam(value = "div", required = false) String div)
    {
        Map<String, Object> model = new HashMap<String, Object>();

        Report report = null;

        if(nm.equals("slstot")) {
            if(type.equals("mtd"))
                report = reportService.run("slstot", "mtd", cono, div);
            else
                report = reportService.run("slstot", "dly", cono, div);

            model.put("results", report);
            return new ModelAndView("/report/tmpl_slstot", model);

        }else if(nm.equals("slsloc")) {
            if(type.equals("mtd"))
                report = reportService.run("slsloc", "mtd", cono, div);
            else
                report = reportService.run("slsloc", "dly", cono, div);

            model.put("results", report);
            return new ModelAndView("/report/tmpl_slsloc", model);

        }else if(nm.equals("slsdiv")) {
            if(type.equals("mtd"))
                report = reportService.run("slsloc", "mtd", cono, div);
            else
                report = reportService.run("slsloc", "dly", cono, div);

            model.put("results", report);
            return new ModelAndView("/report/tmpl_slsloc", model);

        }else if(nm.equals("slstm")) {
            if(type.equals("mtd"))
                report = reportService.run("slstm", "mtd", cono, div);
            else
                report = reportService.run("slstm", "dly", cono, div);

            model.put("results", report);
            return new ModelAndView("/report/tmpl_slstm", model);
        }

        return new ModelAndView("/message", model);
    }
}