/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kawauso.base;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.orm.hibernate3.support.OpenSessionInViewFilter;

/**
 *
 * @author chris
 */
public class CustomOpenSessionInViewFilter extends OpenSessionInViewFilter
{
    @Override
    public void closeSession(Session session, SessionFactory sessionFactory)
    {
        session.flush();
        super.closeSession(session, sessionFactory);
    }
}