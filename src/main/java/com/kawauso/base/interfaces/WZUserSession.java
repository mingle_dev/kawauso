package com.kawauso.base.interfaces;

import javax.ejb.Local;

/**
 * Created by cbyrd on 4/16/15.
 */
//@Local
public interface WZUserSession {


    void setUserId(String userId);
    String getUserId();
    void setToken(String token);
    String getToken();

}
