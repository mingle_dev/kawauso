package com.kawauso.base.utils.impl;


import com.kawauso.base.utils.dto.TrainingEmail;
import com.kawauso.base.utils.dto.TrainingEmailService;
import org.apache.commons.mail.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by cbyrd on 4/29/15.
 */
public class TrainingEmailServiceImpl implements TrainingEmailService{

    final static Logger logger = LoggerFactory.getLogger(TrainingEmailServiceImpl.class);

    @Override
    public String send(TrainingEmail t) {

        String isSuccess = null;
        Email email;

        try {

            logger.info("PREPARE TO SEND TRAINING EMAIL FROM " + t.getSenderEmailAddr() + "...");
            email = new SimpleEmail();
            email.setSmtpPort(25);
            email.setHostName(t.getHostName());
            email.setStartTLSEnabled(true);
            email.setFrom(t.getSenderEmailAddr(), t.getSenderName());
            email.setSubject("Training Event Registration");
            email.addTo(t.getRecipientEmailAddr(), t.getRecipientName());
            email.addTo("cbyrd@mingledorffs.com");
            email.setMsg(t.getMessage());
            email.send();
            logger.info("SENT TRAINING EMAIL FROM " + t.getSenderEmailAddr() + ".");

            isSuccess =  "success";


        }
        catch (EmailException e) {

            logger.error("ERROR IN SENDING EMAIL FOR EMAIL ADDRESS " +  t.getSenderEmailAddr() + ".");
            isSuccess = "failure";
            e.printStackTrace();
        }

        return isSuccess;


    }


}
