/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kawauso.base.service;

import com.kawauso.base.bean.Document;
import com.kawauso.base.bean.ServiceBulletin;
import com.kawauso.base.bean.TrainingCourse;
import com.kawauso.base.bean.TrainingEvent;
import com.kawauso.base.dataobject.hibernate.DocumentLibraryDao;
import com.kawauso.base.dataobject.hibernate.TrainingDao;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author chris
 */
@Service
@Transactional
public class TechServicesServiceImpl implements TechServicesService
{
    @Autowired
    DocumentLibraryDao libraryDao;
    
    @Autowired
    TrainingDao trainingDao;
    
    @Override
    public List<Document> findLibraryDocuments(String query)
    {
        return libraryDao.find(query);
    }
    
    @Override
    public Document findLibraryDocument(String id)
    {
        return libraryDao.findById(id);
    }
    
    @Override
    public void updateLibraryDocmentCounter(String id)
    {
        libraryDao.updateCounter(id);
    }
    
    @Override
    public List<ServiceBulletin> findServiceBulletins(int year)
    {
        return libraryDao.findServiceBulletins(year);
    }
    
    @Override
    public List<ServiceBulletin> findServiceBulletins(String query)
    {
        return libraryDao.findServiceBulletins(query);
    }

    @Override
    public List<ServiceBulletin> findServiceBulletins(String query, int year)
    {
        return libraryDao.findServiceBulletins(query, year);
    }
    
    @Override 
    public List findServiceBulletinYears()
    {
        return libraryDao.findServiceBulletinYears();
    }
    
    @Override
    public ServiceBulletin findServiceBulletin(String id)
    {
        return libraryDao.findServiceBulletin(id);
    }

    @Override
    public TrainingCourse findTrainingCourse(String id) throws Exception
    {
        return trainingDao.findCourse(id);
    }

    @Override
    public List<TrainingCourse> findTrainingUpcomingCourses() throws Exception
    {
        return trainingDao.findUpcomingCourses();
    }

    @Override
    public TrainingEvent findTrainingEvent(String eventId) throws Exception
    {
        return trainingDao.findEvent(eventId);
    }

    @Override
    public void registerStudentTrainingEvent(String eventId, String contactId) throws Exception
    {
        trainingDao.registerStudent(eventId, contactId);
    }
}