package com.kawauso.base.service;

import com.kawauso.base.bean.Attribute;
import com.kawauso.base.bean.Availability;
import com.kawauso.base.bean.Kit;
import com.kawauso.base.bean.Product;
import com.kawauso.base.bean.ProductPDRecord;
import com.kawauso.base.bean.UserProduct;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

public interface ProductService
{
    List<Product> quickLook(int cono, String terms, int limit);

    List<Product> getProductsBySku(int cono, String terms, Integer limit);

    List<String> getAllSku(int cono, Integer limit);

    List<Product> search(String term, List<Attribute> attributes, String whse, Integer menuId, String sort, String dir, int start, int offset);

    List<Product> search(int cono, String oper, BigDecimal custno, String shipTo, String term, List<Attribute> attributes, String whse, Integer menuId, String sort, String dir, int start, int offset);

    List<Attribute> getAttributes(String term, Integer menuId);
    
    List<Product> getAll();
    
    Product getProduct(String productCode);

    Product getProduct(int cono, String oper, BigDecimal custno, String shipTo, String whse, String productCode);
    
    int getSearchResultCount();
    
    List<Availability> getAvailability(int cono, String oper, String productCode, boolean includeZeroQty);

    List<Availability> getAvailability(int cono, BigDecimal custno, String oper, String productCode, boolean includeZeroQty);

    void applyNetAvail(int cono, String oper, List<Product> products);

    String updateUserProduct(UserProduct product);
    
    Product applyPricing(int cono, String oper, BigDecimal custno, String shipTo, String whse, Product prod);

    List<Product> applyPricing(int cono, String oper, BigDecimal custno, String shipTo, String whse, List<Product> products);

    List<Product> getAccessories(String[] products);

    List<Product> getAccessories(int cono, String oper, BigDecimal custno, String shipTo, String whse, String productCode);

    List<Product> getAlternateProducts(int cono, String oper, BigDecimal custno, String shipTo, String whse, String productCode);

    ProductPDRecord getExtendedPricing(int cono, BigDecimal custno, String whse, String productCode, BigDecimal price);
    
    String createWarehouseProduct(int cono, String oper, String productCode, String whse);
    
    Map<String, String> getAttributes(String product);
    
    String getManufacturer(String product);
    
    List<Kit> getKits();

    List<Kit> getKits(String brand);
}