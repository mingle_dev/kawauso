/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kawauso.base.service;

import com.kawauso.base.bean.SpiffClaim;
import com.kawauso.base.bean.SpiffGroup;

import java.math.BigDecimal;
import java.util.List;

/**
 *
 * @author chris
 */
public interface SpiffService
{
    List<SpiffClaim> findAll(int year, String contactId);

    List<SpiffGroup> findGroups(String classification);

    String[] validateProduct(int cono, BigDecimal custno, String prod, String serial);

    boolean save(SpiffClaim claim);

    int getNextClaimNo();
}