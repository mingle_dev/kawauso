/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kawauso.base.service;

import com.kawauso.base.dataobject.BluedartDao;
import java.math.BigDecimal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author chris
 */
@Service
@Transactional(propagation = Propagation.SUPPORTS)
public class BluedartServiceImpl implements BluedartService
{
    @Autowired
    BluedartDao bluedartDao;
    
    @Override
    public String getAccountId(int cono, BigDecimal custno)
    {
        return bluedartDao.getAccountId(cono, custno);
    }
}
