/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kawauso.base.service;

import com.kawauso.base.bean.Document;
import com.kawauso.base.bean.ServiceBulletin;
import com.kawauso.base.bean.TrainingCourse;
import com.kawauso.base.bean.TrainingEvent;
import java.util.List;

/**
 *
 * @author chris
 */
public interface TechServicesService
{
    List<Document> findLibraryDocuments(String query);

    Document findLibraryDocument(String id);

    void updateLibraryDocmentCounter(String id);
    
    List<ServiceBulletin> findServiceBulletins(int year);

    List<ServiceBulletin> findServiceBulletins(String query);

    List<ServiceBulletin> findServiceBulletins(String query, int year);

    List findServiceBulletinYears();
    
    ServiceBulletin findServiceBulletin(String id);
    
    TrainingCourse findTrainingCourse(String id) throws Exception;

    List<TrainingCourse> findTrainingUpcomingCourses() throws Exception;

    TrainingEvent findTrainingEvent(String eventId) throws Exception;

    void registerStudentTrainingEvent(String eventId, String contactId) throws Exception;
}
