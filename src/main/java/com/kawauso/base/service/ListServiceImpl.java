package com.kawauso.base.service;

import com.kawauso.base.bean.Account;
import com.kawauso.base.bean.ListCategory;
import com.kawauso.base.bean.ListItem;
import com.kawauso.base.dataobject.ListDao;
import java.math.BigDecimal;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class ListServiceImpl implements ListService
{
    @Autowired
    ListDao listDao;

    @Override
    public List<ListCategory> getLists(String userId, Integer limit, boolean includeExtra)
    {
        return listDao.getLists(userId, limit, includeExtra);
    }
    
    @Override
    public ListCategory getList(String listId, boolean includeExtra)
    {
        return listDao.getList(listId, includeExtra);
    }
    
    @Override
    public String saveList(ListCategory list)
    {
        return listDao.saveList(list);
    }
    
    @Override
    public String updateList(ListCategory list)
    {
        return listDao.updateList(list);
    }
    
    @Override
    public String renameList(String id, String name)
    {
        return listDao.renameList(id, name);
    }
    
    @Override
    public int getItemCount(ListCategory cat)
    {
        return listDao.getItemCount(cat);
    }
    
    @Override
    public String addItem(ListItem item)
    {
        return listDao.addItem(item);
    }
    
    @Override
    public String addItem(ListCategory item)
    {
        return listDao.addItem(item);
    }

    @Override
    public boolean deleteItem(String listId, String prod) {
        return listDao.deleteItem(listId, prod);
    }

    @Override
    public boolean updateItemQty(String listId, String prod, BigDecimal qty)
    {
        return listDao.updateItemQty(listId, prod, qty);
    }

    @Override
    public boolean deleteList(String listId)
    {
        return listDao.deleteList(listId);
    }
    
    @Override
    public String cloneList(String listId)
    {
        return listDao.cloneList(listId);
    }
    
    @Override
    public boolean isShared(String listId, BigDecimal custno)
    {
        return listDao.isShared(listId, custno);
    }
    
    @Override
    public List<Account> getShares(String listId)
    {
        return listDao.getShares(listId);
    }

    @Override
    public boolean addShare(String listId, String ownerId, String accountId, int cono, BigDecimal custno)
    {
        return listDao.addShare(listId, ownerId, accountId, cono, custno);
    }
    
    @Override
    public boolean removeShare(String listId)
    {
        return listDao.removeShare(listId);
    }    
    
    @Override
    public boolean removeShare(String listId, int cono, BigDecimal custno)
    {
        return listDao.removeShare(listId, cono, custno);
    }

    @Override
    public List<ListCategory> getSharedLists(int cono, BigDecimal custno)
    {
        return listDao.getSharedLists(cono, custno);
    }
}