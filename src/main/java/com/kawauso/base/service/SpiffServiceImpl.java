/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kawauso.base.service;

import com.kawauso.base.bean.SpiffClaim;
import com.kawauso.base.bean.SpiffGroup;
import com.kawauso.base.dataobject.hibernate.SequenceDao;
import com.kawauso.base.dataobject.hibernate.SpiffDao;

import java.math.BigDecimal;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author chris
 */
@Service
@Transactional
public class SpiffServiceImpl implements SpiffService
{
    @Autowired
    SpiffDao spiffDao;

    @Autowired
    SequenceDao seqDao;

    @Override
    public List<SpiffClaim> findAll(int year, String contactId)
    {
        return spiffDao.findAll(year, contactId);
    }
    
    @Override
    public List<SpiffGroup> findGroups(String classification)
    {
        return spiffDao.findGroups(classification);
    }

    @Override
    public String[] validateProduct(int cono, BigDecimal custno, String prod, String serial)
    {
        return spiffDao.validateProduct(cono, custno, prod, serial);
    }

    @Override
    public int getNextClaimNo()
    {
        return seqDao.getNext("retailspiff");
    }

    @Override
    public boolean save(SpiffClaim claim)
    {
        return spiffDao.save(claim);
    }
}
