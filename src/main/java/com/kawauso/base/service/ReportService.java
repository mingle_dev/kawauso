/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kawauso.base.service;

import com.kawauso.base.bean.reporting.Report;

import java.util.Map;

/**
 *
 * @author chris
 */
public interface ReportService {

    Report run(String name, String type, String cono, String div); //@todo; hopefully temporary...

    Report runReport(String reportId, Map<String, Object> params);
}
