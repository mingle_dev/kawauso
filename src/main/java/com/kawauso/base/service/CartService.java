/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kawauso.base.service;

import com.influx.bean.SyncCart;
import com.kawauso.base.bean.Cart;
import com.kawauso.base.bean.CartItem;
import com.kawauso.base.bean.ShipVia;
import java.math.BigDecimal;
import java.util.List;

/**
 *
 * @author chris
 */
public interface CartService
{
    Cart getCart(String userId);

    String saveCart(Cart cart);

    List<CartItem> getCartItems(String userId);

    boolean clear(String userId);

    void saveItem(String userId, String productCode, String note, int qty, boolean overrideQty, BigDecimal price);

    List<ShipVia> getShipViaList();

    SyncCart getExternalCart(String id);
}
