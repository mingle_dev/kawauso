package com.kawauso.base.service;

import com.kawauso.base.bean.quote.MarkFor;
import com.kawauso.base.bean.quote.QBMenu;
import com.kawauso.base.bean.quote.QBTemplate;
import com.kawauso.base.bean.quote.Quote;

import java.util.List;

/**
 * Created by chris on 11/24/14.
 */
public interface QuoteService
{
    Quote findById(String id);
    List<Quote> findByQuoteId(String quoteId);

    List<Quote> findByOwnerId(String ownerId);

    List<Quote> findByCustNo(String custno);
    List<Quote> findByCustNo(List<String> custnoCollection);

    void find();

    String add(Quote quote);

    Quote save(Quote quote);

    void upsertSystem(String quoteId, MarkFor system);

    void deleteSystem(String quoteId, MarkFor system);

    String update(Quote quote);

    QBMenu getNav(String id);
    List<QBMenu> getMenu(String parent);
    List<QBTemplate> getTemplates(String menuId);

    void addMenuItem(QBMenu item);
    void addTemplate(QBTemplate template);
    void truncateCollection(String collection);

    QBTemplate getTemplate(String id);

}
