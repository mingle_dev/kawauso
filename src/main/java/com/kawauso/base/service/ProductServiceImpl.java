package com.kawauso.base.service;

import com.kawauso.base.bean.*;
import com.kawauso.base.bean.nonmanaged.SolrProduct;
import com.kawauso.base.dataobject.SolrDao;
import com.kawauso.base.dataobject.hibernate.LocationDao;
import com.kawauso.base.dataobject.hibernate.ProductDao;
import org.apache.log4j.Logger;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrServer;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrDocumentList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.*;

@Service
@Transactional
public class ProductServiceImpl implements ProductService
{
    @Autowired
    ProductDao productDao;

    @Autowired
    LocationDao locationDao;

    //@todo; load from dispatcher-servlet.xml
    private String solrUrl = "http://192.168.10.200:8983/solr/";

    private static final Logger log = Logger.getLogger( ProductServiceImpl.class );

    @Override
    public List<Product> quickLook(int cono, String terms, int limit)
    {
        return productDao.quickLook(cono, terms, limit);
    }

    @Override
    public List<Product> getProductsBySku(int cono, String terms, Integer limit)

    {
        //return new ArrayList<Product>();
        return productDao.getProductBySku(cono, terms, limit);
    }

    @Override
    public List<String> getAllSku(int cono, Integer limit)
    {
        return productDao.getAllSku(cono, limit);
    }

    @Override
    public List<Product> getAccessories(String[] products)
    {
        return productDao.getAccessories(products);
    }


    @Override
    public List<Product> search(String term, List<Attribute> attributes, String whse, Integer menuId, String sort, String dir, int start, int offset)
    {
        return productDao.search(term, attributes, whse, menuId, sort, dir, start, offset);
    }

    @Override
    public List<Product> search(int cono, String oper, BigDecimal custno, String shipTo, String term, List<Attribute> attributes, String whse, Integer menuId, String sort, String dir, int start, int offset)
    {
        List<Product> products = new ArrayList<Product>();

        try {
            SolrServer server = SolrDao.getInstance(solrUrl).getServer();

            SolrQuery query = new SolrQuery();

            if (menuId != null) {
                query.setQuery("(category:" + menuId + ")");
            } else {
                String name = term;//term.replaceAll(" ", "* ").trim();//not sure if the * works with Apache Solr search; consider removing
                if(!name.endsWith("*")) { name += "*"; }//not sure if the * works with Apache Solr search; consider removing
                if(name.contains("-") || name.contains("_"))
                {
                    int i = 0;
                    int length = name.length();
                    String tmp = new String("+");
                    String hyphen = "-";
                    String uscore = "_";
                    while (i < length && length > (i + 1))
                    {
                        String c = name.substring(i, i+1);
                        if (c.equals(hyphen) || c.equals(uscore))
                        {
                            tmp = tmp + c + " +";
                        }else
                        {
                            tmp = tmp + c;
                        }
                        i++;
                    }
                    name = tmp;

                    //remove the * from the search string; results have been inconsistent
                    //not sure if the * works with Apache Solr search; consider removing
                    int k = 0;
                    int size = name.length();
                    String str = new String();
                    String ask = "*";
                    while (k < size )
                    {
                        String c = name.substring(k, k+1);
                        //if (!ask.equals(c))
                        //{
                            str = str + c;
                        //}
                        k++;
                    }

                    str = str.substring(0, str.length()-1);
                    name = str + "*";
                    if (name.charAt(name.length()-1) == '+'){
                        name = name.substring(0, name.length()-1);
                    }
                    //int idx = -1;
                    //if (name.contains("-"))
                    //    idx = name.indexOf("-");
                    //else if (name.contains("_"))
                    //    idx = name.indexOf("_");

                    //name = "+" + name.substring(0, idx+1) + " +"  + name.substring(idx+1, name.length()-1);
                    //remove the added * after changing the search string by adding solr operators
                }

                String queryStr = "name:(" + name + ") OR description:(" + name + ") OR manu:(" + name + ")";

                query.setQuery(queryStr);
            }

            System.out.println("product avail query string " + query.getQuery());

            QueryResponse response;

            query.setStart(start-1);
            query.setRows(offset);

            if(sort != null && !sort.isEmpty()) {
                if(dir.equals("desc"))
                    query.setSort(sort, SolrQuery.ORDER.desc);
                else
                    query.setSort(sort, SolrQuery.ORDER.asc);
            }

            response = server.query(query);

            long numFound = response.getResults().getNumFound();
            productDao.setSearchResultCount( (int) numFound);

            List<SolrProduct> pArr = response.getBeans(SolrProduct.class);

            if(pArr != null) {
                for(SolrProduct sp : pArr) {
                    Product p = new Product();
                    p.setProduct(sp.getSku());
                    p.setDescription(sp.getDescription());
                    p.setVendor(sp.getManufacturer());
                    p.setDetails(sp.getDetails());
                    p.setId(sp.getId()); //-- ??
                    p.setType(sp.getType());
                    p.setImage(sp.getImage());
                    try { p.setCategoryId( Integer.parseInt(sp.getCategory())); }catch(Exception ig) {}

                    products.add(p);
                }
            }

        }catch(Exception ex) {
            String message = "";

            for(StackTraceElement stackTraceElement : Thread.currentThread().getStackTrace()) {
                message = message + System.lineSeparator() + stackTraceElement.toString();
            }
            log.warn("Something weird happened. I will print the the complete stacktrace even if we have no exception just to help you find the cause" + message);

            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            String dt = df.format(new Date());
            log.error(("Terrell New Log Entry " + dt), ex);
            log.error(ex);
        }

        return products;
    }

    @Override
    public List<Attribute> getAttributes(String term, Integer menuId) {
        List<Attribute> attributes = new ArrayList<Attribute>();
        
        try {
            SolrServer server = SolrDao.getInstance(solrUrl).getServer();

            SolrQuery query = new SolrQuery();
            query.setQuery(term);

            query.setRows(1000000);
            
            QueryResponse response = server.query(query);

            SolrDocumentList sdl = response.getResults();
            for(SolrDocument sd : sdl) {
                String[] names = sd.getFieldNames().toArray(new String[0]);

                for(int i=0; i<names.length; i++) {
                    if(names[i].startsWith("facet_")) {
                        
                        String[] a = names[i].split("_");
                        Attribute attr = new Attribute(a[1]);

                        if(!attributes.contains(attr)) {
                            attr.add(sd.getFieldValue(names[i]).toString(), false);
                            attributes.add(attr);
                        }else{
                            attr = attributes.get( attributes.indexOf(attr) );
                            attr.add(sd.getFieldValue(names[i]).toString(), false);
                        }
                    }
                }
            }

        }catch(Exception ex) {
            log.error(ex);
        }

        return attributes;
    }
    
    @Override
    public List<Product> getAll()
    {
        return productDao.getAll();
    }
    
    /**
     * Returns the total number of records found for the current search
     * @todo; might be better to create a generic class such as
     *      PagedResults : int numItems; List<T>
     *   so that we don't have to call back into the dao to fetch number of records found
     * 
     * @return int
     */
    @Override
    public int getSearchResultCount() {
        return productDao.getSearchResultCount();
    }
    
    @Override
    public Product getProduct(String productCode)
    {
        return productDao.getProduct(productCode);
    }
    
    @Override
    public Product getProduct(int cono, String oper, BigDecimal custno, String shipTo, String whse, String productCode)
    {
        //fetch product data (image, ext data, attributes...)
        Product prod = productDao.getProduct(productCode);

        //fetch whse price/avail and whse details from ERP
        if(prod != null && custno != null) {
            com.kawauso.base.dataobject.sx.ProductDao pd = new com.kawauso.base.dataobject.sx.ProductDao();
            Product tmp = pd.getProduct(cono, oper, custno, shipTo, whse, productCode);

            //merge
            prod.setPrice( tmp.getPrice() );
            prod.setAvail( tmp.getAvail() );
            prod.setPriceWhse( tmp.getPriceWhse() );
        }
        return prod;
    }

    @Override
    public List<Product> getAlternateProducts(int cono, String oper, BigDecimal custno, String shipTo, String whse, String productCode)
    {
        //fetch product data (image, ext data, attributes...)
        List<Product> list = productDao.getAlternateProducts(productCode);

        if(list == null)
            return null;

        List<Product> products = new ArrayList<Product>();

        for(Product item : list) {

            Product prod = this.getProduct(cono, oper, custno, shipTo, whse, item.getProduct());

            products.add(prod);
        }


        return products;
    }

    @Override
    public List<Availability> getAvailability(int cono, String oper, String productCode, boolean includeZeroQty)
    {
        com.kawauso.base.dataobject.sx.ProductDao pd = new com.kawauso.base.dataobject.sx.ProductDao();
        List<Availability> avail = pd.getAvailability(cono, oper, productCode, includeZeroQty);
        
        //add location information
        List<Location> locations = locationDao.findAll();
        
        for(Availability a : avail) {
            int idx = locations.indexOf( new Location(a.getCono(), a.getWhse() ) );
            if(idx > -1) {
                Location loc = locations.get(idx);
                a.setAddress(loc.getAddr());
                a.setCity(loc.getWhseName());
                a.setState(loc.getState());
                a.setZipcd(loc.getZipCd());
                a.setPhoneno(loc.getPhoneNo());
                a.setEmailAddr(loc.getEmailAddr());
            }
        }
        
        return avail;
    }

    @Override
    public List<Availability> getAvailability(int cono, BigDecimal custno, String oper, String productCode, boolean includeZeroQty)
    {
        com.kawauso.base.dataobject.sx.ProductDao pd = new com.kawauso.base.dataobject.sx.ProductDao();
        List<Availability> avail = pd.getAvailability(cono, oper, productCode, includeZeroQty);

        //add location information
        List<LocationV18> locations = locationDao.getLocations(custno,cono);
        List<Availability> returnlist = new ArrayList<Availability>();

        Iterator<Availability> iterator = avail.iterator();

        while (iterator.hasNext()) {

            Availability a = iterator.next();
            int idx = locations.indexOf( new LocationV18(a.getCono(), a.getWhse(),custno.intValue()) );
            if(idx > 0) {
                LocationV18 loc = locations.get(idx);
                a.setAddress(loc.getAddr());
                a.setCity(loc.getWhseName());
                a.setState(loc.getState());
                a.setZipcd(loc.getZipCd());
                a.setPhoneno(loc.getPhoneNo());
                a.setEmailAddr(loc.getEmailAddr());
                returnlist.add(a);
            }
        }

        return returnlist;
    }

    @Override
    public void applyNetAvail(int cono, String oper, List<Product> products)
    {
        for(Product p : products) {
            Product tmp = productDao.getProduct(p.getProduct());
            if(tmp != null)
                p.setNetAvail(tmp.getNetAvail());
        }
    }

    @Override
    public String updateUserProduct(UserProduct product)
    {
        return productDao.updateUserProduct(product);
    }
    
    @Override
    public Product applyPricing(int cono, String oper, BigDecimal custno, String shipTo, String whse, Product prod)
    {
        //fetch whse price/avail from ERP
        if(prod != null && custno != null) {
            com.kawauso.base.dataobject.sx.ProductDao pd = new com.kawauso.base.dataobject.sx.ProductDao();
            Product tmp = pd.getProduct(cono, oper, custno, shipTo, whse, prod.getProduct());

            //merge
            prod.setPrice( tmp.getPrice() );
            prod.setAvail( tmp.getAvail() );
        }

        return prod;
    }
    
    @Override
    public List<Product> applyPricing(int cono, String oper, BigDecimal custno, String shipTo, String whse, List<Product> products)
    {
        //fetch whse price/avail
        if(products != null && custno != null && whse != null) {
            com.kawauso.base.dataobject.sx.ProductDao pd = new com.kawauso.base.dataobject.sx.ProductDao();
            products = pd.getProducts(cono, oper, custno, shipTo, whse, products);
        }
        return products;
    }
    
    @Override
    public List<Product> getAccessories(int cono, String oper, BigDecimal custno, String shipTo, String whse, String productCode)
    {
        List<Product> products = null;

        com.kawauso.base.dataobject.sx.ProductDao pd = new com.kawauso.base.dataobject.sx.ProductDao();
        List<String> accessories = pd.getAccessories(cono, oper, whse, productCode);

        if(accessories == null)
            return null;

        for(String acc : accessories) {
            if(products == null)
                products = new ArrayList<Product>();
            
            Product product = this.getProduct(cono, oper, custno, shipTo, whse, acc);
            products.add(product);
        }
        
        return products;
    }
    
    @Override
    public ProductPDRecord getExtendedPricing(int cono, BigDecimal custno, String whse, String productCode, BigDecimal price)
    {
         return productDao.getExtendedPricing(cono, custno, whse, productCode, price);
    }
    
    @Override
    public String createWarehouseProduct(int cono, String oper, String productCode, String whse)
    {
        com.kawauso.base.dataobject.sx.ProductDao pd = new com.kawauso.base.dataobject.sx.ProductDao();
        return pd.createWarehouseProduct(cono, oper, productCode, whse);
    }
    
    @Override
    public Map<String, String> getAttributes(String product)
    {
        return productDao.getAttributes(product);
    }
    
    @Override
    public String getManufacturer(String product)
    {
        return productDao.getManufacturer(product);
    }
    
    @Override
    public List<Kit> getKits()
    {
        return productDao.getKits();
    }
    
    @Override
    public List<Kit> getKits(String brand)
    {
        return productDao.getKits(brand);
    }
}