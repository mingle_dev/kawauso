/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kawauso.base.service;

import com.kawauso.base.bean.*;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author chris
 */
public interface PricebookService 
{
    List<PricebookTemplate> getTemplates(String type, String[] categories);

    String getActiveRetailProfileId(int cono, BigDecimal custno);

    void run(BigDecimal custno, String shipto, String emailAddr, String type, String name, String[] templates) throws Exception;

    List<MenuCategory> getTemplateNames(int limit);

    List<MenuCategory> getSheetNames(String tid);

    void updateImage(String id, String type, String data);

    List<Product> getItems(int cono, String oper, BigDecimal custno, String shipTo, String whse, String tid, String sid);

    List<Product> getSupersedeItems(int cono, String oper, BigDecimal custno, String shipTo, String whse, String tid, String sid);

    List<Product> getAllItems(int cono, String oper, BigDecimal custno, String shipTo, String whse, String tid, String sid);

    RetailPricebookProfile getRetailProfile(String profileId) throws Exception;

    List<RetailPricebookProfile> getRetailProfiles(int cono, BigDecimal custno) throws Exception;

    List<RetailPricebookItemGroup> getRetailItemGroups(int cono, String itemGroupId, String[] rootCategories) throws Exception;

    void disableRetailProfiles(int cono, BigDecimal custno) throws Exception;

    void enableRetailProfile(int cono, BigDecimal custno, String profileId) throws Exception;

    void deleteRetailProfile(int cono, BigDecimal custno, String profileId) throws Exception;

    void cloneRetailPricebookProfile(String profileId) throws Exception;

    void createRetailProfile(int cono, BigDecimal custno, String profileName, double taxRate, Map<String, String[]> map) throws Exception;

    void updateRetailProfile(String profileId, BigDecimal custno, String profileName, double taxRate, HashMap<String, String[]> map) throws Exception;
}
