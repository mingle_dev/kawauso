/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kawauso.base.service;

import com.kawauso.base.bean.reporting.Report;
import com.kawauso.base.dataobject.hibernate.ReportDao;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author chris
 */
@Service
@Transactional
public class ReportServiceImpl implements ReportService
{
    @Autowired
    ReportDao reportDao;

    @Override
    public Report run(String name, String type, String cono, String div)
    {
        return reportDao.run(name, type, cono, div);
    }

    @Override
    public Report runReport(String reportId, Map<String,Object> params)
    {
        return reportDao.runReport(reportId, params);
    }
}