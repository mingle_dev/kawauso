package com.kawauso.base.service;

import com.kawauso.base.bean.quote.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.springframework.data.mongodb.core.query.Criteria.where;
import static org.springframework.data.mongodb.core.query.Query.query;

@Repository
public class QuoteServiceImpl implements QuoteService
{
    @Autowired
    private MongoTemplate mongoTemplate;

    public static final String COLLECTION_NAME = "qb_quote";

    @Override
    public Quote findById(String id)
    {
        return mongoTemplate.findById(id, Quote.class, COLLECTION_NAME);
    }

    @Override
    public List<Quote> findByQuoteId(String quoteId)
    {
        Criteria criteria = where("quoteId").is(quoteId);

        List<Quote> quotes = mongoTemplate.find(query(criteria), Quote.class, COLLECTION_NAME);

        System.out.println("find size: " + quotes.size());

        return quotes;
    }

    @Override
    public List<Quote> findByOwnerId(String ownerId)
    {
        List<Quote> quotes = mongoTemplate.findAll(Quote.class, COLLECTION_NAME);

        return quotes;
    }


    @Override
    public List<Quote> findByCustNo(List<String> custnoCollection)
    {
        Criteria criteria = where("customerNumber").in(custnoCollection);
        List<Quote> quotes = mongoTemplate.find(query(criteria), Quote.class, COLLECTION_NAME);
        return quotes;
    }

    @Override
    public List<Quote> findByCustNo(String custno)
    {
        List<String> custnoArr = new ArrayList<String>();
        custnoArr.add(custno);
        return this.findByCustNo(custnoArr);
    }

    @Override
    public String add(Quote quote)
    {
        String uuid = UUID.randomUUID().toString();

        if(!mongoTemplate.collectionExists(COLLECTION_NAME))
            mongoTemplate.createCollection(COLLECTION_NAME);

        quote.setId(uuid);
        mongoTemplate.insert(quote, COLLECTION_NAME);

        return uuid;
    }

    @Override
    public Quote save(Quote quote) {
        System.out.println("save begin " + quote.getId());

        if (!mongoTemplate.collectionExists(COLLECTION_NAME)) {
            mongoTemplate.createCollection(COLLECTION_NAME);
        }

        Quote q = this.findById(quote.getId());

        if (q == null) {
            mongoTemplate.insert(quote, COLLECTION_NAME);
            System.out.println("q is null");

        } else {

            System.out.println("update attempt 3 -> " + q.getContactName());

            this.merge(q, quote);

//            quote.setContactName("con name");
//
//            List<com.kawauso.base.bean.quote.System> systems = new ArrayList<com.kawauso.base.bean.quote.System>();
//
//            com.kawauso.base.bean.quote.System s = new com.kawauso.base.bean.quote.System();
//            s.setId( UUID.randomUUID().toString() );
//            s.setSystemName("asdf");
//
//            systems.add(s);
//
//            q.setSystems(systems);

            //q.setContactName("contact name");

            mongoTemplate.save(q, COLLECTION_NAME);

        }
        //Query query = new Query();
        //query.addCriteria(Criteria.where("_id").is(quote.getId()));
        //return mongoTemplate.findOne(query, Quote.class);

        return this.findById(quote.getId());
    }

    /**
     * save or update system in quote
     */
    @Override
    public void upsertSystem(String quoteId, MarkFor system)
    {
        //remove products and accessories that have no sku or qty
        if(system.getStatus() == MarkFor.Status.SAVED) {
            List<QBProduct> products = new ArrayList<>();
            for(QBProduct prod : system.getProducts()) {
                if(!prod.getSku().isEmpty() && prod.getQty() != null && !prod.getQty().equals(BigDecimal.ZERO)) {
                    products.add(prod);
                }
            }
            system.setProducts(products);

            List<QBProduct> accessories = new ArrayList<>();
            for(QBProduct prod : system.getAccessories()) {
                if(!prod.getSku().isEmpty() && prod.getQty() != null && !prod.getQty().equals(BigDecimal.ZERO)) {
                    accessories.add(prod);
                }
            }
            system.setAccessories(accessories);
        }

        boolean update = false;

        Quote q = this.findById(quoteId);

        List<MarkFor> systems = q.getSystems();

        int idx = -1;
        if(systems != null)
            idx = systems.indexOf( new MarkFor(system.getId()) );

        if(idx < 0) {
            q.addSystem(system);
        }else{
            //@todo; this doesn't work like i was thinking it should
            //if something is already set, it isn't replaced by a null
            //if the replacement system doesn't contain the data
            systems.set(idx, system);
        }

        //delete the quote
        Quote oldQuote = mongoTemplate.findById(q.getId(), Quote.class, COLLECTION_NAME);
        mongoTemplate.remove(oldQuote, COLLECTION_NAME);

        //save the new quote
        mongoTemplate.save(q, COLLECTION_NAME);
    }

    //@todo; doesn't remove the system!
    @Override
    public void deleteSystem(String quoteId, MarkFor system)
    {
        System.out.println("delete systme");

        Quote q = this.findById(quoteId);

        List<MarkFor> systems = q.getSystems();

        int idx = -1;
        if(systems != null)
            idx = systems.indexOf( new MarkFor(system.getId()) );

        System.out.println("idx: " + idx + "; size: " + systems.size());

        if(idx >= 0) {
            systems.remove(idx);

            System.out.println("idx: " + idx + "; size: " + systems.size());

            q.setSystems(systems);

            //delete the quote
            Quote oldQuote = mongoTemplate.findById(q.getId(), Quote.class, COLLECTION_NAME);
            mongoTemplate.remove(oldQuote, COLLECTION_NAME);

            //save the quote
            mongoTemplate.save(q, COLLECTION_NAME);
        }
    }

    @Override
    public void find()
    {
        //List<Quote> quotes = mongoTemplate.find(query(where("quoteName").is("Test Quote 2")), Quote.class);


        //Criteria criteria = where("systems.products.sku").is("50TC-A09A2A5-0A0A0");

        //criteria.and("systems.products.sku").is("AG-1100+");

        List collection = new ArrayList();
        collection.add("50TC-A09A2A5-0A0A0");
        collection.add("AG-1100+");

        Criteria criteria = where("systems.products.sku").all( collection );


        List<Quote> quotes = mongoTemplate.find(query(criteria), Quote.class);

        long count = mongoTemplate.count(query(criteria), Quote.class);
        System.out.println("quick count: " + count);

        System.out.println("size: " + quotes.size());
        for(Quote q : quotes) {
            System.out.println(q.getQuoteName());

            for(MarkFor s : q.getSystems()) {
                System.out.println(s.getSystemName());

                for(QBProduct p : s.getProducts()) {
                    System.out.println("  " + p.getSku());
                }
            }
        }
    }

    @Override
    public String update(Quote quote)
    {
        return quote.getQuoteId();
    }

    /**
     * Updates obj with non-null values from update
     *
     * @param obj
     * @param update
     */
    public void merge(Object obj, Object update)
    {
        if(!obj.getClass().isAssignableFrom(update.getClass())){
            return;
        }

        Method[] methods = obj.getClass().getMethods();

        for(Method fromMethod: methods){
            if(fromMethod.getDeclaringClass().equals(obj.getClass())
                    && fromMethod.getName().startsWith("get")){

                String fromName = fromMethod.getName();
                String toName = fromName.replace("get", "set");

                try {
                    Method toMethod = obj.getClass().getMethod(toName, fromMethod.getReturnType());
                    Object value = fromMethod.invoke(update, (Object[])null);
                    if(value != null){
                        toMethod.invoke(obj, value);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    public QBMenu getNav(String id)
    {
        return mongoTemplate.findById(id, QBMenu.class, "qb_menu");
    }

    @Override
    public List<QBMenu> getMenu(String id)
    {
        Criteria criteria = where("parent").is(id);
        return mongoTemplate.find(query(criteria), QBMenu.class, "qb_menu");
    }

    @Override
    public List<QBTemplate> getTemplates(String menuId)
    {
        Criteria criteria = where("menuId").is(menuId);
        return mongoTemplate.find(query(criteria), QBTemplate.class, "qb_template");
    }

    @Override
    public QBTemplate getTemplate(String id)
    {
        return mongoTemplate.findById(id, QBTemplate.class, "qb_template");
    }

    @Override
    public void truncateCollection(String collection)
    {
        if(collection.equals("qb_menu"))
            mongoTemplate.remove(new Query(), "qb_menu");
        if(collection.equals("qb_template"))
            mongoTemplate.remove(new Query(), "qb_template");
    }

    @Override
    public void addMenuItem(QBMenu item) {
        mongoTemplate.insert(item, "qb_menu");
    }

    @Override
    public void addTemplate(QBTemplate template)
    {
        mongoTemplate.insert(template, "qb_template");
    }
}
