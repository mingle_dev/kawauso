/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kawauso.base.form;

/**
 *
 * @author chris
 */
public class SpiffEnrollForm
{
    private String fullName, dealership;
    private String addrStreet, addrCity, addrState, addrZip;
    private String emailAddr;
    private String conditions;
    
    /**
     * @return the fullName
     */
    public String getFullName() {
        return fullName;
    }

    /**
     * @param fullName the fullName to set
     */
    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    /**
     * @return the dealership
     */
    public String getDealership() {
        return dealership;
    }

    /**
     * @param dealership the dealership to set
     */
    public void setDealership(String dealership) {
        this.dealership = dealership;
    }

    /**
     * @return the addrStreet
     */
    public String getAddrStreet() {
        return addrStreet;
    }

    /**
     * @param addrStreet the addrStreet to set
     */
    public void setAddrStreet(String addrStreet) {
        this.addrStreet = addrStreet;
    }

    /**
     * @return the addrCity
     */
    public String getAddrCity() {
        return addrCity;
    }

    /**
     * @param addrCity the addrCity to set
     */
    public void setAddrCity(String addrCity) {
        this.addrCity = addrCity;
    }

    /**
     * @return the addrState
     */
    public String getAddrState() {
        return addrState;
    }

    /**
     * @param addrState the addrState to set
     */
    public void setAddrState(String addrState) {
        this.addrState = addrState;
    }

    /**
     * @return the addrZip
     */
    public String getAddrZip() {
        return addrZip;
    }

    /**
     * @param addrZip the addrZip to set
     */
    public void setAddrZip(String addrZip) {
        this.addrZip = addrZip;
    }

    /**
     * @return the emailAddr
     */
    public String getEmailAddr() {
        return emailAddr;
    }

    /**
     * @param emailAddr the emailAddr to set
     */
    public void setEmailAddr(String emailAddr) {
        this.emailAddr = emailAddr;
    }

    /**
     * @return the conditions
     */
    public String getConditions() {
        return conditions;
    }

    /**
     * @param conditions the conditions to set
     */
    public void setConditions(String conditions) {
        this.conditions = conditions;
    }
    
    
}
