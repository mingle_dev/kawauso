/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.kawauso.base.Utilities;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.ParseException;
import java.util.List;
import java.util.Map;
import org.apache.commons.codec.binary.Base64;
import javax.swing.text.MaskFormatter;

/**
 *
 * @author chris.weaver
 */
public final class ELUtilities
{
    public ELUtilities() {}
    
    public static String urlEncode(String url) throws UnsupportedEncodingException
    {
        return URLEncoder.encode(url, "UTF-8");
    }

    public static String base64Encode(String s)
    {
        //url safe encoding
        Base64 encoder = new Base64(true);
        byte[] encoded = encoder.encode(s.getBytes());
        return new String(encoded).trim();
    }
    
    public static String base64Decode(String s)
    {
        //should not have to pass urlSafe when decoding
        byte[] decoded = Base64.decodeBase64(s);      
        return new String(decoded);
    }
    
    public static String trim(String s)
    {
        return s.trim();
    }

    public static boolean containsKey(Map m, String k)
    {
        return m.containsKey(k);
    }
    
   public static boolean contains(List l, Object o) {
      return l.contains(o);
   }

    public static String formatPhoneNumber (String s)
    {
        String phoneMask= "(###)###-####";

        try {

            MaskFormatter maskFormatter = new MaskFormatter(s);
            maskFormatter.setMask(phoneMask);
            maskFormatter.setValueContainsLiteralCharacters(false);
            s= (String)maskFormatter.stringToValue(s);

        }catch (ParseException e)
        {
            e.printStackTrace();
        }

        return s;
    }
}