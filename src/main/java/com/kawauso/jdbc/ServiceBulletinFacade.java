package com.kawauso.jdbc;

import com.kawauso.jdbc.dao.ServiceBulletin;
import com.kawauso.jdbc.impl.ServiceBulletinManagerImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

public class ServiceBulletinFacade implements ServiceBulletin {
    protected ApplicationContext context;
    private ServiceBulletinManagerImpl impl = null;
    private final Logger logger = LoggerFactory.getLogger(ServiceBulletinFacade.class);

    public ServiceBulletinFacade() {
        this.logger.info("Initializing Service Bulletin Manager...");
        this.impl = new ServiceBulletinManagerImpl();
    }

    public Boolean insert(String companyNumber, String smbid, String description, Integer year, String keywords, Integer fileSize, CommonsMultipartFile fileData) {
        Boolean itWorked = Boolean.valueOf(false);

        try {
            itWorked = this.impl.insert(companyNumber, smbid, description, year, keywords, fileSize, fileData);
            this.logger.info("Successfully inserted the " + smbid.toUpperCase() + " service bulletin");
        } catch (Exception var10) {
            this.logger.error("The insert method for the service bulletin failed!");
            var10.printStackTrace();
        }

        return itWorked;
    }

    public Boolean delete(String uuid) {
        Boolean itWorked = Boolean.valueOf(false);

        try {
            itWorked = this.impl.delete(uuid);
        } catch (Exception var4) {
            this.logger.error("The delete method for the service bulletin failed.");
            var4.printStackTrace();
        }

        return itWorked;
    }

    public static void main(String[] args) {
        ServiceBulletinFacade f = new ServiceBulletinFacade();
        Boolean b = f.delete("e4c78ffa-dae5-4a08-b52d-b85accd269e0");
        if(b.booleanValue()) {
            System.out.println("Deleted");
        }

    }
}
