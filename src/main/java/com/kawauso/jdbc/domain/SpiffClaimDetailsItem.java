package com.kawauso.jdbc.domain;

/**
 * Created by cbyrd on 5/14/15.
 */
public class SpiffClaimDetailsItem {

    private String spiff_claim_id;
    private String classification;
    private String productGroup;
    private String product;
    private String serialNumber;
    private String orderNo;
    private Float spiff_amount;
    private Float multiplier;
    private Integer deleted;
    private String deletedDate;


    public String getSpiff_claim_id() {
        return spiff_claim_id;
    }

    public void setSpiff_claim_id(String spiff_claim_id) {
        this.spiff_claim_id = spiff_claim_id;
    }

    public String getClassification() {
        return classification;
    }

    public void setClassification(String classification) {
        this.classification = classification;
    }

    public String getProductGroup() {
        return productGroup;
    }

    public void setProductGroup(String productGroup) {
        this.productGroup = productGroup;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public Float getSpiff_amount() {
        return spiff_amount;
    }

    public void setSpiff_amount(Float spiff_amount) {
        this.spiff_amount = spiff_amount;
    }

    public Float getMultiplier() {
        return multiplier;
    }

    public void setMultiplier(Float multiplier) {
        this.multiplier = multiplier;
    }

    public Integer getDeleted() {
        return deleted;
    }

    public void setDeleted(Integer deleted) {
        this.deleted = deleted;
    }

    public String getDeletedDate() {
        return deletedDate;
    }

    public void setDeletedDate(String deletedDate) {
        this.deletedDate = deletedDate;
    }
}
