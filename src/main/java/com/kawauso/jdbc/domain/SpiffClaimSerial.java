package com.kawauso.jdbc.domain;

/**
 * Created by cbyrd on 5/23/15.
 */
public class SpiffClaimSerial {

    private String claimNo;
    private String orderNo;

    public String getClaimNo() {
        return claimNo;
    }

    public void setClaimNo(String claimNo) {
        this.claimNo = claimNo;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }


}
