package com.kawauso.jdbc.domain;

/**
 * USe this
 * Created by cbyrd on 6/20/15.
 */
public class ClaimItems {


    private String  classification;
    private String  productGroup;
    private String  product;
    private String  serialNumber;

    public String getClassification() {
        return classification;
    }

    public void setClassification(String classification) {
        this.classification = classification;
    }

    public String getProductGroup() {
        return productGroup;
    }

    public void setProductGroup(String productGroup) {
        this.productGroup = productGroup;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }
}
