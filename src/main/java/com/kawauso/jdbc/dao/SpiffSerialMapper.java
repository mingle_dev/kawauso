package com.kawauso.jdbc.dao;

import com.kawauso.jdbc.domain.SpiffClaimSerial;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by cbyrd on 6/17/15.
 */
public class SpiffSerialMapper {

    public SpiffClaimSerial mapRow(ResultSet rs , int rowNum) throws SQLException {

        SpiffClaimSerial claim = new SpiffClaimSerial();
        claim.setClaimNo(rs.getString("claim_no"));
        claim.setOrderNo(rs.getString("order_no"));
        return claim;

    }
}