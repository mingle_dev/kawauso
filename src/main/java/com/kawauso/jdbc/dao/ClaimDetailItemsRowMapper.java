package com.kawauso.jdbc.dao;


import com.kawauso.jdbc.domain.SpiffClaimDetailsItem;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by cbyrd on 5/15/15.
 */
public class ClaimDetailItemsRowMapper implements RowMapper {

    public SpiffClaimDetailsItem mapRow(ResultSet rs , int rowNum) throws SQLException {

        SpiffClaimDetailsItem item = new SpiffClaimDetailsItem();
        item.setClassification(rs.getString("classifaction"));
        item.setProductGroup(rs.getString("product_group"));
        item.setProduct(rs.getString("serial_number"));
        return item;

    }

}
