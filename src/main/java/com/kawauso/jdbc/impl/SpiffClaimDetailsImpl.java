package com.kawauso.jdbc.impl;


import com.kawauso.jdbc.dao.SpiffClaimDetailsDao;
import com.kawauso.jdbc.domain.SpiffClaimDetails;
import org.springframework.context.ApplicationContext;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;


/**
 * Created by cbyrd on 5/14/15.
 */
public class SpiffClaimDetailsImpl implements SpiffClaimDetailsDao{

    private DataSource dataSource;
    private ApplicationContext context;



    public void setDataSource(DataSource dataSource){

        this.dataSource = dataSource;

    }


    public SpiffClaimDetails getClaimDetails(String id) {


       SpiffClaimDetails claim = new SpiffClaimDetails();
        try{

            String query = "select * from spiff_claims where id = ?";
            JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);

            //using RowMapper anonymous class, we can create a separate RowMapper for reuse
            claim = jdbcTemplate.queryForObject(query, new Object[]{id}, new RowMapper<SpiffClaimDetails>(){


                public SpiffClaimDetails mapRow(ResultSet rs, int rowNum)
                        throws SQLException {
                    SpiffClaimDetails claim = new SpiffClaimDetails();
                    claim.setId(rs.getString("id"));
                    claim.setAddrCity(rs.getString("customer_address_city"));
                    claim.setAddrState(rs.getString("customer_address_state"));
                    claim.setAddrStreet(rs.getString("customer_address"));
                    claim.setAddrZip(rs.getString("customer_address_zip"));
                    claim.setAmount(rs.getDouble("claim_total"));
                    claim.setApprovedDate(rs.getDate("approved"));
                    claim.setClaimNumber(rs.getInt("claim_no"));
                    claim.setContactId(rs.getString("contact_id"));
                    claim.setCustomerName(rs.getString("customer_name"));
                    claim.setEnteredDate(rs.getDate("entered"));
                    claim.setInstallDate(rs.getDate("installed"));
                    claim.setPaidDate(rs.getDate("paid"));
                    claim.setYear(rs.getInt("year"));
                    return claim;
                }});

            return claim;

        }
        catch(Exception ex){


              ex.printStackTrace();


        }


        return claim;
    }


}
