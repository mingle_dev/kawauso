package com.kawauso.jdbc;

import com.kawauso.jdbc.domain.ClaimItems;
import com.kawauso.jdbc.domain.SpiffClaimDetails;
import com.kawauso.jdbc.impl.SpiffClaimDetailItemsImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.*;

/**
 * Created by cbyrd on 5/14/15.
 */
public class SpiffDataFacade {

    protected ApplicationContext context;

    //private SpiffClaimDetailsImpl  claimDetailsImpl = new SpiffClaimDetailsImpl();
    private SpiffClaimDetailItemsImpl itemsImpl = new SpiffClaimDetailItemsImpl();

    private final Logger logger = LoggerFactory.getLogger(SpiffDataFacade.class);


    public SpiffDataFacade(){


       this.context = new ClassPathXmlApplicationContext("SpringJDBC.xml");
     //  this.impl = (SpiffClaimDetailsImpl) context.getBean("SpringClaimDetailsData",SpiffClaimDetailsImpl.class);
       this.itemsImpl = context.getBean("SpringClaimDetailItemsData",SpiffClaimDetailItemsImpl.class);

   }

    /**
     * This is the query for non Coolray Spiffs
     * @param cono
     * @param customerNo
     * @param product
     * @param serialNo
     * @return
     */
    public  List<Object[]> getClaimAndSerial(Integer cono, String customerNo, String product, String serialNo){


        return itemsImpl.getClaimAndSerial(cono, customerNo, product,serialNo);

    }




    /**
     * This is the query used expressly for getting Coolray SPIFF claims
     * @param cono
     * @param product
     * @param serialNo
     * @return
     */
    public  List<Object[]> getClaimAndSerial(Integer cono, String product, String serialNo){


        return itemsImpl.getClaimAndSerial(cono, product,serialNo);


    }


     /**
      * Retrieve  SPIFF claim details info in a parent-child fashion
      */
    public SpiffClaimDetails getSpiffClaimDetails(String claimId){

        SpiffClaimDetails details = new SpiffClaimDetails();
        Map map = new HashMap();
        List<ClaimItems> list = new ArrayList<ClaimItems>();

        try{

            map = itemsImpl.getClaimMaster(claimId);
            String spiffClaimId =  (String) map.get("id");
            list = itemsImpl.getClaimsDetails(spiffClaimId);

            //populate the rest of the details
            details.setId((String)map.get("id"));
            details.setCustomerName((String) map.get("customer_name"));
            details.setAddrStreet((String) map.get("customer_address"));
            details.setAddrCity((String) map.get("customer_address_city"));
            details.setAddrState((String) map.get("customer_address_state"));
            details.setAddrZip((String) map.get("customer_address_zip"));
            details.setClaimNumber((Integer) map.get("claim_no"));
            details.setAmount((Double) map.get("claim_total"));
            details.setPaidDate((Date) map.get("paid"));
            details.setYear((Integer) map.get("year"));

            details.setApprovedDate((Date) map.get("approved"));
            details.setEnteredDate((Date) map.get("entered"));
            details.setInstallDate((Date) map.get("installed"));

            details.setClaimItems(list);
        }
        catch(Exception ex){

            logger.error("Error in getSpiffClaimDetails() ...", ex);

        }

        return details;
    }

    public List<SpiffClaimDetails> findAll(String contactId, Integer year){


        return itemsImpl.findAllClaims(contactId,year);


    }

    public static void main(String[] args){


        SpiffDataFacade f = new SpiffDataFacade();



       // List<Object[]> list = f.getClaimAndSerial(1, "67945", "24VNA936A003", "3914E06311");
       // List<Object[]> list = f.getClaimAndSerial(1, "67945", "24ANB636A003", "1715E15845");

      //  System.out.println("size = " + list.size());

      //  Object[] o = list.get(0);


         // System.out.println(o[0] + "...." + o[1]);






       // SpiffClaimDetails details = new SpiffClaimDetails();

       // details = f.getSpiffClaimDetails("554a7b51-afb0-4ca0-8f44-0456c0a80a21");
       List<SpiffClaimDetails> list =  f.findAll("a674704d-704d-4479-8136-42a37f000101",2015);
      System.out.println("size = " + list.size());

        for(SpiffClaimDetails d : list){

            System.out.println(d.getClaimNumber() + "..." + d.getCustomerName() + "..." + d.getEnteredDate() + "..." + d.getApprovedDate() + "..." + d.getPaidDate() + "..." + d.getAmount() );

        }


    }

}
