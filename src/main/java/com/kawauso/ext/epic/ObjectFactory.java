
package com.kawauso.ext.epic;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.kawauso.ext.epic package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.kawauso.ext.epic
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GetModelDataReply }
     * 
     */
    public GetModelDataReply createGetModelDataReply() {
        return new GetModelDataReply();
    }

    /**
     * Create an instance of {@link GetFapLinksReply }
     * 
     */
    public GetFapLinksReply createGetFapLinksReply() {
        return new GetFapLinksReply();
    }

    /**
     * Create an instance of {@link GetWhereUsedReply }
     * 
     */
    public GetWhereUsedReply createGetWhereUsedReply() {
        return new GetWhereUsedReply();
    }

    /**
     * Create an instance of {@link GetPartDataReply }
     * 
     */
    public GetPartDataReply createGetPartDataReply() {
        return new GetPartDataReply();
    }

    /**
     * Create an instance of {@link ListModelsReply }
     * 
     */
    public ListModelsReply createListModelsReply() {
        return new ListModelsReply();
    }

    /**
     * Create an instance of {@link ListPartsReply }
     * 
     */
    public ListPartsReply createListPartsReply() {
        return new ListPartsReply();
    }

    /**
     * Create an instance of {@link ListModels }
     * 
     */
    public ListModels createListModels() {
        return new ListModels();
    }

    /**
     * Create an instance of {@link GetSigChartLink }
     * 
     */
    public GetSigChartLink createGetSigChartLink() {
        return new GetSigChartLink();
    }

    /**
     * Create an instance of {@link Detail }
     * 
     */
    public Detail createDetail() {
        return new Detail();
    }

    /**
     * Create an instance of {@link GetPartData }
     * 
     */
    public GetPartData createGetPartData() {
        return new GetPartData();
    }

    /**
     * Create an instance of {@link GetModelDataReply.ModelGroup }
     * 
     */
    public GetModelDataReply.ModelGroup createGetModelDataReplyModelGroup() {
        return new GetModelDataReply.ModelGroup();
    }

    /**
     * Create an instance of {@link GetModelDataReply.BomLine }
     * 
     */
    public GetModelDataReply.BomLine createGetModelDataReplyBomLine() {
        return new GetModelDataReply.BomLine();
    }

    /**
     * Create an instance of {@link GetModelDataReply.AddOn }
     * 
     */
    public GetModelDataReply.AddOn createGetModelDataReplyAddOn() {
        return new GetModelDataReply.AddOn();
    }

    /**
     * Create an instance of {@link GetModelDataReply.Document }
     * 
     */
    public GetModelDataReply.Document createGetModelDataReplyDocument() {
        return new GetModelDataReply.Document();
    }

    /**
     * Create an instance of {@link GetFapLinksReply.FapLine }
     * 
     */
    public GetFapLinksReply.FapLine createGetFapLinksReplyFapLine() {
        return new GetFapLinksReply.FapLine();
    }

    /**
     * Create an instance of {@link GetWhereUsed }
     * 
     */
    public GetWhereUsed createGetWhereUsed() {
        return new GetWhereUsed();
    }

    /**
     * Create an instance of {@link GetModelPrintLinkReply }
     * 
     */
    public GetModelPrintLinkReply createGetModelPrintLinkReply() {
        return new GetModelPrintLinkReply();
    }

    /**
     * Create an instance of {@link GetModelPrintLink }
     * 
     */
    public GetModelPrintLink createGetModelPrintLink() {
        return new GetModelPrintLink();
    }

    /**
     * Create an instance of {@link GetWhereUsedReply.WhereUsedLine }
     * 
     */
    public GetWhereUsedReply.WhereUsedLine createGetWhereUsedReplyWhereUsedLine() {
        return new GetWhereUsedReply.WhereUsedLine();
    }

    /**
     * Create an instance of {@link GetWhereUsedPrintLink }
     * 
     */
    public GetWhereUsedPrintLink createGetWhereUsedPrintLink() {
        return new GetWhereUsedPrintLink();
    }

    /**
     * Create an instance of {@link GetFapLinks }
     * 
     */
    public GetFapLinks createGetFapLinks() {
        return new GetFapLinks();
    }

    /**
     * Create an instance of {@link GetPartDataReply.PartSpec }
     * 
     */
    public GetPartDataReply.PartSpec createGetPartDataReplyPartSpec() {
        return new GetPartDataReply.PartSpec();
    }

    /**
     * Create an instance of {@link GetPartDataReply.PartHistory }
     * 
     */
    public GetPartDataReply.PartHistory createGetPartDataReplyPartHistory() {
        return new GetPartDataReply.PartHistory();
    }

    /**
     * Create an instance of {@link GetPartDataReply.AddOn }
     * 
     */
    public GetPartDataReply.AddOn createGetPartDataReplyAddOn() {
        return new GetPartDataReply.AddOn();
    }

    /**
     * Create an instance of {@link GetPartDataReply.Document }
     * 
     */
    public GetPartDataReply.Document createGetPartDataReplyDocument() {
        return new GetPartDataReply.Document();
    }

    /**
     * Create an instance of {@link ListParts }
     * 
     */
    public ListParts createListParts() {
        return new ListParts();
    }

    /**
     * Create an instance of {@link ListModelsReply.PackageLine }
     * 
     */
    public ListModelsReply.PackageLine createListModelsReplyPackageLine() {
        return new ListModelsReply.PackageLine();
    }

    /**
     * Create an instance of {@link GetWhereUsedPrintLinkReply }
     * 
     */
    public GetWhereUsedPrintLinkReply createGetWhereUsedPrintLinkReply() {
        return new GetWhereUsedPrintLinkReply();
    }

    /**
     * Create an instance of {@link GetDrawingLink }
     * 
     */
    public GetDrawingLink createGetDrawingLink() {
        return new GetDrawingLink();
    }

    /**
     * Create an instance of {@link GetDrawingLinkReply }
     * 
     */
    public GetDrawingLinkReply createGetDrawingLinkReply() {
        return new GetDrawingLinkReply();
    }

    /**
     * Create an instance of {@link GetSigChartLinkReply }
     * 
     */
    public GetSigChartLinkReply createGetSigChartLinkReply() {
        return new GetSigChartLinkReply();
    }

    /**
     * Create an instance of {@link ListPartsReply.PartsLine }
     * 
     */
    public ListPartsReply.PartsLine createListPartsReplyPartsLine() {
        return new ListPartsReply.PartsLine();
    }

    /**
     * Create an instance of {@link GetModelData }
     * 
     */
    public GetModelData createGetModelData() {
        return new GetModelData();
    }

    /**
     * Create an instance of {@link Fault }
     * 
     */
    public Fault createFault() {
        return new Fault();
    }

}
