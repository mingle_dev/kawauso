
package com.kawauso.ext.epic;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="UserBrands" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="UserCompanyType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ModelNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="SigChartIndex" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "userBrands",
    "userCompanyType",
    "modelNumber",
    "sigChartIndex"
})
@XmlRootElement(name = "GetSigChartLink")
public class GetSigChartLink {

    @XmlElement(name = "UserBrands", required = true)
    protected String userBrands;
    @XmlElement(name = "UserCompanyType", required = true)
    protected String userCompanyType;
    @XmlElement(name = "ModelNumber", required = true)
    protected String modelNumber;
    @XmlElement(name = "SigChartIndex", required = true)
    protected String sigChartIndex;

    /**
     * Gets the value of the userBrands property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserBrands() {
        return userBrands;
    }

    /**
     * Sets the value of the userBrands property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserBrands(String value) {
        this.userBrands = value;
    }

    /**
     * Gets the value of the userCompanyType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserCompanyType() {
        return userCompanyType;
    }

    /**
     * Sets the value of the userCompanyType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserCompanyType(String value) {
        this.userCompanyType = value;
    }

    /**
     * Gets the value of the modelNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getModelNumber() {
        return modelNumber;
    }

    /**
     * Sets the value of the modelNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setModelNumber(String value) {
        this.modelNumber = value;
    }

    /**
     * Gets the value of the sigChartIndex property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSigChartIndex() {
        return sigChartIndex;
    }

    /**
     * Sets the value of the sigChartIndex property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSigChartIndex(String value) {
        this.sigChartIndex = value;
    }

}
