
package com.kawauso.ext.epic;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ModelNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ModelDescription" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ModelApplication" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ModelPrintFlag" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="SigChartFlag" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="UnitImageLink" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ModelGroup" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="GroupCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="GroupDescription" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="BomLine" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="GroupCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="ItemNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="PartNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="SubModelFlag" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="ItemDescription" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="PartApplication" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="PartLink" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="Quantity" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="DrawingIndex" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="HistoryFlag" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="ItemIndex" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="Recommended" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="PartPackages" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="AddOn" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="AddonLabel" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="AddonPartNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="Document" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="DocumentLabel" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="DocumentLink" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "modelNumber",
    "modelDescription",
    "modelApplication",
    "modelPrintFlag",
    "sigChartFlag",
    "unitImageLink",
    "modelGroup",
    "bomLine",
    "addOn",
    "document"
})
@XmlRootElement(name = "GetModelDataReply")
public class GetModelDataReply {

    @XmlElement(name = "ModelNumber", required = true)
    protected String modelNumber;
    @XmlElement(name = "ModelDescription", required = true)
    protected String modelDescription;
    @XmlElement(name = "ModelApplication", required = true)
    protected String modelApplication;
    @XmlElement(name = "ModelPrintFlag", required = true)
    protected String modelPrintFlag;
    @XmlElement(name = "SigChartFlag", required = true)
    protected String sigChartFlag;
    @XmlElement(name = "UnitImageLink", required = true)
    protected String unitImageLink;
    @XmlElement(name = "ModelGroup")
    protected List<GetModelDataReply.ModelGroup> modelGroup;
    @XmlElement(name = "BomLine")
    protected List<GetModelDataReply.BomLine> bomLine;
    @XmlElement(name = "AddOn")
    protected List<GetModelDataReply.AddOn> addOn;
    @XmlElement(name = "Document")
    protected List<GetModelDataReply.Document> document;

    /**
     * Gets the value of the modelNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getModelNumber() {
        return modelNumber;
    }

    /**
     * Sets the value of the modelNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setModelNumber(String value) {
        this.modelNumber = value;
    }

    /**
     * Gets the value of the modelDescription property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getModelDescription() {
        return modelDescription;
    }

    /**
     * Sets the value of the modelDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setModelDescription(String value) {
        this.modelDescription = value;
    }

    /**
     * Gets the value of the modelApplication property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getModelApplication() {
        return modelApplication;
    }

    /**
     * Sets the value of the modelApplication property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setModelApplication(String value) {
        this.modelApplication = value;
    }

    /**
     * Gets the value of the modelPrintFlag property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getModelPrintFlag() {
        return modelPrintFlag;
    }

    /**
     * Sets the value of the modelPrintFlag property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setModelPrintFlag(String value) {
        this.modelPrintFlag = value;
    }

    /**
     * Gets the value of the sigChartFlag property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSigChartFlag() {
        return sigChartFlag;
    }

    /**
     * Sets the value of the sigChartFlag property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSigChartFlag(String value) {
        this.sigChartFlag = value;
    }

    /**
     * Gets the value of the unitImageLink property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUnitImageLink() {
        return unitImageLink;
    }

    /**
     * Sets the value of the unitImageLink property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUnitImageLink(String value) {
        this.unitImageLink = value;
    }

    /**
     * Gets the value of the modelGroup property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the modelGroup property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getModelGroup().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GetModelDataReply.ModelGroup }
     * 
     * 
     */
    public List<GetModelDataReply.ModelGroup> getModelGroup() {
        if (modelGroup == null) {
            modelGroup = new ArrayList<GetModelDataReply.ModelGroup>();
        }
        return this.modelGroup;
    }

    /**
     * Gets the value of the bomLine property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the bomLine property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getBomLine().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GetModelDataReply.BomLine }
     * 
     * 
     */
    public List<GetModelDataReply.BomLine> getBomLine() {
        if (bomLine == null) {
            bomLine = new ArrayList<GetModelDataReply.BomLine>();
        }
        return this.bomLine;
    }

    /**
     * Gets the value of the addOn property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the addOn property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAddOn().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GetModelDataReply.AddOn }
     * 
     * 
     */
    public List<GetModelDataReply.AddOn> getAddOn() {
        if (addOn == null) {
            addOn = new ArrayList<GetModelDataReply.AddOn>();
        }
        return this.addOn;
    }

    /**
     * Gets the value of the document property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the document property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDocument().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GetModelDataReply.Document }
     * 
     * 
     */
    public List<GetModelDataReply.Document> getDocument() {
        if (document == null) {
            document = new ArrayList<GetModelDataReply.Document>();
        }
        return this.document;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="AddonLabel" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="AddonPartNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "addonLabel",
        "addonPartNumber"
    })
    public static class AddOn {

        @XmlElement(name = "AddonLabel", required = true)
        protected String addonLabel;
        @XmlElement(name = "AddonPartNumber", required = true)
        protected String addonPartNumber;

        /**
         * Gets the value of the addonLabel property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getAddonLabel() {
            return addonLabel;
        }

        /**
         * Sets the value of the addonLabel property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setAddonLabel(String value) {
            this.addonLabel = value;
        }

        /**
         * Gets the value of the addonPartNumber property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getAddonPartNumber() {
            return addonPartNumber;
        }

        /**
         * Sets the value of the addonPartNumber property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setAddonPartNumber(String value) {
            this.addonPartNumber = value;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="GroupCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="ItemNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="PartNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="SubModelFlag" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="ItemDescription" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="PartApplication" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="PartLink" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="Quantity" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="DrawingIndex" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="HistoryFlag" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="ItemIndex" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="Recommended" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="PartPackages" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "groupCode",
        "itemNumber",
        "partNumber",
        "subModelFlag",
        "itemDescription",
        "partApplication",
        "partLink",
        "quantity",
        "drawingIndex",
        "historyFlag",
        "itemIndex",
        "recommended",
        "partPackages"
    })
    public static class BomLine {

        @XmlElement(name = "GroupCode", required = true)
        protected String groupCode;
        @XmlElement(name = "ItemNumber", required = true)
        protected String itemNumber;
        @XmlElement(name = "PartNumber", required = true)
        protected String partNumber;
        @XmlElement(name = "SubModelFlag", required = true)
        protected String subModelFlag;
        @XmlElement(name = "ItemDescription", required = true)
        protected String itemDescription;
        @XmlElement(name = "PartApplication", required = true)
        protected String partApplication;
        @XmlElement(name = "PartLink", required = true)
        protected String partLink;
        @XmlElement(name = "Quantity", required = true)
        protected String quantity;
        @XmlElement(name = "DrawingIndex", required = true)
        protected String drawingIndex;
        @XmlElement(name = "HistoryFlag", required = true)
        protected String historyFlag;
        @XmlElement(name = "ItemIndex", required = true)
        protected String itemIndex;
        @XmlElement(name = "Recommended", required = true)
        protected String recommended;
        @XmlElement(name = "PartPackages", required = true)
        protected String partPackages;

        /**
         * Gets the value of the groupCode property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getGroupCode() {
            return groupCode;
        }

        /**
         * Sets the value of the groupCode property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setGroupCode(String value) {
            this.groupCode = value;
        }

        /**
         * Gets the value of the itemNumber property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getItemNumber() {
            return itemNumber;
        }

        /**
         * Sets the value of the itemNumber property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setItemNumber(String value) {
            this.itemNumber = value;
        }

        /**
         * Gets the value of the partNumber property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPartNumber() {
            return partNumber;
        }

        /**
         * Sets the value of the partNumber property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPartNumber(String value) {
            this.partNumber = value;
        }

        /**
         * Gets the value of the subModelFlag property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSubModelFlag() {
            return subModelFlag;
        }

        /**
         * Sets the value of the subModelFlag property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSubModelFlag(String value) {
            this.subModelFlag = value;
        }

        /**
         * Gets the value of the itemDescription property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getItemDescription() {
            return itemDescription;
        }

        /**
         * Sets the value of the itemDescription property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setItemDescription(String value) {
            this.itemDescription = value;
        }

        /**
         * Gets the value of the partApplication property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPartApplication() {
            return partApplication;
        }

        /**
         * Sets the value of the partApplication property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPartApplication(String value) {
            this.partApplication = value;
        }

        /**
         * Gets the value of the partLink property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPartLink() {
            return partLink;
        }

        /**
         * Sets the value of the partLink property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPartLink(String value) {
            this.partLink = value;
        }

        /**
         * Gets the value of the quantity property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getQuantity() {
            return quantity;
        }

        /**
         * Sets the value of the quantity property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setQuantity(String value) {
            this.quantity = value;
        }

        /**
         * Gets the value of the drawingIndex property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDrawingIndex() {
            return drawingIndex;
        }

        /**
         * Sets the value of the drawingIndex property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDrawingIndex(String value) {
            this.drawingIndex = value;
        }

        /**
         * Gets the value of the historyFlag property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getHistoryFlag() {
            return historyFlag;
        }

        /**
         * Sets the value of the historyFlag property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setHistoryFlag(String value) {
            this.historyFlag = value;
        }

        /**
         * Gets the value of the itemIndex property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getItemIndex() {
            return itemIndex;
        }

        /**
         * Sets the value of the itemIndex property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setItemIndex(String value) {
            this.itemIndex = value;
        }

        /**
         * Gets the value of the recommended property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRecommended() {
            return recommended;
        }

        /**
         * Sets the value of the recommended property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRecommended(String value) {
            this.recommended = value;
        }

        /**
         * Gets the value of the partPackages property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPartPackages() {
            return partPackages;
        }

        /**
         * Sets the value of the partPackages property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPartPackages(String value) {
            this.partPackages = value;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="DocumentLabel" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="DocumentLink" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "documentLabel",
        "documentLink"
    })
    public static class Document {

        @XmlElement(name = "DocumentLabel", required = true)
        protected String documentLabel;
        @XmlElement(name = "DocumentLink", required = true)
        protected String documentLink;

        /**
         * Gets the value of the documentLabel property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDocumentLabel() {
            return documentLabel;
        }

        /**
         * Sets the value of the documentLabel property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDocumentLabel(String value) {
            this.documentLabel = value;
        }

        /**
         * Gets the value of the documentLink property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDocumentLink() {
            return documentLink;
        }

        /**
         * Sets the value of the documentLink property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDocumentLink(String value) {
            this.documentLink = value;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="GroupCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="GroupDescription" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "groupCode",
        "groupDescription"
    })
    public static class ModelGroup {

        @XmlElement(name = "GroupCode", required = true)
        protected String groupCode;
        @XmlElement(name = "GroupDescription", required = true)
        protected String groupDescription;

        /**
         * Gets the value of the groupCode property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getGroupCode() {
            return groupCode;
        }

        /**
         * Sets the value of the groupCode property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setGroupCode(String value) {
            this.groupCode = value;
        }

        /**
         * Gets the value of the groupDescription property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getGroupDescription() {
            return groupDescription;
        }

        /**
         * Sets the value of the groupDescription property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setGroupDescription(String value) {
            this.groupDescription = value;
        }

    }

}
