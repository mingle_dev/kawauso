
package com.kawauso.ext.epic;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="PartNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="PartDescription" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="PartApplication" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="PartLink" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="WhereUsedFlag" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="XrefFlag" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="PartSpec" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="SpecLabel" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="SpecValue" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="PartHistory" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="ChainIndent" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="ChainPartNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="OldPartNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="OldPartDescription" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="NewPartNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="HistoryNote" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="AddOn" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="AddonLabel" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="AddonPartNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="Document" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="DocumentLabel" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="DocumentLink" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "partNumber",
    "partDescription",
    "partApplication",
    "partLink",
    "whereUsedFlag",
    "xrefFlag",
    "partSpec",
    "partHistory",
    "addOn",
    "document"
})
@XmlRootElement(name = "GetPartDataReply")
public class GetPartDataReply {

    @XmlElement(name = "PartNumber", required = true)
    protected String partNumber;
    @XmlElement(name = "PartDescription", required = true)
    protected String partDescription;
    @XmlElement(name = "PartApplication", required = true)
    protected String partApplication;
    @XmlElement(name = "PartLink", required = true)
    protected String partLink;
    @XmlElement(name = "WhereUsedFlag", required = true)
    protected String whereUsedFlag;
    @XmlElement(name = "XrefFlag", required = true)
    protected String xrefFlag;
    @XmlElement(name = "PartSpec")
    protected List<GetPartDataReply.PartSpec> partSpec;
    @XmlElement(name = "PartHistory")
    protected List<GetPartDataReply.PartHistory> partHistory;
    @XmlElement(name = "AddOn")
    protected List<GetPartDataReply.AddOn> addOn;
    @XmlElement(name = "Document")
    protected List<GetPartDataReply.Document> document;

    /**
     * Gets the value of the partNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPartNumber() {
        return partNumber;
    }

    /**
     * Sets the value of the partNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPartNumber(String value) {
        this.partNumber = value;
    }

    /**
     * Gets the value of the partDescription property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPartDescription() {
        return partDescription;
    }

    /**
     * Sets the value of the partDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPartDescription(String value) {
        this.partDescription = value;
    }

    /**
     * Gets the value of the partApplication property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPartApplication() {
        return partApplication;
    }

    /**
     * Sets the value of the partApplication property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPartApplication(String value) {
        this.partApplication = value;
    }

    /**
     * Gets the value of the partLink property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPartLink() {
        return partLink;
    }

    /**
     * Sets the value of the partLink property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPartLink(String value) {
        this.partLink = value;
    }

    /**
     * Gets the value of the whereUsedFlag property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWhereUsedFlag() {
        return whereUsedFlag;
    }

    /**
     * Sets the value of the whereUsedFlag property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWhereUsedFlag(String value) {
        this.whereUsedFlag = value;
    }

    /**
     * Gets the value of the xrefFlag property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getXrefFlag() {
        return xrefFlag;
    }

    /**
     * Sets the value of the xrefFlag property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setXrefFlag(String value) {
        this.xrefFlag = value;
    }

    /**
     * Gets the value of the partSpec property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the partSpec property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPartSpec().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GetPartDataReply.PartSpec }
     * 
     * 
     */
    public List<GetPartDataReply.PartSpec> getPartSpec() {
        if (partSpec == null) {
            partSpec = new ArrayList<GetPartDataReply.PartSpec>();
        }
        return this.partSpec;
    }

    /**
     * Gets the value of the partHistory property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the partHistory property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPartHistory().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GetPartDataReply.PartHistory }
     * 
     * 
     */
    public List<GetPartDataReply.PartHistory> getPartHistory() {
        if (partHistory == null) {
            partHistory = new ArrayList<GetPartDataReply.PartHistory>();
        }
        return this.partHistory;
    }

    /**
     * Gets the value of the addOn property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the addOn property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAddOn().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GetPartDataReply.AddOn }
     * 
     * 
     */
    public List<GetPartDataReply.AddOn> getAddOn() {
        if (addOn == null) {
            addOn = new ArrayList<GetPartDataReply.AddOn>();
        }
        return this.addOn;
    }

    /**
     * Gets the value of the document property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the document property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDocument().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GetPartDataReply.Document }
     * 
     * 
     */
    public List<GetPartDataReply.Document> getDocument() {
        if (document == null) {
            document = new ArrayList<GetPartDataReply.Document>();
        }
        return this.document;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="AddonLabel" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="AddonPartNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "addonLabel",
        "addonPartNumber"
    })
    public static class AddOn {

        @XmlElement(name = "AddonLabel", required = true)
        protected String addonLabel;
        @XmlElement(name = "AddonPartNumber", required = true)
        protected String addonPartNumber;

        /**
         * Gets the value of the addonLabel property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getAddonLabel() {
            return addonLabel;
        }

        /**
         * Sets the value of the addonLabel property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setAddonLabel(String value) {
            this.addonLabel = value;
        }

        /**
         * Gets the value of the addonPartNumber property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getAddonPartNumber() {
            return addonPartNumber;
        }

        /**
         * Sets the value of the addonPartNumber property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setAddonPartNumber(String value) {
            this.addonPartNumber = value;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="DocumentLabel" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="DocumentLink" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "documentLabel",
        "documentLink"
    })
    public static class Document {

        @XmlElement(name = "DocumentLabel", required = true)
        protected String documentLabel;
        @XmlElement(name = "DocumentLink", required = true)
        protected String documentLink;

        /**
         * Gets the value of the documentLabel property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDocumentLabel() {
            return documentLabel;
        }

        /**
         * Sets the value of the documentLabel property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDocumentLabel(String value) {
            this.documentLabel = value;
        }

        /**
         * Gets the value of the documentLink property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDocumentLink() {
            return documentLink;
        }

        /**
         * Sets the value of the documentLink property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDocumentLink(String value) {
            this.documentLink = value;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="ChainIndent" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="ChainPartNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="OldPartNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="OldPartDescription" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="NewPartNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="HistoryNote" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "chainIndent",
        "chainPartNumber",
        "oldPartNumber",
        "oldPartDescription",
        "newPartNumber",
        "historyNote"
    })
    public static class PartHistory {

        @XmlElement(name = "ChainIndent", required = true)
        protected String chainIndent;
        @XmlElement(name = "ChainPartNumber", required = true)
        protected String chainPartNumber;
        @XmlElement(name = "OldPartNumber", required = true)
        protected String oldPartNumber;
        @XmlElement(name = "OldPartDescription", required = true)
        protected String oldPartDescription;
        @XmlElement(name = "NewPartNumber", required = true)
        protected String newPartNumber;
        @XmlElement(name = "HistoryNote", required = true)
        protected String historyNote;

        /**
         * Gets the value of the chainIndent property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getChainIndent() {
            return chainIndent;
        }

        /**
         * Sets the value of the chainIndent property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setChainIndent(String value) {
            this.chainIndent = value;
        }

        /**
         * Gets the value of the chainPartNumber property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getChainPartNumber() {
            return chainPartNumber;
        }

        /**
         * Sets the value of the chainPartNumber property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setChainPartNumber(String value) {
            this.chainPartNumber = value;
        }

        /**
         * Gets the value of the oldPartNumber property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getOldPartNumber() {
            return oldPartNumber;
        }

        /**
         * Sets the value of the oldPartNumber property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setOldPartNumber(String value) {
            this.oldPartNumber = value;
        }

        /**
         * Gets the value of the oldPartDescription property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getOldPartDescription() {
            return oldPartDescription;
        }

        /**
         * Sets the value of the oldPartDescription property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setOldPartDescription(String value) {
            this.oldPartDescription = value;
        }

        /**
         * Gets the value of the newPartNumber property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getNewPartNumber() {
            return newPartNumber;
        }

        /**
         * Sets the value of the newPartNumber property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setNewPartNumber(String value) {
            this.newPartNumber = value;
        }

        /**
         * Gets the value of the historyNote property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getHistoryNote() {
            return historyNote;
        }

        /**
         * Sets the value of the historyNote property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setHistoryNote(String value) {
            this.historyNote = value;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="SpecLabel" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="SpecValue" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "specLabel",
        "specValue"
    })
    public static class PartSpec {

        @XmlElement(name = "SpecLabel", required = true)
        protected String specLabel;
        @XmlElement(name = "SpecValue", required = true)
        protected String specValue;

        /**
         * Gets the value of the specLabel property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSpecLabel() {
            return specLabel;
        }

        /**
         * Sets the value of the specLabel property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSpecLabel(String value) {
            this.specLabel = value;
        }

        /**
         * Gets the value of the specValue property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSpecValue() {
            return specValue;
        }

        /**
         * Sets the value of the specValue property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSpecValue(String value) {
            this.specValue = value;
        }

    }

}
