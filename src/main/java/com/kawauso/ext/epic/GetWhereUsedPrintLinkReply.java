
package com.kawauso.ext.epic;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="WhereUsedPrintLink" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PrintStatus" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "whereUsedPrintLink",
    "printStatus"
})
@XmlRootElement(name = "GetWhereUsedPrintLinkReply")
public class GetWhereUsedPrintLinkReply {

    @XmlElement(name = "WhereUsedPrintLink")
    protected String whereUsedPrintLink;
    @XmlElement(name = "PrintStatus")
    protected String printStatus;

    /**
     * Gets the value of the whereUsedPrintLink property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWhereUsedPrintLink() {
        return whereUsedPrintLink;
    }

    /**
     * Sets the value of the whereUsedPrintLink property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWhereUsedPrintLink(String value) {
        this.whereUsedPrintLink = value;
    }

    /**
     * Gets the value of the printStatus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrintStatus() {
        return printStatus;
    }

    /**
     * Sets the value of the printStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrintStatus(String value) {
        this.printStatus = value;
    }

}
