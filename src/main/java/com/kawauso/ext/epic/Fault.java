
package com.kawauso.ext.epic;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="MI_Detail" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MI_Exception" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "miDetail",
    "miException"
})
@XmlRootElement(name = "Fault")
public class Fault {

    @XmlElement(name = "MI_Detail")
    protected String miDetail;
    @XmlElement(name = "MI_Exception")
    protected String miException;

    /**
     * Gets the value of the miDetail property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMIDetail() {
        return miDetail;
    }

    /**
     * Sets the value of the miDetail property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMIDetail(String value) {
        this.miDetail = value;
    }

    /**
     * Gets the value of the miException property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMIException() {
        return miException;
    }

    /**
     * Sets the value of the miException property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMIException(String value) {
        this.miException = value;
    }

}
