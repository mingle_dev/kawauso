
package com.kawauso.ext.epic;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="DrawingLink" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="DrawingTitle" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="NextDrawingIndex" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="PreviousDrawingIndex" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "drawingLink",
    "drawingTitle",
    "nextDrawingIndex",
    "previousDrawingIndex"
})
@XmlRootElement(name = "GetDrawingLinkReply")
public class GetDrawingLinkReply {

    @XmlElement(name = "DrawingLink", required = true)
    protected String drawingLink;
    @XmlElement(name = "DrawingTitle", required = true)
    protected String drawingTitle;
    @XmlElement(name = "NextDrawingIndex", required = true)
    protected String nextDrawingIndex;
    @XmlElement(name = "PreviousDrawingIndex", required = true)
    protected String previousDrawingIndex;

    /**
     * Gets the value of the drawingLink property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDrawingLink() {
        return drawingLink;
    }

    /**
     * Sets the value of the drawingLink property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDrawingLink(String value) {
        this.drawingLink = value;
    }

    /**
     * Gets the value of the drawingTitle property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDrawingTitle() {
        return drawingTitle;
    }

    /**
     * Sets the value of the drawingTitle property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDrawingTitle(String value) {
        this.drawingTitle = value;
    }

    /**
     * Gets the value of the nextDrawingIndex property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNextDrawingIndex() {
        return nextDrawingIndex;
    }

    /**
     * Sets the value of the nextDrawingIndex property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNextDrawingIndex(String value) {
        this.nextDrawingIndex = value;
    }

    /**
     * Gets the value of the previousDrawingIndex property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPreviousDrawingIndex() {
        return previousDrawingIndex;
    }

    /**
     * Sets the value of the previousDrawingIndex property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPreviousDrawingIndex(String value) {
        this.previousDrawingIndex = value;
    }

}
