
package com.kawauso.ext.epic;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ModelNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ModelDescription" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ModelApplication" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="PackageLine" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="SalesPackage" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="PackageVolts" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="ModelApplication" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "modelNumber",
    "modelDescription",
    "modelApplication",
    "packageLine"
})
@XmlRootElement(name = "ListModelsReply")
public class ListModelsReply {

    @XmlElement(name = "ModelNumber", required = true)
    protected String modelNumber;
    @XmlElement(name = "ModelDescription", required = true)
    protected String modelDescription;
    @XmlElement(name = "ModelApplication", required = true)
    protected String modelApplication;
    @XmlElement(name = "PackageLine")
    protected List<ListModelsReply.PackageLine> packageLine;

    /**
     * Gets the value of the modelNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getModelNumber() {
        return modelNumber;
    }

    /**
     * Sets the value of the modelNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setModelNumber(String value) {
        this.modelNumber = value;
    }

    /**
     * Gets the value of the modelDescription property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getModelDescription() {
        return modelDescription;
    }

    /**
     * Sets the value of the modelDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setModelDescription(String value) {
        this.modelDescription = value;
    }

    /**
     * Gets the value of the modelApplication property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getModelApplication() {
        return modelApplication;
    }

    /**
     * Sets the value of the modelApplication property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setModelApplication(String value) {
        this.modelApplication = value;
    }

    /**
     * Gets the value of the packageLine property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the packageLine property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPackageLine().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ListModelsReply.PackageLine }
     * 
     * 
     */
    public List<ListModelsReply.PackageLine> getPackageLine() {
        if (packageLine == null) {
            packageLine = new ArrayList<ListModelsReply.PackageLine>();
        }
        return this.packageLine;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="SalesPackage" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="PackageVolts" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="ModelApplication" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "salesPackage",
        "packageVolts",
        "modelApplication"
    })
    public static class PackageLine {

        @XmlElement(name = "SalesPackage", required = true)
        protected String salesPackage;
        @XmlElement(name = "PackageVolts", required = true)
        protected String packageVolts;
        @XmlElement(name = "ModelApplication", required = true)
        protected String modelApplication;

        /**
         * Gets the value of the salesPackage property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSalesPackage() {
            return salesPackage;
        }

        /**
         * Sets the value of the salesPackage property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSalesPackage(String value) {
            this.salesPackage = value;
        }

        /**
         * Gets the value of the packageVolts property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPackageVolts() {
            return packageVolts;
        }

        /**
         * Sets the value of the packageVolts property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPackageVolts(String value) {
            this.packageVolts = value;
        }

        /**
         * Gets the value of the modelApplication property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getModelApplication() {
            return modelApplication;
        }

        /**
         * Sets the value of the modelApplication property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setModelApplication(String value) {
            this.modelApplication = value;
        }

    }

}
