
package com.kawauso.ext.epic;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SigChartLink" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="SigChartTitle" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="NextSigChartIndex" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="PreviousSigChartIndex" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "sigChartLink",
    "sigChartTitle",
    "nextSigChartIndex",
    "previousSigChartIndex"
})
@XmlRootElement(name = "GetSigChartLinkReply")
public class GetSigChartLinkReply {

    @XmlElement(name = "SigChartLink", required = true)
    protected String sigChartLink;
    @XmlElement(name = "SigChartTitle", required = true)
    protected String sigChartTitle;
    @XmlElement(name = "NextSigChartIndex", required = true)
    protected String nextSigChartIndex;
    @XmlElement(name = "PreviousSigChartIndex", required = true)
    protected String previousSigChartIndex;

    /**
     * Gets the value of the sigChartLink property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSigChartLink() {
        return sigChartLink;
    }

    /**
     * Sets the value of the sigChartLink property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSigChartLink(String value) {
        this.sigChartLink = value;
    }

    /**
     * Gets the value of the sigChartTitle property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSigChartTitle() {
        return sigChartTitle;
    }

    /**
     * Sets the value of the sigChartTitle property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSigChartTitle(String value) {
        this.sigChartTitle = value;
    }

    /**
     * Gets the value of the nextSigChartIndex property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNextSigChartIndex() {
        return nextSigChartIndex;
    }

    /**
     * Sets the value of the nextSigChartIndex property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNextSigChartIndex(String value) {
        this.nextSigChartIndex = value;
    }

    /**
     * Gets the value of the previousSigChartIndex property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPreviousSigChartIndex() {
        return previousSigChartIndex;
    }

    /**
     * Sets the value of the previousSigChartIndex property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPreviousSigChartIndex(String value) {
        this.previousSigChartIndex = value;
    }

}
