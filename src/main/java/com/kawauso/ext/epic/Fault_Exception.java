
package com.kawauso.ext.epic;

import javax.xml.ws.WebFault;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.2.4-b01
 * Generated source version: 2.2
 * 
 */
@WebFault(name = "Fault", targetNamespace = "tag:xmlns.carrier.com,2012-11-01:rcd.epic:webservices")
public class Fault_Exception
    extends Exception
{

    /**
     * Java type that goes as soapenv:Fault detail element.
     * 
     */
    private Fault faultInfo;

    /**
     * 
     * @param message
     * @param faultInfo
     */
    public Fault_Exception(String message, Fault faultInfo) {
        super(message);
        this.faultInfo = faultInfo;
    }

    /**
     * 
     * @param message
     * @param faultInfo
     * @param cause
     */
    public Fault_Exception(String message, Fault faultInfo, Throwable cause) {
        super(message, cause);
        this.faultInfo = faultInfo;
    }

    /**
     * 
     * @return
     *     returns fault bean: com.kawauso.ext.epic.Fault
     */
    public Fault getFaultInfo() {
        return faultInfo;
    }

}
